﻿using System.Drawing;

namespace SharedLibrary
{
    public interface IStatusBarNotification
    {
        StatusBarIndicator Status { get; }
        string StatusMessage { get; }
    }
    public enum StatusBarIndicator
    {
        OK,
        WARNING,
        ERROR
    }
}
