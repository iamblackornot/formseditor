﻿using System;
using System.Data;
using System.Data.SQLite;
using Newtonsoft.Json.Linq;
using SharedLibrary;

namespace FormsEditorEx.Database
{
    public static class CommandFactory
    {
        private static readonly string LOAD_PATIENT_QUERY = "SELECT * FROM Patients WHERE AmbuCardNum = @id";

        public static SQLiteCommand GetLoadPatientCommand(SQLiteConnection dbConnection, string id)
        {
            SQLiteCommand cmd = new SQLiteCommand(LOAD_PATIENT_QUERY, dbConnection);

            cmd.Parameters.Add(new SQLiteParameter("@id", id));

            cmd.CommandType = CommandType.Text;

            return cmd;
        }
        public static SQLiteCommand GetSavePatientCommand(SQLiteConnection dbConnection, Patient p)
        {
            string updates = string.Empty;

            var Properties = p.GetType().GetProperties();

            foreach (var Property in Properties)
            {
                if (Property.CanWrite && Property.GetSetMethod(true).IsPublic)
                {
                    if (updates.Length > 0)
                        updates += ", ";

                    updates += $"{Property.Name} = @{Property.Name}";
                }
            }

            SQLiteCommand cmd = new SQLiteCommand($"UPDATE Patients SET {updates} WHERE AmbuCardNum = @AmbuCardNum", dbConnection);

            foreach (var Property in Properties)
            {
                if (Property.CanWrite && Property.GetSetMethod(true).IsPublic)
                {
                    var value = Property.GetValue(p, null);
                    cmd.Parameters.Add(new SQLiteParameter("@" + Property.Name, value));
                }
            }

            cmd.CommandType = CommandType.Text;

            return cmd;
        }

        private static readonly string ASSIGN_PATIENT_QUERY = "UPDATE Patients SET Department = @dep, Doctor = @doc, DateDischarge = NULL," +
            "Stage = @stage, ClinicalRecordNum = @recnum, DateExam = NULL WHERE AmbuCardNum = @id AND Department IS NULL AND Doctor IS NULL " +
            "AND Stage IS NULL AND ClinicalRecordNum IS NULL";
        public static SQLiteCommand GetAssignPatientCommand(SQLiteConnection dbConnection, string id, string recnum, int dep, long doc)
        {
            SQLiteCommand cmd = new SQLiteCommand(ASSIGN_PATIENT_QUERY, dbConnection);

            cmd.Parameters.Add(new SQLiteParameter("@dep", dep));
            cmd.Parameters.Add(new SQLiteParameter("@doc", doc));
            cmd.Parameters.Add(new SQLiteParameter("@id", id));
            cmd.Parameters.Add(new SQLiteParameter("@stage", (int)MainFrameState.Anamnesis));
            cmd.Parameters.Add(new SQLiteParameter("@recnum", recnum));

            cmd.CommandType = CommandType.Text;

            return cmd;
        }

        private static readonly string LOAD_PATIENT_LIST_QUERY = 
            $"SELECT ClinicalRecordNum, AmbuCardNum," +
            //$"SecondName, FirstName, ThirdName, strftime('%s', DateExam) as DateExam, strftime('%s', DateDischarge) as DateDischarge, Department, Doctor," +
            $"SecondName, FirstName, ThirdName, CAST(strftime('%s', DateExam) AS INT) as DateExam, CAST(strftime('%s', DateDischarge) AS INT) as DateDischarge, Department, Doctor," +
            $" CASE WHEN Doctor IS NOT NULL" +
            $" THEN cast(julianday(julianday('now') -julianday(DateExam)) as INT)" +
            $" ELSE NULL" +
            $" END AS Duration" +
            $" FROM Patients";

        public static SQLiteCommand GetRefreshPatientListCommand(SQLiteConnection dbConnection, int? dep, long? doc)
        {
            string where = string.Empty;

            if (dep.HasValue)
                where = $" WHERE Department = @dep";

            if (doc.HasValue)
            {
                if (where.Length == 0)
                    where = $" WHERE ";
                else
                    where += $" AND ";

                where += $"Doctor = @doc";
            }

            SQLiteCommand cmd = new SQLiteCommand(LOAD_PATIENT_LIST_QUERY + where, dbConnection);

            cmd.CommandType = CommandType.Text;

            cmd.Parameters.Add(new SQLiteParameter("@dep", dep));
            cmd.Parameters.Add(new SQLiteParameter("@doc", doc));

            return cmd;
        }
        public static SQLiteCommand GetSearchPatientListCommand(SQLiteConnection dbConnection, string id, string SecondName, string FirstName, string ThirdName,
            DateTime? startDate, DateTime? endDate)
        {
            string where = string.Empty;

            if (!string.IsNullOrEmpty(id))
            {
                where += "AmbuCardNum LIKE @id || '%'";
            }

            if (!string.IsNullOrEmpty(SecondName))
            {
                if (where.Length > 0)
                {
                    where += $" AND ";
                }

                where += $"SecondName LIKE @SecondName || '%'";
            }

            if (!string.IsNullOrEmpty(FirstName))
            {
                if (where.Length > 0)
                {
                    where += $" AND ";
                }

                where += $"FirstName LIKE @FirstName || '%'";
            }

            if (!string.IsNullOrEmpty(ThirdName))
            {
                if (where.Length > 0)
                {
                    where += $" AND ";
                }

                where += $"ThirdName LIKE @ThirdName || '%'";
            }

            if (startDate.HasValue || endDate.HasValue)
            {
                if (where.Length > 0)
                {
                    where += $" AND ";
                }

                if (startDate.HasValue && endDate.HasValue)
                {
                    where += $"(DateExam is NULL OR DateExam <= @endDate) AND (DateDischarge is NULL OR DateDischarge >= @startDate)";
                }

                else if (startDate.HasValue)
                    where += $"(DateDischarge is NULL OR DateDischarge >= @startDate)";
                else if (endDate.HasValue)
                    where += $"(DateExam is NULL OR DateExam <= @endDate)";
            }

            if(where.Length > 0)
            {
                where = " WHERE " + where;
            }

            SQLiteCommand cmd = new SQLiteCommand(LOAD_PATIENT_LIST_QUERY + where, dbConnection)
            {
                CommandType = CommandType.Text
            };

            if(!string.IsNullOrEmpty(id))
                cmd.Parameters.Add(new SQLiteParameter("@id", id));

            if (!string.IsNullOrEmpty(SecondName))
                cmd.Parameters.Add(new SQLiteParameter("@SecondName", SecondName));

            if (!string.IsNullOrEmpty(FirstName))
                cmd.Parameters.Add(new SQLiteParameter("@FirstName", FirstName));

            if (!string.IsNullOrEmpty(ThirdName))
                cmd.Parameters.Add(new SQLiteParameter("@ThirdName", ThirdName));

            if(startDate.HasValue)
                cmd.Parameters.Add(new SQLiteParameter("@startDate", Utility.DateToSQLiteDate(startDate.Value)));

            if (endDate.HasValue)
                cmd.Parameters.Add(new SQLiteParameter("@endDate", Utility.DateToSQLiteDate(endDate.Value)));

            return cmd;
        }
        public static SQLiteCommand GetLast100PatientListCommand(SQLiteConnection dbConnection)
        {
            //string order = $" ORDER BY DateDischarge DESC LIMIT 100";
            string order = $" ORDER BY DateExam DESC LIMIT 100";

            SQLiteCommand cmd = new SQLiteCommand(LOAD_PATIENT_LIST_QUERY + order, dbConnection)
            {
                CommandType = CommandType.Text
            };

            return cmd;
        }
        private static readonly string DELETE_PATIENT_QUERY = "DELETE FROM Patients WHERE AmbuCardNum = @id";

        public static SQLiteCommand GetDeletePatientCommand(SQLiteConnection dbConnection, string id)
        {
            SQLiteCommand cmd = new SQLiteCommand(DELETE_PATIENT_QUERY, dbConnection);

            cmd.Parameters.Add(new SQLiteParameter("@id", id));

            cmd.CommandType = CommandType.Text;

            return cmd;
        }
        private static readonly string CREATE_PATIENT_QUERY = "INSERT INTO Patients (AmbuCardNum, SecondName, FirstName, ThirdName, Sex, DateBirth) " +
            "VALUES(@id, @SecondName, @FirstName, @ThirdName, @Sex, @DateBirth)";

        public static SQLiteCommand GetCreatePatientCommand(SQLiteConnection dbConnection, string id, 
            string SecondName, string FirstName, string ThirdName, int Sex, DateTime DateBirth)
        {
            SQLiteCommand cmd = new SQLiteCommand(CREATE_PATIENT_QUERY, dbConnection);

            cmd.Parameters.Add(new SQLiteParameter("@id", id));
            cmd.Parameters.Add(new SQLiteParameter("@SecondName", SecondName));
            cmd.Parameters.Add(new SQLiteParameter("@FirstName", FirstName));
            cmd.Parameters.Add(new SQLiteParameter("@ThirdName", ThirdName));
            cmd.Parameters.Add(new SQLiteParameter("@Sex", Sex));
            cmd.Parameters.Add(new SQLiteParameter("@DateBirth", DateBirth));

            cmd.CommandType = CommandType.Text;

            return cmd;
        }
        private static readonly string UPDATE_BASIC_PATIENT_DATA_QUERY = "UPDATE Patients SET AmbuCardNum = @newid, SecondName = @SecondName," +
            " FirstName = @FirstName, ThirdName = @ThirdName, Sex = @Sex, DateBirth = @DateBirth WHERE AmbuCardNum = @id";

        public static SQLiteCommand GetUpdateBasicPatientDataCommand(SQLiteConnection dbConnection, string id, string new_id,
            string SecondName, string FirstName, string ThirdName, int Sex, DateTime DateBirth)
        {
            SQLiteCommand cmd = new SQLiteCommand(UPDATE_BASIC_PATIENT_DATA_QUERY, dbConnection);

            cmd.Parameters.Add(new SQLiteParameter("@newid", new_id));
            cmd.Parameters.Add(new SQLiteParameter("@SecondName", SecondName));
            cmd.Parameters.Add(new SQLiteParameter("@FirstName", FirstName));
            cmd.Parameters.Add(new SQLiteParameter("@ThirdName", ThirdName));
            cmd.Parameters.Add(new SQLiteParameter("@Sex", Sex));
            cmd.Parameters.Add(new SQLiteParameter("@DateBirth", DateBirth));
            cmd.Parameters.Add(new SQLiteParameter("@id", id));

            cmd.CommandType = CommandType.Text;

            return cmd;
        }
        public static SQLiteCommand CheckIfParameterIsUniqueCommand(SQLiteConnection dbConnection, string name, object value)
        {
            SQLiteCommand cmd = new SQLiteCommand($"SELECT count(*) FROM Patients WHERE {name} = @value", dbConnection);

            cmd.Parameters.Add(new SQLiteParameter("@value", value));

            cmd.CommandType = CommandType.Text;

            return cmd;
        }
        public static SQLiteCommand CheckVersionCommand(SQLiteConnection dbConnection)
        {
            SQLiteCommand cmd = new SQLiteCommand("SELECT VersionHMCpro FROM Config WHERE id = 0", dbConnection);

            cmd.CommandType = CommandType.Text;

            return cmd;
        }
        public static SQLiteCommand LoadDoctorsCommand(SQLiteConnection dbConnection)
        {
            SQLiteCommand cmd = new SQLiteCommand(
                "SELECT p.*, doc.DepartmentId " +
                "FROM DepartmentStaff doc LEFT JOIN Personnel p " +
                "ON doc.PersonnelId = p.PersonnelId " +
                "WHERE p.IsActive = @IsActive", dbConnection);

            cmd.Parameters.Add(new SQLiteParameter("@IsActive", true));

            cmd.CommandType = CommandType.Text;

            return cmd;
        }

        public static SQLiteCommand LoadDepHeadCommand(SQLiteConnection dbConnection)
        {
            SQLiteCommand cmd = new SQLiteCommand(
                "SELECT p.*, doc.DepartmentId " +
                "FROM DepartmentHead doc LEFT JOIN Personnel p " +
                "ON doc.PersonnelId = p.PersonnelId " +
                "WHERE p.IsActive = @IsActive", dbConnection);

            cmd.Parameters.Add(new SQLiteParameter("@IsActive", true));

            cmd.CommandType = CommandType.Text;

            return cmd;
        }
        public static SQLiteCommand LoadAdministrationCommand(SQLiteConnection dbConnection)
        {
            SQLiteCommand cmd = new SQLiteCommand(
                "SELECT p.*, a.* FROM Administration a LEFT JOIN Personnel p " +
                "ON a.PersonnelId = p.PersonnelId",
                dbConnection);

            cmd.CommandType = CommandType.Text;

            return cmd;
        }
        public static SQLiteCommand LoadCMOCommand(SQLiteConnection dbConnection)
        {
            SQLiteCommand cmd = new SQLiteCommand("SELECT CMO FROM HospitalInfo WHERE id = 0", dbConnection);

            cmd.CommandType = CommandType.Text;

            return cmd;
        }
    }
}
