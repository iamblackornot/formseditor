﻿using System;
using SharedLibrary;
using System.Windows.Forms;

namespace FormsEditorEx
{
    public partial class OperTeamForm : Form
    {
        bool isReadOnly;

        public OperTeamForm(bool isReadOnly = true)
        {
            InitializeComponent();

            OperTeamToFields();

            CenterToScreen();

            this.isReadOnly = isReadOnly;

            InitComponents(isReadOnly);
        }
        private void InitComponents(bool isReadOnly)
        {
            tbSurgeon.SetTextBoxAvailability(isReadOnly);
            tbAssistant.SetTextBoxAvailability(isReadOnly);
            tbOperSister.SetTextBoxAvailability(isReadOnly);
            tbAnesthetist.SetTextBoxAvailability(isReadOnly);
            tbBloodTransSpecialist.SetTextBoxAvailability(isReadOnly);

            btOK.Left = (this.Width - btOK.Width) / 2;

            ActiveControl = btOK;
        }
        private void FieldsToOperTeam()
        {
            OperTeam ot = AppConfig.Instance.OperTeam;
            ot.Surgeon = tbSurgeon.Text.Trim();
            ot.Assistant = tbAssistant.Text.Trim();
            ot.Anesthetist = tbAnesthetist.Text.Trim();
            ot.OperSister = tbOperSister.Text.Trim();
            ot.BloodTransSpecialist = tbBloodTransSpecialist.Text.Trim();
        }
        private void OperTeamToFields()
        {
            OperTeam ot = AppConfig.Instance.OperTeam;

            if (!string.IsNullOrWhiteSpace(ot.Surgeon))
                tbSurgeon.Text = ot.Surgeon;
            if (!string.IsNullOrWhiteSpace(ot.Assistant))
                tbAssistant.Text = ot.Assistant;
            if (!string.IsNullOrWhiteSpace(ot.Anesthetist))
                tbAnesthetist.Text = ot.Anesthetist;
            if (!string.IsNullOrWhiteSpace(ot.OperSister))
                tbOperSister.Text = ot.OperSister;
            if (!string.IsNullOrWhiteSpace(ot.BloodTransSpecialist))
                tbBloodTransSpecialist.Text = ot.BloodTransSpecialist;
        }

        private void btOK_Click(object sender, EventArgs e)
        {
            FieldsToOperTeam();
            Close();
        }

        private void TextBox_KeyDown(object sender, KeyEventArgs e)
        {
            Utility.ReturnToTab(this, e);
        }
    }
}
