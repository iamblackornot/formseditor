﻿namespace FormsEditorEx
{
    public enum Sex
    {
        Male = 0,
        Female = 1
    }
    public enum BloodGroup
    {
        I = 0,
        II = 1,
        III = 2,
        IV = 3
    }
    public enum Rh
    {
        Positive = 0,
        Negative = 1
    }
    public enum Condition
    {
        Satisfactory = 0,
        ModerateSeverity = 1,
        Severe = 2,
        ExtremelyDifficult = 3
    }
    public enum Nutrition
    {
        Excess = 0,
        Sufficient = 1,
        Сachexia = 2
    }
    public enum Risk
    {
        High = 0,
        Moderate = 1,
        Low = 2
    }
    public enum ConclusionType
    {
        Histological = 0,
        Сytological = 1,
        Immunohistological = 2
    }
    public enum TreatmentResult
    {
        Recovery = 0,
        Improvement = 1,
        Remission = 2,
        WithoutСhanges = 3
    }
    public enum GenExamType
    {
        Disease = 0,
        OAK = 1,
        BAK = 2,
        Urine = 3,
        Coagulogram = 4,
        SARS = 5
    }
    public enum CheckPatientStatus
    {
        AVAILABLE,
        NOT_AVAILABLE,
        NOT_FOUND,
        FAILED
    }
    public enum CheckVersionResult
    {
        UPTODATE,
        DEPRECATED,
        ERROR
    }
    public enum Departments
    {
        Gynecological = 0,
        Surgical = 1,
        Urological = 2,
        Mammological = 3,
        Thoracic = 4,
        Radiological = 5,
        ChemotherapyPalliative = 6,
        ChemotherapyAntitumor = 7,
        ChemotherapyDayHospital = 8,
        RadiologyDayHospital = 9
    }
    public enum MainFrameState
    {
        ReadOnly = 0,
        Anamnesis = 1,
        PreoperativeEpicrisis = 2,
        DischargeSummary = 3,
        Discharge = 4
    }
    public enum DocType
    {
        Anamnesis = 0,
        PreoperativeEpicrisis = 1,
        DischargeSummary = 2,
        TitlePage = 3
    }
    public enum SickLeaveEndType
    {
        DischargedToWork = 0,
        ExpectedAttendance = 1,
    }
    public enum FieldWithCue
    {
        Temperature,
        PressureA,
        PressureD,
        HeartRate,
        Skin,
        LymphNodes,
        Mammary,
        Thyroid,
        Chest,
        Dyspnea,
        LungsResp,
        Percussion,
        HeartSounds,
        Pulse,
        Stomach,
        Liver,
        Urination,
        Stool,
        Genetics,
        Allergy,
        Transfusion,
        ProfHarm,
        PastIllnesses,
        Tuberculosis,

        Complications,
        Accompanying,
        AccompMedication,
        AccompMedicationPost,
        MedicalAllergy,
        Medication,
        Treatment,
    }
    public enum Disability
    {
        None = 0,
        I = 1,
        II = 2,
        III = 3
    }
    public enum WorkCapacity
    {
        FullyRestored = 0,
        Reduced = 1,
        TemporarilyDisabled = 2,
        SteadfastlyDisabled = 3
    }
    public enum StreetType
    {
        Street,
        Prospect,
        Proyezd,
    }
    //"Гинекологическое",
    //"Хирургическое",
    //"Урологическое",
    //"Маммологическое",
    //"Торакальное",
    //"Радиологическое",
    //"Эндоскопическое"
    //"ООПХ",
    //"ОПЛТ",
    //"ДСХП",
    //"ДСРП"
    //"Паллиативная химиотерапия"
    //"Противоопух. химиотерапия"
    //"ДС химиотерапия"
    //"ДС радиология"
}
