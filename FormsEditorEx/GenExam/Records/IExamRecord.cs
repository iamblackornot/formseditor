﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FormsEditorEx.GenExam.Records
{
    public interface IExamRecord
    {
        DateTime? Date { get; set; }
        string GetString();
    }
}
