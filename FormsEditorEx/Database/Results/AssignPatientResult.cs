﻿using SharedLibrary;

namespace FormsEditorEx.Database
{
    public class PatientAssigned : DatabaseResult
    {
        public PatientAssigned() : base(true, StatusBarIndicator.OK, "Начата новая история болезни") { }
    }
    public class PatientNotAvailable : DatabaseResult
    {
        public PatientNotAvailable() : base(false, StatusBarIndicator.ERROR, $"Пациент уже проходит лечение") { }
    }
    public class PatientAssignError : DatabaseResult
    {
        public PatientAssignError() : base(false, StatusBarIndicator.ERROR, $"Не удалось обратиться к базе данных") { }
    }
}
