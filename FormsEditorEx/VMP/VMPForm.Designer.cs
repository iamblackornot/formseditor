﻿using SharedLibrary;

namespace FormsEditorEx.VMP
{
    partial class VMPForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(VMPForm));
            this.btApply = new System.Windows.Forms.Button();
            this.tbSecondName = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.tbFirstName = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.tbThirdName = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.tbDateBorn = new System.Windows.Forms.MaskedTextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.cbStreet = new System.Windows.Forms.ComboBox();
            this.cbTown = new System.Windows.Forms.ComboBox();
            this.cbStreetType = new System.Windows.Forms.ComboBox();
            this.rbVillage = new System.Windows.Forms.RadioButton();
            this.rbTown = new System.Windows.Forms.RadioButton();
            this.label11 = new System.Windows.Forms.Label();
            this.tbBuilding = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.tbFlat = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.tbKorpus = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.tbOblast = new System.Windows.Forms.TextBox();
            this.tbIndex = new System.Windows.Forms.MaskedTextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.tbPhone = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.tbPassDate = new System.Windows.Forms.MaskedTextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.rtbPassFrom = new SharedLibrary.PlainRichTextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.tbPassId = new System.Windows.Forms.MaskedTextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.tbPensId = new System.Windows.Forms.MaskedTextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.tbInsComp = new System.Windows.Forms.TextBox();
            this.tbInsId = new System.Windows.Forms.MaskedTextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.cbStatus = new System.Windows.Forms.ComboBox();
            this.label22 = new System.Windows.Forms.Label();
            this.tbMKBcode = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.tbVMPcode = new System.Windows.Forms.TextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.btOpen = new System.Windows.Forms.Button();
            this.mainMenu = new System.Windows.Forms.MenuStrip();
            this.tstbDepartment = new System.Windows.Forms.ToolStripTextBox();
            this.tscbDepartment = new System.Windows.Forms.ToolStripComboBox();
            this.miSettings = new System.Windows.Forms.ToolStripMenuItem();
            this.miVMP = new System.Windows.Forms.ToolStripMenuItem();
            this.miStaff = new System.Windows.Forms.ToolStripMenuItem();
            this.btVMP = new System.Windows.Forms.Button();
            this.label28 = new System.Windows.Forms.Label();
            this.cbSex = new System.Windows.Forms.ComboBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.tbMethodCode = new System.Windows.Forms.TextBox();
            this.tbModelCode = new System.Windows.Forms.TextBox();
            this.cbTreatmentType = new System.Windows.Forms.ComboBox();
            this.label32 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.rtbModel = new SharedLibrary.PlainRichTextBox();
            this.cbVMPFinance = new System.Windows.Forms.ComboBox();
            this.label30 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.tbVMPGroup = new System.Windows.Forms.TextBox();
            this.rtbVMPDesc = new SharedLibrary.PlainRichTextBox();
            this.rtbMethod = new SharedLibrary.PlainRichTextBox();
            this.tbExemption = new System.Windows.Forms.MaskedTextBox();
            this.label25 = new System.Windows.Forms.Label();
            this.btSelectCommission = new System.Windows.Forms.Button();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.tbMembers = new System.Windows.Forms.TextBox();
            this.tbChairman = new System.Windows.Forms.TextBox();
            this.label33 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.label38 = new System.Windows.Forms.Label();
            this.rtbAnamnez = new SharedLibrary.PlainRichTextBox();
            this.label37 = new System.Windows.Forms.Label();
            this.rtbComplaints = new SharedLibrary.PlainRichTextBox();
            this.label36 = new System.Windows.Forms.Label();
            this.rtbSoputstv = new SharedLibrary.PlainRichTextBox();
            this.label35 = new System.Windows.Forms.Label();
            this.rtbOslozhnenie = new SharedLibrary.PlainRichTextBox();
            this.tbCardNumber = new System.Windows.Forms.TextBox();
            this.rtbDiagnoz = new SharedLibrary.PlainRichTextBox();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.label40 = new System.Windows.Forms.Label();
            this.rtbGeneralExamination = new SharedLibrary.PlainRichTextBox();
            this.label41 = new System.Windows.Forms.Label();
            this.rtbInstrumentalExamination = new SharedLibrary.PlainRichTextBox();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.label42 = new System.Windows.Forms.Label();
            this.tbOperationDate = new System.Windows.Forms.MaskedTextBox();
            this.tbGospDate = new System.Windows.Forms.MaskedTextBox();
            this.tbCommissionDate = new System.Windows.Forms.MaskedTextBox();
            this.label34 = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.label44 = new System.Windows.Forms.Label();
            this.tbCMO = new System.Windows.Forms.TextBox();
            this.label45 = new System.Windows.Forms.Label();
            this.tbSuperintendent = new System.Windows.Forms.TextBox();
            this.btTestData = new System.Windows.Forms.Button();
            this.btPrint = new System.Windows.Forms.Button();
            this.btOpenDirectory = new System.Windows.Forms.Button();
            this.tbDoctor = new System.Windows.Forms.TextBox();
            this.btClose = new System.Windows.Forms.Button();
            this.btOpenDirectoryOld = new System.Windows.Forms.Button();
            this.cbIO = new System.Windows.Forms.CheckBox();
            this.ucStatusBar = new SharedLibrary.ucStatusBarControl();
            this.cbDisability = new SharedLibrary.ComboBoxEx();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.mainMenu.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this.SuspendLayout();
            // 
            // btApply
            // 
            this.btApply.Location = new System.Drawing.Point(959, 553);
            this.btApply.Name = "btApply";
            this.btApply.Size = new System.Drawing.Size(89, 32);
            this.btApply.TabIndex = 0;
            this.btApply.TabStop = false;
            this.btApply.Text = "Создать";
            this.btApply.UseVisualStyleBackColor = true;
            this.btApply.Click += new System.EventHandler(this.btApply_Click);
            // 
            // tbSecondName
            // 
            this.tbSecondName.Location = new System.Drawing.Point(83, 47);
            this.tbSecondName.Name = "tbSecondName";
            this.tbSecondName.ReadOnly = true;
            this.tbSecondName.Size = new System.Drawing.Size(107, 20);
            this.tbSecondName.TabIndex = 1;
            this.tbSecondName.TabStop = false;
            this.tbSecondName.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.tbSecondName.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextBox_KeyDown);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(12, 49);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(59, 15);
            this.label1.TabIndex = 2;
            this.label1.Text = "Фамилия";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.Location = new System.Drawing.Point(25, 75);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(30, 15);
            this.label2.TabIndex = 4;
            this.label2.Text = "Имя";
            // 
            // tbFirstName
            // 
            this.tbFirstName.AcceptsReturn = true;
            this.tbFirstName.Location = new System.Drawing.Point(83, 73);
            this.tbFirstName.Name = "tbFirstName";
            this.tbFirstName.ReadOnly = true;
            this.tbFirstName.Size = new System.Drawing.Size(107, 20);
            this.tbFirstName.TabIndex = 2;
            this.tbFirstName.TabStop = false;
            this.tbFirstName.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.tbFirstName.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextBox_KeyDown);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.Location = new System.Drawing.Point(12, 101);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(57, 15);
            this.label3.TabIndex = 6;
            this.label3.Text = "Отчество";
            // 
            // tbThirdName
            // 
            this.tbThirdName.AcceptsReturn = true;
            this.tbThirdName.Location = new System.Drawing.Point(83, 99);
            this.tbThirdName.Name = "tbThirdName";
            this.tbThirdName.ReadOnly = true;
            this.tbThirdName.Size = new System.Drawing.Size(107, 20);
            this.tbThirdName.TabIndex = 3;
            this.tbThirdName.TabStop = false;
            this.tbThirdName.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.tbThirdName.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextBox_KeyDown);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label4.Location = new System.Drawing.Point(12, 127);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(93, 15);
            this.label4.TabIndex = 8;
            this.label4.Text = "Дата Рождения";
            // 
            // tbDateBorn
            // 
            this.tbDateBorn.Culture = new System.Globalization.CultureInfo("");
            this.tbDateBorn.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.tbDateBorn.Location = new System.Drawing.Point(111, 125);
            this.tbDateBorn.Mask = "00.00.0000";
            this.tbDateBorn.Name = "tbDateBorn";
            this.tbDateBorn.ReadOnly = true;
            this.tbDateBorn.Size = new System.Drawing.Size(79, 20);
            this.tbDateBorn.TabIndex = 4;
            this.tbDateBorn.TabStop = false;
            this.tbDateBorn.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.tbDateBorn.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TextBox_KeyPress);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.cbStreet);
            this.groupBox1.Controls.Add(this.cbTown);
            this.groupBox1.Controls.Add(this.cbStreetType);
            this.groupBox1.Controls.Add(this.rbVillage);
            this.groupBox1.Controls.Add(this.rbTown);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.tbBuilding);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.tbFlat);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.tbKorpus);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.tbOblast);
            this.groupBox1.Controls.Add(this.tbIndex);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Location = new System.Drawing.Point(12, 181);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(191, 211);
            this.groupBox1.TabIndex = 6;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Адрес";
            // 
            // cbStreet
            // 
            this.cbStreet.FormattingEnabled = true;
            this.cbStreet.Items.AddRange(new object[] {
            "Октября",
            "Ленинградский"});
            this.cbStreet.Location = new System.Drawing.Point(71, 102);
            this.cbStreet.Name = "cbStreet";
            this.cbStreet.Size = new System.Drawing.Size(107, 21);
            this.cbStreet.TabIndex = 5;
            this.cbStreet.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextBox_KeyDown);
            // 
            // cbTown
            // 
            this.cbTown.FormattingEnabled = true;
            this.cbTown.Location = new System.Drawing.Point(71, 75);
            this.cbTown.Name = "cbTown";
            this.cbTown.Size = new System.Drawing.Size(107, 21);
            this.cbTown.TabIndex = 3;
            this.cbTown.TextChanged += new System.EventHandler(this.cbTown_TextChanged);
            this.cbTown.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextBox_KeyDown);
            // 
            // cbStreetType
            // 
            this.cbStreetType.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cbStreetType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbStreetType.FormattingEnabled = true;
            this.cbStreetType.Items.AddRange(new object[] {
            "ул.",
            "пр-т",
            "пр."});
            this.cbStreetType.Location = new System.Drawing.Point(9, 102);
            this.cbStreetType.Name = "cbStreetType";
            this.cbStreetType.Size = new System.Drawing.Size(56, 21);
            this.cbStreetType.TabIndex = 4;
            this.cbStreetType.SelectedIndexChanged += new System.EventHandler(this.cbStreetType_SelectedIndexChanged);
            this.cbStreetType.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextBox_KeyDown);
            // 
            // rbVillage
            // 
            this.rbVillage.AutoSize = true;
            this.rbVillage.Location = new System.Drawing.Point(123, 154);
            this.rbVillage.Name = "rbVillage";
            this.rbVillage.Size = new System.Drawing.Size(50, 17);
            this.rbVillage.TabIndex = 101;
            this.rbVillage.Text = "Село";
            this.rbVillage.UseVisualStyleBackColor = true;
            // 
            // rbTown
            // 
            this.rbTown.AutoSize = true;
            this.rbTown.Checked = true;
            this.rbTown.Location = new System.Drawing.Point(123, 131);
            this.rbTown.Name = "rbTown";
            this.rbTown.Size = new System.Drawing.Size(55, 17);
            this.rbTown.TabIndex = 10;
            this.rbTown.Text = "Город";
            this.rbTown.UseVisualStyleBackColor = true;
            this.rbTown.CheckedChanged += new System.EventHandler(this.rbTown_CheckedChanged);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label11.Location = new System.Drawing.Point(19, 131);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(31, 15);
            this.label11.TabIndex = 23;
            this.label11.Text = "Дом";
            // 
            // tbBuilding
            // 
            this.tbBuilding.Location = new System.Drawing.Point(71, 129);
            this.tbBuilding.Name = "tbBuilding";
            this.tbBuilding.Size = new System.Drawing.Size(34, 20);
            this.tbBuilding.TabIndex = 6;
            this.tbBuilding.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.tbBuilding.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextBox_KeyDown);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label10.Location = new System.Drawing.Point(6, 179);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(60, 15);
            this.label10.TabIndex = 21;
            this.label10.Text = "Квартира";
            // 
            // tbFlat
            // 
            this.tbFlat.Location = new System.Drawing.Point(71, 177);
            this.tbFlat.Name = "tbFlat";
            this.tbFlat.Size = new System.Drawing.Size(34, 20);
            this.tbFlat.TabIndex = 8;
            this.tbFlat.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.tbFlat.KeyDown += new System.Windows.Forms.KeyEventHandler(this.tbFlat_KeyDown);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label9.Location = new System.Drawing.Point(10, 155);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(47, 15);
            this.label9.TabIndex = 19;
            this.label9.Text = "Корпус";
            // 
            // tbKorpus
            // 
            this.tbKorpus.Location = new System.Drawing.Point(71, 153);
            this.tbKorpus.Name = "tbKorpus";
            this.tbKorpus.Size = new System.Drawing.Size(34, 20);
            this.tbKorpus.TabIndex = 7;
            this.tbKorpus.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.tbKorpus.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextBox_KeyDown);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label7.Location = new System.Drawing.Point(10, 75);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(40, 15);
            this.label7.TabIndex = 15;
            this.label7.Text = "Город";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label6.Location = new System.Drawing.Point(6, 51);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(54, 15);
            this.label6.TabIndex = 13;
            this.label6.Text = "Область";
            // 
            // tbOblast
            // 
            this.tbOblast.Location = new System.Drawing.Point(71, 49);
            this.tbOblast.Name = "tbOblast";
            this.tbOblast.Size = new System.Drawing.Size(107, 20);
            this.tbOblast.TabIndex = 2;
            this.tbOblast.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.tbOblast.TextChanged += new System.EventHandler(this.tbOblast_TextChanged);
            this.tbOblast.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextBox_KeyDown);
            // 
            // tbIndex
            // 
            this.tbIndex.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.tbIndex.Location = new System.Drawing.Point(71, 23);
            this.tbIndex.Mask = "000000";
            this.tbIndex.Name = "tbIndex";
            this.tbIndex.Size = new System.Drawing.Size(52, 20);
            this.tbIndex.TabIndex = 1;
            this.tbIndex.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.tbIndex.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TextBox_KeyPress);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label5.Location = new System.Drawing.Point(10, 25);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(47, 15);
            this.label5.TabIndex = 10;
            this.label5.Text = "Индекс";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label12.Location = new System.Drawing.Point(22, 404);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(53, 15);
            this.label12.TabIndex = 12;
            this.label12.Text = "Телефон";
            // 
            // tbPhone
            // 
            this.tbPhone.AcceptsReturn = true;
            this.tbPhone.Location = new System.Drawing.Point(83, 402);
            this.tbPhone.Name = "tbPhone";
            this.tbPhone.Size = new System.Drawing.Size(107, 20);
            this.tbPhone.TabIndex = 7;
            this.tbPhone.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.tbPhone.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextBox_KeyDown);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.tbPassDate);
            this.groupBox2.Controls.Add(this.label15);
            this.groupBox2.Controls.Add(this.rtbPassFrom);
            this.groupBox2.Controls.Add(this.label14);
            this.groupBox2.Controls.Add(this.tbPassId);
            this.groupBox2.Controls.Add(this.label13);
            this.groupBox2.Location = new System.Drawing.Point(12, 433);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(191, 174);
            this.groupBox2.TabIndex = 8;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Паспорт";
            // 
            // tbPassDate
            // 
            this.tbPassDate.Culture = new System.Globalization.CultureInfo("");
            this.tbPassDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.tbPassDate.Location = new System.Drawing.Point(106, 132);
            this.tbPassDate.Mask = "00.00.0000";
            this.tbPassDate.Name = "tbPassDate";
            this.tbPassDate.Size = new System.Drawing.Size(79, 20);
            this.tbPassDate.TabIndex = 3;
            this.tbPassDate.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.tbPassDate.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TextBox_KeyPress);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label15.Location = new System.Drawing.Point(6, 134);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(79, 15);
            this.label15.TabIndex = 16;
            this.label15.Text = "Дата выдачи";
            // 
            // rtbPassFrom
            // 
            this.rtbPassFrom.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.rtbPassFrom.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.rtbPassFrom.Location = new System.Drawing.Point(6, 69);
            this.rtbPassFrom.Name = "rtbPassFrom";
            this.rtbPassFrom.PlainTextMode = true;
            this.rtbPassFrom.ShowSelectionMargin = true;
            this.rtbPassFrom.Size = new System.Drawing.Size(179, 56);
            this.rtbPassFrom.TabIndex = 2;
            this.rtbPassFrom.Text = "";
            this.rtbPassFrom.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextBox_KeyDown);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label14.Location = new System.Drawing.Point(6, 52);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(44, 15);
            this.label14.TabIndex = 14;
            this.label14.Text = "Выдан";
            // 
            // tbPassId
            // 
            this.tbPassId.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.tbPassId.Location = new System.Drawing.Point(106, 24);
            this.tbPassId.Mask = "0000000000";
            this.tbPassId.Name = "tbPassId";
            this.tbPassId.Size = new System.Drawing.Size(79, 20);
            this.tbPassId.TabIndex = 1;
            this.tbPassId.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.tbPassId.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TextBox_KeyPress);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label13.Location = new System.Drawing.Point(6, 26);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(82, 15);
            this.label13.TabIndex = 12;
            this.label13.Text = "Серия, номер";
            // 
            // tbPensId
            // 
            this.tbPensId.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.tbPensId.Location = new System.Drawing.Point(329, 47);
            this.tbPensId.Mask = "000-000-000-00";
            this.tbPensId.Name = "tbPensId";
            this.tbPensId.Size = new System.Drawing.Size(107, 20);
            this.tbPensId.TabIndex = 9;
            this.tbPensId.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.tbPensId.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TextBox_KeyPress);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label16.Location = new System.Drawing.Point(254, 49);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(45, 15);
            this.label16.TabIndex = 21;
            this.label16.Text = "СНИЛС";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label17.Location = new System.Drawing.Point(232, 75);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(91, 15);
            this.label17.TabIndex = 24;
            this.label17.Text = "Название СМО";
            // 
            // tbInsComp
            // 
            this.tbInsComp.Location = new System.Drawing.Point(329, 73);
            this.tbInsComp.Name = "tbInsComp";
            this.tbInsComp.Size = new System.Drawing.Size(165, 20);
            this.tbInsComp.TabIndex = 10;
            this.tbInsComp.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.tbInsComp.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextBox_KeyDown);
            // 
            // tbInsId
            // 
            this.tbInsId.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.tbInsId.Location = new System.Drawing.Point(329, 99);
            this.tbInsId.Mask = "0000000000000000";
            this.tbInsId.Name = "tbInsId";
            this.tbInsId.Size = new System.Drawing.Size(107, 20);
            this.tbInsId.TabIndex = 11;
            this.tbInsId.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.tbInsId.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TextBox_KeyPress);
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label18.Location = new System.Drawing.Point(240, 101);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(74, 15);
            this.label18.TabIndex = 25;
            this.label18.Text = "Номер ОМС";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label19.Location = new System.Drawing.Point(231, 127);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(87, 15);
            this.label19.TabIndex = 28;
            this.label19.Text = "Инвалидность";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label21.Location = new System.Drawing.Point(244, 179);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(69, 15);
            this.label21.TabIndex = 30;
            this.label21.Text = "Соц. статус";
            // 
            // cbStatus
            // 
            this.cbStatus.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cbStatus.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbStatus.FormattingEnabled = true;
            this.cbStatus.Items.AddRange(new object[] {
            "дошкольник-организован",
            "дошкольник-неорганизован",
            "учащийся",
            "работающий",
            "неработающий",
            "пенсионер",
            "военнослужащий",
            "БОМЖ"});
            this.cbStatus.Location = new System.Drawing.Point(329, 177);
            this.cbStatus.Name = "cbStatus";
            this.cbStatus.Size = new System.Drawing.Size(165, 21);
            this.cbStatus.TabIndex = 14;
            this.cbStatus.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextBox_KeyDown);
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label22.Location = new System.Drawing.Point(294, 30);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(32, 15);
            this.label22.TabIndex = 33;
            this.label22.Text = "МКБ";
            // 
            // tbMKBcode
            // 
            this.tbMKBcode.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.tbMKBcode.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.tbMKBcode.Location = new System.Drawing.Point(331, 28);
            this.tbMKBcode.Name = "tbMKBcode";
            this.tbMKBcode.Size = new System.Drawing.Size(42, 20);
            this.tbMKBcode.TabIndex = 33;
            this.tbMKBcode.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.tbMKBcode.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextBox_KeyDown);
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label23.Location = new System.Drawing.Point(16, 22);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(54, 15);
            this.label23.TabIndex = 34;
            this.label23.Text = "Диагноз";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label24.Location = new System.Drawing.Point(14, 302);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(91, 15);
            this.label24.TabIndex = 36;
            this.label24.Text = "Метод лечения";
            // 
            // tbVMPcode
            // 
            this.tbVMPcode.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.tbVMPcode.Location = new System.Drawing.Point(267, 56);
            this.tbVMPcode.Name = "tbVMPcode";
            this.tbVMPcode.Size = new System.Drawing.Size(106, 20);
            this.tbVMPcode.TabIndex = 34;
            this.tbVMPcode.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.tbVMPcode.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextBox_KeyDown);
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label26.Location = new System.Drawing.Point(14, 61);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(57, 15);
            this.label26.TabIndex = 42;
            this.label26.Text = "Вид ВМП";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label27.Location = new System.Drawing.Point(628, 543);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(87, 15);
            this.label27.TabIndex = 45;
            this.label27.Text = "Лечащий врач";
            // 
            // btOpen
            // 
            this.btOpen.Enabled = false;
            this.btOpen.Image = global::FormsEditorEx.Properties.Resources.document;
            this.btOpen.Location = new System.Drawing.Point(1054, 553);
            this.btOpen.Name = "btOpen";
            this.btOpen.Size = new System.Drawing.Size(32, 32);
            this.btOpen.TabIndex = 46;
            this.btOpen.TabStop = false;
            this.btOpen.UseVisualStyleBackColor = true;
            this.btOpen.Click += new System.EventHandler(this.btOpen_Click);
            // 
            // mainMenu
            // 
            this.mainMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tstbDepartment,
            this.tscbDepartment,
            this.miSettings});
            this.mainMenu.Location = new System.Drawing.Point(0, 0);
            this.mainMenu.Name = "mainMenu";
            this.mainMenu.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.mainMenu.Size = new System.Drawing.Size(1327, 27);
            this.mainMenu.TabIndex = 52;
            this.mainMenu.Text = "Настройки";
            // 
            // tstbDepartment
            // 
            this.tstbDepartment.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tstbDepartment.CausesValidation = false;
            this.tstbDepartment.Name = "tstbDepartment";
            this.tstbDepartment.ReadOnly = true;
            this.tstbDepartment.Size = new System.Drawing.Size(90, 23);
            this.tstbDepartment.Text = "Отделение:";
            this.tstbDepartment.TextBoxTextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tscbDepartment
            // 
            this.tscbDepartment.CausesValidation = false;
            this.tscbDepartment.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.tscbDepartment.Enabled = false;
            this.tscbDepartment.FlatStyle = System.Windows.Forms.FlatStyle.Standard;
            this.tscbDepartment.Name = "tscbDepartment";
            this.tscbDepartment.Size = new System.Drawing.Size(130, 23);
            // 
            // miSettings
            // 
            this.miSettings.AutoSize = false;
            this.miSettings.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.miSettings.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.miVMP,
            this.miStaff});
            this.miSettings.Name = "miSettings";
            this.miSettings.Padding = new System.Windows.Forms.Padding(0);
            this.miSettings.Size = new System.Drawing.Size(94, 23);
            this.miSettings.Text = "Настройки";
            // 
            // miVMP
            // 
            this.miVMP.AutoSize = false;
            this.miVMP.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.miVMP.Name = "miVMP";
            this.miVMP.Size = new System.Drawing.Size(158, 22);
            this.miVMP.Text = "Перечень ВМП";
            this.miVMP.Click += new System.EventHandler(this.miVMP_Click);
            // 
            // miStaff
            // 
            this.miStaff.Name = "miStaff";
            this.miStaff.Size = new System.Drawing.Size(158, 22);
            this.miStaff.Text = "Персонал";
            this.miStaff.Click += new System.EventHandler(this.miStaff_Click);
            // 
            // btVMP
            // 
            this.btVMP.Location = new System.Drawing.Point(275, 201);
            this.btVMP.Name = "btVMP";
            this.btVMP.Size = new System.Drawing.Size(70, 23);
            this.btVMP.TabIndex = 15;
            this.btVMP.Text = "Выбрать";
            this.btVMP.UseVisualStyleBackColor = true;
            this.btVMP.Click += new System.EventHandler(this.btVMP_Click);
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label28.Location = new System.Drawing.Point(26, 153);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(29, 15);
            this.label28.TabIndex = 54;
            this.label28.Text = "Пол";
            // 
            // cbSex
            // 
            this.cbSex.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cbSex.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbSex.Enabled = false;
            this.cbSex.FormattingEnabled = true;
            this.cbSex.Items.AddRange(new object[] {
            "мужской",
            "женский"});
            this.cbSex.Location = new System.Drawing.Point(83, 151);
            this.cbSex.Name = "cbSex";
            this.cbSex.Size = new System.Drawing.Size(107, 21);
            this.cbSex.TabIndex = 5;
            this.cbSex.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextBox_KeyDown);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.tbMethodCode);
            this.groupBox3.Controls.Add(this.tbModelCode);
            this.groupBox3.Controls.Add(this.cbTreatmentType);
            this.groupBox3.Controls.Add(this.label32);
            this.groupBox3.Controls.Add(this.label31);
            this.groupBox3.Controls.Add(this.rtbModel);
            this.groupBox3.Controls.Add(this.cbVMPFinance);
            this.groupBox3.Controls.Add(this.label30);
            this.groupBox3.Controls.Add(this.label29);
            this.groupBox3.Controls.Add(this.tbVMPGroup);
            this.groupBox3.Controls.Add(this.tbVMPcode);
            this.groupBox3.Controls.Add(this.tbMKBcode);
            this.groupBox3.Controls.Add(this.label22);
            this.groupBox3.Controls.Add(this.label26);
            this.groupBox3.Controls.Add(this.rtbVMPDesc);
            this.groupBox3.Controls.Add(this.rtbMethod);
            this.groupBox3.Controls.Add(this.label24);
            this.groupBox3.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.groupBox3.Location = new System.Drawing.Point(209, 204);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(382, 403);
            this.groupBox3.TabIndex = 31;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "   ВМП";
            // 
            // tbMethodCode
            // 
            this.tbMethodCode.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.tbMethodCode.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.tbMethodCode.Location = new System.Drawing.Point(325, 295);
            this.tbMethodCode.MaxLength = 3;
            this.tbMethodCode.Name = "tbMethodCode";
            this.tbMethodCode.Size = new System.Drawing.Size(47, 20);
            this.tbMethodCode.TabIndex = 39;
            this.tbMethodCode.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tbModelCode
            // 
            this.tbModelCode.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.tbModelCode.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.tbModelCode.Location = new System.Drawing.Point(326, 165);
            this.tbModelCode.MaxLength = 5;
            this.tbModelCode.Name = "tbModelCode";
            this.tbModelCode.Size = new System.Drawing.Size(47, 20);
            this.tbModelCode.TabIndex = 36;
            this.tbModelCode.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // cbTreatmentType
            // 
            this.cbTreatmentType.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cbTreatmentType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbTreatmentType.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.cbTreatmentType.FormattingEnabled = true;
            this.cbTreatmentType.Location = new System.Drawing.Point(116, 276);
            this.cbTreatmentType.Name = "cbTreatmentType";
            this.cbTreatmentType.Size = new System.Drawing.Size(182, 21);
            this.cbTreatmentType.TabIndex = 38;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label32.Location = new System.Drawing.Point(23, 278);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(76, 15);
            this.label32.TabIndex = 62;
            this.label32.Text = "Вид лечения";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label31.Location = new System.Drawing.Point(14, 172);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(107, 15);
            this.label31.TabIndex = 61;
            this.label31.Text = "Модель пациента";
            // 
            // rtbModel
            // 
            this.rtbModel.Location = new System.Drawing.Point(10, 190);
            this.rtbModel.Name = "rtbModel";
            this.rtbModel.PlainTextMode = true;
            this.rtbModel.ShowSelectionMargin = true;
            this.rtbModel.Size = new System.Drawing.Size(364, 80);
            this.rtbModel.TabIndex = 37;
            this.rtbModel.Text = "";
            // 
            // cbVMPFinance
            // 
            this.cbVMPFinance.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cbVMPFinance.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbVMPFinance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.cbVMPFinance.FormattingEnabled = true;
            this.cbVMPFinance.Location = new System.Drawing.Point(116, 28);
            this.cbVMPFinance.Name = "cbVMPFinance";
            this.cbVMPFinance.Size = new System.Drawing.Size(87, 21);
            this.cbVMPFinance.TabIndex = 31;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label30.Location = new System.Drawing.Point(6, 30);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(104, 15);
            this.label30.TabIndex = 58;
            this.label30.Text = "Финансирование";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label29.Location = new System.Drawing.Point(209, 30);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(46, 15);
            this.label29.TabIndex = 57;
            this.label29.Text = "Группа";
            // 
            // tbVMPGroup
            // 
            this.tbVMPGroup.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.tbVMPGroup.Location = new System.Drawing.Point(261, 28);
            this.tbVMPGroup.MaxLength = 2;
            this.tbVMPGroup.Name = "tbVMPGroup";
            this.tbVMPGroup.Size = new System.Drawing.Size(27, 20);
            this.tbVMPGroup.TabIndex = 32;
            this.tbVMPGroup.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // rtbVMPDesc
            // 
            this.rtbVMPDesc.Location = new System.Drawing.Point(9, 82);
            this.rtbVMPDesc.Name = "rtbVMPDesc";
            this.rtbVMPDesc.PlainTextMode = true;
            this.rtbVMPDesc.ShowSelectionMargin = true;
            this.rtbVMPDesc.Size = new System.Drawing.Size(365, 77);
            this.rtbVMPDesc.TabIndex = 35;
            this.rtbVMPDesc.Text = "";
            this.rtbVMPDesc.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextBox_KeyDown);
            // 
            // rtbMethod
            // 
            this.rtbMethod.Location = new System.Drawing.Point(9, 320);
            this.rtbMethod.Name = "rtbMethod";
            this.rtbMethod.PlainTextMode = true;
            this.rtbMethod.ShowSelectionMargin = true;
            this.rtbMethod.Size = new System.Drawing.Size(364, 77);
            this.rtbMethod.TabIndex = 40;
            this.rtbMethod.Text = "";
            this.rtbMethod.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextBox_KeyDown);
            // 
            // tbExemption
            // 
            this.tbExemption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.tbExemption.Location = new System.Drawing.Point(329, 151);
            this.tbExemption.Mask = "000";
            this.tbExemption.Name = "tbExemption";
            this.tbExemption.Size = new System.Drawing.Size(31, 20);
            this.tbExemption.TabIndex = 13;
            this.tbExemption.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.tbExemption.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TextBox_KeyPress);
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label25.Location = new System.Drawing.Point(244, 153);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(70, 15);
            this.label25.TabIndex = 56;
            this.label25.Text = "Код льготы";
            // 
            // btSelectCommission
            // 
            this.btSelectCommission.Location = new System.Drawing.Point(1097, 398);
            this.btSelectCommission.Name = "btSelectCommission";
            this.btSelectCommission.Size = new System.Drawing.Size(70, 23);
            this.btSelectCommission.TabIndex = 53;
            this.btSelectCommission.Text = "Выбрать";
            this.btSelectCommission.UseVisualStyleBackColor = true;
            this.btSelectCommission.Click += new System.EventHandler(this.btSelectCommission_Click);
            // 
            // groupBox4
            // 
            this.groupBox4.AutoSize = true;
            this.groupBox4.Controls.Add(this.tbMembers);
            this.groupBox4.Controls.Add(this.tbChairman);
            this.groupBox4.Controls.Add(this.label33);
            this.groupBox4.Controls.Add(this.label8);
            this.groupBox4.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.groupBox4.Location = new System.Drawing.Point(960, 402);
            this.groupBox4.Margin = new System.Windows.Forms.Padding(3, 3, 3, 0);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Padding = new System.Windows.Forms.Padding(3, 3, 3, 0);
            this.groupBox4.Size = new System.Drawing.Size(260, 145);
            this.groupBox4.TabIndex = 54;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "   Состав комиссии";
            // 
            // tbMembers
            // 
            this.tbMembers.Location = new System.Drawing.Point(113, 55);
            this.tbMembers.MinimumSize = new System.Drawing.Size(123, 71);
            this.tbMembers.Multiline = true;
            this.tbMembers.Name = "tbMembers";
            this.tbMembers.Size = new System.Drawing.Size(133, 71);
            this.tbMembers.TabIndex = 56;
            this.tbMembers.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.tbMembers.WordWrap = false;
            this.tbMembers.TextChanged += new System.EventHandler(this.tbMembers_TextChanged);
            // 
            // tbChairman
            // 
            this.tbChairman.Location = new System.Drawing.Point(113, 26);
            this.tbChairman.Name = "tbChairman";
            this.tbChairman.Size = new System.Drawing.Size(133, 23);
            this.tbChairman.TabIndex = 55;
            this.tbChairman.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(6, 58);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(101, 15);
            this.label33.TabIndex = 1;
            this.label33.Text = "Члены комиссии";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(6, 29);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(85, 15);
            this.label8.TabIndex = 0;
            this.label8.Text = "Председатель";
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.label38);
            this.groupBox5.Controls.Add(this.rtbAnamnez);
            this.groupBox5.Controls.Add(this.label37);
            this.groupBox5.Controls.Add(this.rtbComplaints);
            this.groupBox5.Controls.Add(this.label36);
            this.groupBox5.Controls.Add(this.rtbSoputstv);
            this.groupBox5.Controls.Add(this.label35);
            this.groupBox5.Controls.Add(this.rtbOslozhnenie);
            this.groupBox5.Controls.Add(this.tbCardNumber);
            this.groupBox5.Controls.Add(this.label23);
            this.groupBox5.Controls.Add(this.rtbDiagnoz);
            this.groupBox5.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.groupBox5.Location = new System.Drawing.Point(597, 47);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Padding = new System.Windows.Forms.Padding(3, 3, 3, 0);
            this.groupBox5.Size = new System.Drawing.Size(356, 416);
            this.groupBox5.TabIndex = 41;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Выписка из карты №";
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label38.Location = new System.Drawing.Point(16, 306);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(80, 15);
            this.label38.TabIndex = 45;
            this.label38.Text = "Из анамнеза";
            // 
            // rtbAnamnez
            // 
            this.rtbAnamnez.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.rtbAnamnez.Location = new System.Drawing.Point(10, 324);
            this.rtbAnamnez.Name = "rtbAnamnez";
            this.rtbAnamnez.PlainTextMode = true;
            this.rtbAnamnez.ReadOnly = true;
            this.rtbAnamnez.ShowSelectionMargin = true;
            this.rtbAnamnez.Size = new System.Drawing.Size(338, 83);
            this.rtbAnamnez.TabIndex = 46;
            this.rtbAnamnez.TabStop = false;
            this.rtbAnamnez.Text = "";
            this.rtbAnamnez.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextBox_KeyDown);
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label37.Location = new System.Drawing.Point(16, 235);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(168, 15);
            this.label37.TabIndex = 42;
            this.label37.Text = "Жалобы на момент осмотра";
            // 
            // rtbComplaints
            // 
            this.rtbComplaints.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.rtbComplaints.Location = new System.Drawing.Point(10, 253);
            this.rtbComplaints.Name = "rtbComplaints";
            this.rtbComplaints.PlainTextMode = true;
            this.rtbComplaints.ReadOnly = true;
            this.rtbComplaints.ShowSelectionMargin = true;
            this.rtbComplaints.Size = new System.Drawing.Size(338, 50);
            this.rtbComplaints.TabIndex = 45;
            this.rtbComplaints.TabStop = false;
            this.rtbComplaints.Text = "";
            this.rtbComplaints.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextBox_KeyDown);
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label36.Location = new System.Drawing.Point(16, 164);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(95, 15);
            this.label36.TabIndex = 40;
            this.label36.Text = "Сопутствующий";
            // 
            // rtbSoputstv
            // 
            this.rtbSoputstv.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.rtbSoputstv.Location = new System.Drawing.Point(10, 182);
            this.rtbSoputstv.Name = "rtbSoputstv";
            this.rtbSoputstv.PlainTextMode = true;
            this.rtbSoputstv.ReadOnly = true;
            this.rtbSoputstv.ShowSelectionMargin = true;
            this.rtbSoputstv.Size = new System.Drawing.Size(338, 50);
            this.rtbSoputstv.TabIndex = 44;
            this.rtbSoputstv.TabStop = false;
            this.rtbSoputstv.Text = "";
            this.rtbSoputstv.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextBox_KeyDown);
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label35.Location = new System.Drawing.Point(16, 93);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(140, 15);
            this.label35.TabIndex = 38;
            this.label35.Text = "Осложнение основного";
            // 
            // rtbOslozhnenie
            // 
            this.rtbOslozhnenie.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.rtbOslozhnenie.Location = new System.Drawing.Point(10, 111);
            this.rtbOslozhnenie.Name = "rtbOslozhnenie";
            this.rtbOslozhnenie.PlainTextMode = true;
            this.rtbOslozhnenie.ReadOnly = true;
            this.rtbOslozhnenie.ShowSelectionMargin = true;
            this.rtbOslozhnenie.Size = new System.Drawing.Size(338, 50);
            this.rtbOslozhnenie.TabIndex = 43;
            this.rtbOslozhnenie.TabStop = false;
            this.rtbOslozhnenie.Text = "";
            this.rtbOslozhnenie.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextBox_KeyDown);
            // 
            // tbCardNumber
            // 
            this.tbCardNumber.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.tbCardNumber.Location = new System.Drawing.Point(279, 0);
            this.tbCardNumber.Name = "tbCardNumber";
            this.tbCardNumber.ReadOnly = true;
            this.tbCardNumber.Size = new System.Drawing.Size(69, 20);
            this.tbCardNumber.TabIndex = 41;
            this.tbCardNumber.TabStop = false;
            this.tbCardNumber.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.tbCardNumber.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextBox_KeyDown);
            // 
            // rtbDiagnoz
            // 
            this.rtbDiagnoz.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.rtbDiagnoz.Location = new System.Drawing.Point(10, 40);
            this.rtbDiagnoz.Name = "rtbDiagnoz";
            this.rtbDiagnoz.PlainTextMode = true;
            this.rtbDiagnoz.ReadOnly = true;
            this.rtbDiagnoz.ShowSelectionMargin = true;
            this.rtbDiagnoz.Size = new System.Drawing.Size(338, 50);
            this.rtbDiagnoz.TabIndex = 42;
            this.rtbDiagnoz.TabStop = false;
            this.rtbDiagnoz.Text = "";
            this.rtbDiagnoz.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextBox_KeyDown);
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.label40);
            this.groupBox6.Controls.Add(this.rtbGeneralExamination);
            this.groupBox6.Controls.Add(this.label41);
            this.groupBox6.Controls.Add(this.rtbInstrumentalExamination);
            this.groupBox6.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.groupBox6.Location = new System.Drawing.Point(959, 47);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(356, 237);
            this.groupBox6.TabIndex = 47;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Обследование";
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label40.Location = new System.Drawing.Point(20, 126);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(128, 15);
            this.label40.TabIndex = 49;
            this.label40.Text = "Общее обследование";
            // 
            // rtbGeneralExamination
            // 
            this.rtbGeneralExamination.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.rtbGeneralExamination.Location = new System.Drawing.Point(9, 144);
            this.rtbGeneralExamination.Name = "rtbGeneralExamination";
            this.rtbGeneralExamination.PlainTextMode = true;
            this.rtbGeneralExamination.ReadOnly = true;
            this.rtbGeneralExamination.ShowSelectionMargin = true;
            this.rtbGeneralExamination.Size = new System.Drawing.Size(338, 83);
            this.rtbGeneralExamination.TabIndex = 48;
            this.rtbGeneralExamination.TabStop = false;
            this.rtbGeneralExamination.Text = "";
            this.rtbGeneralExamination.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextBox_KeyDown);
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label41.Location = new System.Drawing.Point(16, 22);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(237, 15);
            this.label41.TabIndex = 47;
            this.label41.Text = "Иструментальные методы обследования";
            // 
            // rtbInstrumentalExamination
            // 
            this.rtbInstrumentalExamination.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.rtbInstrumentalExamination.Location = new System.Drawing.Point(9, 40);
            this.rtbInstrumentalExamination.Name = "rtbInstrumentalExamination";
            this.rtbInstrumentalExamination.PlainTextMode = true;
            this.rtbInstrumentalExamination.ReadOnly = true;
            this.rtbInstrumentalExamination.ShowSelectionMargin = true;
            this.rtbInstrumentalExamination.Size = new System.Drawing.Size(338, 83);
            this.rtbInstrumentalExamination.TabIndex = 47;
            this.rtbInstrumentalExamination.TabStop = false;
            this.rtbInstrumentalExamination.Text = "";
            this.rtbInstrumentalExamination.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextBox_KeyDown);
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.label42);
            this.groupBox7.Controls.Add(this.tbOperationDate);
            this.groupBox7.Controls.Add(this.tbGospDate);
            this.groupBox7.Controls.Add(this.tbCommissionDate);
            this.groupBox7.Controls.Add(this.label34);
            this.groupBox7.Controls.Add(this.label39);
            this.groupBox7.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.groupBox7.Location = new System.Drawing.Point(960, 292);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(260, 100);
            this.groupBox7.TabIndex = 49;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "  Протоколы решения комиссии";
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label42.Location = new System.Drawing.Point(16, 75);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(138, 15);
            this.label42.TabIndex = 53;
            this.label42.Text = "Заплан. дата операции";
            // 
            // tbOperationDate
            // 
            this.tbOperationDate.Culture = new System.Globalization.CultureInfo("");
            this.tbOperationDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.tbOperationDate.Location = new System.Drawing.Point(167, 73);
            this.tbOperationDate.Mask = "00.00.0000";
            this.tbOperationDate.Name = "tbOperationDate";
            this.tbOperationDate.Size = new System.Drawing.Size(79, 20);
            this.tbOperationDate.TabIndex = 52;
            this.tbOperationDate.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.tbOperationDate.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TextBox_KeyPress);
            // 
            // tbGospDate
            // 
            this.tbGospDate.Culture = new System.Globalization.CultureInfo("");
            this.tbGospDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.tbGospDate.Location = new System.Drawing.Point(167, 47);
            this.tbGospDate.Mask = "00.00.0000";
            this.tbGospDate.Name = "tbGospDate";
            this.tbGospDate.Size = new System.Drawing.Size(79, 20);
            this.tbGospDate.TabIndex = 51;
            this.tbGospDate.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.tbGospDate.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TextBox_KeyPress);
            // 
            // tbCommissionDate
            // 
            this.tbCommissionDate.Culture = new System.Globalization.CultureInfo("");
            this.tbCommissionDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.tbCommissionDate.Location = new System.Drawing.Point(167, 21);
            this.tbCommissionDate.Mask = "00.00.0000";
            this.tbCommissionDate.Name = "tbCommissionDate";
            this.tbCommissionDate.Size = new System.Drawing.Size(79, 20);
            this.tbCommissionDate.TabIndex = 50;
            this.tbCommissionDate.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.tbCommissionDate.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TextBox_KeyPress);
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label34.Location = new System.Drawing.Point(16, 49);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(129, 15);
            this.label34.TabIndex = 49;
            this.label34.Text = "Дата госпитализации";
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label39.Location = new System.Drawing.Point(16, 23);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(116, 15);
            this.label39.TabIndex = 47;
            this.label39.Text = "Дата 1ой комиссии";
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label44.Location = new System.Drawing.Point(631, 485);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(84, 15);
            this.label44.TabIndex = 67;
            this.label44.Text = "Главный врач";
            // 
            // tbCMO
            // 
            this.tbCMO.AcceptsReturn = true;
            this.tbCMO.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.tbCMO.Location = new System.Drawing.Point(724, 482);
            this.tbCMO.Name = "tbCMO";
            this.tbCMO.ReadOnly = true;
            this.tbCMO.Size = new System.Drawing.Size(121, 23);
            this.tbCMO.TabIndex = 68;
            this.tbCMO.TabStop = false;
            this.tbCMO.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label45.Location = new System.Drawing.Point(617, 514);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(98, 15);
            this.label45.TabIndex = 69;
            this.label45.Text = "Зав. отделением";
            // 
            // tbSuperintendent
            // 
            this.tbSuperintendent.AcceptsReturn = true;
            this.tbSuperintendent.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.tbSuperintendent.Location = new System.Drawing.Point(724, 511);
            this.tbSuperintendent.Name = "tbSuperintendent";
            this.tbSuperintendent.ReadOnly = true;
            this.tbSuperintendent.Size = new System.Drawing.Size(121, 23);
            this.tbSuperintendent.TabIndex = 70;
            this.tbSuperintendent.TabStop = false;
            this.tbSuperintendent.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // btTestData
            // 
            this.btTestData.Location = new System.Drawing.Point(876, 553);
            this.btTestData.Name = "btTestData";
            this.btTestData.Size = new System.Drawing.Size(77, 32);
            this.btTestData.TabIndex = 71;
            this.btTestData.TabStop = false;
            this.btTestData.Text = "TestData";
            this.btTestData.UseVisualStyleBackColor = true;
            this.btTestData.Click += new System.EventHandler(this.btTestData_Click);
            // 
            // btPrint
            // 
            this.btPrint.BackColor = System.Drawing.SystemColors.ControlLight;
            this.btPrint.Image = global::FormsEditorEx.Properties.Resources.ink;
            this.btPrint.Location = new System.Drawing.Point(1092, 553);
            this.btPrint.Name = "btPrint";
            this.btPrint.Size = new System.Drawing.Size(32, 32);
            this.btPrint.TabIndex = 72;
            this.btPrint.TabStop = false;
            this.btPrint.UseVisualStyleBackColor = false;
            this.btPrint.Click += new System.EventHandler(this.btPrint_Click);
            // 
            // btOpenDirectory
            // 
            this.btOpenDirectory.BackColor = System.Drawing.SystemColors.ControlLight;
            this.btOpenDirectory.Image = global::FormsEditorEx.Properties.Resources.folder;
            this.btOpenDirectory.Location = new System.Drawing.Point(1130, 553);
            this.btOpenDirectory.Name = "btOpenDirectory";
            this.btOpenDirectory.Size = new System.Drawing.Size(32, 32);
            this.btOpenDirectory.TabIndex = 50;
            this.btOpenDirectory.TabStop = false;
            this.btOpenDirectory.UseVisualStyleBackColor = false;
            this.btOpenDirectory.Click += new System.EventHandler(this.btOpenDirectory_Click);
            // 
            // tbDoctor
            // 
            this.tbDoctor.AcceptsReturn = true;
            this.tbDoctor.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.tbDoctor.Location = new System.Drawing.Point(724, 540);
            this.tbDoctor.Name = "tbDoctor";
            this.tbDoctor.ReadOnly = true;
            this.tbDoctor.Size = new System.Drawing.Size(121, 23);
            this.tbDoctor.TabIndex = 73;
            this.tbDoctor.TabStop = false;
            this.tbDoctor.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // btClose
            // 
            this.btClose.Location = new System.Drawing.Point(1206, 553);
            this.btClose.Name = "btClose";
            this.btClose.Size = new System.Drawing.Size(110, 32);
            this.btClose.TabIndex = 74;
            this.btClose.TabStop = false;
            this.btClose.Text = "Отмена";
            this.btClose.UseVisualStyleBackColor = true;
            this.btClose.Click += new System.EventHandler(this.btClose_Click);
            // 
            // btOpenDirectoryOld
            // 
            this.btOpenDirectoryOld.BackColor = System.Drawing.SystemColors.ControlLight;
            this.btOpenDirectoryOld.Image = global::FormsEditorEx.Properties.Resources.folder;
            this.btOpenDirectoryOld.Location = new System.Drawing.Point(1168, 553);
            this.btOpenDirectoryOld.Name = "btOpenDirectoryOld";
            this.btOpenDirectoryOld.Size = new System.Drawing.Size(32, 32);
            this.btOpenDirectoryOld.TabIndex = 75;
            this.btOpenDirectoryOld.TabStop = false;
            this.btOpenDirectoryOld.UseVisualStyleBackColor = false;
            this.btOpenDirectoryOld.Click += new System.EventHandler(this.btOpenDirectoryOld_Click);
            // 
            // cbIO
            // 
            this.cbIO.AutoSize = true;
            this.cbIO.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.cbIO.Enabled = false;
            this.cbIO.Location = new System.Drawing.Point(860, 514);
            this.cbIO.Name = "cbIO";
            this.cbIO.Size = new System.Drawing.Size(48, 17);
            this.cbIO.TabIndex = 76;
            this.cbIO.TabStop = false;
            this.cbIO.Text = "И.О.";
            this.cbIO.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.cbIO.UseVisualStyleBackColor = true;
            // 
            // ucStatusBar
            // 
            this.ucStatusBar.Location = new System.Drawing.Point(597, 591);
            this.ucStatusBar.MaximumSize = new System.Drawing.Size(1920, 15);
            this.ucStatusBar.MinimumSize = new System.Drawing.Size(300, 15);
            this.ucStatusBar.Name = "ucStatusBar";
            this.ucStatusBar.Size = new System.Drawing.Size(718, 15);
            this.ucStatusBar.TabIndex = 77;
            // 
            // cbDisability
            // 
            this.cbDisability.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cbDisability.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbDisability.Enabled = false;
            this.cbDisability.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.cbDisability.FormattingEnabled = true;
            this.cbDisability.Items.AddRange(new object[] {
            "(+) полож.",
            "(-) отриц."});
            this.cbDisability.Location = new System.Drawing.Point(329, 125);
            this.cbDisability.Name = "cbDisability";
            this.cbDisability.Size = new System.Drawing.Size(107, 21);
            this.cbDisability.TabIndex = 81;
            this.cbDisability.TabStop = false;
            // 
            // VMPForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.AutoValidate = System.Windows.Forms.AutoValidate.EnableAllowFocusChange;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(1327, 619);
            this.ControlBox = false;
            this.Controls.Add(this.cbDisability);
            this.Controls.Add(this.ucStatusBar);
            this.Controls.Add(this.cbIO);
            this.Controls.Add(this.btOpenDirectoryOld);
            this.Controls.Add(this.btClose);
            this.Controls.Add(this.tbDoctor);
            this.Controls.Add(this.btPrint);
            this.Controls.Add(this.btTestData);
            this.Controls.Add(this.label45);
            this.Controls.Add(this.tbSuperintendent);
            this.Controls.Add(this.label44);
            this.Controls.Add(this.tbCMO);
            this.Controls.Add(this.groupBox7);
            this.Controls.Add(this.groupBox6);
            this.Controls.Add(this.groupBox5);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.btSelectCommission);
            this.Controls.Add(this.tbExemption);
            this.Controls.Add(this.label25);
            this.Controls.Add(this.btVMP);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.cbSex);
            this.Controls.Add(this.label28);
            this.Controls.Add(this.btOpenDirectory);
            this.Controls.Add(this.btOpen);
            this.Controls.Add(this.label27);
            this.Controls.Add(this.cbStatus);
            this.Controls.Add(this.label21);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.tbInsId);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.tbInsComp);
            this.Controls.Add(this.tbPensId);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.tbPhone);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.tbDateBorn);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.tbThirdName);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.tbFirstName);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.tbSecondName);
            this.Controls.Add(this.btApply);
            this.Controls.Add(this.mainMenu);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "VMPForm";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.Text = "HMCpro ВМП";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.MainFrame_FormClosed);
            this.Load += new System.EventHandler(this.MainFrame_Load);
            this.Shown += new System.EventHandler(this.VMPForm_Shown);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.mainMenu.ResumeLayout(false);
            this.mainMenu.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btApply;
        private System.Windows.Forms.TextBox tbSecondName;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tbFirstName;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox tbThirdName;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.MaskedTextBox tbDateBorn;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox tbFlat;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox tbKorpus;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox tbOblast;
        private System.Windows.Forms.MaskedTextBox tbIndex;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox tbBuilding;
        private System.Windows.Forms.RadioButton rbVillage;
        private System.Windows.Forms.RadioButton rbTown;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox tbPhone;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.MaskedTextBox tbPassDate;
        private System.Windows.Forms.Label label15;
        private PlainRichTextBox rtbPassFrom;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.MaskedTextBox tbPassId;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.MaskedTextBox tbPensId;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox tbInsComp;
        private System.Windows.Forms.MaskedTextBox tbInsId;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.ComboBox cbStatus;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TextBox tbMKBcode;
        private System.Windows.Forms.Label label23;
        private PlainRichTextBox rtbDiagnoz;
        private System.Windows.Forms.Label label24;
        private PlainRichTextBox rtbMethod;
        private System.Windows.Forms.TextBox tbVMPcode;
        private PlainRichTextBox rtbVMPDesc;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Button btOpen;
        private System.Windows.Forms.Button btOpenDirectory;
        private System.Windows.Forms.MenuStrip mainMenu;
        private System.Windows.Forms.ToolStripMenuItem miSettings;
        private System.Windows.Forms.ToolStripComboBox tscbDepartment;
        private System.Windows.Forms.ToolStripTextBox tstbDepartment;
        private System.Windows.Forms.ToolStripMenuItem miVMP;
        private System.Windows.Forms.Button btVMP;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.ComboBox cbSex;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.ComboBox cbTreatmentType;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label31;
        private PlainRichTextBox rtbModel;
        private System.Windows.Forms.ComboBox cbVMPFinance;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.TextBox tbVMPGroup;
        private System.Windows.Forms.TextBox tbMethodCode;
        private System.Windows.Forms.TextBox tbModelCode;
        private System.Windows.Forms.MaskedTextBox tbExemption;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.ToolStripMenuItem miStaff;
        private System.Windows.Forms.ComboBox cbStreetType;
        private System.Windows.Forms.ComboBox cbStreet;
        private System.Windows.Forms.ComboBox cbTown;
        private System.Windows.Forms.Button btSelectCommission;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.TextBox tbMembers;
        private System.Windows.Forms.TextBox tbChairman;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Label label37;
        private PlainRichTextBox rtbComplaints;
        private System.Windows.Forms.Label label36;
        private PlainRichTextBox rtbSoputstv;
        private System.Windows.Forms.Label label35;
        private PlainRichTextBox rtbOslozhnenie;
        private System.Windows.Forms.TextBox tbCardNumber;
        private System.Windows.Forms.Label label38;
        private PlainRichTextBox rtbAnamnez;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.Label label40;
        private PlainRichTextBox rtbGeneralExamination;
        private System.Windows.Forms.Label label41;
        private PlainRichTextBox rtbInstrumentalExamination;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.MaskedTextBox tbOperationDate;
        private System.Windows.Forms.MaskedTextBox tbGospDate;
        private System.Windows.Forms.MaskedTextBox tbCommissionDate;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.TextBox tbCMO;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.TextBox tbSuperintendent;
        private System.Windows.Forms.Button btTestData;
        private System.Windows.Forms.Button btPrint;
        private System.Windows.Forms.TextBox tbDoctor;
        private System.Windows.Forms.Button btClose;
        private System.Windows.Forms.Button btOpenDirectoryOld;
        private System.Windows.Forms.CheckBox cbIO;
        private ucStatusBarControl ucStatusBar;
        private ComboBoxEx cbDisability;
    }
}

