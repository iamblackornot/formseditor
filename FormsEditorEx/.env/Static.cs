﻿using System.Collections.Generic;
using Newtonsoft.Json.Linq;
using System;
using System.Drawing;

namespace FormsEditorEx
{
    public class Static
    {
        public static readonly string AppName = "HMCpro";
        public static readonly Version AppVersion = new Version(1, 3, 0);

        public static readonly string EmptyJObject = new JObject().ToString();
        public static readonly string EmptyJArray = new JArray().ToString();

        public static readonly string GuideURL = "https://drive.google.com/file/d/1IqqN1r2k159Eonvgc_2o3HMnA1yabz22/view?usp=sharing";

        public static readonly List<string> OutdatedFiles = new List<string>()
        {
            //"AppSoftware.LicenceEngine.Common.dll",
            //"AppSoftware.LicenceEngine.KeyVerification.dll",
            //"SharedLibrary.dll",
            "HMCpro.exe"
        };

        public static readonly List<string> NonClearControls = new List<string>()
        {
            //"tbSuperintendent",
            //"tbSuperintendentFormatted",
            //"cbDoctors",
            //"cbEnding"
        };

        public static readonly List<string> Endings = new List<string>()
        {
            "ым",
            "ой",
            ""
        };

        public static readonly List<string> VTEO_Methods = new List<string>()
        {
            "Антикоагулянтная терапия",
            "Ранняя активизация",
            "Механические способы профилактики (эластичная компрессия нижних конечностей)"
        };

        public static readonly List<string> ConditionPost = new List<string>()
        {
            "удовлетворительном состоянии",
            "состоянии средней степени тяжести",
            "тяжелом состоянии"
        };
        public static readonly List<string> CommissionTypes = new List<string>()
        {
            "Врачебная комиссия",
            "Консилиум"
        };
        public static readonly List<string> ConclusionTypes = new List<string>()
        {
            "Гистологическое",
            "Цитологическое",
            "Иммуногистохимическое"
        };

        public static readonly Dictionary<int, string> DepartmentNames = new Dictionary<int, string>()
        {
            { (int)Departments.Gynecological, "Отделение онкогинекологии" },
            { (int)Departments.Surgical, "Онкологическое абдоминальное отделение" },
            { (int)Departments.Urological, "Отделение онкоурологии" },
            { (int)Departments.Mammological, "Онкологическое отделение опухолей молочной железы и кожи" },
            { (int)Departments.Thoracic, "Онкологическое торакальное отделение" },
            { (int)Departments.Radiological, "Радиологическое отделение" },
            { (int)Departments.ChemotherapyPalliative, "Онкологическое отделение химиотерапевтического профиля № 2 дневного стационара при стационаре" },
            { (int)Departments.ChemotherapyAntitumor, "Онкологическое отделение противоопухолевой лекарственной терапии" },
            { (int)Departments.ChemotherapyDayHospital, "Онкологическое отделение химиотерапевтического профиля № 1 дневного стационара при стационаре" },
            { (int)Departments.RadiologyDayHospital, "Онкологическое отделение лучевой терапии дневного стационара при круглосуточном стационаре" }
        };

        public static readonly Dictionary<int, string> DepartmentsFormatted = new Dictionary<int, string>()
        {
            { (int)Departments.Gynecological, "отделением онкогинекологии" },
            { (int)Departments.Surgical, "онкологическим абдоминальным отделением" },
            { (int)Departments.Urological, "отделением онкоурологии" },
            { (int)Departments.Mammological, "онкологическим отделением опухолей молочной железы и кожи" },
            { (int)Departments.Thoracic, "онкологическим торакальным отделением" },
            { (int)Departments.Radiological, "радиологическим отделением" },
            { (int)Departments.ChemotherapyPalliative, "онкологическим отделением химиотерапевтического профиля № 2 дневного стационара при стационаре" },
            { (int)Departments.ChemotherapyAntitumor, "онкологическим отделением противоопухолевой лекарственной терапии" },
            { (int)Departments.ChemotherapyDayHospital, "онкологическим отделением химиотерапевтического профиля № 1 дневного стационара при стационаре " },
            { (int)Departments.RadiologyDayHospital, "онкологическим отделением лучевой терапии дневного стационара при круглосуточном стационаре" }
        };

        public static readonly Dictionary<int, string> DepartmentAddresses = new Dictionary<int, string>()
        {
            { (int)Departments.Gynecological, "г. Ярославль, ул. Чкалова, д. 4а" },
            { (int)Departments.Surgical, "г. Ярославль, ул. Чкалова, д. 4а" },
            { (int)Departments.Urological, "г. Ярославль, ул. Чкалова, д. 4а" },
            { (int)Departments.Mammological, "г. Ярославль, пр-т Октября, д. 67" },
            { (int)Departments.Thoracic, "г. Ярославль, пр-т Октября, д. 67" },
            { (int)Departments.Radiological, "г. Ярославль, ул. Чкалова, д. 4а" },
            { (int)Departments.ChemotherapyPalliative, "г. Ярославль, пр-т Октября, д. 67" },
            { (int)Departments.ChemotherapyAntitumor, "г. Ярославль, пр-т Октября, д. 67" },
            { (int)Departments.ChemotherapyDayHospital, "г. Ярославль, пр-т Октября, д. 67" },
            { (int)Departments.RadiologyDayHospital, "г. Ярославль, ул. Чкалова, д. 4а" }
        };

        public static readonly Dictionary<int, string> StatusLocalisPreCue = new Dictionary<int, string>()
        {
            { (int)Departments.Gynecological, "Слизистая влагалища без особенностей. Шейка матки макроскопически не изменена. Многослойный плоский эпителий без визуальных особенностей. Тело матки не увеличено. Придатки матки не пальпируются. Параметрии свободны." },
            { (int)Departments.Surgical, string.Empty },
            { (int)Departments.Urological, string.Empty },
            { (int)Departments.Mammological, string.Empty },
            { (int)Departments.Thoracic, string.Empty },
            { (int)Departments.Radiological, string.Empty },
            { (int)Departments.ChemotherapyPalliative, string.Empty },
            { (int)Departments.ChemotherapyAntitumor, string.Empty },
            { (int)Departments.ChemotherapyDayHospital, string.Empty },
            { (int)Departments.RadiologyDayHospital, string.Empty }
        };

        public static readonly Dictionary<int, string> StatusLocalisPostCue = new Dictionary<int, string>()
        {
            { (int)Departments.Gynecological, "Швы состоятельны. Заживление первичным натяжением. Послеоперационная рана без признаков воспаления." },
            { (int)Departments.Surgical, "Швы состоятельны. Заживление первичным натяжением. Послеоперационная рана без признаков воспаления." },
            { (int)Departments.Urological, "Швы состоятельны. Заживление первичным натяжением. Послеоперационная рана без признаков воспаления." },
            { (int)Departments.Mammological, "Швы состоятельны. Заживление первичным натяжением. Послеоперационная рана без признаков воспаления." },
            { (int)Departments.Thoracic, "Швы состоятельны. Заживление первичным натяжением. Послеоперационная рана без признаков воспаления." },
            { (int)Departments.Radiological, "Послеоперационная рана без признаков воспаления." },
            { (int)Departments.ChemotherapyPalliative, "Послеоперационная рана без признаков воспаления." },
            { (int)Departments.ChemotherapyAntitumor, "Послеоперационная рана без признаков воспаления." },
            { (int)Departments.ChemotherapyDayHospital, "Послеоперационная рана без признаков воспаления." },
            { (int)Departments.RadiologyDayHospital, "Послеоперационная рана без признаков воспаления." }
        };

        public static readonly Dictionary<int, bool> NeuroStatusEnabled = new Dictionary<int, bool>()
        {
            { (int)Departments.Gynecological, false },
            { (int)Departments.Surgical, false },
            { (int)Departments.Urological, false },
            { (int)Departments.Mammological, false },
            { (int)Departments.Thoracic, false },
            { (int)Departments.Radiological, true },
            { (int)Departments.ChemotherapyPalliative, true },
            { (int)Departments.ChemotherapyAntitumor, true },
            { (int)Departments.ChemotherapyDayHospital, true },
            { (int)Departments.RadiologyDayHospital, true }
        };
        public static readonly Dictionary<int, bool> DoesOperativeTreatment = new Dictionary<int, bool>()
        {
            { (int)Departments.Gynecological, true },
            { (int)Departments.Surgical, true },
            { (int)Departments.Urological, true },
            { (int)Departments.Mammological, true },
            { (int)Departments.Thoracic, true },
            { (int)Departments.Radiological, false },
            { (int)Departments.ChemotherapyPalliative, false },
            { (int)Departments.ChemotherapyAntitumor, false },
            { (int)Departments.ChemotherapyDayHospital, false },
            { (int)Departments.RadiologyDayHospital, false }
        };

        public static readonly string ComplaintsDefault = "на слабость.";
        public static readonly string OblastDefault = "Ярославская";

        public static readonly List<string> FieldsSaveIgnoreEx = new List<string>()
        {
            "tbSuperintendentFormatted",
            "rtbGeneralExaminationPre",
            "rtbGeneralExaminationPost"
        };

        public static readonly string NeuroStatusDefault = "Сознание ясное. Правильно ориентирован в пространстве, во времени и собственной личности. Речь не нарушена." +
            " На вопросы дает правильные ответы. Менингеальных признаков нет. Обоняние, вкус и  слух не нарушены. Нистагма нет. Парез конвергенции нет. Глотание не нарушено." +
            " Чувствительность на лице не нарушена. Объем движения в конечностях не ограничен. Координаторные пробы выполняет уверенно. Позе Ромберга устойчив.";


        public static readonly List<string> GenCondition = new List<string>()
        {
            "удовлетворительное",
            "средней ст. тяжести",
            "тяжелое",
            "крайне тяжелое"
        };
        public static readonly List<string> Nutrition = new List<string>()
        {
            "избыточное",
            "достаточное",
            "кахексия"
        };

        public static readonly List<string> VTEO_Risk = new List<string>()
        {
            "высокий",
            "умеренный",
            "низкий"
        };

        public static readonly List<string> GO_Risk = new List<string>()
        {
            "высокий",
            "умеренный",
            "низкий"
        };

        public static readonly Dictionary<DocType, IFillStrategy> FillStrategies = new Dictionary<DocType, IFillStrategy>()
        {
            { DocType.Anamnesis, new AnamnesisFillStrategy() },
            { DocType.PreoperativeEpicrisis, new PreoperativeEpicrisisFillStrategy() },
            { DocType.DischargeSummary, new DischargeSummaryFillStrategy() },
            { DocType.TitlePage, new TitlePageStrategy() },
        };

        public static readonly Dictionary<DocType, string> DocNames = new Dictionary<DocType, string>()
        {
            { DocType.Anamnesis, "ЛИСТ АНАМНЕЗА" },
            { DocType.PreoperativeEpicrisis, "ПРЕДОПЕРАЦИОННЫЙ ЭПИКРИЗ" },
            { DocType.DischargeSummary, "ВЫПИСКА" },
            { DocType.TitlePage, "ТИТУЛЬНЫЙ ЛИСТ" }
        };
        public static readonly Dictionary<FieldWithCue, string> Cues = new Dictionary<FieldWithCue, string>()
        {
            { FieldWithCue.Temperature, "36.6" },
            { FieldWithCue.PressureA, "130" },
            { FieldWithCue.PressureD, "80" },
            { FieldWithCue.HeartRate, "72" },
            { FieldWithCue.Skin, "нормальной окраски и влажности." },
            { FieldWithCue.LymphNodes, "не пальпируются." },
            { FieldWithCue.Mammary, "пальпаторно без узловых образований." },
            { FieldWithCue.Thyroid, "пальпаторно не увеличена, без узловых образований." },
            { FieldWithCue.Chest, "правильной формы, нормостенического типа. Пальпация её безболезненна." },
            { FieldWithCue.Dyspnea, "нет." },
            { FieldWithCue.LungsResp, "везикулярное. Хрипов нет. ЧДД – 18 в 1 мин." },
            { FieldWithCue.Percussion, "легочный звук." },
            { FieldWithCue.HeartSounds, "ритмичные,  ясные. Шумы – нет." },
            { FieldWithCue.Pulse, "ритмичный, удовлетворительного наполнения и напряжения." },
            { FieldWithCue.Stomach, "мягкий, безболезненный." },
            { FieldWithCue.Liver, "не увеличена, безболезненна." },
            { FieldWithCue.Urination, "свободное, неучащенное, безболезненное." },
            { FieldWithCue.Stool, "регулярный, оформленный." },
            { FieldWithCue.Genetics, "не отягощена." },
            { FieldWithCue.Allergy, "отрицает." },
            { FieldWithCue.Transfusion, "не было." },
            { FieldWithCue.ProfHarm, "нет." },
            { FieldWithCue.PastIllnesses, "нет." },
            { FieldWithCue.Tuberculosis, "отрицает." },

            { FieldWithCue.Complications, "нет." },
            { FieldWithCue.Accompanying, "нет (клинически значимой патологии не выявлено)." },
            { FieldWithCue.AccompMedication, "постоянно лекарств не принимает." },
            { FieldWithCue.AccompMedicationPost, "не планируется." },
            { FieldWithCue.MedicalAllergy, "отсутствуют." },

            { FieldWithCue.Medication, "не применялись." },
            { FieldWithCue.Treatment, "не проводились." },
        };
        public static readonly List<string> BloodGroup = new List<string>()
        {
            "0 (I)",
            "A (II)",
            "B (III)",
            "AB (IV)"
        };
        public static readonly List<string> Rh = new List<string>()
        {
            "(+) полож.",
            "(-) отриц."
        }; 
        public static readonly List<string> TreatmentResult = new List<string>()
        {
            "выздоровление",
            "улучшение",
            "ремиссия",
            "без изменений"
        };
        public static readonly Dictionary<SickLeaveEndType, string> SickLeaveEndTypeName = new Dictionary<SickLeaveEndType,string>()
        {
            { SickLeaveEndType.DischargedToWork, "выписана к труду" },
            { SickLeaveEndType.ExpectedAttendance, "явка в поликлинику по месту жительства" },
        };
        public static readonly Dictionary<MainFrameState, Type> MainFrameStateType = new Dictionary<MainFrameState, Type>()
        {
            { MainFrameState.ReadOnly, typeof(ReadOnlyState) },
            { MainFrameState.Anamnesis, typeof(AnamnesisState) },
            { MainFrameState.PreoperativeEpicrisis, typeof(PreoperativeEpicrisisState) },
            { MainFrameState.DischargeSummary, typeof(DischargeSummaryState) },
            { MainFrameState.Discharge, typeof(DischargeState) },
        };
        public static readonly Dictionary<MainFrameState, string> ActionButtonText = new Dictionary<MainFrameState, string>()
        {
            { MainFrameState.ReadOnly, "Начать новую историю болезни" },
            { MainFrameState.Anamnesis, "Создать лист анамнеза" },
            { MainFrameState.PreoperativeEpicrisis, "Создать предоперационный эпикриз" },
            { MainFrameState.DischargeSummary, "Создать выписку" },
            { MainFrameState.Discharge, "Завершить историю болезни" },
        };
        public static readonly Dictionary<Disability, string> DisabilityName = new Dictionary<Disability, string>()
        {
            { Disability.None, "нет" },
            { Disability.I, "I группы" },
            { Disability.II, "II группы" },
            { Disability.III, "III группы" },
        };
        public static readonly Dictionary<WorkCapacity, string> WorkCapacityName = new Dictionary<WorkCapacity, string>()
        {
            { WorkCapacity.FullyRestored, "восстановлена полностью" },
            { WorkCapacity.Reduced, "снижена" },
            { WorkCapacity.TemporarilyDisabled, "временно утрачена" },
            { WorkCapacity.SteadfastlyDisabled, "стойко утрачена" },
        };
        public static readonly Dictionary<DocType, string> TemplateFileNames = new Dictionary<DocType, string>()
        {
            { DocType.Anamnesis, "template1" },
            { DocType.PreoperativeEpicrisis, "template2" },
            { DocType.DischargeSummary, "template3" },
            { DocType.TitlePage, "template4" },
        };
        public static readonly Dictionary<StreetType, string> StreetTypeNames = new Dictionary<StreetType, string>()
        {
            { StreetType.Street, "ул." },
            { StreetType.Prospect, "пр-т" },
            { StreetType.Proyezd, "пр." },
        };
        public static readonly Dictionary<Sex, string> SexTitle = new Dictionary<Sex, string>()
        {
            { Sex.Male, "мужской" },
            { Sex.Female, "женский" },
        };
    }
}
