﻿using System.Windows.Forms;
using System.IO;
using System;
using SharedLibrary;
using SharedLibrary.Settings;

namespace FormsEditorEx
{
    public sealed class Paths : PathsBase
    {
        public static readonly string Dumps = "dumps\\";
        public static readonly string SolutionsOld = "Готовые Решения\\";
        public static readonly string LocalSettingsFolder = "cfg";
        public static readonly string LocalSettings = $"{LocalSettingsFolder}\\settingsEx"; 

        public string Solutions { get; private set; }
        public string Database { get; private set; }
        public string VMP { get; private set; }
        public string RemoteLogs { get; private set; }

        public static Paths Instance { get; private set; } = new Paths();
        private Paths() { }

        public override SimpleResult Init()
        {
            var res = base.Init();

            if (res.Done)
            {
                Solutions = ServerPath;
                Database = Path.Combine(ServerPath, "patients.db");
                RemoteLogs = Path.Combine(ServerPath, "logs");
                VMP = Path.Combine(Settings, "vmp_cfg");
            }

            return res;
        }
        public string GetOutputPath(DocType docType)
        {
            Patient p = Patient.Instance;
            return Path.Combine(GetSolutionsDirectory(p.Department.Value, p.Doctor), GetFileName(p, docType));
        }
        public string GetSolutionsDirectory(int dep_id, long? doc)
        {
            string path = Path.Combine(Solutions, AppConfig.Instance.Info.Departments[dep_id]);

            if (!Directory.Exists(path))
                Directory.CreateDirectory(path);

            if (doc.HasValue)
            {
                string docName = AppConfig.Instance.Staff[doc.Value].ShortName;
                path = Path.Combine(path, docName);

                if (!Directory.Exists(path))
                    Directory.CreateDirectory(path);
            }

            return path;
        }
        private static string GetFileName(Patient p, DocType docType)
        {
            DateTime date = docType == DocType.DischargeSummary ? p.DateDischarge.Value : p.DateExam.Value;

            return Utility.RemoveSpecialCharacters($"{p.ClinicalRecordNum} {p.AmbuCardNum} {p.SecondName} {p.FirstName.Substring(0, 1)}" +
                    $".{p.ThirdName.EmptyIfNull().Substring(0, 1)}. {Utility.DateToShortString(date)} {Static.DocNames[docType]}.doc");
        }
    }
}
