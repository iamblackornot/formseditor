﻿using System;

namespace FormsEditorEx.GenExam.Records
{
    public class UrineRecord : IExamRecord
    {
        public DateTime? Date { get; set; }
        public string Urine { get; set; }

        public string GetString()
        {
            string temp = string.Empty;

            if (Date.HasValue)
            {
                //temp += Utility.DateToShortString(Date.Value) + Symbols.NBSpace + Symbols.NBSpace + "Общий клинический анализ мочи: ";
                temp += "Общий клинический анализ мочи: ";

                if (!string.IsNullOrWhiteSpace(Urine))
                {
                    temp += Urine;
                }
            }

            return temp;
        }
        public static GenExamRecord GetTestRecord()
        {
            UrineRecord rec = new UrineRecord()
            {
                Date = new DateTime(2020, 1, 23),
                Urine = "в пределах нормы."
            };
            return new GenExamRecord() { Date = rec.Date, Result = rec.GetString() };
        }
    }
}
