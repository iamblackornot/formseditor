﻿using System;
using System.Runtime.InteropServices;
using System.Drawing;
using System.Windows.Forms;

namespace SharedLibrary
{
    public class ComboBoxEx : ComboBox
    {
        public ComboBoxEx()
        {
            DrawMode = DrawMode.OwnerDrawFixed;

            textBox = new TextBox();
            textBox.ReadOnly = true;
            textBox.Visible = false;
            textBox.TextAlign = HorizontalAlignment.Center;
        }

        [DllImport("user32.dll")]
        static extern int GetWindowLong(IntPtr hWnd, int nIndex);
        [DllImport("user32.dll")]
        static extern int SetWindowLong(IntPtr hWnd, int nIndex, int dwNewLong);
        const int GWL_STYLE = -16;
        const int ES_LEFT = 0x0000;
        const int ES_CENTER = 0x0001;
        const int ES_RIGHT = 0x0002;
        [StructLayout(LayoutKind.Sequential)]
        public struct RECT
        {
            public int Left;
            public int Top;
            public int Right;
            public int Bottom;
            public int Width { get { return Right - Left; } }
            public int Height { get { return Bottom - Top; } }
        }
        [DllImport("user32.dll")]
        public static extern bool GetComboBoxInfo(IntPtr hWnd, ref COMBOBOXINFO pcbi);

        [StructLayout(LayoutKind.Sequential)]
        public struct COMBOBOXINFO
        {
            public int cbSize;
            public RECT rcItem;
            public RECT rcButton;
            public int stateButton;
            public IntPtr hwndCombo;
            public IntPtr hwndEdit;
            public IntPtr hwndList;
        }
        protected override void OnHandleCreated(EventArgs e)
        {
            base.OnHandleCreated(e);
            SetupEdit();
        }
        private int buttonWidth = SystemInformation.HorizontalScrollBarArrowWidth;
        private void SetupEdit()
        {
            var info = new COMBOBOXINFO();
            info.cbSize = Marshal.SizeOf(info);
            GetComboBoxInfo(this.Handle, ref info);
            var style = GetWindowLong(info.hwndEdit, GWL_STYLE);
            style |= 1;
            SetWindowLong(info.hwndEdit, GWL_STYLE, style);
        }
        protected override void OnDrawItem(DrawItemEventArgs e)
        {
            base.OnDrawItem(e);
            e.DrawBackground();
            var txt = "";
            if (e.Index >= 0)
                txt = GetItemText(Items[e.Index]);

            // If drawing highlighted selection, change brush
            Color color;

            if ((e.State & DrawItemState.Selected) == DrawItemState.Selected)
                color = SystemColors.HighlightText;
            else
                color = ForeColor;

            TextRenderer.DrawText(e.Graphics, txt, Font, e.Bounds,
                color, TextFormatFlags.Left | TextFormatFlags.HorizontalCenter);
        }

        private TextBox textBox;

        private bool readOnly = false;

        public bool ReadOnly
        {
            get { return readOnly; }
            set
            {
                readOnly = value;

                if (readOnly)
                {
                    this.Visible = false;
                    textBox.Text = this.Text;
                    textBox.Location = this.Location;
                    textBox.Size = this.Size;
                    textBox.Visible = true;

                    if (textBox.Parent == null)
                        this.Parent.Controls.Add(textBox);
                }
                else
                {
                    this.Visible = true;
                    this.textBox.Visible = false;
                }
            }
        }
    }
}
