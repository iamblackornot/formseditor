﻿using SharedLibrary;

namespace FormsEditorEx.Database
{
    public abstract class DatabaseResult : IDatabaseQueryResult, IStatusBarNotification
    {
        public bool Completed { get; }

        public StatusBarIndicator Status { get; }

        public string StatusMessage { get; }

        public DatabaseResult(bool Completed, StatusBarIndicator Status, string StatusMessage)
        {
            this.Completed = Completed;
            this.Status = Status;
            this.StatusMessage = StatusMessage;
        }
    }
    public class DefaultDatabaseError : DatabaseResult
    {
        public DefaultDatabaseError() : base(false, StatusBarIndicator.ERROR, "Не удалось обратиться к базе данных") { }
    }
    public class NoConnectionError : DatabaseResult
    {
        public NoConnectionError() : base(false, StatusBarIndicator.ERROR, "Нет подключения к базе данных") { }
    }
    public abstract class DatabaseResult<T> : DatabaseResult
    {
        public T Result { get; }
        public DatabaseResult(bool Completed, StatusBarIndicator Status, string StatusMessage, T Result) : base (Completed, Status, StatusMessage)
        {
            this.Result = Result;
        }
    }
    public class DefaultDatabaseError<T> : DatabaseResult<T>
    {
        public DefaultDatabaseError() : base(false, StatusBarIndicator.ERROR, "Не удалось обратиться к базе данных", default(T)) { }
    }
    public class NoConnectionError<T> : DatabaseResult<T>
    {
        public NoConnectionError() : base(false, StatusBarIndicator.ERROR, "Нет подключения к базе данных", default(T)) { }
    }
}
