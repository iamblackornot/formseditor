﻿using System;
using FormsEditorEx.GenExam.Records;
using SharedLibrary;

namespace FormsEditorEx.GenExam.Tabs
{
    public partial class ucSARSTab : ucExamTabBase
    {
        public ucSARSTab()
        {
            InitializeComponent();
            ResizePanel();

            tbDateSARS.Validating += DateTextBox_Validating;
            tbDateSARS.KeyPress += DateTextBox_KeyPress;
            cbResult.KeyDown += TextBox_KeyDown;
        }

        private void ResizePanel()
        {
            pnContent.Location = ClientRectangle.Location;
            ClientSize = pnContent.Size;
        }
        public override bool AddExamRecord()
        {
            bool res = AddRecord(GenExamType.SARS, FieldsToRecord());

            if (res)
                Clear();

            return res;
        }
        public override void FillTestData()
        {
            tbDateSARS.Text = "25.01.2020";
        }

        private void Clear()
        {
            tbDateSARS.Clear();
            cbResult.Checked = false;
            //cbResult2.SelectedIndex = -1;
        }

        private SARSRecord FieldsToRecord()
        {
            if (tbDateSARS.MaskCompleted && DateTime.TryParse(tbDateSARS.Text, out DateTime date))
            {
                return new SARSRecord()
                {
                    Date = date,
                    Positive = cbResult.Checked
                };
            }
            else return null;
        }
    }
}
