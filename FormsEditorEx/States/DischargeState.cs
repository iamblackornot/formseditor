﻿using System.Windows.Forms;
using SharedLibrary;

namespace FormsEditorEx
{
    public class DischargeState : MainFrameStateBase
    {
        public DischargeState(MainFrame handler) : base(handler)
        {
            Type = MainFrameState.Discharge;
            Handler.PatientToDischargeSummaryFields();
            Handler.SetDischargeWorkspace(Static.ActionButtonText[Type]);
        }

        public override IMainFrameState Action(ucStatusBarControl ucStatusBar)
        {
            if (Handler.CheckDischargeData())
            {
                if (MessageBox.Show("Подтвердите завершение истории болезни (действие необратимо)",
                    string.Empty, MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation) == DialogResult.OK)
                {
                    if (Patient.Discharge(Handler.dbAdapter))
                    {
                        Handler.ClearAndReset();
                        return new ReadOnlyState(Handler);
                    }
                }
            }

            return this;
        }
        public override IMainFrameState Revert(ucStatusBarControl ucStatusBar)
        {
            if (ShowRevertPrompt())
            {
                Patient p = Patient.Instance;

                if (string.IsNullOrEmpty(p.ClinicalRecordNum) && !Patient.Load(p.AmbuCardNum, Handler.dbAdapter, ucStatusBar))
                {
                    return this;
                }

                p.Stage = (int)MainFrameState.DischargeSummary;
                p.PatientData.GenExam.Revert();

                ucStatusBar.ShowOK("Произведен откат на предыдущий этап");

                return new DischargeSummaryState(Handler);
            }

            return this;
        }
    }
}
