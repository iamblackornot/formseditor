﻿using System;
using FormsEditorEx.GenExam.Records;
using SharedLibrary;

namespace FormsEditorEx.GenExam.Tabs
{
    public partial class ucBAKTab : ucExamTabBase, IExamTab
    {
        public ucBAKTab() : base()
        {
            InitializeComponent();
            ResizePanel();

            tbBAKDate.Validating += DateTextBox_Validating;
            tbBAKDate.KeyPress += DateTextBox_KeyPress;

            tbAST.KeyDown += TextBox_KeyDown;
            tbALT.KeyDown += TextBox_KeyDown;
            tbBilirubinTotal.KeyDown += TextBox_KeyDown;
            tbBilirubinDirect.KeyDown += TextBox_KeyDown;
            tbAlkalinePhosphatase.KeyDown += TextBox_KeyDown;
            tbAlphaAmylase.KeyDown += TextBox_KeyDown;
            tbGlucose.KeyDown += TextBox_KeyDown;
            tbPotassium.KeyDown += TextBox_KeyDown;
            tbSodium.KeyDown += TextBox_KeyDown;
            tbProteinTotal.KeyDown += TextBox_KeyDown;
            tbCreatinine.KeyDown += TextBox_KeyDown;
            tbUrea.KeyDown += TextBox_KeyDown;
        }
        private void ResizePanel()
        {
            pnContent.Location = ClientRectangle.Location;
            ClientSize = pnContent.Size;
        }
        public override bool AddExamRecord()
        {
            bool res = AddRecord(GenExamType.BAK, FieldsToRecord());

            if (res)
                Clear();

            return res;
        }
        public override void FillTestData()
        {
            tbBAKDate.Text = "20.03.2020";
            tbAST.Text = "35,9";
            tbALT.Text = "30,6";
            tbBilirubinTotal.Text = "9,1";
            tbBilirubinDirect.Text = "3,5";
            tbAlkalinePhosphatase.Text = "49";
            tbAlphaAmylase.Text = "880";
            tbGlucose.Text = "4,3";
            tbPotassium.Text = "5,1";
            tbSodium.Text = "153";
            tbProteinTotal.Text = "64";
            tbCreatinine.Text = "55";
            tbUrea.Text = "5,1";
        }
        private void Clear()
        {
            tbBAKDate.Clear();
            tbAST.Clear();
            tbALT.Clear();
            tbBilirubinTotal.Clear();
            tbBilirubinDirect.Clear();
            tbAlkalinePhosphatase.Clear();
            tbAlphaAmylase.Clear();
            tbGlucose.Clear();
            tbPotassium.Clear();
            tbSodium.Clear();
            tbProteinTotal.Clear();
            tbCreatinine.Clear();
            tbUrea.Clear();
        }
        private BAKRecord FieldsToRecord()
        {
            if (tbBAKDate.MaskCompleted && DateTime.TryParse(tbBAKDate.Text, out DateTime date))
            {
                return new BAKRecord()
                {
                    Date = date,
                    AST = !string.IsNullOrWhiteSpace(tbAST.Text) ? tbAST.Text : null,
                    ALT = !string.IsNullOrWhiteSpace(tbALT.Text) ? tbALT.Text : null,
                    BilirubinTotal = !string.IsNullOrWhiteSpace(tbBilirubinTotal.Text) ? tbBilirubinTotal.Text : null,
                    BilirubinDirect = !string.IsNullOrWhiteSpace(tbBilirubinDirect.Text) ? tbBilirubinDirect.Text : null,
                    AlkalinePhosphatase = !string.IsNullOrWhiteSpace(tbAlkalinePhosphatase.Text) ? tbAlkalinePhosphatase.Text : null,
                    AlphaAmylase = !string.IsNullOrWhiteSpace(tbAlphaAmylase.Text) ? tbAlphaAmylase.Text : null,
                    Glucose = !string.IsNullOrWhiteSpace(tbGlucose.Text) ? tbGlucose.Text : null,
                    Potassium = !string.IsNullOrWhiteSpace(tbPotassium.Text) ? tbPotassium.Text : null,
                    Sodium = !string.IsNullOrWhiteSpace(tbSodium.Text) ? tbSodium.Text : null,
                    ProteinTotal = !string.IsNullOrWhiteSpace(tbProteinTotal.Text) ? tbProteinTotal.Text : null,
                    Creatinine = !string.IsNullOrWhiteSpace(tbCreatinine.Text) ? tbCreatinine.Text : null,
                    Urea = !string.IsNullOrWhiteSpace(tbUrea.Text) ? tbUrea.Text : null
                };
            }
            else return null;
        }
        //public override void RecordToFields()
        //{
        //    BAKRecord bak = GenExamResult.Instance.BAK;

        //    if (bak.Date.HasValue)
        //    {
        //        tbBAKDate.Text = Utility.DateToShortString(bak.Date.Value);

        //        tbAST.Text = !string.IsNullOrWhiteSpace(bak.AST) ? bak.AST : string.Empty;
        //        tbALT.Text = !string.IsNullOrWhiteSpace(bak.ALT) ? bak.ALT : string.Empty;
        //        tbBilirubinTotal.Text = !string.IsNullOrWhiteSpace(bak.BilirubinTotal) ? bak.BilirubinTotal : string.Empty;
        //        tbBilirubinDirect.Text = !string.IsNullOrWhiteSpace(bak.BilirubinDirect) ? bak.BilirubinDirect : string.Empty;
        //        tbAlkalinePhosphatase.Text = !string.IsNullOrWhiteSpace(bak.AlkalinePhosphatase) ? bak.AlkalinePhosphatase : string.Empty;
        //        tbAlphaAmylase.Text = !string.IsNullOrWhiteSpace(bak.AlphaAmylase) ? bak.AlphaAmylase : string.Empty;
        //        tbGlucose.Text = !string.IsNullOrWhiteSpace(bak.Glucose) ? bak.Glucose : string.Empty;
        //        tbPotassium.Text = !string.IsNullOrWhiteSpace(bak.Potassium) ? bak.Potassium : string.Empty;
        //        tbSodium.Text = !string.IsNullOrWhiteSpace(bak.Sodium) ? bak.Sodium : string.Empty;
        //        tbProteinTotal.Text = !string.IsNullOrWhiteSpace(bak.ProteinTotal) ? bak.ProteinTotal : string.Empty;
        //        tbCreatinine.Text = !string.IsNullOrWhiteSpace(bak.Creatinine) ? bak.Creatinine : string.Empty;
        //        tbUrea.Text = !string.IsNullOrWhiteSpace(bak.Urea) ? bak.Urea : string.Empty;
        //    }
        //}
    }
}
