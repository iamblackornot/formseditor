﻿namespace FormsEditorEx.Commission
{
    partial class CommissionEditForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.pnContent = new System.Windows.Forms.Panel();
            this.rtbDecision = new SharedLibrary.PlainRichTextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.tbDate = new System.Windows.Forms.MaskedTextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.tbNum = new System.Windows.Forms.TextBox();
            this.cbType = new SharedLibrary.ComboBoxEx();
            this.pnButtons = new System.Windows.Forms.Panel();
            this.btCancel = new System.Windows.Forms.Button();
            this.btOK = new System.Windows.Forms.Button();
            this.pnContent.SuspendLayout();
            this.pnButtons.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnContent
            // 
            this.pnContent.Controls.Add(this.rtbDecision);
            this.pnContent.Controls.Add(this.label3);
            this.pnContent.Controls.Add(this.label2);
            this.pnContent.Controls.Add(this.tbDate);
            this.pnContent.Controls.Add(this.label1);
            this.pnContent.Controls.Add(this.tbNum);
            this.pnContent.Controls.Add(this.cbType);
            this.pnContent.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnContent.Location = new System.Drawing.Point(14, 14);
            this.pnContent.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.pnContent.Name = "pnContent";
            this.pnContent.Padding = new System.Windows.Forms.Padding(0, 0, 0, 12);
            this.pnContent.Size = new System.Drawing.Size(435, 235);
            this.pnContent.TabIndex = 0;
            // 
            // rtbDecision
            // 
            this.rtbDecision.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.rtbDecision.Location = new System.Drawing.Point(0, 54);
            this.rtbDecision.Name = "rtbDecision";
            this.rtbDecision.PlainTextMode = true;
            this.rtbDecision.ShowSelectionMargin = true;
            this.rtbDecision.Size = new System.Drawing.Size(435, 172);
            this.rtbDecision.TabIndex = 4;
            this.rtbDecision.Text = "";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(3, 36);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(55, 15);
            this.label3.TabIndex = 5;
            this.label3.Text = "Решение";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(315, 6);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(34, 15);
            this.label2.TabIndex = 4;
            this.label2.Text = "Дата";
            // 
            // tbDate
            // 
            this.tbDate.Culture = new System.Globalization.CultureInfo("");
            this.tbDate.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.tbDate.Location = new System.Drawing.Point(354, 1);
            this.tbDate.Mask = "00.00.0000";
            this.tbDate.Name = "tbDate";
            this.tbDate.Size = new System.Drawing.Size(80, 26);
            this.tbDate.TabIndex = 2;
            this.tbDate.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.tbDate.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextBox_KeyDown);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(220, 6);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(20, 15);
            this.label1.TabIndex = 2;
            this.label1.Text = "№";
            // 
            // tbNum
            // 
            this.tbNum.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.tbNum.Location = new System.Drawing.Point(246, 1);
            this.tbNum.Name = "tbNum";
            this.tbNum.Size = new System.Drawing.Size(60, 26);
            this.tbNum.TabIndex = 1;
            this.tbNum.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.tbNum.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextBox_KeyDown);
            // 
            // cbType
            // 
            this.cbType.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cbType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbType.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.cbType.FormattingEnabled = true;
            this.cbType.Items.AddRange(new object[] {
            "Врачебная комиссия",
            "Консилиум"});
            this.cbType.Location = new System.Drawing.Point(0, 0);
            this.cbType.Name = "cbType";
            this.cbType.Size = new System.Drawing.Size(205, 27);
            this.cbType.TabIndex = 0;
            this.cbType.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextBox_KeyDown);
            // 
            // pnButtons
            // 
            this.pnButtons.Controls.Add(this.btCancel);
            this.pnButtons.Controls.Add(this.btOK);
            this.pnButtons.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnButtons.Location = new System.Drawing.Point(14, 249);
            this.pnButtons.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.pnButtons.Name = "pnButtons";
            this.pnButtons.Size = new System.Drawing.Size(435, 30);
            this.pnButtons.TabIndex = 1;
            // 
            // btCancel
            // 
            this.btCancel.Location = new System.Drawing.Point(200, 0);
            this.btCancel.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btCancel.Name = "btCancel";
            this.btCancel.Size = new System.Drawing.Size(90, 30);
            this.btCancel.TabIndex = 6;
            this.btCancel.TabStop = false;
            this.btCancel.Text = "Отмена";
            this.btCancel.UseVisualStyleBackColor = true;
            this.btCancel.Click += new System.EventHandler(this.btCancel_Click);
            // 
            // btOK
            // 
            this.btOK.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btOK.Location = new System.Drawing.Point(102, 0);
            this.btOK.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btOK.Name = "btOK";
            this.btOK.Size = new System.Drawing.Size(90, 30);
            this.btOK.TabIndex = 5;
            this.btOK.TabStop = false;
            this.btOK.Text = "ОК";
            this.btOK.UseVisualStyleBackColor = true;
            this.btOK.Click += new System.EventHandler(this.btOK_Click);
            // 
            // CommissionEditForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(463, 293);
            this.Controls.Add(this.pnContent);
            this.Controls.Add(this.pnButtons);
            this.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "CommissionEditForm";
            this.Padding = new System.Windows.Forms.Padding(14);
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.Text = "Добавить запись";
            this.pnContent.ResumeLayout(false);
            this.pnContent.PerformLayout();
            this.pnButtons.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnContent;
        private System.Windows.Forms.Panel pnButtons;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tbNum;
        private SharedLibrary.ComboBoxEx cbType;
        private System.Windows.Forms.Button btCancel;
        private System.Windows.Forms.Button btOK;
        private SharedLibrary.PlainRichTextBox rtbDecision;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.MaskedTextBox tbDate;
    }
}