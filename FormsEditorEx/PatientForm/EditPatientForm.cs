﻿using System;
using FormsEditorEx.Database;
using SharedLibrary;
using System.Windows.Forms;

namespace FormsEditorEx.PatientForm
{
    public partial class EditPatientForm : Form
    {
        public string InitialPatientId { get; private set; }
        public bool IsNewPatient { get; private set; }
        public string PatientID { get; private set; }
        public event EventHandler<LoadPatientEventArgs> SearchClick;

        DatabaseAdapter dbAdapter;
        public EditPatientForm(DatabaseAdapter dbAdapter, bool IsNewPatient, int? dep_id = null)
        {
            InitializeComponent();

            this.dbAdapter = dbAdapter;
            this.IsNewPatient = IsNewPatient;

            if (IsNewPatient)
            {
                if (dep_id.HasValue)
                    InitFields(dep_id.Value);
            }
            else
            {
                SetFieldValues();
            }

            InitButtons();
        }

        private void SetFieldValues()
        {
            Patient p = Patient.Instance;

            InitialPatientId = tbAmbuCardNum.Text = p.AmbuCardNum;
            tbSecondName.Text = p.SecondName.EmptyIfNull();
            tbFirstName.Text = p.FirstName.EmptyIfNull();
            tbThirdName.Text = p.ThirdName.EmptyIfNull();
            cbSex.SelectedIndex = p.Sex.HasValue ? p.Sex.Value : -1;
            tbDateBirth.Text = Utility.DateToShortString(p.DateBirth);
        }
        private void TextBox_KeyDown(object sender, KeyEventArgs e)
        {
            Utility.ReturnToTab(this, e);
        }
        private void InitButtons()
        {
            //btOK.Left = (pnButtons.Width - btOK.Width - btCancel.Width - 8) / 2;
            //btCancel.Left = btOK.Left + btOK.Width + 8;
            btOK.Enabled = false;
#if !DEBUG
            btTest.Visible = false;
#endif
        }
        private void ShowSearch(bool toShow = true)
        {
            if (btSearch.Visible != toShow)
            {
                btSearch.Visible = toShow;

                int margin = btSearch.Width + 3;

                tbAmbuCardNum.Width += !toShow ? margin : -margin;
            }
        }
        //private void tbAmbuCardNum_Validating(object sender, CancelEventArgs e)
        //{
        //    string value = tbAmbuCardNum.Text.Trim();

        //    if (!string.IsNullOrWhiteSpace(value))
        //    {
        //        DatabaseResult res = Middleware.HandleDBAction(() => DbHandler.Instance.CheckIfParameterIsUnique("AmbuCardNum", value));

        //        if(!res.Completed)
        //        {
        //            string text = string.Empty;

        //            if (res is ParameterIsNotUnique)
        //                text = "Пациент с указанным номером амбулаторной карты уже существует";
        //            //else if(res is DefaultDatabaseError)
        //            //    text = "Не удалось обратиться к базе данных"

        //            e.Cancel = true;
        //            MessageBox.Show(text, string.Empty, MessageBoxButtons.OK, MessageBoxIcon.Error);
        //        }
        //    }
        //}

        private void btCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            Close();
        }

        private void btOK_Click(object sender, EventArgs e)
        {
            if (CheckFields())
            {
                DatabaseResult res;

                if (IsNewPatient)
                {
                    res = Middleware.HandleDBAction(() => dbAdapter.CreatePatient(
                        tbAmbuCardNum.Text.Trim(),
                        Utility.ToTitleCase(tbSecondName.Text.Trim()),
                        Utility.ToTitleCase(tbFirstName.Text.Trim()),
                        Utility.ToTitleCase(tbThirdName.Text.Trim()),
                        cbSex.SelectedIndex,
                        DateTime.Parse(tbDateBirth.Text)));
                }
                else
                {
                    res = Middleware.HandleDBAction(() => dbAdapter.UpdateBasicPatientData(
                        InitialPatientId,
                        tbAmbuCardNum.Text.Trim(),
                        Utility.ToTitleCase(tbSecondName.Text.Trim()),
                        Utility.ToTitleCase(tbFirstName.Text.Trim()),
                        Utility.ToTitleCase(tbThirdName.Text.Trim()),
                        cbSex.SelectedIndex,
                        DateTime.Parse(tbDateBirth.Text)));
                }

                if (res.Completed)
                {
                    PatientID = tbAmbuCardNum.Text.Trim();
                    DialogResult = DialogResult.OK;
                    Close();
                }
                else
                {
                    string message = message = res.StatusMessage;

                    if (res is PatientAlreadyExists pae && IsNewPatient)
                    {
                        message += " (рядом с соответствующим полем появится кнопка поиска, " +
                            "которая закроет окно и покажет пациента в базе данных)";

                        ShowSearch(true);
                    }

                    MessageBox.Show(message, string.Empty, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
                MessageBox.Show("Требуется корректное заполнение всех полей", string.Empty, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
        }

        private void btTest_Click(object sender, EventArgs e)
        {
            tbAmbuCardNum.Text = "999999";
            tbSecondName.Text = "Родионова";
            tbFirstName.Text = "Нина";
            tbThirdName.Text = "Владимировна";
            cbSex.SelectedIndex = (int)Sex.Female;
            tbDateBirth.Text = "09.12.1956";
        }
        private bool CheckFields()
        {
            return !string.IsNullOrWhiteSpace(tbAmbuCardNum.Text) &&
                !string.IsNullOrWhiteSpace(tbSecondName.Text) &&
                !string.IsNullOrWhiteSpace(tbFirstName.Text) &&
                !string.IsNullOrWhiteSpace(tbThirdName.Text) &&
                cbSex.SelectedIndex > -1 &&
                tbDateBirth.MaskCompleted &&
                Utility.DateToAge(DateTime.Parse(tbDateBirth.Text.Trim())) >= 18;
        }
        private void tbAmbuCardNum_TextChanged(object sender, EventArgs e)
        {
            ShowSearch(false);
        }

        private void AddPatientForm_Load(object sender, EventArgs e)
        {
            ShowSearch(false);
        }

        private void btSearch_Click(object sender, EventArgs e)
        {
            SearchClick?.Invoke(this, new LoadPatientEventArgs() { PatientId = tbAmbuCardNum.Text.Trim() });
            DialogResult = DialogResult.Cancel;
            Close();
        }

        private void InitFields(int dep_id)
        {
            if (dep_id == (int)Departments.Gynecological)
            {
                cbSex.SelectedIndex = (int)Sex.Female;
                cbSex.Enabled = false;
            }
            else if (dep_id == (int)Departments.Urological)
            {
                cbSex.SelectedIndex = (int)Sex.Male;
                cbSex.Enabled = false;
            }
        }

        private void TextBox_TextChanged(object sender, EventArgs e)
        {
            btOK.Enabled = true;
        }
    }
}
