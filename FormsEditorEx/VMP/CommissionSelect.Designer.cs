﻿namespace FormsEditorEx.VMP
{
    partial class CommissionSelect
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CommissionSelect));
            this.lvStaff = new System.Windows.Forms.ListView();
            this.colDick = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colPosition = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.lvCommission = new System.Windows.Forms.ListView();
            this.colBitch = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colStatus = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colNames = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.btAdd = new System.Windows.Forms.Button();
            this.btSelect = new System.Windows.Forms.Button();
            this.btCancel = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lvStaff
            // 
            this.lvStaff.Alignment = System.Windows.Forms.ListViewAlignment.Default;
            this.lvStaff.AutoArrange = false;
            this.lvStaff.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lvStaff.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.colDick,
            this.colPosition,
            this.colName});
            this.lvStaff.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lvStaff.FullRowSelect = true;
            this.lvStaff.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.lvStaff.HideSelection = false;
            this.lvStaff.Location = new System.Drawing.Point(12, 12);
            this.lvStaff.MultiSelect = false;
            this.lvStaff.Name = "lvStaff";
            this.lvStaff.Size = new System.Drawing.Size(200, 267);
            this.lvStaff.TabIndex = 0;
            this.lvStaff.UseCompatibleStateImageBehavior = false;
            this.lvStaff.View = System.Windows.Forms.View.Details;
            // 
            // colDick
            // 
            this.colDick.DisplayIndex = 2;
            this.colDick.Width = 0;
            // 
            // colPosition
            // 
            this.colPosition.DisplayIndex = 0;
            this.colPosition.Text = "Должность";
            this.colPosition.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.colPosition.Width = 100;
            // 
            // colName
            // 
            this.colName.DisplayIndex = 1;
            this.colName.Text = "ФИО";
            this.colName.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.colName.Width = 100;
            // 
            // lvCommission
            // 
            this.lvCommission.Alignment = System.Windows.Forms.ListViewAlignment.Default;
            this.lvCommission.AutoArrange = false;
            this.lvCommission.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lvCommission.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.colBitch,
            this.colStatus,
            this.colNames});
            this.lvCommission.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lvCommission.FullRowSelect = true;
            this.lvCommission.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.lvCommission.HideSelection = false;
            this.lvCommission.Location = new System.Drawing.Point(252, 12);
            this.lvCommission.MultiSelect = false;
            this.lvCommission.Name = "lvCommission";
            this.lvCommission.Size = new System.Drawing.Size(201, 267);
            this.lvCommission.TabIndex = 1;
            this.lvCommission.UseCompatibleStateImageBehavior = false;
            this.lvCommission.View = System.Windows.Forms.View.Details;
            // 
            // colBitch
            // 
            this.colBitch.Width = 0;
            // 
            // colStatus
            // 
            this.colStatus.Text = "Статус";
            this.colStatus.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.colStatus.Width = 100;
            // 
            // colNames
            // 
            this.colNames.Text = "ФИО";
            this.colNames.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.colNames.Width = 100;
            // 
            // btAdd
            // 
            this.btAdd.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btAdd.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btAdd.Location = new System.Drawing.Point(218, 130);
            this.btAdd.Name = "btAdd";
            this.btAdd.Size = new System.Drawing.Size(28, 28);
            this.btAdd.TabIndex = 2;
            this.btAdd.Text = ">";
            this.btAdd.UseVisualStyleBackColor = true;
            this.btAdd.Click += new System.EventHandler(this.btAdd_Click);
            // 
            // btSelect
            // 
            this.btSelect.Enabled = false;
            this.btSelect.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btSelect.Location = new System.Drawing.Point(303, 285);
            this.btSelect.Name = "btSelect";
            this.btSelect.Size = new System.Drawing.Size(72, 32);
            this.btSelect.TabIndex = 3;
            this.btSelect.Text = "Выбрать";
            this.btSelect.UseVisualStyleBackColor = true;
            this.btSelect.Click += new System.EventHandler(this.btSelect_Click);
            // 
            // btCancel
            // 
            this.btCancel.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btCancel.Location = new System.Drawing.Point(381, 285);
            this.btCancel.Name = "btCancel";
            this.btCancel.Size = new System.Drawing.Size(72, 32);
            this.btCancel.TabIndex = 4;
            this.btCancel.Text = "Отмена";
            this.btCancel.UseVisualStyleBackColor = true;
            this.btCancel.Click += new System.EventHandler(this.btCancel_Click);
            // 
            // CommissionSelect
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(465, 328);
            this.Controls.Add(this.btCancel);
            this.Controls.Add(this.btSelect);
            this.Controls.Add(this.btAdd);
            this.Controls.Add(this.lvCommission);
            this.Controls.Add(this.lvStaff);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "CommissionSelect";
            this.ShowInTaskbar = false;
            this.Text = "CommissionSelect";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ColumnHeader colPosition;
        private System.Windows.Forms.ColumnHeader colName;
        private System.Windows.Forms.ListView lvStaff;
        private System.Windows.Forms.ColumnHeader colDick;
        private System.Windows.Forms.ListView lvCommission;
        private System.Windows.Forms.ColumnHeader colBitch;
        private System.Windows.Forms.ColumnHeader colStatus;
        private System.Windows.Forms.ColumnHeader colNames;
        private System.Windows.Forms.Button btAdd;
        private System.Windows.Forms.Button btSelect;
        private System.Windows.Forms.Button btCancel;
    }
}