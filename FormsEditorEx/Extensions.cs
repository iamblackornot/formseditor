﻿using System.Drawing;
using System.Windows.Forms;
using System.Linq;
using SharedLibrary;
using Newtonsoft.Json;
using System.Reflection;
using System;

namespace FormsEditorEx
{
    public static class Extensions
    {
        public static string ValueOrDefault(this string value, FieldWithCue fwc)
        {
            return !string.IsNullOrEmpty(value) ? value : Static.Cues[fwc];
        }
        public static string RemoveHardBreaks(this string value)
        {
            return !string.IsNullOrEmpty(value) ? value.Replace(char.ConvertFromUtf32(13), Symbols.SoftLineBreak) : string.Empty;
        }
        public static bool In<T>(this T obj, params T[] args)
        {
            return args.Contains(obj);
        }
        public static void SetTextBoxAvailability(this TextBoxBase tb, bool toDisable = false)
        {
            if (tb is RichTextBox rtb)
            {
                rtb.BackColor = !toDisable ? SystemColors.Window : SystemColors.Control;
            }

            tb.ReadOnly = toDisable;
            tb.TabStop = !toDisable;
        }
        public static bool TryJsonDeserealize<T>(this string json, out T result)
        {
            try
            {
                result = JsonConvert.DeserializeObject<T>(json);
                return true;
            }
            catch (Exception ex)
            {
                Logger.AddToLogs(MethodBase.GetCurrentMethod().ToString(), ex.ToString());
                result = default(T);
                return false;
            }
        }
    }
}
