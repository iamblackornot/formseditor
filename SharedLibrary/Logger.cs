﻿using SharedLibrary;
using System;
using System.IO;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;

namespace SharedLibrary.Logger
{
    public interface ILogger
    {
        void AddToLogs(string location, string message);
        void AddToLogs(string message);
        (bool, string) CopyLogsToFileHosting(string who, string dedicatedPath);
    }
    public class TextFileLogger : ILogger
    {
        private static readonly object lcLogs = new Object();
        private string appName;
        private string filePath;

        public TextFileLogger(string filePath, string appName = "")
        {
            this.appName = appName; 
            this.filePath = filePath;
        }
        public void AddToLogs(string location, string message)
        {
            Task.Factory.StartNew(delegate
            {
                lock (lcLogs)
                {
                    try
                    {
                        File.AppendAllText(filePath,
                            DateTime.Now.ToShortDateString() + " " +
                            DateTime.Now.ToLongTimeString() + " " +
                            location + " : " + message + Environment.NewLine);
                    } catch { }
                }
            }, CancellationToken.None, TaskCreationOptions.PreferFairness, TaskScheduler.Default);
        }
        public void AddToLogs(string message)
        {
            AddToLogs(string.Empty, message);
        }
        public (bool, string) CopyLogsToFileHosting(string who, string dedicatedPath)
        {           
            try
            {
                string logs_original_path = filePath;
                if (!File.Exists(logs_original_path))
                {
                    return (false, "Файл с данными о сбоях приложения не найден (скорее всего, ни один сбой еще не был зафиксирован");
                }

                string dir = Path.Combine(dedicatedPath);

                if (!Directory.Exists(dir))
                {
                    Directory.CreateDirectory(dir);
                }

                string logs_destination_path = Path.Combine(dir, Utility.RemoveSpecialCharacters(
                    $"{Utility.DateToExtendedShortString(DateTime.Now)} {who} {appName}.txt"));

                File.Copy(logs_original_path, logs_destination_path, true);

                return (true, "Данные о сбоях приложения отправлены на файлообменник");
            }
            catch (Exception ex)
            {
                AddToLogs(MethodBase.GetCurrentMethod().Name, ex.ToString());
                return (false, "Не удалось отправить данные на файлообменник");
            }
        }
    }
}
