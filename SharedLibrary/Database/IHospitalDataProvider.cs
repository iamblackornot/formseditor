﻿using SharedLibrary.Settings;
using System.Collections.Generic;

namespace SharedLibrary.Database
{
    public interface IHospitalDataProvider
    {
        DatabaseResult<PersonnelCollection> GetPersonnel();
    }
}
