﻿using System;
using Microsoft.Office.Interop.Word;
using System.Threading;
using SharedLibrary;

namespace FormsEditorEx
{
    public static partial class WordInterop
    {
        public static int TargetHeightInPoints { get; } = 300;
        private const int TITLE_PAGE_ENTRIES_NUM = 5;
        private const int INITIAL_TITLE_FONT_SIZE = 11;
        private const int MIN_TITLE_FONT_SIZE = 6;
        private const int TITEL_TAG_FONT_SIZE = 8;
        private const float TITLE_LINE_SPACING = 1.15f;

        public static void AdjustTitlePage(Document doc)
        {
            Range range = doc.Content;
            range.Find.Execute("%tpe");

            int page = (int)range.Information[WdInformation.wdActiveEndAdjustedPageNumber];
            float points = (float)doc.Range(range.Start - 1, range.Start - 1).Information[WdInformation.wdVerticalPositionRelativeToPage];
            float font_size = INITIAL_TITLE_FONT_SIZE;

            DateTime starttime = DateTime.Now;

            while ((points > TargetHeightInPoints || page > 1) && (DateTime.Now - starttime).Seconds < 15)
            {
                font_size -= 0.5f;

                if (font_size < MIN_TITLE_FONT_SIZE - 0.0005f)
                {
                    break;
                }

                for (int i = 1; i <= TITLE_PAGE_ENTRIES_NUM; i++)
                {
                    range = doc.Content;
                    range.Find.Execute($"%t{i}s");
                    int start = range.End;

                    range = doc.Content;
                    range.Find.Execute($"%t{i}e");
                    int end = range.Start;

                    if (end - start > 0)
                    {
                        doc.Range(start, end).Font.Size = font_size;

                        Range checkRange = doc.Range(start, start + 5);

                        int font_change_timeout = 0;

                        while (Math.Abs(checkRange.Font.Size - font_size) > 0.05f && font_change_timeout <= 1000)
                        {
                            Thread.Sleep(50);
                            font_change_timeout += 50;

                            if (font_change_timeout >= 999)
                            {
                                string error = $"title_page entry #{i} timeout: " +
                                    $"target font size = {font_size}, " +
                                    $"first5 font size = {checkRange.Font.Size}, " +
                                    $"first5 text = {checkRange.Text}";

                                Logger.AddToLogs(error);
                            }
                        }
                    }
                }

                range = doc.Content;
                range.Find.Execute("%tpe");

                page = (int)range.Information[WdInformation.wdActiveEndAdjustedPageNumber];
                points = (float)doc.Range(range.Start - 1, range.Start - 1).Information[WdInformation.wdVerticalPositionRelativeToPage];
            }

            range = doc.Content;
            range.Find.Execute("%tpe");
            int s = range.End;

            range = doc.Content;
            int e = range.End;

            if (e - s > 0)
                doc.Range(s, e).Delete();

            /////////////////////insert line breaks to fit 1/2 A4 height////////////////////

            range = doc.Content;
            range.Find.Execute("%tpe");
            points = (float)range.Information[WdInformation.wdVerticalPositionRelativeToPage];

            //float lineheight = font_size * (1 + 1 + (LINE_SPACING - 1) * 2);
            float lineheight = TITEL_TAG_FONT_SIZE * (1 + 1 + (TITLE_LINE_SPACING - 1) * 2);
            Console.WriteLine($"line_height = {lineheight}");
            starttime = DateTime.Now;

            while ((points < TargetHeightInPoints - lineheight) && (DateTime.Now - starttime).Seconds < 5)
            {
                int j = FindTextBlockWithLeastHeight(doc);

                if (j >= 0)
                {
                    range = doc.Content;
                    range.Find.Execute($"%t{j}e");
                    range = doc.Range(range.Start, range.Start);

                    float pos = (float)range.Information[WdInformation.wdVerticalPositionRelativeToPage];

                    range.Text = Symbols.Tab;
                    range.Font.Size = TITEL_TAG_FONT_SIZE;

                    float new_pos = (float)range.Information[WdInformation.wdVerticalPositionRelativeToPage];

                    //Console.WriteLine($"pos = {pos}, new_pos = {new_pos}");

                    if (new_pos <= pos)
                    {
                        range = doc.Range(range.End, range.End);
                        range.Text = Symbols.SoftLineBreak;
                    }
                }

                range = doc.Content;
                range.Find.Execute("%tpe");
                points = (float)range.Information[WdInformation.wdVerticalPositionRelativeToPage];
            }

            Console.WriteLine($"height = {points}, halfA4 = {TargetHeightInPoints}, spacing = {lineheight}");

            for (int i = 1; i <= TITLE_PAGE_ENTRIES_NUM; i++)
            {
                WordInterop.FindAndReplace(doc, $"%t{i}s", " ");
                WordInterop.FindAndReplace(doc, $"%t{i}e", " ");
            }

            WordInterop.FindAndReplace(doc, "%tpe", " ");
        }

        private static int FindTextBlockWithLeastHeight(Document doc)
        {
            int index = -1;
            float height = TargetHeightInPoints;
            Range range;

            //Console.WriteLine("Search");

            for (int i = 1; i <= TITLE_PAGE_ENTRIES_NUM; i++)
            {
                range = doc.Content;
                range.Find.Execute($"%t{i}s");
                float start = (float)range.Information[WdInformation.wdVerticalPositionRelativeToPage];

                range = doc.Content;
                range.Find.Execute($"%t{i}e");
                float end = (float)range.Information[WdInformation.wdVerticalPositionRelativeToPage];

                float diff = end - start;
                //Console.WriteLine(diff);

                if(diff >= 0 && diff < height + 0.01f)
                {
                    index = i;
                    height = diff;
                }
            }

            return index;
        }
    }
}
