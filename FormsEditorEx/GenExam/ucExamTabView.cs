﻿using System;
using System.Collections.Generic;
using FormsEditorEx.GenExam.Tabs;
using System.Windows.Forms;

namespace FormsEditorEx.GenExam
{
    public partial class ucExamTabView : UserControl
    {
        private List<IExamTab> tabs;
        private readonly int MIN_ADD_BUTTON_PANEL_HEIGHT;
        private const int PADDING = 24;

        public event EventHandler<TabViewEventArgs> Resized;
        public event EventHandler RecordAdded;
        public int SelectedTab { get; private set; } = -1;

        public ucExamTabView()
        {
            InitializeComponent();

            InitTabs();
            InitContent();

            MIN_ADD_BUTTON_PANEL_HEIGHT = 8 + btAddRecord.Height + 12;

            Load += UcExamTabView_Load;
        }

        private void UcExamTabView_Load(object sender, EventArgs e)
        {
            dgvTabs.SelectionChanged += new EventHandler(dgvTabs_SelectionChanged);

            for(int i = 0; i < dgvTabs.Rows.Count; i++)
            {
                if(dgvTabs.Rows[i].Visible)
                {
                    ActivateTab(i);
                    break;
                }
            }
        }

        private void InitTabs()
        {
            List<string> tabs = GenExamTabsFactory.GetTabNames();

            dgvTabs.Columns.Add("tabs", "tabs");

            foreach (string tab in tabs)
            {
                dgvTabs.Rows.Add(tab);
            }

            dgvTabs.Columns[0].SortMode = DataGridViewColumnSortMode.NotSortable;
        }
        public void HideAddButton()
        {
            pnAddButton.Visible = false;
        }
        private void InitContent()
        {
            tabs = GenExamTabsFactory.CreateTabs();
        }

        private void OnResize()
        {
            int height = 0;
            int width = 0;

            foreach (var tab in tabs)
            {
                if (tab is Control)
                {
                    Control c = tab as Control;

                    pnTabContent.Controls.Add(c);

                    c.Resize += OnTab_Resize;

                    height = c.Height > height ? c.Height : height;
                    width = c.Width > width ? c.Width : width;
                }
            }

            this.Height = height;

            this.Width = dgvTabs.Left + dgvTabs.Width + PADDING + width;

            foreach (var tab in tabs)
            {
                PositonControlOnContentPanel(tab as Control);
            }

            btAddRecord.Left = (pnAddButton.Width - btAddRecord.Width) / 2;
        }

        private void OnTab_Resize(object sender, EventArgs e)
        {
            //int height = 0;

            //foreach (Control c in pnTabContent.Controls)
            //    height = c.Height > height ? c.Height : height;
            if (sender is Control c)
            {
                int height = c.Height;

                pnTabContent.Height = height; // > pnTabContent.Height ? height : pnTabContent.Height;

                Resized?.Invoke(this, new TabViewEventArgs() { HeightDiff = height + (pnAddButton.Visible ? MIN_ADD_BUTTON_PANEL_HEIGHT : 0) - this.Height });
            }
        }

        private void PositonControlOnContentPanel(Control c)
        {
            int temp = (pnTabContent.Width - c.Width) / 2;

            if (temp < PADDING)
                c.Left = PADDING;
            else
                c.Left = temp;
        }

        private void dgvTabs_SelectionChanged(object sender, EventArgs e)
        {
            var rowsCount = dgvTabs.SelectedRows.Count;
            if (rowsCount == 0 || rowsCount > 1) return;

            ActivateTab(dgvTabs.SelectedRows[0].Index);
        }

        private void ActivateTab(int index)
        {
            if (index != SelectedTab)
            {
                if (SelectedTab >= 0)
                    tabs[SelectedTab].Deactivate();
                tabs[index].Activate();

                SelectedTab = index;

                AdjustTabContentHeight();
            }
        }
        private void AdjustTabContentHeight()
        {
            if (tabs[SelectedTab] is Control c)
                pnTabContent.Height = c.Height;
        }
        private void ucExamTabView_Load(object sender, EventArgs e)
        {
            OnResize();
        }

        private bool AddRecord()
        {
            if (SelectedTab > -1)
            {
                if (tabs[SelectedTab].AddExamRecord())
                {
                    return true;
                }
                else
                {
                    MessageBox.Show("Дата обследования не указана или введена неверно");
                    return false;
                }
            }
            else
                return false;
        }
        public void FillTestData()
        {
            if (SelectedTab > -1)
                tabs[SelectedTab].FillTestData();
        }
        public void FillTestDataAllTabs()
        {
            foreach (var tab in tabs)
                tab.FillTestData();
        }

        private void btAddRecord_Click(object sender, EventArgs e)
        {
            if (AddRecord())
                RecordAdded?.Invoke(this, EventArgs.Empty);
        }
    }

    public class TabViewEventArgs : EventArgs
    {
        public int HeightDiff { get; set; }
    }
}
