﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace FormsEditorEx
{
    public partial class NewClinicalRecordForm : Form
    {
        public string ClinicalRecordNum { get; private set; }
        public NewClinicalRecordForm()
        {
            InitializeComponent();

            InitButtons();
        }
        private void InitButtons()
        {
            //btOK.Left = (pnButtons.Width - btOK.Width - btCancel.Width - 8) / 2;
            //btCancel.Left = btOK.Left + btOK.Width + 8;
            btOK.Enabled = false;
#if !DEBUG
            btTest.Visible = false;
#endif
        }

        private void tbEdit_TextChanged(object sender, EventArgs e)
        {
            btOK.Enabled = tbEdit.MaskCompleted;
        }

        private void btCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            Close();
        }

        private void btTest_Click(object sender, EventArgs e)
        {
            tbEdit.Text = "1111111/21";
        }

        private void btOK_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Подтвердите создание новой истории болезни (действие необратимо)",
                string.Empty, MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation) == DialogResult.OK)
            {
                DialogResult = DialogResult.OK;
                ClinicalRecordNum = tbEdit.Text.Trim();
                Close();
            }
        }
    }
}
