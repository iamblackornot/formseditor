﻿using System;
using SharedLibrary;

namespace FormsEditorEx.GenExam.Records
{
    public class OAKRecord : IExamRecord
    {
        public DateTime? Date { get; set; }
        public string Erythrocytes { get; set; }
        public string Leucocytes { get; set; }
        public string Hemoglobin { get; set; }
        public string Trombocytes { get; set; }
        public string Neutrophils { get; set; }

        public string GetString()
        {
            string temp = string.Empty;

            if (Date.HasValue)
            {
                //temp += Utility.DateToShortString(Date.Value) + Symbols.NBSpace + Symbols.NBSpace + "Общий клинический анализ крови: ";
                temp += "Общий клинический анализ крови: ";

                bool isAllEmpty = true;

                if (!string.IsNullOrWhiteSpace(Erythrocytes))
                {
                    isAllEmpty = false;
                    temp += "эритроциты" + Symbols.NBSpace + Symbols.Dash + Symbols.NBSpace + Erythrocytes + "x10" + Symbols.Power12 + Symbols.NBSpace + "ед/л";
                }

                if (!string.IsNullOrWhiteSpace(Leucocytes))
                {
                    if (!isAllEmpty)
                        temp += ", ";
                    else
                        isAllEmpty = false;

                    temp += "лейкоциты" + Symbols.NBSpace + Symbols.Dash + Symbols.NBSpace + Leucocytes + "x10" + Symbols.Power9 + Symbols.NBSpace + "ед/л";
                }

                if (!string.IsNullOrWhiteSpace(Hemoglobin))
                {
                    if (!isAllEmpty)
                        temp += ", ";
                    else
                        isAllEmpty = false;

                    temp += "гемоглобин" + Symbols.NBSpace + Symbols.Dash + Symbols.NBSpace + Hemoglobin + Symbols.NBSpace + "г/л";
                }

                if (!string.IsNullOrWhiteSpace(Trombocytes))
                {
                    if (!isAllEmpty)
                        temp += ", ";
                    else
                        isAllEmpty = false;

                    temp += "тромбоциты" + Symbols.NBSpace + Symbols.Dash + Symbols.NBSpace + Trombocytes + "x10" + Symbols.Power9 + Symbols.NBSpace + "ед/л";
                }

                if (!string.IsNullOrWhiteSpace(Neutrophils))
                {
                    if (!isAllEmpty)
                        temp += ", ";
                    else
                        isAllEmpty = false;

                    temp += "нейтрофилы" + Symbols.NBSpace + Symbols.Dash + Symbols.NBSpace + Neutrophils + "x10" + Symbols.Power9 + Symbols.NBSpace + "ед/л";
                }

                temp += ".";
            }

            return temp;
        }
        public static GenExamRecord GetTestRecord()
        {
            OAKRecord rec = new OAKRecord()
            {
                Date = new DateTime(2020, 3, 20),
                Erythrocytes = "4,6",
                Leucocytes = "8,5",
                Hemoglobin = "131",
                Trombocytes = "339",
                Neutrophils = "47",
            };
            return new GenExamRecord() { Date = rec.Date, Result = rec.GetString() };
        }
    }
}
