﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace SharedLibrary
{
    public class TextBoxEx : TextBox
    {
        public string Cue
        {
            get { return cue; }
            set
            {
                showCue(false);
                cue = value;
                if (this.Focused && !String.IsNullOrWhiteSpace(value)) showCue(true);
            }
        }
        private string cue;

        public bool IsCueOn { get; private set; }

        protected override void OnEnter(EventArgs e)
        {
            showCue(false);
            base.OnEnter(e);
        }

        protected override void OnLeave(EventArgs e)
        {
            showCue(true);
            base.OnLeave(e);
        }

        protected override void OnVisibleChanged(EventArgs e)
        {
            if (!this.Focused && !String.IsNullOrWhiteSpace(cue)) showCue(true);
            base.OnVisibleChanged(e);
        }

        private void showCue(bool visible)
        {
            if (this.DesignMode) visible = false;
            if (visible)
            {
                if (this.Text.Length == 0)
                {
                    IsCueOn = true;
                    this.Text = cue;
                    //this.SelectAll();
                    //this.SelectionColor = Color.Gray;
                    this.ForeColor = Color.Gray;
                    this.Font = new Font(this.Font, FontStyle.Italic);
                }
            }
            else
            {
                if (this.Text == cue)
                {
                    IsCueOn = false;
                    this.Clear();
                    //this.SelectionColor = this.ForeColor;
                    this.ForeColor = SystemColors.WindowText;
                    this.Font = new Font(this.Font, FontStyle.Regular);
                }
            }
        }
        public void SetText(string text)
        {
            if (String.IsNullOrWhiteSpace(text))
            {
                if (!String.IsNullOrWhiteSpace(Cue) && !IsCueOn)
                {
                    Clear();
                    showCue(true);
                }
            }
            else
            {
                if (!string.Equals(text, cue))
                {
                    if (IsCueOn)
                        showCue(false);
                    Text = text;
                }
            }
        }
    }
}
