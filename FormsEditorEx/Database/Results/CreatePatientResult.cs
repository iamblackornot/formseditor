﻿using SharedLibrary;

namespace FormsEditorEx.Database
{
    public class PatientCreated : DatabaseResult
    {
        public PatientCreated() : base(true, StatusBarIndicator.OK, "Пациент добавлен") { }
    }
    public class PatientAlreadyExists : DatabaseResult
    {
        public PatientAlreadyExists() : base(false, StatusBarIndicator.ERROR, "Пациент с указанным номером амбулаторной карты уже существует") { }
    }
}

