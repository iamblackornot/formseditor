﻿using FormsEditorEx.Settings;
using SharedLibrary.Settings;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SQLite;
using System.IO;
using System.Reflection;
using SharedLibrary.Database;
using SharedLibrary.Logger;

namespace FormsEditorEx.Database
{
    public class DatabaseAdapter : CommonSQLiteDatabaseAdapter
    {
        public delegate DatabaseResult DBAction();
        public delegate DatabaseResult<T> DBAction<T>();

        public DatabaseAdapter(ILogger logger) : base(logger, Paths.Instance.Database, Paths.Instance.ConfigDatabase)
        {
        }

        private DatabaseResult ExecDBAction(DBAction action, SQLiteConnection dbConnection)
        {
            try
            {
                dbConnection.Open();

                if (dbConnection.State != ConnectionState.Open)
                    return new NoConnectionError();

                return action();
            }
            catch (SQLiteException ex) when ((SQLiteErrorCode)ex.ErrorCode == SQLiteErrorCode.Constraint)
            {
                return new PatientAlreadyExists();

            }
            catch (Exception ex)
            {
                Logger.AddToLogs(ex.ToString());
                return new DefaultDatabaseError();
            }
            finally
            {
                dbConnection.Close();
            }
        }
        private DatabaseResult<T> ExecDBAction<T>(DBAction<T> action, SQLiteConnection dbConnection)
        {
            try
            {
                dbConnection.Open();

                if (dbConnection.State != ConnectionState.Open)
                    return new NoConnectionError<T>();

                return action();
            }
            catch (Exception ex)
            {
                Logger.AddToLogs(ex.ToString());
                return new DefaultDatabaseError<T>();
            }
            finally
            {
                dbConnection.Close();
            }
        }
        private int ExecuteNonQuery(SQLiteCommand cmd)
        {
            using (cmd)
            {
                return cmd.ExecuteNonQuery(CommandBehavior.CloseConnection);
            }
        }
        private T ExecuteScalar<T>(SQLiteCommand cmd)
        {
            using (cmd)
            {
                return (T)Convert.ChangeType(cmd.ExecuteScalar(CommandBehavior.CloseConnection), typeof(T));
            }
        }

        private void ReadObjectPropertiesFromDatabaseRow<T>(SQLiteDataReader rdr, ref T obj)
        {
            int count = rdr.FieldCount;

            for (int i = 0; i < count; i++)
            {
                var value = rdr.GetValue(i);
                var name = rdr.GetName(i);

                PropertyInfo prop = obj.GetType().GetProperty(name, BindingFlags.Public | BindingFlags.Instance);

                if (prop != null && prop.CanWrite && prop.GetSetMethod(true).IsPublic)
                {
                    if (value != DBNull.Value)
                    {
                        Type type = prop.PropertyType;

                        if (type == typeof(string))
                            prop.SetValue(obj, value.ToString(), null);
                        else if (type == typeof(int))
                            prop.SetValue(obj, Convert.ChangeType(value, typeof(int)), null);
                        else if (type == typeof(long))
                            prop.SetValue(obj, Convert.ChangeType(value, typeof(long)), null);
                        else if (type == typeof(Nullable<int>))
                            prop.SetValue(obj, Convert.ChangeType(value, typeof(int)), null);
                        else if (type == typeof(Nullable<long>))
                            prop.SetValue(obj, Convert.ChangeType(value, typeof(long)), null);
                        else if (type == typeof(bool))
                            prop.SetValue(obj, Convert.ChangeType(value, typeof(bool)), null);
                        else if (type == typeof(Nullable<DateTime>))
                            prop.SetValue(obj, DateTime.Parse(rdr.GetDateTime(i).ToString()), null);
                    }
                    else
                        prop.SetValue(obj, null, null);
                }
            }
        }

        public DatabaseResult LoadPatient(string id)
        {
            return ExecDBAction(delegate { return LoadPatientAction(id); }, mainDbConn);
        }
        private DatabaseResult LoadPatientAction(string id)
        {
            using (SQLiteCommand cmd = CommandFactory.GetLoadPatientCommand(mainDbConn, id))
            using (SQLiteDataReader rdr = cmd.ExecuteReader())
            {
                //throw new SQLiteException();
                if (rdr.Read())
                {
                    Patient p = Patient.Instance;

                    ReadObjectPropertiesFromDatabaseRow(rdr, ref p);

                    return new PatientLoaded();
                }
                else
                {
                    return new PatientNotFound(id);
                }
            }
        }
        public DatabaseResult SavePatient()
        {
            return ExecDBAction(delegate { return SavePatientAction(); }, mainDbConn);
        }
        private DatabaseResult SavePatientAction()
        {
            return ExecuteNonQuery(CommandFactory.GetSavePatientCommand(mainDbConn, Patient.Instance)) == 1 ?
                new PatientSaved() as DatabaseResult : new PatientSaveError() as DatabaseResult;
        }
        public DatabaseResult AssignPatient(string id, string recnum, int dep, long doc)
        {
            return ExecDBAction(delegate { return AssignPatientAction(id, recnum, dep, doc); }, mainDbConn);
        }
        private DatabaseResult AssignPatientAction(string id, string recnum, int dep, long doc)
        {
            int aff_rows = ExecuteNonQuery(CommandFactory.GetAssignPatientCommand(mainDbConn, id, recnum, dep, doc));

            if (aff_rows == 1)
                return new PatientAssigned();
            else if (aff_rows == 0)
                return new PatientNotAvailable();
            else
                throw new SQLiteException($"affected rows number = {aff_rows}");
        }

        public DatabaseResult LoadRefreshPatientList(DataTable dtPatients, int? dep, long? doc)
        {
            return ExecDBAction(delegate { return LoadRefreshPatientListAction(dtPatients, dep, doc); }, mainDbConn);
        }
        private DatabaseResult LoadRefreshPatientListAction(DataTable dtPatients, int? dep, long? doc)
        {
            return LoadPatientList(CommandFactory.GetRefreshPatientListCommand(mainDbConn, dep, doc), dtPatients);
        }

        public DatabaseResult LoadSearchPatientList(DataTable dtPatients, string id, string SecondName, string FirstName,
            string ThirdName, DateTime? startDate, DateTime? endDate)
        {
            return ExecDBAction(delegate
            {
                return LoadSearchPatientListAction(dtPatients, id, SecondName, FirstName,
                                                    ThirdName, startDate, endDate);
            }, mainDbConn);
        }
        private DatabaseResult LoadSearchPatientListAction(DataTable dtPatients, string id, string SecondName, string FirstName,
            string ThirdName, DateTime? startDate, DateTime? endDate)
        {
            return LoadPatientList(CommandFactory.GetSearchPatientListCommand(mainDbConn, id, SecondName, FirstName,
                ThirdName, startDate, endDate), dtPatients);
        }

        public DatabaseResult LoadLast100PatientList(DataTable dtPatients)
        {
            return ExecDBAction(delegate { return LoadLast100PatientListAction(dtPatients); }, mainDbConn);
        }
        private DatabaseResult LoadLast100PatientListAction(DataTable dtPatients)
        {
            return LoadPatientList(CommandFactory.GetLast100PatientListCommand(mainDbConn), dtPatients);
        }
        private DatabaseResult LoadPatientList(SQLiteCommand cmd, DataTable dtPatients)
        {
            using (cmd)
            using (SQLiteDataAdapter myAdapter = new SQLiteDataAdapter(cmd))
            {
                dtPatients.Clear();

                myAdapter.Fill(dtPatients);

                return new PatientListLoaded(dtPatients.Rows.Count);
            }
        }
        public DatabaseResult DeletePatient(string id)
        {
            return ExecDBAction(delegate { return DeletePatientAction(id); }, mainDbConn);
        }
        private DatabaseResult DeletePatientAction(string id)
        {
            return ExecuteNonQuery(CommandFactory.GetDeletePatientCommand(mainDbConn, id)) == 1 ?
                new PatientRemoved() as DatabaseResult : new PatientRemoveError() as DatabaseResult;
        }
        public DatabaseResult CreatePatient(string id, string SecondName, string FirstName, string ThirdName, int Sex, DateTime DateBirth)
        {
            return ExecDBAction(delegate { return CreatePatientAction(id, SecondName, FirstName, ThirdName, Sex, DateBirth); }, mainDbConn);
        }
        private DatabaseResult CreatePatientAction(string id, string SecondName, string FirstName, string ThirdName, int Sex, DateTime DateBirth)
        {
            return ExecuteNonQuery(CommandFactory.GetCreatePatientCommand(mainDbConn, id, SecondName, FirstName, ThirdName, Sex, DateBirth)) == 1 ?
                new PatientCreated() as DatabaseResult : new DefaultDatabaseError() as DatabaseResult;
        }
        public DatabaseResult UpdateBasicPatientData(string id, string new_id, string SecondName, string FirstName, string ThirdName, int Sex, DateTime DateBirth)
        {
            return ExecDBAction(delegate { return UpdateBasicPatientDataAction(id, new_id, SecondName, FirstName, ThirdName, Sex, DateBirth); }, mainDbConn);
        }
        private DatabaseResult UpdateBasicPatientDataAction(string id, string new_id, string SecondName, string FirstName, string ThirdName, int Sex, DateTime DateBirth)
        {
            return ExecuteNonQuery(CommandFactory.GetUpdateBasicPatientDataCommand(mainDbConn, id, new_id, SecondName, FirstName, ThirdName, Sex, DateBirth)) == 1 ?
                new BasicPatientDataUpdated() as DatabaseResult : new DefaultDatabaseError() as DatabaseResult;
        }
        public DatabaseResult CheckIfParameterIsUnique(string name, object value)
        {
            return ExecDBAction(delegate { return CheckIfParameterIsUniqueAction(name, value); }, mainDbConn);
        }
        private DatabaseResult CheckIfParameterIsUniqueAction(string name, object value)
        {
            return ExecuteScalar<int>(CommandFactory.CheckIfParameterIsUniqueCommand(mainDbConn, name, value)) == 0 ?
                new ParameterIsUnique() as DatabaseResult : new ParameterIsNotUnique() as DatabaseResult;

        }
    }
}
