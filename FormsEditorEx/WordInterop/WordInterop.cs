﻿using System;
using Microsoft.Office.Interop.Word;
using SharedLibrary;
using System.Collections.Generic;
using System.Linq;
using System.Collections.Specialized;
using SharedLibrary.Settings;
using System.Reflection;
using static SharedLibrary.Settings.InfoClass;
using Newtonsoft.Json;

namespace FormsEditorEx
{
    public static partial class WordInterop
    {
        //private static float PointToCmMultiplier = 0.0352777776f;
        public static void BuildNeuroStatusString(Document doc, Patient p)
        {
            Range range = doc.Content;
            if (range.Find.Execute("%neurost"))
            {
                if (p.Department.HasValue && Static.NeuroStatusEnabled[p.Department.Value])
                {
                    string neuro = !string.IsNullOrWhiteSpace(p.NeuroStatus) ? p.NeuroStatus : Static.NeuroStatusDefault;

                    range.Text = Environment.NewLine;
                    range = doc.Range(range.End, range.End);

                    range.Text = $"Неврологический статус: ";
                    range.Bold = 1;

                    range = doc.Range(range.End, range.End);
                    range.Text = neuro + Environment.NewLine;
                    range.Bold = 0;
                }
                else
                    range.Text = string.Empty;
            }
        }
        public static void BuildBloodString(Document doc, Patient p)
        {
            Range range = doc.Content;
            if (range.Find.Execute("%bld"))
            {
                if (p.BloodGroup.HasValue || p.Rh.HasValue)
                {
                    range.Text = Environment.NewLine;
                    range = doc.Range(range.End, range.End);

                    if(p.BloodGroup.HasValue)
                    {
                        range.Text = $"Группа крови: {Static.BloodGroup[p.BloodGroup.Value]}";
                        range.Bold = 1;
                        range = doc.Range(range.End, range.End);
                    }

                    if(p.Rh.HasValue)
                    {
                        if(p.BloodGroup.HasValue)
                        {
                            range.Text = ", ";
                            range.Bold = 1;
                            range = doc.Range(range.End, range.End);
                        }

                        range.Text = $"Rh {Static.Rh[p.Rh.Value]}";
                        range.Bold = 1;
                        range = doc.Range(range.End, range.End);
                    }

                    range.Text = Environment.NewLine;
                }
                else
                    range.Text = string.Empty;
            }
        }
        public static void BuildRecommendationsString(Document doc)
        {
            Range range = doc.Content;
            if (range.Find.Execute("%rcmnd"))
            {
                string rcmnd = Patient.Instance.PatientData.TemporaryFields.Recommendations.EmptyIfNull();
                range.Text = rcmnd;

                string temp = Patient.Instance.PatientData.GenExam.GetLastDiseaseString();

                if (!string.IsNullOrEmpty(temp))
                {
                    range = doc.Range(range.End, range.End);
                    range.Text = (!string.IsNullOrEmpty(rcmnd) ? Environment.NewLine : string.Empty) + $"- {temp} (не более 6 мес)";
                }
            }
        }
        public static void BuildAccompMedicationPreString(Document doc)
        {
            BuildAccompMedicationString(doc, Patient.Instance.PatientData.TemporaryFields.AccompMedicationPre.EmptyIfNull(), 
                "%accmp_med", "По сопутствующей патологии постоянно лекарств не принимает.",
                "По сопутствующей патологии постоянно принимает: ");
        }
        public static void BuildAccompMedicationPlannedString(Document doc)
        {
            BuildAccompMedicationString(doc, Patient.Instance.AccompMedication, "%plnd_accmp_med", "Лечение сопутствующей патологии не планируется.",
                "Планируется лечение сопутствующей патологии: ");
        }
        public static void BuildAccompMedicationDoneString(Document doc)
        {
            BuildAccompMedicationString(doc, Patient.Instance.AccompMedication, "%plnd_accmp_med",
                "Лечение сопутствующей патологии не проводилось. Регулярно лекарственные препараты не принимает.",
                "Проводилось лечение сопутствующей патологии: ");
        }
        private static void BuildAccompMedicationString(Document doc, string text, string tag, string noMedStatement, string medStatement)
        {
            Range range = doc.Content;

            if (range.Find.Execute(tag))
            {
                if (string.IsNullOrEmpty(text))
                {
                    range.Text = noMedStatement;
                    range.Bold = 1;
                }
                else
                {
                    range.Text = medStatement;
                    range.Bold = 1;

                    range = doc.Range(range.End, range.End);
                    range.Text = Utility.AddDotAtTheEndIfNeeded(text);
                    range.Bold = 0;
                }
            }
        }
        public static string GetVTEOPreventionString()
        {
            string temp = string.Empty;
            string vteo = Patient.Instance.PatientData.TemporaryFields.VTEOPrevention;

            if (!string.IsNullOrEmpty(vteo))
            {
                List<string> items = vteo.Split(',').ToList<string>();

                foreach (string item in items)
                    if (int.TryParse(item, out int index))
                    {
                        //temp += Environment.NewLine;
                        temp += Symbols.SoftLineBreak;

                        temp += "-" + Symbols.NBSpace + Static.VTEO_Methods[index];
                    }
            }
            else
                temp = "не планируется.";

            return temp;
        }
        public static void BuildSickLeaveString(Document doc)
        {
            TemporaryData td = Patient.Instance.PatientData.TemporaryFields;
            Range range = doc.Content;
            if (range.Find.Execute("%sickleave"))
            {
                if (!string.IsNullOrEmpty(td.SickLeaveNum))
                {
                    //range.Text = Environment.NewLine;
                    //range = doc.Range(range.End, range.End);

                    range.Text = $"Лист временной нетрудоспособности № {td.SickLeaveNum}";
                    range.Bold = 1;

                    range = doc.Range(range.End, range.End);
                    range.Text = $" с{Symbols.NBSpace}{Utility.DateToShortString(td.SickLeaveStart)} по{Symbols.NBSpace}{Utility.DateToShortString(td.SickLeaveEnd)}";
                    range.Bold = 0;

                    if (td.SickLeaveExtStart.HasValue)
                    {
                        range = doc.Range(range.End, range.End);
                        range.Text = $", продлен с{Symbols.NBSpace}{Utility.DateToShortString(td.SickLeaveExtStart)} по{Symbols.NBSpace}{Utility.DateToShortString(td.SickLeaveExtEnd)}";
                    }

                    if(td.SickLeaveEndType.HasValue && (td.SickLeaveExtEnd.HasValue || td.SickLeaveEnd.HasValue))
                    {
                        DateTime endDate = td.SickLeaveExtEnd.HasValue ? td.SickLeaveExtEnd.Value : td.SickLeaveEnd.Value;

                        string temp = string.Empty;

                        if (td.SickLeaveEndType.Value == (int)SickLeaveEndType.DischargedToWork)
                        {
                            endDate = endDate.AddDays(1);

                            temp = $", с{Symbols.NBSpace}{Utility.DateToShortString(endDate)} выписана к труду";
                        }
                        else
                            temp = $", {Utility.DateToShortString(endDate)} явка в поликлинику по месту жительства";

                        range = doc.Range(range.End, range.End);
                        range.Text = temp;

                    }
                    //else
                    //{
                        range = doc.Range(range.End, range.End);
                        range.Text = ".";
                    //}

                    range = doc.Range(range.End, range.End);
                    range.Text = Environment.NewLine;
                }
                else
                    range.Text = string.Empty;
            }
        }
        public static string GetStreetString(PersonalInfo pi)
        {
            if (string.IsNullOrEmpty(pi.Street))
            {
                return string.Empty;
            }
            if(!pi.StreetType.HasValue)
            {
                return $"{Static.StreetTypeNames[StreetType.Street]}{Symbols.NBSpace}{pi.Street}";
            }
            if (pi.StreetType.Value != (int)StreetType.Prospect)
            {
                return $"{Static.StreetTypeNames[(StreetType)pi.StreetType.Value]}{Symbols.NBSpace}{pi.Street}";
            }

            var prospects = AppConfig.Instance.Info.Prospects;
            var prospectIndex = prospects.FindIndex((InfoClass.Prospect pr) => pr.Name == pi.Street);
            var prospectAlign = ProspectAlign.Left;

            if (prospectIndex >= 0)
            {
                prospectAlign = AppConfig.Instance.Info.Prospects[prospectIndex].Align;
            }

            if (prospectAlign == ProspectAlign.Left)
                return $"{Static.StreetTypeNames[StreetType.Prospect]}{Symbols.NBSpace}{pi.Street}";
            else
                return $"{pi.Street}{Symbols.NBSpace}{Static.StreetTypeNames[StreetType.Prospect]}";
        }
        public static string GetAddressString(PersonalInfo pi)
        {
            string temp = string.Empty;

            if (!string.IsNullOrEmpty(pi.Index))
                temp += $"{pi.Index}, ";

            string region = Static.OblastDefault;

            if (!string.IsNullOrEmpty(pi.Oblast))
                region = pi.Oblast;

            temp += $"{region}{Symbols.NBSpace}обл., ";

            if (!string.IsNullOrEmpty(pi.Town))
            {
                if (!pi.IsVillage.HasValue)
                    temp += "г." + Symbols.NBSpace;
                else
                    temp += "c." + Symbols.NBSpace;

                temp += $"{pi.Town}, ";
            }

            if (!string.IsNullOrEmpty(pi.Street))
            {
                temp += $"{GetStreetString(pi)}, ";
            }

            if (!string.IsNullOrEmpty(pi.Building))
            {
                temp += $"д.{Symbols.NBSpace}{pi.Building}";
            }

            if (!string.IsNullOrEmpty(pi.Korpus))
            {
                temp += $", корп.{Symbols.NBSpace}{pi.Korpus}";
            }

            if (!string.IsNullOrEmpty(pi.Flat))
            {
                temp += $", кв.{Symbols.NBSpace}{pi.Flat}";
            }

            return temp;
        }
        public static void FillDoctor(Application wordApp, Patient p)
        {
            string temp = AppConfig.Instance.Staff.GetPersonnelPositionTitle(p.Doctor.Value).ToLower();
            temp += ", ";
            temp += AppConfig.Instance.Staff.GetPersonnelSpecialtyTitle(p.Doctor.Value);
            temp += " ";
            temp += AppConfig.Instance.Staff[p.Doctor.Value].ShortName.Replace(" ", Symbols.NBSpace);

            WordInterop.FindAndReplace(wordApp, "%dctr", temp);
        }
        public static void FillSuperIntendent(Application wordApp, TemporaryData td)
        {
            string temp;

            if (!td.IO.HasValue)
                temp = "Заведующий";
            else
                temp = "И.о. заведующего";

            WordInterop.FindAndReplace(wordApp, "%si_st", temp);

            temp = string.Empty;

            if (td.SuperIntendent.HasValue)
            {
                temp = AppConfig.Instance.Staff.GetPersonnelSpecialtyTitle(td.SuperIntendent.Value);
                temp += " ";
                temp += AppConfig.Instance.Staff[td.SuperIntendent.Value].ShortName.Replace(" ", Symbols.NBSpace);
            }
            WordInterop.FindAndReplace(wordApp, "%spr_int", temp);
        }
        public static void FillGeneralStatusPre(Application wordApp, Patient p)
        {
            TemporaryData td = p.PatientData.TemporaryFields;

            string temp = td.GenCondition.HasValue ? Static.GenCondition[td.GenCondition.Value] : string.Empty;
            WordInterop.FindAndReplace(wordApp, "%gencndtn", temp);
            WordInterop.FindAndReplace(wordApp, "%nutr", td.Nutrition.HasValue ? Static.Nutrition[td.Nutrition.Value] : string.Empty);

            string preData = p.PatientData.TemporaryFields.LifeAnamnesisPre;
            bool usePreData = !string.IsNullOrEmpty(preData);

            LifeAnamnesisData lad = usePreData
                ? JsonConvert.DeserializeObject<LifeAnamnesisData>(preData)
                : p.PatientData.LifeAnamnesisData;

            FillGeneralStatus(wordApp, lad, string.Empty);
        }
        public static void FillGeneralStatusPost(Application wordApp, Patient p)
        {
            TemporaryData td = p.PatientData.TemporaryFields;

            string prefix = "p";
            string temp = td.ConditionPost.HasValue ? temp = Static.GenCondition[td.ConditionPost.Value] : string.Empty;
            WordInterop.FindAndReplace(wordApp, $"%{prefix}gencndtn", temp);
            WordInterop.FindAndReplace(wordApp, $"%{prefix}nutr", td.Nutrition.HasValue ? Static.Nutrition[td.Nutrition.Value] : string.Empty);

            FillGeneralStatus(wordApp, p.PatientData.LifeAnamnesisData, prefix);
        }
        private static void FillGeneralStatus(Application wordApp, LifeAnamnesisData lad, string prefix)
        {
            WordInterop.FindAndReplace(wordApp, $"%{prefix}heig", lad.Height.EmptyIfNull());
            WordInterop.FindAndReplace(wordApp, $"%{prefix}weig", lad.Weight.EmptyIfNull());
            WordInterop.FindAndReplace(wordApp, $"%{prefix}tmpr", lad.Temperature.ValueOrDefault(FieldWithCue.Temperature));
            WordInterop.FindAndReplace(wordApp, $"%{prefix}prssr", lad.PressureA.ValueOrDefault(FieldWithCue.PressureA) + "/" + lad.PressureD.ValueOrDefault(FieldWithCue.PressureD));
            WordInterop.FindAndReplace(wordApp, $"%{prefix}hrtrate", lad.HeartRate.ValueOrDefault(FieldWithCue.HeartRate));
            WordInterop.FindAndReplace(wordApp, $"%{prefix}skn", lad.Skin.ValueOrDefault(FieldWithCue.Skin));
            WordInterop.FindAndReplace(wordApp, $"%{prefix}lympth", lad.LymphNodes.ValueOrDefault(FieldWithCue.LymphNodes));
            WordInterop.FindAndReplace(wordApp, $"%{prefix}mmry", lad.Mammary.ValueOrDefault(FieldWithCue.Mammary));
            WordInterop.FindAndReplace(wordApp, $"%{prefix}thyr", lad.Thyroid.ValueOrDefault(FieldWithCue.Thyroid));
            WordInterop.FindAndReplace(wordApp, $"%{prefix}chst", lad.Chest.ValueOrDefault(FieldWithCue.Chest));
            WordInterop.FindAndReplace(wordApp, $"%{prefix}dyspn", lad.Dyspnea.ValueOrDefault(FieldWithCue.Dyspnea));
            WordInterop.FindAndReplace(wordApp, $"%{prefix}rspr", lad.LungsResp.ValueOrDefault(FieldWithCue.LungsResp));
            WordInterop.FindAndReplace(wordApp, $"%{prefix}percssn", lad.Percussion.ValueOrDefault(FieldWithCue.Percussion));
            WordInterop.FindAndReplace(wordApp, $"%{prefix}hrtsnd", lad.HeartSounds.ValueOrDefault(FieldWithCue.HeartSounds));
            WordInterop.FindAndReplace(wordApp, $"%{prefix}pulse", lad.Pulse.ValueOrDefault(FieldWithCue.Pulse));
            WordInterop.FindAndReplace(wordApp, $"%{prefix}stmch", lad.Stomach.ValueOrDefault(FieldWithCue.Stomach));
            WordInterop.FindAndReplace(wordApp, $"%{prefix}livr", lad.Liver.ValueOrDefault(FieldWithCue.Liver));
            WordInterop.FindAndReplace(wordApp, $"%{prefix}urnt", lad.Urination.ValueOrDefault(FieldWithCue.Urination));
            WordInterop.FindAndReplace(wordApp, $"%{prefix}stl", lad.Stool.ValueOrDefault(FieldWithCue.Stool));
        }
        public static string GetMKBString(string mkb)
        {
            if(string.IsNullOrWhiteSpace(mkb))
            {
                return string.Empty;
            }

            return $" [{mkb}]";
        }
        public static void FindAndReplace(Application app, string findText, string replaceWithText)
        {
            var doc = app.ActiveDocument;

            FindAndReplace(doc, findText, replaceWithText);
        }
        public static void FindAndReplace(Document doc, string findText, string replaceWithText)
        {
            var range = doc.Content;
            while (range.Find.Execute(findText))
            {
                range.Text = replaceWithText;
                range = doc.Content;
            }
        }
    }
}
