﻿using SharedLibrary;

namespace FormsEditorEx.Database
{
    public class BasicPatientDataUpdated : DatabaseResult
    {
        public BasicPatientDataUpdated() : base(true, StatusBarIndicator.OK, "Пациент добавлен") { }
    }
    //public class PatientAlreadyExists : DatabaseResult
    //{
    //    public PatientAlreadyExists() : base(false, StatusBarIndicator.ERROR, "Пациент с указанным номером амбулаторной карты уже существует") { }
    //}
}