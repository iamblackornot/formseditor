﻿using System;
using System.Windows.Forms;

namespace FormsEditorEx.Conclusion
{
    public partial class ConclusionForm : Form
    {
        public ConclusionForm(bool isReadOnly = true)
        {
            InitializeComponent();

            ConclusionTableEditControl ucDataTableEdit = new ConclusionTableEditControl(isReadOnly);
            ucDataTableEdit.OnClose += UcDataTableEdit_OnClose;

            pnContent.Controls.Add(ucDataTableEdit);
            ucDataTableEdit.Dock = DockStyle.Fill;

            CenterToScreen();
        }

        private void UcDataTableEdit_OnClose(object sender, EventArgs e)
        {
            Close();
        }
    }
}
