﻿namespace FormsEditorEx
{
    public class LoadSettingsResult
    {
        public bool Done { get; }
        public string ErrorMessage { get; } = string.Empty;

        public LoadSettingsResult(bool Done, string settingsPart = null)
        {
            this.Done = Done;

            if (!Done)
            {
                if(!string.IsNullOrEmpty(settingsPart))
                {
                    ErrorMessage = $"Не удалось загрузить настройки ({settingsPart})";
                }
                else
                {
                    ErrorMessage = "Произошла ошибка при загрузке настроек";
                }
            }

        }
    }
}
