﻿using System;
using Newtonsoft.Json;
using SharedLibrary;

namespace FormsEditorEx
{
    public class PersonalInfo
    {
        public PersonalInfo() { }

        [JsonProperty("i")]
        public string Index { get; set; }
        [JsonProperty("o")]
        public string Oblast { get; set; }
        [JsonProperty("t")]
        public string Town { get; set; }
        [JsonProperty("isV")]
        public bool? IsVillage { get; set; }
        [JsonProperty("st")]
        public int? StreetType { get; set; }
        [JsonProperty("s")]
        public string Street { get; set; }
        [JsonProperty("b")]
        public string Building { get; set; }
        [JsonProperty("k")]
        public string Korpus { get; set; }
        [JsonProperty("f")]
        public string Flat { get; set; }

        [JsonProperty("ph")]
        public string Phone { get; set; }

        [JsonProperty("pai")]
        public string PassId { get; set; }
        [JsonProperty("paf")]
        public string PassFrom { get; set; }
        [JsonProperty("pad")]
        public DateTime? PassDate { get; set; }

        [JsonProperty("pei")]
        public string PensId { get; set; }
        [JsonProperty("ic")]
        public string InsComp { get; set; }
        [JsonProperty("ii")]
        public string InsId { get; set; }
        [JsonProperty("inv")]
        public int? Disability { get; set; }
        [JsonProperty("ex")]
        public string Exemption { get; set; }
        [JsonProperty("ss")]
        public int? SocialStatus { get; set; }

        public string Serialize()
        {
            var json = Utility.SerializeDataObject(this);

            return json != Static.EmptyJObject ? Cryptography.EncryptString(json, Cryptography.ENC) : null;
        }
    }
}
