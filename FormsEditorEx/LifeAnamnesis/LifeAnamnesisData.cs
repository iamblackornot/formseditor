﻿using System;
using SharedLibrary;
using Newtonsoft.Json;
using System.ComponentModel;

namespace FormsEditorEx
{
    public class LifeAnamnesisData
    {
        [JsonProperty("he")]
        public string Height { get; set; }
        [JsonProperty("we")]
        public string Weight { get; set; }
        [JsonProperty("te")]
        public string Temperature { get; set; }
        [JsonProperty("pa")]
        public string PressureA { get; set; }
        [JsonProperty("pd")]
        public string PressureD { get; set; }
        [JsonProperty("hr")]
        public string HeartRate { get; set; }
        [JsonProperty("smk")]
        public int? Smokes { get; set; }
        [JsonProperty("cgg")]
        public string CiggsPerDay { get; set; }

        [JsonProperty("sk")]
        public string Skin { get; set; }
        [JsonProperty("ln")]
        public string LymphNodes { get; set; }
        [JsonProperty("mm")]
        public string Mammary { get; set; }
        [JsonProperty("th")]
        public string Thyroid { get; set; }
        [JsonProperty("ch")]
        public string Chest { get; set; }
        [JsonProperty("dy")]
        public string Dyspnea { get; set; }
        [JsonProperty("lr")]
        public string LungsResp { get; set; }
        [JsonProperty("pe")]
        public string Percussion { get; set; }
        [JsonProperty("hs")]
        public string HeartSounds { get; set; }
        [JsonProperty("pu")]
        public string Pulse { get; set; }
        [JsonProperty("st")]
        public string Stomach { get; set; }
        [JsonProperty("li")]
        public string Liver { get; set; }
        [JsonProperty("ur")]
        public string Urination { get; set; }
        [JsonProperty("stl")]
        public string Stool { get; set; }

        [JsonProperty("gn")]
        public string Genetics { get; set; }
        [JsonProperty("al")]
        public string Allergy { get; set; }
        [JsonProperty("tr")]
        public string Transfusion { get; set; }
        [JsonProperty("pr")]
        public string ProfHarm { get; set; }
        [JsonProperty("pil")]
        public string PastIllnesses { get; set; }
        [JsonProperty("tub")]
        public string Tuberculosis { get; set; }

        public LifeAnamnesisData() { }
        public string Serialize()
        {
            var json = Utility.SerializeDataObject(this);

            return json != Static.EmptyJObject ? json : null;
        }
    }
}
