﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using FormsEditorEx.GenExam.Records;
using SharedLibrary;

namespace FormsEditorEx.GenExam
{
    public partial class GenExamForm : Form
    {
        private DataTable dtRecords;
        private List<string> lstTabNames;
        private readonly int FORM_MIN_HEIGHT;

        public GenExamForm()
        {
            InitializeComponent();

            InitRecordTable();
            InitTabNamesList();
            InitDataGrid();
            InitTabView();
            InitOKCancelPanel();

            rtbFinalResult.Text = Patient.Instance.PatientData.GenExam.GetString();
            FORM_MIN_HEIGHT = Height;

            CenterToScreen();
        }

        private void InitOKCancelPanel()
        {
            btOK.Left = (pnOKCancel.Width - btOK.Width) / 2;

#if !DEBUG
            btTestData.Visible = false;
#endif
        }

        private void InitTabView()
        {
            tabView.Resized += TabView_Resized;
            tabView.RecordAdded += TabView_RecordAdded;
        }

        private void TabView_Resized(object sender, TabViewEventArgs e)
        {
            int new_height = this.Height + e.HeightDiff;

            new_height = new_height >= FORM_MIN_HEIGHT ? new_height : FORM_MIN_HEIGHT;

            this.Height = new_height;

            rtbFinalResult.ScrollBars = RichTextBoxScrollBars.None;
            rtbFinalResult.ScrollBars = RichTextBoxScrollBars.Vertical;

            rtbFinalResult.Invalidate();
        }
        private void TabView_RecordAdded(object sender, EventArgs e)
        {
            BuildRecordsTable();
            rtbFinalResult.Text = Patient.Instance.PatientData.GenExam.GetString();
        }

        private void InitTabNamesList()
        {
            lstTabNames = GenExamTabsFactory.GetTabNames();
        }

        private void InitRecordTable()
        {
            dtRecords = new DataTable();

            dtRecords.Columns.Add("date", typeof(string));
            dtRecords.Columns.Add("type", typeof(UInt32));
            dtRecords.Columns.Add("id", typeof(UInt32));

            dtRecords.DefaultView.Sort = "type ASC, id ASC";
        }
        private void InitDataGrid()
        {
            BuildRecordsTable();
            dgvRecords.AlternatingRowsDefaultCellStyle.BackColor = ColorTranslator.FromHtml("#EFEFEF");
            dgvRecords.DataSource = new BindingSource(dtRecords.DefaultView, "");
        }

        private void dgvRecords_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            DataGridView dgv = (DataGridView)sender;
            if (dgv.Columns[e.ColumnIndex].Name == "type" && e.RowIndex >= 0 && e.Value is UInt32)
            {
                e.Value = lstTabNames[int.Parse(e.Value.ToString())];
                e.FormattingApplied = true;
            }
        }

        private void dgvRecords_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
            dgvRecords.Columns["id"].Visible = false;
            dgvRecords.Columns["date"].Width = 150;

            dgvRecords.CurrentCell = null;
            btRemoveRecord.Enabled = false;
        }

        private void btRemoveRecord_Click(object sender, EventArgs e)
        {
            if (dgvRecords.CurrentCell != null)
            {
                DialogResult result = MessageBox.Show("Подтверджение удаления", "", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2);
                if (result == DialogResult.OK)
                {
                    int id = dgvRecords.CurrentRow.Index;

                    GenExamType type = (GenExamType)int.Parse(dgvRecords.Rows[id].Cells["type"].Value.ToString());
                    int record_id = int.Parse(dgvRecords.Rows[id].Cells["id"].Value.ToString());

                    Patient.Instance.PatientData.GenExam.RemoveRecord(type, record_id);
                    dgvRecords.Rows.Remove(dgvRecords.CurrentRow);

                    BuildRecordsTable();

                    rtbFinalResult.Text = Patient.Instance.PatientData.GenExam.GetString();
                }
            }
        }
        private void BuildRecordsTable()
        {
            dtRecords.Clear();

            GenExamResult ge = Patient.Instance.PatientData.GenExam;

            Array enums = typeof(GenExamType).GetEnumValues();
            foreach (GenExamType type in enums)
            {
                List<GenExamRecord> lst = ge[type];

                for (int i = 0; i < lst.Count; i++)
                    dtRecords.Rows.Add(Utility.DateToShortString(lst[i].Date.Value), (int)type, i);
            }

            dtRecords.AcceptChanges();
        }
        private void btTestData_Click(object sender, EventArgs e)
        {
            tabView.FillTestDataAllTabs();
        }
        private void dgvRecords_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            btRemoveRecord.Enabled = true;
        }

        private void btOK_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
            Close();
        }
    }
}
