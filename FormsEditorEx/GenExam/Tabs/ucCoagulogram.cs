﻿using System;
using FormsEditorEx.GenExam.Records;

namespace FormsEditorEx.GenExam.Tabs
{
    public partial class ucCoagulogram : ucExamTabBase
    {
        public ucCoagulogram()
        {
            InitializeComponent();
            ResizePanel();

            tbCoagulogramDate.Validating += DateTextBox_Validating;
            tbCoagulogramDate.KeyPress += DateTextBox_KeyPress;

            tbProthrombinTime.KeyDown += TextBox_KeyDown;
            tbProthrombin.KeyDown += TextBox_KeyDown;
            tbMNO.KeyDown += TextBox_KeyDown;
            tbA4TV.KeyDown += TextBox_KeyDown;
            tbFibrinogen.KeyDown += TextBox_KeyDown;
            tbThrombinTime.KeyDown += TextBox_KeyDown;
            tbDdimer.KeyDown += TextBox_KeyDown;
        }
        private void ResizePanel()
        {
            pnContent.Location = ClientRectangle.Location;
            ClientSize = pnContent.Size;
        }
        public override bool AddExamRecord()
        {
            bool res = AddRecord(GenExamType.Coagulogram, FieldsToRecord());

            if (res)
                Clear();

            return res;
        }
        public override void FillTestData()
        {
            tbCoagulogramDate.Text = "21.03.2020";
            tbProthrombinTime.Text = "11,5";
            tbProthrombin.Text = "103";
            tbMNO.Text = "0,98";
            tbA4TV.Text = "33,1";
            tbFibrinogen.Text = "3,8";
            tbThrombinTime.Text = "12,9";
            tbDdimer.Text = "173";
        }
        private void Clear()
        {
            tbCoagulogramDate.Clear();
            tbProthrombinTime.Clear();
            tbProthrombin.Clear();
            tbMNO.Clear();
            tbA4TV.Clear();
            tbFibrinogen.Clear();
            tbThrombinTime.Clear();
            tbDdimer.Clear();
        }
        private CoagulogramRecord FieldsToRecord()
        {
            if (tbCoagulogramDate.MaskCompleted && DateTime.TryParse(tbCoagulogramDate.Text, out DateTime date))
            {
                return new CoagulogramRecord()
                {
                    Date = date,
                    ProthrombinTime = !string.IsNullOrWhiteSpace(tbProthrombinTime.Text) ? tbProthrombinTime.Text : null,
                    Prothrombin = !string.IsNullOrWhiteSpace(tbProthrombin.Text) ? tbProthrombin.Text : null,
                    MNO = !string.IsNullOrWhiteSpace(tbMNO.Text) ? tbMNO.Text : null,
                    A4TV = !string.IsNullOrWhiteSpace(tbA4TV.Text) ? tbA4TV.Text : null,
                    Fibrinogen = !string.IsNullOrWhiteSpace(tbFibrinogen.Text) ? tbFibrinogen.Text : null,
                    ThrombinTime = !string.IsNullOrWhiteSpace(tbThrombinTime.Text) ? tbThrombinTime.Text : null,
                    Ddimer = !string.IsNullOrWhiteSpace(tbDdimer.Text) ? tbDdimer.Text : null
                };
            }
            else return null;
        }
    }
}
