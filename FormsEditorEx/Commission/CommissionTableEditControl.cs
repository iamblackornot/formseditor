﻿using System.Data;
using System.Windows.Forms;

namespace FormsEditorEx.Commission
{
    public class CommissionTableEditControl : DatatableEditControl
    {
        public CommissionTableEditControl(bool isReadOnly = true) : base(Patient.Instance.PatientData.Commissions.Data, isReadOnly)
        {
                
        }
        protected override void OnCellFormatting(DataGridView dgv, DataGridViewCellFormattingEventArgs e)
        {
            if (dgv.Columns[e.ColumnIndex].Name == "type" && e.RowIndex >= 0 && e.Value is int)
            {
                e.Value = Static.CommissionTypes[int.Parse(e.Value.ToString())];
                e.FormattingApplied = true;
            }
        }
        protected override void FillTestData()
        {
            Patient.Instance.PatientData.Commissions.FillTestData();
        }
        protected override void OnDataBindingComplete(DataGridView dgv)
        {
            dgv.Columns[0].Width = 70;
            dgv.Columns[0].HeaderText = "№";
            dgv.Columns[1].HeaderText = "Дата";
            dgv.Columns[2].Width = 170;
            dgv.Columns[2].HeaderText = "Вид";
            dgv.Columns[3].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            dgv.Columns[3].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            dgv.Columns[3].HeaderText = "Решение";
            dgv.Columns[4].Visible = false;

            foreach (DataGridViewColumn col in dgv.Columns)
                col.SortMode = DataGridViewColumnSortMode.NotSortable;

            if (dgv.Rows.Count > 0)
            {
                SetChangeRemoveButtonsAvailability(true);
            }
            else
            {
                SetChangeRemoveButtonsAvailability(false);
            }
        }
        protected override void OpenRowEditor(DataRow row, bool isReadOnly)
        {
            using (CommissionEditForm cef = new CommissionEditForm(row["num"].ToString(), row["date"].ToString(), row["decision"].ToString(), int.Parse(row["type"].ToString()), isReadOnly))
            {
                if (cef.ShowDialog() == DialogResult.OK)
                {
                    row["num"] = cef.Num;
                    row["date"] = cef.Date;
                    row["decision"] = cef.Decision;
                    row["type"] = cef.Type;

                    SetChangedState();
                }
            }
        }
        protected override void OpenAddRowDialog(DataTable dt)
        {
            using (CommissionEditForm caf = new CommissionEditForm(false))
            {
                if (caf.ShowDialog() == DialogResult.OK)
                {
                    Patient p = Patient.Instance;
                    dt.Rows.Add(caf.Num, caf.Date, caf.Type, caf.Decision, p.Stage.HasValue && p.Stage == (int)MainFrameState.DischargeSummary);
                    SetChangedState();
                }
            }
        }
    }
}
