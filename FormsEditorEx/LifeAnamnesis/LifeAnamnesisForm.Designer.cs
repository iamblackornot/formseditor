﻿namespace FormsEditorEx
{
    partial class LifeAnamnesisForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.rtbStool = new SharedLibrary.PlainRichTextBoxExt();
            this.label31 = new System.Windows.Forms.Label();
            this.rtbUrination = new SharedLibrary.PlainRichTextBoxExt();
            this.label32 = new System.Windows.Forms.Label();
            this.rtbLiver = new SharedLibrary.PlainRichTextBoxExt();
            this.label43 = new System.Windows.Forms.Label();
            this.rtbStomach = new SharedLibrary.PlainRichTextBoxExt();
            this.label46 = new System.Windows.Forms.Label();
            this.rtbPulse = new SharedLibrary.PlainRichTextBoxExt();
            this.label25 = new System.Windows.Forms.Label();
            this.rtbHeartSounds = new SharedLibrary.PlainRichTextBoxExt();
            this.label24 = new System.Windows.Forms.Label();
            this.rtbPercussion = new SharedLibrary.PlainRichTextBoxExt();
            this.label22 = new System.Windows.Forms.Label();
            this.rtbLungsResp = new SharedLibrary.PlainRichTextBoxExt();
            this.label21 = new System.Windows.Forms.Label();
            this.rtbDyspnea = new SharedLibrary.PlainRichTextBoxExt();
            this.label20 = new System.Windows.Forms.Label();
            this.rtbChest = new SharedLibrary.PlainRichTextBoxExt();
            this.label19 = new System.Windows.Forms.Label();
            this.rtbThyroid = new SharedLibrary.PlainRichTextBoxExt();
            this.label18 = new System.Windows.Forms.Label();
            this.rtbMammary = new SharedLibrary.PlainRichTextBoxExt();
            this.label17 = new System.Windows.Forms.Label();
            this.rtbSkin = new SharedLibrary.PlainRichTextBoxExt();
            this.rtbLymphNodes = new SharedLibrary.PlainRichTextBoxExt();
            this.label16 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.rtbProfHarm = new SharedLibrary.PlainRichTextBoxExt();
            this.label37 = new System.Windows.Forms.Label();
            this.rtbTransfusion = new SharedLibrary.PlainRichTextBoxExt();
            this.label7 = new System.Windows.Forms.Label();
            this.rtbAllergy = new SharedLibrary.PlainRichTextBoxExt();
            this.label9 = new System.Windows.Forms.Label();
            this.rtbGenetics = new SharedLibrary.PlainRichTextBoxExt();
            this.label12 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.tbBloodPressureA = new SharedLibrary.TextBoxEx();
            this.label30 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.tbHeartRate = new SharedLibrary.TextBoxEx();
            this.tbBloodPressureD = new SharedLibrary.TextBoxEx();
            this.btOK = new System.Windows.Forms.Button();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.pnHeightWeight = new System.Windows.Forms.Panel();
            this.tbWeight = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.tbHeight = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.pnTemperature = new System.Windows.Forms.Panel();
            this.tbTemperature = new SharedLibrary.TextBoxEx();
            this.label5 = new System.Windows.Forms.Label();
            this.pnPressureBreathe = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.rtbPastIllnesses = new SharedLibrary.PlainRichTextBoxExt();
            this.label2 = new System.Windows.Forms.Label();
            this.rtbTuberculosis = new SharedLibrary.PlainRichTextBoxExt();
            this.panel2 = new System.Windows.Forms.Panel();
            this.tbCiggsPerDay = new SharedLibrary.TextBoxEx();
            this.cbSmokes = new SharedLibrary.FocusableCheckBox();
            this.label3 = new System.Windows.Forms.Label();
            this.pnButtons = new System.Windows.Forms.Panel();
            this.btTestData = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.flowLayoutPanel1.SuspendLayout();
            this.pnHeightWeight.SuspendLayout();
            this.pnTemperature.SuspendLayout();
            this.pnPressureBreathe.SuspendLayout();
            this.panel2.SuspendLayout();
            this.pnButtons.SuspendLayout();
            this.SuspendLayout();
            // 
            // rtbStool
            // 
            this.rtbStool.Cue = "регулярный, оформленный.";
            this.rtbStool.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.rtbStool.Location = new System.Drawing.Point(16, 1416);
            this.rtbStool.Margin = new System.Windows.Forms.Padding(0, 3, 0, 3);
            this.rtbStool.Name = "rtbStool";
            this.rtbStool.PlainTextMode = true;
            this.rtbStool.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.Vertical;
            this.rtbStool.ScrollParentOnMouseWheel = true;
            this.rtbStool.ShowSelectionMargin = true;
            this.rtbStool.Size = new System.Drawing.Size(366, 66);
            this.rtbStool.TabIndex = 14;
            this.rtbStool.Text = "";
            this.rtbStool.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextBox_KeyDown);
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label31.Location = new System.Drawing.Point(19, 1390);
            this.label31.Name = "label31";
            this.label31.Padding = new System.Windows.Forms.Padding(5, 5, 0, 0);
            this.label31.Size = new System.Drawing.Size(42, 23);
            this.label31.TabIndex = 77;
            this.label31.Text = "Стул";
            // 
            // rtbUrination
            // 
            this.rtbUrination.Cue = "свободное, неучащенное, безболезненное.";
            this.rtbUrination.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.rtbUrination.Location = new System.Drawing.Point(16, 1321);
            this.rtbUrination.Margin = new System.Windows.Forms.Padding(0, 3, 0, 3);
            this.rtbUrination.Name = "rtbUrination";
            this.rtbUrination.PlainTextMode = true;
            this.rtbUrination.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.Vertical;
            this.rtbUrination.ScrollParentOnMouseWheel = true;
            this.rtbUrination.ShowSelectionMargin = true;
            this.rtbUrination.Size = new System.Drawing.Size(366, 66);
            this.rtbUrination.TabIndex = 13;
            this.rtbUrination.Text = "";
            this.rtbUrination.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextBox_KeyDown);
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label32.Location = new System.Drawing.Point(19, 1295);
            this.label32.Name = "label32";
            this.label32.Padding = new System.Windows.Forms.Padding(5, 5, 0, 0);
            this.label32.Size = new System.Drawing.Size(121, 23);
            this.label32.TabIndex = 75;
            this.label32.Text = "Мочеиспускание";
            // 
            // rtbLiver
            // 
            this.rtbLiver.Cue = "не увеличена, безболезненна.";
            this.rtbLiver.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.rtbLiver.Location = new System.Drawing.Point(16, 1226);
            this.rtbLiver.Margin = new System.Windows.Forms.Padding(0, 3, 0, 3);
            this.rtbLiver.Name = "rtbLiver";
            this.rtbLiver.PlainTextMode = true;
            this.rtbLiver.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.Vertical;
            this.rtbLiver.ScrollParentOnMouseWheel = true;
            this.rtbLiver.ShowSelectionMargin = true;
            this.rtbLiver.Size = new System.Drawing.Size(366, 66);
            this.rtbLiver.TabIndex = 12;
            this.rtbLiver.Text = "";
            this.rtbLiver.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextBox_KeyDown);
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label43.Location = new System.Drawing.Point(19, 1200);
            this.label43.Name = "label43";
            this.label43.Padding = new System.Windows.Forms.Padding(5, 5, 0, 0);
            this.label43.Size = new System.Drawing.Size(60, 23);
            this.label43.TabIndex = 73;
            this.label43.Text = "Печень";
            // 
            // rtbStomach
            // 
            this.rtbStomach.Cue = "мягкий, безболезненный.";
            this.rtbStomach.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.rtbStomach.Location = new System.Drawing.Point(16, 1131);
            this.rtbStomach.Margin = new System.Windows.Forms.Padding(0, 3, 0, 3);
            this.rtbStomach.Name = "rtbStomach";
            this.rtbStomach.PlainTextMode = true;
            this.rtbStomach.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.Vertical;
            this.rtbStomach.ScrollParentOnMouseWheel = true;
            this.rtbStomach.ShowSelectionMargin = true;
            this.rtbStomach.Size = new System.Drawing.Size(366, 66);
            this.rtbStomach.TabIndex = 11;
            this.rtbStomach.Text = "";
            this.rtbStomach.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextBox_KeyDown);
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label46.Location = new System.Drawing.Point(19, 1105);
            this.label46.Name = "label46";
            this.label46.Padding = new System.Windows.Forms.Padding(5, 5, 0, 0);
            this.label46.Size = new System.Drawing.Size(54, 23);
            this.label46.TabIndex = 71;
            this.label46.Text = "Живот";
            // 
            // rtbPulse
            // 
            this.rtbPulse.Cue = "ритмичный, удовлетворительного наполнения и напряжения.";
            this.rtbPulse.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.rtbPulse.Location = new System.Drawing.Point(16, 1036);
            this.rtbPulse.Margin = new System.Windows.Forms.Padding(0, 3, 0, 3);
            this.rtbPulse.Name = "rtbPulse";
            this.rtbPulse.PlainTextMode = true;
            this.rtbPulse.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.Vertical;
            this.rtbPulse.ScrollParentOnMouseWheel = true;
            this.rtbPulse.ShowSelectionMargin = true;
            this.rtbPulse.Size = new System.Drawing.Size(366, 66);
            this.rtbPulse.TabIndex = 10;
            this.rtbPulse.Text = "";
            this.rtbPulse.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextBox_KeyDown);
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label25.Location = new System.Drawing.Point(19, 1010);
            this.label25.Name = "label25";
            this.label25.Padding = new System.Windows.Forms.Padding(5, 5, 0, 0);
            this.label25.Size = new System.Drawing.Size(50, 23);
            this.label25.TabIndex = 69;
            this.label25.Text = "Пульс";
            // 
            // rtbHeartSounds
            // 
            this.rtbHeartSounds.Cue = "ритмичные,  ясные. Шумы – нет.";
            this.rtbHeartSounds.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.rtbHeartSounds.Location = new System.Drawing.Point(16, 941);
            this.rtbHeartSounds.Margin = new System.Windows.Forms.Padding(0, 3, 0, 3);
            this.rtbHeartSounds.Name = "rtbHeartSounds";
            this.rtbHeartSounds.PlainTextMode = true;
            this.rtbHeartSounds.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.Vertical;
            this.rtbHeartSounds.ScrollParentOnMouseWheel = true;
            this.rtbHeartSounds.ShowSelectionMargin = true;
            this.rtbHeartSounds.Size = new System.Drawing.Size(366, 66);
            this.rtbHeartSounds.TabIndex = 9;
            this.rtbHeartSounds.Text = "";
            this.rtbHeartSounds.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextBox_KeyDown);
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label24.Location = new System.Drawing.Point(19, 915);
            this.label24.Name = "label24";
            this.label24.Padding = new System.Windows.Forms.Padding(5, 5, 0, 0);
            this.label24.Size = new System.Drawing.Size(93, 23);
            this.label24.TabIndex = 67;
            this.label24.Text = "Тоны сердца";
            // 
            // rtbPercussion
            // 
            this.rtbPercussion.Cue = "легочный звук.";
            this.rtbPercussion.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.rtbPercussion.Location = new System.Drawing.Point(16, 846);
            this.rtbPercussion.Margin = new System.Windows.Forms.Padding(0, 3, 0, 3);
            this.rtbPercussion.Name = "rtbPercussion";
            this.rtbPercussion.PlainTextMode = true;
            this.rtbPercussion.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.Vertical;
            this.rtbPercussion.ScrollParentOnMouseWheel = true;
            this.rtbPercussion.ShowSelectionMargin = true;
            this.rtbPercussion.Size = new System.Drawing.Size(366, 66);
            this.rtbPercussion.TabIndex = 8;
            this.rtbPercussion.Text = "";
            this.rtbPercussion.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextBox_KeyDown);
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label22.Location = new System.Drawing.Point(19, 820);
            this.label22.Name = "label22";
            this.label22.Padding = new System.Windows.Forms.Padding(5, 5, 0, 0);
            this.label22.Size = new System.Drawing.Size(90, 23);
            this.label22.TabIndex = 65;
            this.label22.Text = "Перкуторно";
            // 
            // rtbLungsResp
            // 
            this.rtbLungsResp.Cue = "везикулярное. Хрипов нет. ЧДД – 18 в 1 мин. ";
            this.rtbLungsResp.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.rtbLungsResp.Location = new System.Drawing.Point(16, 751);
            this.rtbLungsResp.Margin = new System.Windows.Forms.Padding(0, 3, 0, 3);
            this.rtbLungsResp.Name = "rtbLungsResp";
            this.rtbLungsResp.PlainTextMode = true;
            this.rtbLungsResp.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.Vertical;
            this.rtbLungsResp.ScrollParentOnMouseWheel = true;
            this.rtbLungsResp.ShowSelectionMargin = true;
            this.rtbLungsResp.Size = new System.Drawing.Size(366, 66);
            this.rtbLungsResp.TabIndex = 7;
            this.rtbLungsResp.Text = "";
            this.rtbLungsResp.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextBox_KeyDown);
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label21.Location = new System.Drawing.Point(19, 725);
            this.label21.Name = "label21";
            this.label21.Padding = new System.Windows.Forms.Padding(5, 5, 0, 0);
            this.label21.Size = new System.Drawing.Size(126, 23);
            this.label21.TabIndex = 63;
            this.label21.Text = "В легких дыхание";
            // 
            // rtbDyspnea
            // 
            this.rtbDyspnea.Cue = "нет.";
            this.rtbDyspnea.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.rtbDyspnea.Location = new System.Drawing.Point(16, 656);
            this.rtbDyspnea.Margin = new System.Windows.Forms.Padding(0, 3, 0, 3);
            this.rtbDyspnea.Name = "rtbDyspnea";
            this.rtbDyspnea.PlainTextMode = true;
            this.rtbDyspnea.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.Vertical;
            this.rtbDyspnea.ScrollParentOnMouseWheel = true;
            this.rtbDyspnea.ShowSelectionMargin = true;
            this.rtbDyspnea.Size = new System.Drawing.Size(366, 66);
            this.rtbDyspnea.TabIndex = 6;
            this.rtbDyspnea.Text = "";
            this.rtbDyspnea.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextBox_KeyDown);
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label20.Location = new System.Drawing.Point(19, 630);
            this.label20.Name = "label20";
            this.label20.Padding = new System.Windows.Forms.Padding(5, 5, 0, 0);
            this.label20.Size = new System.Drawing.Size(66, 23);
            this.label20.TabIndex = 61;
            this.label20.Text = "Одышка";
            // 
            // rtbChest
            // 
            this.rtbChest.Cue = "правильной формы, нормостенического типа. Пальпация её безболезненна.";
            this.rtbChest.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.rtbChest.Location = new System.Drawing.Point(16, 561);
            this.rtbChest.Margin = new System.Windows.Forms.Padding(0, 3, 0, 3);
            this.rtbChest.Name = "rtbChest";
            this.rtbChest.PlainTextMode = true;
            this.rtbChest.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.Vertical;
            this.rtbChest.ScrollParentOnMouseWheel = true;
            this.rtbChest.ShowSelectionMargin = true;
            this.rtbChest.Size = new System.Drawing.Size(366, 66);
            this.rtbChest.TabIndex = 5;
            this.rtbChest.Text = "";
            this.rtbChest.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextBox_KeyDown);
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label19.Location = new System.Drawing.Point(19, 535);
            this.label19.Name = "label19";
            this.label19.Padding = new System.Windows.Forms.Padding(5, 5, 0, 0);
            this.label19.Size = new System.Drawing.Size(108, 23);
            this.label19.TabIndex = 59;
            this.label19.Text = "Грудная клетка";
            // 
            // rtbThyroid
            // 
            this.rtbThyroid.Cue = "пальпаторно не увеличена, без узловых образований.";
            this.rtbThyroid.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.rtbThyroid.Location = new System.Drawing.Point(16, 466);
            this.rtbThyroid.Margin = new System.Windows.Forms.Padding(0, 3, 0, 3);
            this.rtbThyroid.Name = "rtbThyroid";
            this.rtbThyroid.PlainTextMode = true;
            this.rtbThyroid.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.Vertical;
            this.rtbThyroid.ScrollParentOnMouseWheel = true;
            this.rtbThyroid.ShowSelectionMargin = true;
            this.rtbThyroid.Size = new System.Drawing.Size(366, 66);
            this.rtbThyroid.TabIndex = 4;
            this.rtbThyroid.Text = "";
            this.rtbThyroid.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextBox_KeyDown);
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label18.Location = new System.Drawing.Point(19, 440);
            this.label18.Name = "label18";
            this.label18.Padding = new System.Windows.Forms.Padding(5, 5, 0, 0);
            this.label18.Size = new System.Drawing.Size(143, 23);
            this.label18.TabIndex = 57;
            this.label18.Text = "Щитовидная железа";
            // 
            // rtbMammary
            // 
            this.rtbMammary.Cue = "пальпаторно без узловых образований.";
            this.rtbMammary.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.rtbMammary.Location = new System.Drawing.Point(16, 371);
            this.rtbMammary.Margin = new System.Windows.Forms.Padding(0, 3, 0, 3);
            this.rtbMammary.Name = "rtbMammary";
            this.rtbMammary.PlainTextMode = true;
            this.rtbMammary.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.Vertical;
            this.rtbMammary.ScrollParentOnMouseWheel = true;
            this.rtbMammary.ShowSelectionMargin = true;
            this.rtbMammary.Size = new System.Drawing.Size(366, 66);
            this.rtbMammary.TabIndex = 3;
            this.rtbMammary.Text = "";
            this.rtbMammary.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextBox_KeyDown);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label17.Location = new System.Drawing.Point(19, 345);
            this.label17.Name = "label17";
            this.label17.Padding = new System.Windows.Forms.Padding(5, 5, 0, 0);
            this.label17.Size = new System.Drawing.Size(135, 23);
            this.label17.TabIndex = 55;
            this.label17.Text = "Молочные железы";
            // 
            // rtbSkin
            // 
            this.rtbSkin.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.rtbSkin.Cue = "нормальной окраски и влажности.";
            this.rtbSkin.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.rtbSkin.Location = new System.Drawing.Point(16, 181);
            this.rtbSkin.Margin = new System.Windows.Forms.Padding(0, 3, 0, 3);
            this.rtbSkin.Name = "rtbSkin";
            this.rtbSkin.PlainTextMode = true;
            this.rtbSkin.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.Vertical;
            this.rtbSkin.ScrollParentOnMouseWheel = true;
            this.rtbSkin.ShowSelectionMargin = true;
            this.rtbSkin.Size = new System.Drawing.Size(366, 66);
            this.rtbSkin.TabIndex = 1;
            this.rtbSkin.Text = "";
            this.rtbSkin.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextBox_KeyDown);
            // 
            // rtbLymphNodes
            // 
            this.rtbLymphNodes.Cue = "не пальпируются.";
            this.rtbLymphNodes.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.rtbLymphNodes.Location = new System.Drawing.Point(16, 276);
            this.rtbLymphNodes.Margin = new System.Windows.Forms.Padding(0, 3, 0, 3);
            this.rtbLymphNodes.Name = "rtbLymphNodes";
            this.rtbLymphNodes.PlainTextMode = true;
            this.rtbLymphNodes.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.Vertical;
            this.rtbLymphNodes.ScrollParentOnMouseWheel = true;
            this.rtbLymphNodes.ShowSelectionMargin = true;
            this.rtbLymphNodes.Size = new System.Drawing.Size(366, 66);
            this.rtbLymphNodes.TabIndex = 2;
            this.rtbLymphNodes.Text = "";
            this.rtbLymphNodes.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextBox_KeyDown);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label16.Location = new System.Drawing.Point(19, 250);
            this.label16.Name = "label16";
            this.label16.Padding = new System.Windows.Forms.Padding(5, 5, 0, 0);
            this.label16.Size = new System.Drawing.Size(206, 23);
            this.label16.TabIndex = 50;
            this.label16.Text = "Периферические лимфоузлы  ";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label15.Location = new System.Drawing.Point(19, 150);
            this.label15.Name = "label15";
            this.label15.Padding = new System.Windows.Forms.Padding(5, 10, 0, 0);
            this.label15.Size = new System.Drawing.Size(124, 28);
            this.label15.TabIndex = 48;
            this.label15.Text = "Кожные покровы";
            // 
            // rtbProfHarm
            // 
            this.rtbProfHarm.Cue = "нет.";
            this.rtbProfHarm.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.rtbProfHarm.Location = new System.Drawing.Point(16, 1796);
            this.rtbProfHarm.Margin = new System.Windows.Forms.Padding(0, 3, 0, 3);
            this.rtbProfHarm.Name = "rtbProfHarm";
            this.rtbProfHarm.PlainTextMode = true;
            this.rtbProfHarm.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.Vertical;
            this.rtbProfHarm.ScrollParentOnMouseWheel = true;
            this.rtbProfHarm.ShowSelectionMargin = true;
            this.rtbProfHarm.Size = new System.Drawing.Size(366, 66);
            this.rtbProfHarm.TabIndex = 18;
            this.rtbProfHarm.Text = "";
            this.rtbProfHarm.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextBox_KeyDown);
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label37.Location = new System.Drawing.Point(19, 1770);
            this.label37.Name = "label37";
            this.label37.Padding = new System.Windows.Forms.Padding(5, 5, 0, 0);
            this.label37.Size = new System.Drawing.Size(209, 23);
            this.label37.TabIndex = 86;
            this.label37.Text = "Профессиональные вредности";
            // 
            // rtbTransfusion
            // 
            this.rtbTransfusion.Cue = "не было.";
            this.rtbTransfusion.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.rtbTransfusion.Location = new System.Drawing.Point(16, 1701);
            this.rtbTransfusion.Margin = new System.Windows.Forms.Padding(0, 3, 0, 3);
            this.rtbTransfusion.Name = "rtbTransfusion";
            this.rtbTransfusion.PlainTextMode = true;
            this.rtbTransfusion.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.Vertical;
            this.rtbTransfusion.ScrollParentOnMouseWheel = true;
            this.rtbTransfusion.ShowSelectionMargin = true;
            this.rtbTransfusion.Size = new System.Drawing.Size(366, 66);
            this.rtbTransfusion.TabIndex = 17;
            this.rtbTransfusion.Text = "";
            this.rtbTransfusion.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextBox_KeyDown);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label7.Location = new System.Drawing.Point(19, 1675);
            this.label7.Name = "label7";
            this.label7.Padding = new System.Windows.Forms.Padding(5, 5, 0, 0);
            this.label7.Size = new System.Drawing.Size(117, 23);
            this.label7.TabIndex = 85;
            this.label7.Text = "Гемотрансфузии";
            // 
            // rtbAllergy
            // 
            this.rtbAllergy.Cue = "отрицает.";
            this.rtbAllergy.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.rtbAllergy.Location = new System.Drawing.Point(16, 1606);
            this.rtbAllergy.Margin = new System.Windows.Forms.Padding(0, 3, 0, 3);
            this.rtbAllergy.Name = "rtbAllergy";
            this.rtbAllergy.PlainTextMode = true;
            this.rtbAllergy.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.Vertical;
            this.rtbAllergy.ScrollParentOnMouseWheel = true;
            this.rtbAllergy.ShowSelectionMargin = true;
            this.rtbAllergy.Size = new System.Drawing.Size(366, 66);
            this.rtbAllergy.TabIndex = 16;
            this.rtbAllergy.Text = "";
            this.rtbAllergy.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextBox_KeyDown);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label9.Location = new System.Drawing.Point(19, 1580);
            this.label9.Name = "label9";
            this.label9.Padding = new System.Windows.Forms.Padding(5, 5, 0, 0);
            this.label9.Size = new System.Drawing.Size(74, 23);
            this.label9.TabIndex = 84;
            this.label9.Text = "Аллергия";
            // 
            // rtbGenetics
            // 
            this.rtbGenetics.Cue = "не отягощена.";
            this.rtbGenetics.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.rtbGenetics.Location = new System.Drawing.Point(16, 1511);
            this.rtbGenetics.Margin = new System.Windows.Forms.Padding(0, 3, 0, 3);
            this.rtbGenetics.Name = "rtbGenetics";
            this.rtbGenetics.PlainTextMode = true;
            this.rtbGenetics.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.Vertical;
            this.rtbGenetics.ScrollParentOnMouseWheel = true;
            this.rtbGenetics.ShowSelectionMargin = true;
            this.rtbGenetics.Size = new System.Drawing.Size(366, 66);
            this.rtbGenetics.TabIndex = 15;
            this.rtbGenetics.Text = "";
            this.rtbGenetics.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextBox_KeyDown);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label12.Location = new System.Drawing.Point(19, 1485);
            this.label12.Name = "label12";
            this.label12.Padding = new System.Windows.Forms.Padding(5, 5, 0, 0);
            this.label12.Size = new System.Drawing.Size(129, 23);
            this.label12.TabIndex = 83;
            this.label12.Text = "Наследственность";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label26.Location = new System.Drawing.Point(65, 15);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(33, 18);
            this.label26.TabIndex = 91;
            this.label26.Text = "А/Д";
            // 
            // tbBloodPressureA
            // 
            this.tbBloodPressureA.Cue = "130";
            this.tbBloodPressureA.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.tbBloodPressureA.ForeColor = System.Drawing.SystemColors.WindowText;
            this.tbBloodPressureA.Location = new System.Drawing.Point(103, 12);
            this.tbBloodPressureA.Name = "tbBloodPressureA";
            this.tbBloodPressureA.Size = new System.Drawing.Size(36, 26);
            this.tbBloodPressureA.TabIndex = 19;
            this.tbBloodPressureA.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.tbBloodPressureA.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextBox_KeyDown);
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label30.Location = new System.Drawing.Point(218, 15);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(32, 18);
            this.label30.TabIndex = 93;
            this.label30.Text = "ЧСС";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label29.Location = new System.Drawing.Point(139, 18);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(12, 15);
            this.label29.TabIndex = 92;
            this.label29.Text = "/";
            // 
            // tbHeartRate
            // 
            this.tbHeartRate.Cue = "72";
            this.tbHeartRate.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.tbHeartRate.ForeColor = System.Drawing.SystemColors.WindowText;
            this.tbHeartRate.Location = new System.Drawing.Point(256, 12);
            this.tbHeartRate.Name = "tbHeartRate";
            this.tbHeartRate.Size = new System.Drawing.Size(36, 26);
            this.tbHeartRate.TabIndex = 21;
            this.tbHeartRate.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.tbHeartRate.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextBox_KeyDown);
            // 
            // tbBloodPressureD
            // 
            this.tbBloodPressureD.Cue = "80";
            this.tbBloodPressureD.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.tbBloodPressureD.Location = new System.Drawing.Point(150, 12);
            this.tbBloodPressureD.Name = "tbBloodPressureD";
            this.tbBloodPressureD.Size = new System.Drawing.Size(36, 26);
            this.tbBloodPressureD.TabIndex = 20;
            this.tbBloodPressureD.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.tbBloodPressureD.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextBox_KeyDown);
            // 
            // btOK
            // 
            this.btOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btOK.Location = new System.Drawing.Point(313, 18);
            this.btOK.Name = "btOK";
            this.btOK.Size = new System.Drawing.Size(86, 32);
            this.btOK.TabIndex = 23;
            this.btOK.Text = "OK";
            this.btOK.UseVisualStyleBackColor = true;
            this.btOK.Click += new System.EventHandler(this.btOK_Click);
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.AutoScroll = true;
            this.flowLayoutPanel1.Controls.Add(this.pnHeightWeight);
            this.flowLayoutPanel1.Controls.Add(this.pnTemperature);
            this.flowLayoutPanel1.Controls.Add(this.pnPressureBreathe);
            this.flowLayoutPanel1.Controls.Add(this.label15);
            this.flowLayoutPanel1.Controls.Add(this.rtbSkin);
            this.flowLayoutPanel1.Controls.Add(this.label16);
            this.flowLayoutPanel1.Controls.Add(this.rtbLymphNodes);
            this.flowLayoutPanel1.Controls.Add(this.label17);
            this.flowLayoutPanel1.Controls.Add(this.rtbMammary);
            this.flowLayoutPanel1.Controls.Add(this.label18);
            this.flowLayoutPanel1.Controls.Add(this.rtbThyroid);
            this.flowLayoutPanel1.Controls.Add(this.label19);
            this.flowLayoutPanel1.Controls.Add(this.rtbChest);
            this.flowLayoutPanel1.Controls.Add(this.label20);
            this.flowLayoutPanel1.Controls.Add(this.rtbDyspnea);
            this.flowLayoutPanel1.Controls.Add(this.label21);
            this.flowLayoutPanel1.Controls.Add(this.rtbLungsResp);
            this.flowLayoutPanel1.Controls.Add(this.label22);
            this.flowLayoutPanel1.Controls.Add(this.rtbPercussion);
            this.flowLayoutPanel1.Controls.Add(this.label24);
            this.flowLayoutPanel1.Controls.Add(this.rtbHeartSounds);
            this.flowLayoutPanel1.Controls.Add(this.label25);
            this.flowLayoutPanel1.Controls.Add(this.rtbPulse);
            this.flowLayoutPanel1.Controls.Add(this.label46);
            this.flowLayoutPanel1.Controls.Add(this.rtbStomach);
            this.flowLayoutPanel1.Controls.Add(this.label43);
            this.flowLayoutPanel1.Controls.Add(this.rtbLiver);
            this.flowLayoutPanel1.Controls.Add(this.label32);
            this.flowLayoutPanel1.Controls.Add(this.rtbUrination);
            this.flowLayoutPanel1.Controls.Add(this.label31);
            this.flowLayoutPanel1.Controls.Add(this.rtbStool);
            this.flowLayoutPanel1.Controls.Add(this.label12);
            this.flowLayoutPanel1.Controls.Add(this.rtbGenetics);
            this.flowLayoutPanel1.Controls.Add(this.label9);
            this.flowLayoutPanel1.Controls.Add(this.rtbAllergy);
            this.flowLayoutPanel1.Controls.Add(this.label7);
            this.flowLayoutPanel1.Controls.Add(this.rtbTransfusion);
            this.flowLayoutPanel1.Controls.Add(this.label37);
            this.flowLayoutPanel1.Controls.Add(this.rtbProfHarm);
            this.flowLayoutPanel1.Controls.Add(this.label1);
            this.flowLayoutPanel1.Controls.Add(this.rtbPastIllnesses);
            this.flowLayoutPanel1.Controls.Add(this.label2);
            this.flowLayoutPanel1.Controls.Add(this.rtbTuberculosis);
            this.flowLayoutPanel1.Controls.Add(this.panel2);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.flowLayoutPanel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.flowLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.flowLayoutPanel1.Margin = new System.Windows.Forms.Padding(0);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Padding = new System.Windows.Forms.Padding(16, 0, 16, 16);
            this.flowLayoutPanel1.Size = new System.Drawing.Size(415, 593);
            this.flowLayoutPanel1.TabIndex = 1;
            this.flowLayoutPanel1.WrapContents = false;
            // 
            // pnHeightWeight
            // 
            this.pnHeightWeight.Controls.Add(this.label10);
            this.pnHeightWeight.Controls.Add(this.label8);
            this.pnHeightWeight.Controls.Add(this.tbWeight);
            this.pnHeightWeight.Controls.Add(this.label6);
            this.pnHeightWeight.Controls.Add(this.tbHeight);
            this.pnHeightWeight.Controls.Add(this.label4);
            this.pnHeightWeight.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.pnHeightWeight.Location = new System.Drawing.Point(16, 0);
            this.pnHeightWeight.Margin = new System.Windows.Forms.Padding(0);
            this.pnHeightWeight.Name = "pnHeightWeight";
            this.pnHeightWeight.Size = new System.Drawing.Size(366, 50);
            this.pnHeightWeight.TabIndex = 98;
            // 
            // tbWeight
            // 
            this.tbWeight.Location = new System.Drawing.Point(215, 12);
            this.tbWeight.Name = "tbWeight";
            this.tbWeight.Size = new System.Drawing.Size(50, 26);
            this.tbWeight.TabIndex = 96;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label6.Location = new System.Drawing.Point(179, 15);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(30, 18);
            this.label6.TabIndex = 95;
            this.label6.Text = "Вес";
            // 
            // tbHeight
            // 
            this.tbHeight.Location = new System.Drawing.Point(75, 12);
            this.tbHeight.Name = "tbHeight";
            this.tbHeight.Size = new System.Drawing.Size(50, 26);
            this.tbHeight.TabIndex = 94;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label4.Location = new System.Drawing.Point(33, 15);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(36, 18);
            this.label4.TabIndex = 93;
            this.label4.Text = "Рост";
            // 
            // pnTemperature
            // 
            this.pnTemperature.Controls.Add(this.label11);
            this.pnTemperature.Controls.Add(this.tbTemperature);
            this.pnTemperature.Controls.Add(this.label5);
            this.pnTemperature.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.pnTemperature.Location = new System.Drawing.Point(16, 50);
            this.pnTemperature.Margin = new System.Windows.Forms.Padding(0);
            this.pnTemperature.Name = "pnTemperature";
            this.pnTemperature.Size = new System.Drawing.Size(366, 50);
            this.pnTemperature.TabIndex = 97;
            // 
            // tbTemperature
            // 
            this.tbTemperature.Cue = "36.6";
            this.tbTemperature.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.tbTemperature.ForeColor = System.Drawing.SystemColors.WindowText;
            this.tbTemperature.Location = new System.Drawing.Point(215, 12);
            this.tbTemperature.Name = "tbTemperature";
            this.tbTemperature.Size = new System.Drawing.Size(50, 26);
            this.tbTemperature.TabIndex = 21;
            this.tbTemperature.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label5.Location = new System.Drawing.Point(114, 15);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(95, 18);
            this.label5.TabIndex = 93;
            this.label5.Text = "Термометрия";
            // 
            // pnPressureBreathe
            // 
            this.pnPressureBreathe.Controls.Add(this.tbBloodPressureD);
            this.pnPressureBreathe.Controls.Add(this.tbHeartRate);
            this.pnPressureBreathe.Controls.Add(this.label29);
            this.pnPressureBreathe.Controls.Add(this.label30);
            this.pnPressureBreathe.Controls.Add(this.label26);
            this.pnPressureBreathe.Controls.Add(this.tbBloodPressureA);
            this.pnPressureBreathe.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.pnPressureBreathe.Location = new System.Drawing.Point(16, 100);
            this.pnPressureBreathe.Margin = new System.Windows.Forms.Padding(0);
            this.pnPressureBreathe.Name = "pnPressureBreathe";
            this.pnPressureBreathe.Size = new System.Drawing.Size(366, 50);
            this.pnPressureBreathe.TabIndex = 96;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(19, 1865);
            this.label1.Name = "label1";
            this.label1.Padding = new System.Windows.Forms.Padding(5, 5, 0, 0);
            this.label1.Size = new System.Drawing.Size(195, 23);
            this.label1.TabIndex = 97;
            this.label1.Text = "Перенесенные заболевания";
            // 
            // rtbPastIllnesses
            // 
            this.rtbPastIllnesses.Cue = "нет.";
            this.rtbPastIllnesses.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.rtbPastIllnesses.Location = new System.Drawing.Point(16, 1891);
            this.rtbPastIllnesses.Margin = new System.Windows.Forms.Padding(0, 3, 0, 3);
            this.rtbPastIllnesses.Name = "rtbPastIllnesses";
            this.rtbPastIllnesses.PlainTextMode = true;
            this.rtbPastIllnesses.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.Vertical;
            this.rtbPastIllnesses.ScrollParentOnMouseWheel = true;
            this.rtbPastIllnesses.ShowSelectionMargin = true;
            this.rtbPastIllnesses.Size = new System.Drawing.Size(366, 66);
            this.rtbPastIllnesses.TabIndex = 98;
            this.rtbPastIllnesses.Text = "";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.Location = new System.Drawing.Point(19, 1960);
            this.label2.Name = "label2";
            this.label2.Padding = new System.Windows.Forms.Padding(5, 5, 0, 0);
            this.label2.Size = new System.Drawing.Size(86, 23);
            this.label2.TabIndex = 100;
            this.label2.Text = "Туберкулез";
            // 
            // rtbTuberculosis
            // 
            this.rtbTuberculosis.Cue = "нет.";
            this.rtbTuberculosis.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.rtbTuberculosis.Location = new System.Drawing.Point(16, 1986);
            this.rtbTuberculosis.Margin = new System.Windows.Forms.Padding(0, 3, 0, 3);
            this.rtbTuberculosis.Name = "rtbTuberculosis";
            this.rtbTuberculosis.PlainTextMode = true;
            this.rtbTuberculosis.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.Vertical;
            this.rtbTuberculosis.ScrollParentOnMouseWheel = true;
            this.rtbTuberculosis.ShowSelectionMargin = true;
            this.rtbTuberculosis.Size = new System.Drawing.Size(366, 66);
            this.rtbTuberculosis.TabIndex = 101;
            this.rtbTuberculosis.Text = "";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.tbCiggsPerDay);
            this.panel2.Controls.Add(this.cbSmokes);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.panel2.Location = new System.Drawing.Point(16, 2055);
            this.panel2.Margin = new System.Windows.Forms.Padding(0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(366, 50);
            this.panel2.TabIndex = 99;
            // 
            // tbCiggsPerDay
            // 
            this.tbCiggsPerDay.Cue = "-";
            this.tbCiggsPerDay.Enabled = false;
            this.tbCiggsPerDay.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.tbCiggsPerDay.ForeColor = System.Drawing.SystemColors.WindowText;
            this.tbCiggsPerDay.Location = new System.Drawing.Point(256, 12);
            this.tbCiggsPerDay.Name = "tbCiggsPerDay";
            this.tbCiggsPerDay.Size = new System.Drawing.Size(36, 26);
            this.tbCiggsPerDay.TabIndex = 21;
            this.tbCiggsPerDay.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // cbSmokes
            // 
            this.cbSmokes.AutoSize = true;
            this.cbSmokes.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.cbSmokes.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.cbSmokes.Location = new System.Drawing.Point(68, 14);
            this.cbSmokes.Name = "cbSmokes";
            this.cbSmokes.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.cbSmokes.Size = new System.Drawing.Size(64, 22);
            this.cbSmokes.TabIndex = 22;
            this.cbSmokes.Text = "Курит";
            this.cbSmokes.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.cbSmokes.UseVisualStyleBackColor = true;
            this.cbSmokes.CheckedChanged += new System.EventHandler(this.cbSmokes_CheckedChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.Location = new System.Drawing.Point(148, 15);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(102, 18);
            this.label3.TabIndex = 93;
            this.label3.Text = "Сигарет в день";
            // 
            // pnButtons
            // 
            this.pnButtons.AutoSize = true;
            this.pnButtons.Controls.Add(this.btTestData);
            this.pnButtons.Controls.Add(this.btOK);
            this.pnButtons.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnButtons.Location = new System.Drawing.Point(0, 593);
            this.pnButtons.Margin = new System.Windows.Forms.Padding(0);
            this.pnButtons.Name = "pnButtons";
            this.pnButtons.Padding = new System.Windows.Forms.Padding(0, 0, 16, 16);
            this.pnButtons.Size = new System.Drawing.Size(415, 66);
            this.pnButtons.TabIndex = 2;
            // 
            // btTestData
            // 
            this.btTestData.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btTestData.Location = new System.Drawing.Point(16, 18);
            this.btTestData.Name = "btTestData";
            this.btTestData.Size = new System.Drawing.Size(86, 32);
            this.btTestData.TabIndex = 24;
            this.btTestData.Text = "Test";
            this.btTestData.UseVisualStyleBackColor = true;
            this.btTestData.Click += new System.EventHandler(this.btTestData_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label8.Location = new System.Drawing.Point(131, 15);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(24, 18);
            this.label8.TabIndex = 97;
            this.label8.Text = "см";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label10.Location = new System.Drawing.Point(271, 15);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(20, 18);
            this.label10.TabIndex = 98;
            this.label10.Text = "кг";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label11.Location = new System.Drawing.Point(271, 15);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(21, 18);
            this.label11.TabIndex = 98;
            this.label11.Text = "°С";
            // 
            // LifeAnamnesisForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(415, 659);
            this.ControlBox = false;
            this.Controls.Add(this.flowLayoutPanel1);
            this.Controls.Add(this.pnButtons);
            this.Name = "LifeAnamnesisForm";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Анамнез жизни";
            this.flowLayoutPanel1.ResumeLayout(false);
            this.flowLayoutPanel1.PerformLayout();
            this.pnHeightWeight.ResumeLayout(false);
            this.pnHeightWeight.PerformLayout();
            this.pnTemperature.ResumeLayout(false);
            this.pnTemperature.PerformLayout();
            this.pnPressureBreathe.ResumeLayout(false);
            this.pnPressureBreathe.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.pnButtons.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private SharedLibrary.PlainRichTextBoxExt rtbStool;
        private System.Windows.Forms.Label label31;
        private SharedLibrary.PlainRichTextBoxExt rtbUrination;
        private System.Windows.Forms.Label label32;
        private SharedLibrary.PlainRichTextBoxExt rtbLiver;
        private System.Windows.Forms.Label label43;
        private SharedLibrary.PlainRichTextBoxExt rtbStomach;
        private System.Windows.Forms.Label label46;
        private SharedLibrary.PlainRichTextBoxExt rtbPulse;
        private System.Windows.Forms.Label label25;
        private SharedLibrary.PlainRichTextBoxExt rtbHeartSounds;
        private System.Windows.Forms.Label label24;
        private SharedLibrary.PlainRichTextBoxExt rtbPercussion;
        private System.Windows.Forms.Label label22;
        private SharedLibrary.PlainRichTextBoxExt rtbLungsResp;
        private System.Windows.Forms.Label label21;
        private SharedLibrary.PlainRichTextBoxExt rtbDyspnea;
        private System.Windows.Forms.Label label20;
        private SharedLibrary.PlainRichTextBoxExt rtbChest;
        private System.Windows.Forms.Label label19;
        private SharedLibrary.PlainRichTextBoxExt rtbThyroid;
        private System.Windows.Forms.Label label18;
        private SharedLibrary.PlainRichTextBoxExt rtbMammary;
        private System.Windows.Forms.Label label17;
        private SharedLibrary.PlainRichTextBoxExt rtbSkin;
        private SharedLibrary.PlainRichTextBoxExt rtbLymphNodes;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label15;
        private SharedLibrary.PlainRichTextBoxExt rtbProfHarm;
        private System.Windows.Forms.Label label37;
        private SharedLibrary.PlainRichTextBoxExt rtbTransfusion;
        private System.Windows.Forms.Label label7;
        private SharedLibrary.PlainRichTextBoxExt rtbAllergy;
        private System.Windows.Forms.Label label9;
        private SharedLibrary.PlainRichTextBoxExt rtbGenetics;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label26;
        private SharedLibrary.TextBoxEx tbBloodPressureA;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label29;
        private SharedLibrary.TextBoxEx tbHeartRate;
        private SharedLibrary.TextBoxEx tbBloodPressureD;
        private System.Windows.Forms.Button btOK;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.Panel pnPressureBreathe;
        private System.Windows.Forms.Panel pnButtons;
        private System.Windows.Forms.Button btTestData;
        private System.Windows.Forms.Label label1;
        private SharedLibrary.PlainRichTextBoxExt rtbPastIllnesses;
        private System.Windows.Forms.Panel panel2;
        private SharedLibrary.TextBoxEx tbCiggsPerDay;
        private SharedLibrary.FocusableCheckBox cbSmokes;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private SharedLibrary.PlainRichTextBoxExt rtbTuberculosis;
        private System.Windows.Forms.Panel pnTemperature;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Panel pnHeightWeight;
        private System.Windows.Forms.Label label4;
        private SharedLibrary.TextBoxEx tbTemperature;
        private System.Windows.Forms.TextBox tbWeight;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox tbHeight;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label11;
    }
}