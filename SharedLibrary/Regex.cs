﻿namespace SharedLibrary
{
    public static class RegexPattern
    {
        public static readonly string ShortName = @"^([А-Я][а-я]+(\-[А-Я][а-я]+)*)\s([А-Я]\.[А-Я]\.)$";
    }
}
