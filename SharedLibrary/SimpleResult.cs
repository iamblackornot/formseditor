﻿namespace SharedLibrary
{
    public struct SimpleResult
    {
        public bool Done { get; }
        public string Message { get; }

        public SimpleResult(bool done, string message)
        {
            Done = done;
            Message = message;
        }
    }
}
