﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace FormsEditorEx.Commission
{
    public partial class CommissionEditForm : Form
    {
        bool isReadOnly;
        bool hasChanged = false;
        public string Num { get; private set; }
        public string Date { get; private set; }
        public string Decision { get; private set; }
        public int Type { get; private set; }
        public CommissionEditForm(bool isReadOnly = true)
        {
            InitializeComponent();

            this.isReadOnly = isReadOnly;

            InitButtons();
            InitComponents();
            CenterToScreen();
        }
        public CommissionEditForm(string num, string date, string decision, int type, bool isReadOnly = true) : this(isReadOnly)
        {
            tbNum.Text = num;
            tbDate.Text = date;
            rtbDecision.Text = decision;
            cbType.SelectedIndex = type;

            this.Text = isReadOnly ? "Посмотр записи" : this.Text = "Изменить запись";

            btOK.Enabled = isReadOnly;
            hasChanged = false;
        }
        private void InitComponents()
        {
            cbType.Enabled = !isReadOnly;
            tbNum.SetTextBoxAvailability(isReadOnly);
            tbDate.SetTextBoxAvailability(isReadOnly);
            rtbDecision.SetTextBoxAvailability(isReadOnly);

            cbType.DataSource = Static.CommissionTypes;
            cbType.SelectedIndex = -1;

            cbType.SelectedIndexChanged += InputChanged;
            tbNum.TextChanged += InputChanged;
            tbDate.TextChanged += InputChanged;
            rtbDecision.TextChanged += InputChanged;
        }
        private void InitButtons()
        {
            btCancel.Visible = !isReadOnly;
            btOK.Left = (pnButtons.Width - btOK.Width - (!isReadOnly ? btCancel.Width + 8 : 0)) / 2;
            btCancel.Left = btOK.Left + btOK.Width + 8;
        }
        private bool CheckFields()
        {
            if (!tbDate.MaskCompleted)
            {
                MessageBox.Show("Дата не указана или введена неверно");
                return false;
            }
            if (tbNum.Text.Length == 0)
            {
                MessageBox.Show("Не указан номер комиссии");
                return false;
            }
            if (cbType.SelectedIndex < 0)
            {
                MessageBox.Show("Не выбран вид комиссии");
                return false;
            }
            return true;
        }

        private void btOK_Click(object sender, EventArgs e)
        {
            if (isReadOnly)
            {
                DialogResult = DialogResult.Cancel;
                Close();
            }
            else
            {
                if (CheckFields())
                {
                    Num = tbNum.Text.Trim();
                    Date = tbDate.Text;
                    Decision = rtbDecision.Text.Trim();
                    Type = cbType.SelectedIndex;

                    DialogResult = DialogResult.OK;
                    Close();
                }
            }
        }
        private void ReturnToTab(KeyEventArgs e)
        {
            if (e.KeyData == Keys.Enter)
            {
                e.SuppressKeyPress = true;
                SelectNextControl(ActiveControl, true, true, true, true);
            }
        }

        private void TextBox_KeyDown(object sender, KeyEventArgs e)
        {
            ReturnToTab(e);
        }
        private void btCancel_Click(object sender, EventArgs e)
        {
            if (hasChanged)
            {
                if (MessageBox.Show("Закрыть окно без сохранения изменений?", "Требуется подтверждение",
                    MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                {
                    DialogResult = DialogResult.Cancel;
                    Close();
                }
            }
            else
            {
                DialogResult = DialogResult.Cancel;
                Close();
            }
        }
        private void InputChanged(object sender, EventArgs e)
        {
            hasChanged = true;
            btOK.Enabled = true;
        }
    }
}
