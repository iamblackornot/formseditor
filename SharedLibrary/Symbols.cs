﻿using System;

namespace SharedLibrary
{
    public static class Symbols
    {
        public static readonly string NBSpace = "\u00A0";
        public static readonly string Power12 = "\u00B9\u00B2";
        public static readonly string Power9 = "\u2079";
        public static readonly string Dash = "\u2012";
        public static readonly string Comma = "\u002C";
        public static readonly string SoftLineBreak = char.ConvertFromUtf32(11);
        public static readonly string Tab = char.ConvertFromUtf32(9);
    }
}
