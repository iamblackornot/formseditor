﻿using System;
using System.Collections.Generic;
using SharedLibrary;

namespace FormsEditorEx.GenExam.Records
{
    public class DiseaseRecord : IExamRecord
    {
        public DateTime? Date { get; set; }
        public bool RW { get; set; }
        public bool HIV { get; set; }
        public bool HBsAg { get; set; }
        public bool HCV { get; set; }

        public string GetString()
        {
            string res = string.Empty;

            if (Date.HasValue)
            {
                List<string> pos = new List<string>();
                List<string> neg = new List<string>();
                string temp = string.Empty;

                //res += Utility.DateToShortString(Date.Value) + Symbols.NBSpace + Symbols.NBSpace;

                temp = "RW";

                if (RW)
                    pos.Add(temp);
                else
                    neg.Add(temp);

                temp = "ВИЧ";

                if (HIV)
                    pos.Add(temp);
                else
                    neg.Add(temp);

                temp = "HBsAg";

                if (HBsAg)
                    pos.Add(temp);
                else
                    neg.Add(temp);

                temp = "HCV";

                if (HCV)
                    pos.Add(temp);
                else
                    neg.Add(temp);

                if (neg.Count > 0)
                {
                    res += neg[0];

                    for(int i = 1; i < neg.Count; i++)
                        res += $", { neg[i] }";

                    res += ":" + Symbols.NBSpace + "отриц.";
                }

                if (pos.Count > 0)
                {
                    if (neg.Count > 0)
                        res += $"; ";
                    res += pos[0];

                    for (int i = 1; i < pos.Count; i++)
                        res += $", { pos[i] }";

                    res += ":" + Symbols.NBSpace + "положит.";
                }
            }

            return res;
        }
        public static GenExamRecord GetTestRecord()
        {
            DiseaseRecord rec = new DiseaseRecord()
            {
                Date = new DateTime(2020, 1, 24),
                RW = true
            };
            return new GenExamRecord() { Date = rec.Date, Result = rec.GetString() };
        }
    }
}
