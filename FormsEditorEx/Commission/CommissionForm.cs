﻿using System;
using System.Windows.Forms;

namespace FormsEditorEx.Commission
{
    public partial class CommissionForm : Form
    {
        public CommissionForm(bool isReadOnly = true)
        {
            InitializeComponent();

            CommissionTableEditControl ucDataTableEdit = new CommissionTableEditControl(isReadOnly);
            ucDataTableEdit.OnClose += UcDataTableEdit_OnClose;

            pnContent.Controls.Add(ucDataTableEdit);
            ucDataTableEdit.Dock = DockStyle.Fill;

            CenterToScreen();
        }

        private void UcDataTableEdit_OnClose(object sender, EventArgs e)
        {
            Close();
        }
    }
}
