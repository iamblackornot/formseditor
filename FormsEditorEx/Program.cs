﻿using System;
using System.Reflection;
using System.Threading;
using System.Windows.Forms;
using SharedLibrary;
using FormsEditorEx.VersionChecker;
using FormsEditorEx.Database;
using SharedLibrary.Logger;

namespace FormsEditorEx
{
    static class Program
    {
        private static LoadScreen splashForm = null;
        //private static MainFrame mainForm;
        private static PatientsForm patientsForm;
        private static Thread splashThread;
        //public static GlobalExceptionHandler GlobalExcHandler = new GlobalExceptionHandler();
#if !DEBUG
        public static GlobalExceptionHandler GlobalExcHandler = new GlobalExceptionHandler();
#endif
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

#if !DEBUG
            VersionControl.DeleteIrrelevantFiles();
#endif
            var res = Paths.Instance.Init();

            if (!res.Done)
            {
                ShowErrorMessage(res.Message);
                return;
            }

            ILogger logger = new TextFileLogger(Paths.LOCAL_LOGS_FILENAME, Static.AppName);
            DatabaseAdapter dbAdapter = new DatabaseAdapter(logger);

            var getVersionRes = dbAdapter.GetAppVersion(Static.AppName);

            if(getVersionRes.Completed)
            {
                var versionCheckResult = VersionControl.Check(getVersionRes.Data);

                if(versionCheckResult == CheckVersionResult.DEPRECATED)
                {
                    return;
                }
            }
            else
            {
                ShowErrorMessage("Не удалось проверить актуальность версии");
            }

            splashThread = new Thread(new ThreadStart(
                delegate
                {
                    splashForm = new LoadScreen("Инициализация...");
                    Application.Run(splashForm);
                }
                ));

            splashThread.SetApartmentState(ApartmentState.STA);
            splashThread.Start();

            if(LoadSettings(dbAdapter))
            {
                patientsForm = new PatientsForm(dbAdapter);
                patientsForm.Load += new EventHandler(mainForm_Load);

                Application.Run(patientsForm);
            }
        }

        static void mainForm_Load(object sender, EventArgs e)
        {
            CloseSplash();
        }

        private static bool LoadSettings(DatabaseAdapter dbAdapter)
        {
            LoadSettingsResult res = AppConfig.Load(dbAdapter);

            if (!res.Done)
            {
                
                CloseSplash();
                MessageBox.Show(res.ErrorMessage, string.Empty, 
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error,
                    MessageBoxDefaultButton.Button1,
                    MessageBoxOptions.ServiceNotification);

                return false;
            }

            return true;
        }

        private static void ShowErrorMessage(string message)
        {
            MessageBox.Show(message, string.Empty,
                MessageBoxButtons.OK,
                MessageBoxIcon.Error,
                MessageBoxDefaultButton.Button1,
                MessageBoxOptions.ServiceNotification);
        }

        private static void CloseSplash()
        {
            //close splash
            try
            {
                if (splashForm != null)
                {
                    if (splashForm.InvokeRequired)
                        splashForm.Invoke(new Action(splashForm.Close));
                    else
                        splashForm.Close();
                }
            }
            catch (Exception ex)
            {
                Logger.AddToLogs(MethodBase.GetCurrentMethod().Name, ex.ToString());
            }
            finally
            {
                //splashForm.Close();
                if (splashForm != null)
                {
                    if (splashForm.InvokeRequired)
                        splashForm?.Invoke(new Action(splashForm.Dispose));
                    else
                        splashForm.Dispose();

                    splashForm = null;
                }
            }
        }
    }
}
