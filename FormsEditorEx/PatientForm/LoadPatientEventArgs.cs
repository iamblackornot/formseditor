﻿using System;

namespace FormsEditorEx
{
    public class LoadPatientEventArgs : EventArgs
    {
        public string PatientId { get; set; }
    }
}
