﻿using System;
using SharedLibrary;

namespace FormsEditorEx.GenExam.Records
{
    public class BAKRecord : IExamRecord
    {
        public DateTime? Date { get; set; }
        public string AST { get; set; }
        public string ALT { get; set; }
        public string BilirubinTotal { get; set; }
        public string BilirubinDirect { get; set; }
        public string AlkalinePhosphatase { get; set; }
        public string AlphaAmylase { get; set; }
        public string Glucose { get; set; }
        public string Potassium { get; set; }
        public string Sodium { get; set; }
        public string ProteinTotal { get; set; }
        public string Creatinine { get; set; }
        public string Urea { get; set; }

        public string GetString()
        {
            string temp = string.Empty;

            if (Date.HasValue)
            {
                //temp += Utility.DateToShortString(Date.Value) + Symbols.NBSpace + Symbols.NBSpace + "Биохимический анализ крови: ";
                temp += "Биохимический анализ крови: ";

                bool isAllEmpty = true;

                if (!string.IsNullOrWhiteSpace(ALT))
                {
                    isAllEmpty = false;
                    temp += "АЛТ" + Symbols.NBSpace + Symbols.Dash + Symbols.NBSpace + ALT + Symbols.NBSpace + "ед/л";
                }

                if (!string.IsNullOrWhiteSpace(AST))
                {
                    if (!isAllEmpty)
                        temp += ", ";
                    else
                        isAllEmpty = false;

                    temp += "АСТ" + Symbols.NBSpace + Symbols.Dash + Symbols.NBSpace + AST + Symbols.NBSpace + "ед/л";
                }

                if (!string.IsNullOrWhiteSpace(BilirubinTotal))
                {
                    if (!isAllEmpty)
                        temp += ", ";
                    else
                        isAllEmpty = false;

                    temp += "билирубин общий" + Symbols.NBSpace + Symbols.Dash + Symbols.NBSpace + BilirubinTotal + Symbols.NBSpace + "мкмоль/л";
                }

                if (!string.IsNullOrWhiteSpace(BilirubinDirect))
                {
                    if (!isAllEmpty)
                        temp += ", ";
                    else
                        isAllEmpty = false;

                    temp += "билирубин прямой" + Symbols.NBSpace + Symbols.Dash + Symbols.NBSpace + BilirubinDirect + Symbols.NBSpace + "мкмоль/л";
                }

                if (!string.IsNullOrWhiteSpace(AlkalinePhosphatase))
                {
                    if (!isAllEmpty)
                        temp += ", ";
                    else
                        isAllEmpty = false;

                    temp += "щелочная фосфатаза" + Symbols.NBSpace + Symbols.Dash + Symbols.NBSpace + AlkalinePhosphatase + Symbols.NBSpace + "ед/л";
                }

                if (!string.IsNullOrWhiteSpace(AlphaAmylase))
                {
                    if (!isAllEmpty)
                        temp += ", ";
                    else
                        isAllEmpty = false;

                    temp += "альфа-амилаза" + Symbols.NBSpace + Symbols.Dash + Symbols.NBSpace + AlphaAmylase + Symbols.NBSpace + "ед/л";
                }

                if (!string.IsNullOrWhiteSpace(Glucose))
                {
                    if (!isAllEmpty)
                        temp += ", ";
                    else
                        isAllEmpty = false;

                    temp += "глюкоза" + Symbols.NBSpace + Symbols.Dash + Symbols.NBSpace + Glucose + Symbols.NBSpace + "ммоль/л";
                }

                if (!string.IsNullOrWhiteSpace(Potassium))
                {
                    if (!isAllEmpty)
                        temp += ", ";
                    else
                        isAllEmpty = false;

                    temp += "калий" + Symbols.NBSpace + Symbols.Dash + Symbols.NBSpace + Potassium + Symbols.NBSpace + "ммоль/л";
                }

                if (!string.IsNullOrWhiteSpace(Sodium))
                {
                    if (!isAllEmpty)
                        temp += ", ";
                    else
                        isAllEmpty = false;

                    temp += "натрий" + Symbols.NBSpace + Symbols.Dash + Symbols.NBSpace + Sodium + Symbols.NBSpace + "ммоль/л";
                }

                if (!string.IsNullOrWhiteSpace(ProteinTotal))
                {
                    if (!isAllEmpty)
                        temp += ", ";
                    else
                        isAllEmpty = false;

                    temp += "общий белок" + Symbols.NBSpace + Symbols.Dash + Symbols.NBSpace + ProteinTotal + Symbols.NBSpace + "г/л";
                }

                if (!string.IsNullOrWhiteSpace(Creatinine))
                {
                    if (!isAllEmpty)
                        temp += ", ";
                    else
                        isAllEmpty = false;

                    temp += "креатинин" + Symbols.NBSpace + Symbols.Dash + Symbols.NBSpace + Creatinine + Symbols.NBSpace + "мкмоль/л";
                }

                if (!string.IsNullOrWhiteSpace(Urea))
                {
                    if (!isAllEmpty)
                        temp += ", ";
                    else
                        isAllEmpty = false;

                    temp += "мочевина" + Symbols.NBSpace + Symbols.Dash + Symbols.NBSpace + Urea + Symbols.NBSpace + "ммоль/л";
                }

                temp += ".";
            }

            return temp;
        }
        public static GenExamRecord GetTestRecord()
        {
            BAKRecord rec = new BAKRecord()
            {
                Date = new DateTime(2020, 3, 20),
                AST = "35,9",
                ALT = "30,6",
                BilirubinTotal = "9,1",
                BilirubinDirect = "3,5",
                AlkalinePhosphatase = "49",
                AlphaAmylase = "880",
                Glucose = "4,3",
                Potassium = "5,1",
                Sodium = "153",
                ProteinTotal = "64",
                Creatinine = "55",
                Urea = "5,1",
            };
            return new GenExamRecord() { Date = rec.Date, Result = rec.GetString() };
         }
    }
}
