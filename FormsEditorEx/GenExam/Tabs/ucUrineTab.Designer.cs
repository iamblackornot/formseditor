﻿namespace FormsEditorEx.GenExam.Tabs
{
    partial class ucUrineTab
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.pnContent = new System.Windows.Forms.Panel();
            this.label13 = new System.Windows.Forms.Label();
            this.tbUrineDate = new System.Windows.Forms.MaskedTextBox();
            this.rtbUrine = new SharedLibrary.PlainRichTextBoxExt();
            this.pnContent.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnContent
            // 
            this.pnContent.AutoSize = true;
            this.pnContent.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.pnContent.Controls.Add(this.label13);
            this.pnContent.Controls.Add(this.tbUrineDate);
            this.pnContent.Controls.Add(this.rtbUrine);
            this.pnContent.Location = new System.Drawing.Point(47, 60);
            this.pnContent.Margin = new System.Windows.Forms.Padding(4);
            this.pnContent.Name = "pnContent";
            this.pnContent.Padding = new System.Windows.Forms.Padding(8);
            this.pnContent.Size = new System.Drawing.Size(363, 162);
            this.pnContent.TabIndex = 0;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(13, 15);
            this.label13.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(38, 18);
            this.label13.TabIndex = 38;
            this.label13.Text = "Дата";
            // 
            // tbUrineDate
            // 
            this.tbUrineDate.Culture = new System.Globalization.CultureInfo("");
            this.tbUrineDate.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.tbUrineDate.Location = new System.Drawing.Point(60, 12);
            this.tbUrineDate.Margin = new System.Windows.Forms.Padding(4);
            this.tbUrineDate.Mask = "00.00.0000";
            this.tbUrineDate.Name = "tbUrineDate";
            this.tbUrineDate.Size = new System.Drawing.Size(105, 26);
            this.tbUrineDate.TabIndex = 1;
            this.tbUrineDate.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // rtbUrine
            // 
            this.rtbUrine.Cue = "в пределах нормы.";
            this.rtbUrine.Font = new System.Drawing.Font("Calibri", 11.25F);
            this.rtbUrine.Location = new System.Drawing.Point(12, 48);
            this.rtbUrine.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.rtbUrine.Name = "rtbUrine";
            this.rtbUrine.PlainTextMode = true;
            this.rtbUrine.ShowSelectionMargin = true;
            this.rtbUrine.Size = new System.Drawing.Size(339, 100);
            this.rtbUrine.TabIndex = 2;
            this.rtbUrine.Text = "";
            // 
            // ucUrineTab
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.pnContent);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "ucUrineTab";
            this.Size = new System.Drawing.Size(664, 665);
            this.pnContent.ResumeLayout(false);
            this.pnContent.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel pnContent;
        private System.Windows.Forms.MaskedTextBox tbUrineDate;
        private SharedLibrary.PlainRichTextBoxExt rtbUrine;
        private System.Windows.Forms.Label label13;
    }
}
