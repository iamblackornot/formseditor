﻿namespace FormsEditorEx.VMP
{
    partial class AdminForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pnPass = new System.Windows.Forms.Panel();
            this.tbPass = new System.Windows.Forms.TextBox();
            this.pnSettings = new System.Windows.Forms.Panel();
            this.btBlack = new System.Windows.Forms.Button();
            this.pnPass.SuspendLayout();
            this.pnSettings.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnPass
            // 
            this.pnPass.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnPass.Controls.Add(this.tbPass);
            this.pnPass.Location = new System.Drawing.Point(0, 0);
            this.pnPass.Name = "pnPass";
            this.pnPass.Size = new System.Drawing.Size(200, 100);
            this.pnPass.TabIndex = 0;
            // 
            // tbPass
            // 
            this.tbPass.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbPass.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.tbPass.Location = new System.Drawing.Point(60, 38);
            this.tbPass.Name = "tbPass";
            this.tbPass.PasswordChar = '☼';
            this.tbPass.Size = new System.Drawing.Size(80, 23);
            this.tbPass.TabIndex = 1;
            this.tbPass.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.tbPass.KeyDown += new System.Windows.Forms.KeyEventHandler(this.tbPass_KeyDown);
            // 
            // pnSettings
            // 
            this.pnSettings.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnSettings.Controls.Add(this.btBlack);
            this.pnSettings.Location = new System.Drawing.Point(247, 0);
            this.pnSettings.Name = "pnSettings";
            this.pnSettings.Size = new System.Drawing.Size(200, 100);
            this.pnSettings.TabIndex = 1;
            this.pnSettings.Visible = false;
            // 
            // btBlack
            // 
            this.btBlack.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btBlack.Location = new System.Drawing.Point(60, 28);
            this.btBlack.Name = "btBlack";
            this.btBlack.Size = new System.Drawing.Size(80, 40);
            this.btBlack.TabIndex = 0;
            this.btBlack.Text = "Blacklist";
            this.btBlack.UseVisualStyleBackColor = true;
            this.btBlack.Click += new System.EventHandler(this.btBlack_Click);
            // 
            // AdminForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(665, 256);
            this.Controls.Add(this.pnSettings);
            this.Controls.Add(this.pnPass);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "AdminForm";
            this.ShowIcon = false;
            this.pnPass.ResumeLayout(false);
            this.pnPass.PerformLayout();
            this.pnSettings.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnPass;
        private System.Windows.Forms.TextBox tbPass;
        private System.Windows.Forms.Panel pnSettings;
        private System.Windows.Forms.Button btBlack;
    }
}