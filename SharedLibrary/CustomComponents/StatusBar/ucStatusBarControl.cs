﻿using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace SharedLibrary
{
    public partial class ucStatusBarControl : UserControl
    {
        public static readonly Dictionary<StatusBarIndicator, Color> StatusBarColor = new Dictionary<StatusBarIndicator, System.Drawing.Color>()
        {
            { StatusBarIndicator.OK, Color.Green },
            { StatusBarIndicator.WARNING, Color.Yellow },
            { StatusBarIndicator.ERROR, Color.Red }
        };

        public ucStatusBarControl()
        {
            InitializeComponent();
        }
        public void ShowStatus(IStatusBarNotification sbn)
        {
            ChangeStatus(sbn.Status, sbn.StatusMessage);
        }
        private void ChangeStatus(StatusBarIndicator sbi, string text)
        {
            lbStatusColor.BackColor = StatusBarColor[sbi];
            lbStatus.Text = text;
        }
        public void ShowOK(string text)
        {
            ChangeStatus(StatusBarIndicator.OK, text);
        }
        public void ShowWarning(string text)
        {
            ChangeStatus(StatusBarIndicator.WARNING, text);
        }
        public void ShowError(string text)
        {
            ChangeStatus(StatusBarIndicator.ERROR, text);
        }
    }
}
