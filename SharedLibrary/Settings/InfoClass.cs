﻿using System.Collections.Generic;
using System.IO;
using Newtonsoft.Json.Linq;

namespace SharedLibrary.Settings
{
    public enum Finance
    {
        OMS = 0,
        Region = 1
    }

    public enum ProspectAlign
    {
        Left = 0,
        Right = 1
    }
    public class InfoClass
    {
        public List<string> Departments { get; private set; }
        public VMPInfo VMP { get; private set; }
        public List<Prospect> Prospects { get; private set; }

        public List<string> Cities { get; private set; }
        public Dictionary<int, string> Phones { get; private set; }

        public InfoClass()
        {
            Departments = new List<string>();
            VMP = new VMPInfo();
            Prospects = new List<Prospect>();
            Cities = new List<string>();
            Phones = new Dictionary<int, string>();
        }

        public bool Load(string path)
        {
            if (!File.Exists(Path.Combine(path, "info")))
                return false;

            string json = File.ReadAllText(Path.Combine(path, "info"));

            JObject jobj = JObject.Parse(json);

            if (jobj == null)
                return false;

            if(jobj.ContainsKey("Departments"))
                Departments = jobj["Departments"].ToObject<List<string>>();

            if(jobj.ContainsKey("VMP"))
                VMP = jobj["VMP"].ToObject<VMPInfo>();

            if (jobj.ContainsKey("Prospects"))
                Prospects = jobj["Prospects"].ToObject<List<Prospect>>();

            if (jobj.ContainsKey("Cities"))
                Cities = jobj["Cities"].ToObject<List<string>>();

            if (jobj.ContainsKey("Phones"))
                Phones = jobj["Phones"].ToObject<Dictionary<int, string>>();

            return true;
        }
        public class VMPInfo
        {
            public List<string> TreatmentTypes { get; set; }
            public List<string> FinanceTypes { get; set; }

            public VMPInfo()
            {
                TreatmentTypes = new List<string>();
                FinanceTypes = new List<string>();
            }
        }
        public class Prospect
        {
            public ProspectAlign Align { get; set; }
            public string Name { get; set; }
        }
    }
}
