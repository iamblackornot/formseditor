﻿using System;
using System.Windows.Forms;
using System.Threading;

namespace FormsEditorEx
{
    public class GlobalExceptionHandler
    {
        public GlobalExceptionHandler()
        {
            AppDomain.CurrentDomain.UnhandledException += new UnhandledExceptionEventHandler(CurrentDomain_UnhandledException);
            Application.ThreadException += new ThreadExceptionEventHandler(HandleException);
        }
        private void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            Logger.AddToLogs(e.ExceptionObject.ToString());
            //try
            //{
            //    throw (e.ExceptionObject as Exception);
            //}
            //catch (Exception ex)
            //{
            //    Logger.AddToLogs(ex.ToString());
            //}
        }
        public void HandleException(object sender, ThreadExceptionEventArgs t)
        {
            Logger.AddToLogs(t.Exception.ToString());
            //try
            //{
            //    throw (t.Exception);
            //}
            //catch (ArgumentException ex)
            //{
            //}
            //catch (Exception ex)
            //{
            //}
            //exceptions handling code here...


            // you can throw exception at the end of method if you want this exception to go unhandled, not writing throw will swallow exception
            /// throw t.Exception
        }
    }
}
