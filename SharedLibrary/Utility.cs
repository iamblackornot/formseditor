﻿using System;
using System.Text.RegularExpressions;
using System.Globalization;
using System.Windows.Forms;
using System.Collections.Generic;
using System.Drawing;
using Newtonsoft.Json;
using System.Text;

namespace SharedLibrary
{
    public static partial class Utility
    {
        public static string DateToFullString(DateTime? date)
        {
            return date.HasValue ? date.Value.ToString("d MMMM yyyy", CultureInfo.CreateSpecificCulture("ru-RU")) + Symbols.NBSpace + "г." : string.Empty;
        }
        public static string DateToShortString(DateTime? date)
        {
            return date.HasValue ? string.Format("{0:dd.MM.yyyy}", date) : string.Empty;
        }
        public static string DateToExtendedShortString(DateTime? date)
        {
            return date.HasValue ? date.Value.ToString("dd.MM.yyyy HH:mm") : string.Empty;
        }
        public static string ToTitleCase(string str)
        {
            return CultureInfo.CurrentCulture.TextInfo.ToTitleCase(str.ToLower());
        }
        public static string GetTreatmentDaysCountString(DateTime dateExam, DateTime dateDischarge)
        {
            int daysDiff = (dateDischarge - dateExam).Days;

            string res = $"{daysDiff}";
            string ending = "суток";

            if(res.Length > 0 && res[res.Length - 1] == '1')
            {
                if(!(res.Length > 1 && res[res.Length - 2] == '1'))
                {
                    ending = "сутки";
                }
            }

            return $"{res} {ending}";
        }
        public static int? DateToAge(DateTime? date)
        {
            if (date.HasValue)
            {
                // Save today's date.
                var today = DateTime.Today;

                // Calculate the age.
                var age = today.Year - date.Value.Year;

                // Go back to the year the person was born in case of a leap year
                if (date.Value.Date > today.AddYears(-age))
                    age--;

                return age;
            }
            else
                return null;
        }
        public static string UnixTimestampToDateString(double unix)
        {
            DateTime dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc);
            dtDateTime = dtDateTime.AddSeconds(unix).ToLocalTime();

            return DateToShortString(dtDateTime);
        }
        public static string DateToSQLiteDate(DateTime date)
        {
            if (date != default(DateTime))
                return String.Format("{0:yyyy-MM-dd 00:00:00}", date);
            else
                return string.Empty;
        }
        public static string RemoveSpecialCharacters(string str)
        {
            return Regex.Replace(str, @"[^a-zA-Zа-яА-Я0-9_\s\.\(\)]+", "_", RegexOptions.Compiled);
        }
        public static void ClearControls(Control ctrl, List<string> ControlsToIgnore = null)
        {
            foreach (Control c in ctrl.Controls)
            {
                if (c.Name != "" && (ControlsToIgnore is null || (ControlsToIgnore is List<string> && !ControlsToIgnore.Contains(c.Name))))
                {
                    if (c is PlainRichTextBoxExt)
                    {
                        (c as PlainRichTextBoxExt).SetText(string.Empty);
                    }
                    else
                    {
                        if (c is TextBoxEx)
                        {
                            (c as TextBoxEx).SetText(string.Empty);
                        }
                        else
                        {
                            if (c is TextBoxBase tbb)
                            {
                                tbb.Clear();
                                //c.Text = String.Empty;
                            }
                            else
                            {
                                if (c is ComboBox)
                                {
                                    ((ComboBox)c).SelectedIndex = -1;
                                    ((ComboBox)c).Text = string.Empty;
                                }
                            }
                        }
                    }
                }

                if (c.Controls.Count > 0)
                {
                    ClearControls(c, ControlsToIgnore);
                }
            }
        }
        public static void ShrinkFormIfNeeded(Control c)
        {
            System.Drawing.Rectangle rect = Screen.FromControl(c).WorkingArea;
            if (c.Width > rect.Width)
                c.Width = rect.Width;

            if (c.Height > rect.Height)
                c.Height = rect.Height;
        }
        public static void cbxDesign_DrawItem(object sender, DrawItemEventArgs e)
        {
            // By using Sender, one method could handle multiple ComboBoxes
            ComboBox cbx = sender as ComboBox;
            if (cbx != null)
            {
                // Always draw the background
                e.DrawBackground();

                // Drawing one of the items?
                if (e.Index >= 0)
                {
                    // Set the string alignment.  Choices are Center, Near and Far
                    StringFormat sf = new StringFormat();
                    sf.LineAlignment = StringAlignment.Center;
                    sf.Alignment = StringAlignment.Center;

                    // Set the Brush to ComboBox ForeColor to maintain any ComboBox color settings
                    // Assumes Brush is solid
                    Brush brush = new SolidBrush(cbx.ForeColor);

                    // If drawing highlighted selection, change brush
                    if ((e.State & DrawItemState.Selected) == DrawItemState.Selected)
                        brush = SystemBrushes.HighlightText;

                    // Draw the string
                    e.Graphics.DrawString(cbx.Items[e.Index].ToString(), cbx.Font, brush, e.Bounds, sf);
                }
            }
        }
        public static void ChangeSelectedRowOnRemove(DataGridView dgv, int removedRowId)
        {
            if (dgv.Rows.Count > 0 && removedRowId == dgv.Rows.Count)
                dgv.Rows[dgv.Rows.Count - 1].Selected = true;
        }
        public static string AddDotAtTheEndIfNeeded(string str)
        {
            return str + (str[str.Length - 1] == '.' ? string.Empty : ".");
        }
        public static void ClearWritableProperties(object obj)
        {
            var Properties = obj.GetType().GetProperties();

            foreach (var Property in Properties)
            {
                if (Property.CanWrite)
                    Property.SetValue(obj, null, null);
            }
        }
        public static string SerializeDataObject(object obj)
        {
            return JsonConvert.SerializeObject(obj, Formatting.None,
                            new JsonSerializerSettings
                            {
                                NullValueHandling = NullValueHandling.Ignore
                            });
        }
        public static void ReturnToTab(Form frm, KeyEventArgs e)
        {
            if (e.KeyData == Keys.Enter)
            {
                e.SuppressKeyPress = true;
                frm.SelectNextControl(frm.ActiveControl, true, true, true, true);
            }
        }
        public static void ReturnToTab(Form frm, KeyPressEventArgs e)
        {
            if (e.KeyChar == Convert.ToChar(Keys.Return))
            {
                frm.SelectNextControl(frm.ActiveControl, true, true, true, true);
                e.Handled = true;
            }
        }
        public static string GetEntriesString(int num)
        {
            string temp = "записей";

            if (num < 5 || num > 20)
                if (int.TryParse(num.ToString().Substring(num.ToString().Length - 1), out int last))
                {
                    if (last > 1 && last < 5)
                        temp = "записи";
                    else if (last == 1)
                        temp = "запись";
                }

            return temp;
        }
        public static string EmptyIfNull(this string str)
        {
            return str ?? string.Empty;
        }
        public static string NullIfEmpty(this string str)
        {
            return str != null && str.Length == 0 ? null : str;
        }
        public static string DateBirthToAgeFullString(DateTime? birth)
        {
            string temp = "лет";
            string age_str = string.Empty;

            if (birth.HasValue)
            {
                int? age = DateToAge(birth);

                if (age.HasValue)
                {
                    age_str = age.Value.ToString();

                    if (age.Value < 5 || age.Value > 20)
                        if (int.TryParse(age_str.Substring(age_str.Length - 1), out int last))
                        {
                            if (last > 1 && last < 5)
                                temp = "года";
                            else if (last == 1)
                                temp = "год";
                        }
                }
            }

            return age_str.Length > 0 ? age_str + Symbols.NBSpace + temp : string.Empty;
        }
        public static string WrapToolTipText(string text)
        {
            if (!string.IsNullOrWhiteSpace(text))
            {
                Regex rgx = new Regex("(.{100}\\s)");
                return rgx.Replace(text, "$1\n");
            }
            else
                return string.Empty;
        }
        public static bool CheckIfFullNameIsCorrect(string text)
        {
            bool res = false;

            text = text?.Trim();

            if (!string.IsNullOrWhiteSpace(text))
            {
                Regex expression = new Regex(RegexPattern.ShortName);

                Match match = expression.Match(text);
                res = match.Success;// && match.Groups.Count == 4;
            }

            return res;
        }
        public static T Create<T>() where T : class, new()
        {
            T t = new T();
            return t;
        }
        public static string FullNameToShortName(string secondName, string firstName, string thirdName)
        {
            StringBuilder sb = new StringBuilder();

            sb.Append(secondName);
            sb.Append(" ");
            sb.Append(firstName[0]);
            sb.Append(".");

            if(!string.IsNullOrWhiteSpace(thirdName))
            {
                sb.Append(thirdName[0]);
                sb.Append(".");
            }

            return sb.ToString();
        }
        public static SizeF MeasureString(string s, Font font)
        {
            SizeF result;
            using (var image = new Bitmap(1, 1))
            {
                using (var g = Graphics.FromImage(image))
                {
                    result = g.MeasureString(s, font);
                }
            }

            return result;
        }

        public static Size MeasureStringRoundedUp(string s, Font font)
        {
            SizeF sizeF = MeasureString(s, font);

            return new Size( 
                Convert.ToInt32(Math.Ceiling(sizeF.Width)),
                Convert.ToInt32(Math.Ceiling(sizeF.Height))
            );
        }
    }
}
