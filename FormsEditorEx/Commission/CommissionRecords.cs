﻿using System;
using SharedLibrary;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Data;
using Word = Microsoft.Office.Interop.Word;

namespace FormsEditorEx.Commission
{
    public class CommissionRecords
    {
        public DataTable Data { get; private set; }
        public CommissionRecords()
        {
            InitTable();
        }
        public void Reset()
        {
            Data.Clear();
        }
        private void InitTable()
        {
            Data = new DataTable();
            Data.Columns.Add("num", typeof(string));
            Data.Columns.Add("date", typeof(string));
            Data.Columns.Add("type", typeof(int));
            Data.Columns.Add("decision", typeof(string));
            Data.Columns.Add("ispost", typeof(bool));

            Data.AcceptChanges();
        }
        public string Serialize()
        {
            Data.AcceptChanges();

            string json = JsonConvert.SerializeObject(Data);
            return !json.Equals(Static.EmptyJArray) ? json : null; 
        }
        public void Deserialize(string json)
        {
            DataTable dt = Data;
            dt.Clear();

            if (!string.IsNullOrEmpty(json))
            {
                JArray jarr = JArray.Parse(json);

                if (jarr is JArray && jarr.HasValues)
                {
                    foreach(var jobj in jarr)
                    {
                        dt.Rows.Add(jobj["num"].ToString(), jobj["date"].ToString(), int.Parse(jobj["type"].ToString()), jobj["decision"].ToString(),
                            jobj["ispost"] is JToken ? bool.Parse(jobj["ispost"].ToString()) : false ///remove later
                            );
                    }
                    dt.AcceptChanges();
                }
            }
        }
        public void FillTestData()
        {
            Data.Rows.Add("123", "21.11.2020", 0, "решение1 решение1 решение1 решение1 решение1 решение1 решение1 решение1 решение1 решение1 решение1 решение1 решение1", false);
            Data.Rows.Add("124", "23.11.2020", 1, "решение2", false);

            Data.AcceptChanges();
        }
        public void FillTestPostData()
        {
            Data.Rows.Add("125", "25.11.2020", 0, "послеоперационное решение 1", true);
            Data.Rows.Add("126", "26.11.2020", 1, "послеоперационное решение 2", true);

            Data.AcceptChanges();
        }
        public string GetString()
        {
            Data.AcceptChanges();

            string temp = string.Empty;

            foreach(DataRow row in Data.Rows)
            {
                if (temp.Length > 0)
                    temp += Environment.NewLine;

                string type = int.TryParse(row["type"].ToString(), out int type_id) ? Static.CommissionTypes[type_id] : string.Empty;

                temp += $"{type} № {row["num"].ToString()} от {row["date"].ToString()}: {row["decision"].ToString()}";
            }

            return temp;
        }
        public void ResetPost()
        {
            foreach (DataRow row in Data.Rows)
            {
                row["ispost"] = false;
            }

            Data.AcceptChanges();
        }
        public void BuildDocString(Word.Document doc, string tag, bool ispost = false)
        {
            Data.AcceptChanges();

            Word.Range range = doc.Content;
            if (range.Find.Execute(tag))
            {
                range.Text = string.Empty;

                if (Data.Rows.Count > 0)
                {
                    DataRowCollection rows = Data.Rows;

                    bool rows_printed = false;

                    for (int i = 0; i < rows.Count; i++)
                    {
                        if (bool.TryParse(rows[i]["ispost"].ToString(), out bool post) && post == ispost)
                        {
                            rows_printed = true;

                            range = doc.Range(range.End, range.End);
                            range.Text = Environment.NewLine;
                            range = doc.Range(range.End, range.End);

                            string type = int.TryParse(rows[i]["type"].ToString(), out int type_id) ? Static.CommissionTypes[type_id] : string.Empty;

                            range.Text = $"{type} № {rows[i]["num"].ToString()} от {rows[i]["date"].ToString()}: ";
                            range.Bold = 1;

                            range = doc.Range(range.End, range.End);
                            range.Text = $"{ rows[i]["decision"].ToString()}";
                            range.Bold = 0;
                        }
                    }

                    if (rows_printed)
                    {
                        range = doc.Range(range.End, range.End);
                        range.Text = Environment.NewLine;
                    }
                }
            }
        }
    }
}
