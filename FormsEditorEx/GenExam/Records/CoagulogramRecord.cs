﻿using System;
using SharedLibrary;

namespace FormsEditorEx.GenExam.Records
{
    public class CoagulogramRecord : IExamRecord
    {
        public DateTime? Date { get; set; }
        public string ProthrombinTime { get; set; }
        public string Prothrombin { get; set; }
        public string MNO { get; set; }
        public string A4TV { get; set; }
        public string Fibrinogen { get; set; }
        public string ThrombinTime { get; set; }
        public string Ddimer { get; set; }

        public string GetString()
        {
            string temp = string.Empty;

            if (Date.HasValue)
            {
                //temp += Utility.DateToShortString(Date.Value) + Symbols.NBSpace + Symbols.NBSpace + "Коагулограмма: ";
                temp += "Коагулограмма: ";

                bool isAllEmpty = true;

                if (!string.IsNullOrWhiteSpace(ProthrombinTime))
                {
                    isAllEmpty = false;
                    temp += "протромбиновое время" + Symbols.NBSpace + Symbols.Dash + Symbols.NBSpace + ProthrombinTime + Symbols.NBSpace + "сек";
                }

                if (!string.IsNullOrWhiteSpace(Prothrombin))
                {
                    if (!isAllEmpty)
                        temp += ", ";
                    else
                        isAllEmpty = false;

                    temp += "протромбин (по Квику)" + Symbols.NBSpace + Symbols.Dash + Symbols.NBSpace + Prothrombin + "%";
                }

                if (!string.IsNullOrWhiteSpace(MNO))
                {
                    if (!isAllEmpty)
                        temp += ", ";
                    else
                        isAllEmpty = false;

                    temp += "МНО" + Symbols.NBSpace + Symbols.Dash + Symbols.NBSpace + MNO;
                }

                if (!string.IsNullOrWhiteSpace(A4TV))
                {
                    if (!isAllEmpty)
                        temp += ", ";
                    else
                        isAllEmpty = false;

                    temp += "АЧТВ" + Symbols.NBSpace + Symbols.Dash + Symbols.NBSpace + A4TV + Symbols.NBSpace + "сек";
                }

                if (!string.IsNullOrWhiteSpace(Fibrinogen))
                {
                    if (!isAllEmpty)
                        temp += ", ";
                    else
                        isAllEmpty = false;

                    temp += "фибриноген" + Symbols.NBSpace + Symbols.Dash + Symbols.NBSpace + Fibrinogen + Symbols.NBSpace + "г/л";
                }

                if (!string.IsNullOrWhiteSpace(ThrombinTime))
                {
                    if (!isAllEmpty)
                        temp += ", ";
                    else
                        isAllEmpty = false;

                    temp += "тромбиновое время" + Symbols.NBSpace + Symbols.Dash + Symbols.NBSpace + ThrombinTime + Symbols.NBSpace + "сек";
                }

                if (!string.IsNullOrWhiteSpace(Ddimer))
                {
                    if (!isAllEmpty)
                        temp += ", ";
                    else
                        isAllEmpty = false;

                    temp += "D-димер" + Symbols.NBSpace + Symbols.Dash + Symbols.NBSpace + Ddimer + Symbols.NBSpace + "нг/мл";
                }

                temp += ".";
            }

            return temp;
        }
        public static GenExamRecord GetTestRecord()
        {
            CoagulogramRecord rec = new CoagulogramRecord()
            {
                Date = new DateTime(2020, 3, 21),
                ProthrombinTime = "11,5",
                Prothrombin = "103",
                MNO = "0,98",
                A4TV = "33,1",
                Fibrinogen = "3,8",
                ThrombinTime = "12,9",
                Ddimer = "173",
            };
            return new GenExamRecord() { Date = rec.Date, Result = rec.GetString() };
        }
    }
}
