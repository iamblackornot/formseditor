﻿using System;
using System.Collections.Generic;
using System.Linq;
using SharedLibrary;
using SharedLibrary.Settings;

namespace FormsEditorEx
{
    public partial class MainFrame
    {
        private void CommonFieldsToPatient()
        {
            Patient p = Patient.Instance;
            TemporaryData td = Patient.Instance.PatientData.TemporaryFields;

            PersonalInfo pi = Patient.Instance.PatientData.PersonalInfo;

            pi.Index = !string.IsNullOrWhiteSpace(tbIndex.Text) ? tbIndex.Text.Trim() : null;
            pi.Oblast = !string.IsNullOrWhiteSpace(tbOblast.Text) ? tbOblast.Text.Trim() : null;
            pi.Town = !string.IsNullOrWhiteSpace(cbTown.Text) ? cbTown.Text.Trim() : null;

            pi.IsVillage = rbVillage.Checked ? true : (bool?)null;
            pi.StreetType = cbStreetType.SelectedIndex > -1 ? cbStreetType.SelectedIndex : (int?)null;

            pi.Street = !string.IsNullOrWhiteSpace(cbStreet.Text) ? cbStreet.Text.Trim() : null;
            pi.Building = !string.IsNullOrWhiteSpace(tbBuilding.Text) ? tbBuilding.Text.Trim() : null;
            pi.Korpus = !string.IsNullOrWhiteSpace(tbKorpus.Text) ? tbKorpus.Text.Trim() : null;
            pi.Flat = !string.IsNullOrWhiteSpace(tbFlat.Text) ? tbFlat.Text.Trim() : null;

            if (cbBloodGroup.Enabled)
                p.BloodGroup = cbBloodGroup.SelectedIndex >= 0 ? cbBloodGroup.SelectedIndex : (int?)null;
            if (cbRh.Enabled)
                p.Rh = cbRh.SelectedIndex >= 0 ? cbRh.SelectedIndex : (int?)null;
            if(cbDisability.Enabled)
                p.PatientData.PersonalInfo.Disability = cbDisability.SelectedIndex >= 0 ? cbDisability.SelectedIndex : (int?)null;

            if (cbIO.Enabled)
            {
                td.IO = cbIO.Checked ? 1 : (int?)null;
                td.SuperIntendent = (tbSuperintendent.SelectedItem as Personnel).PersonnelId.Value;
            }
            else
            {
                td.SuperIntendent = td.SuperIntendent.HasValue
                    ? td.SuperIntendent
                    : AppConfig.Instance.Staff.Departments.HeadByDepartment[p.Department.Value].PersonnelId;
            }

            if (cbSuperIntendentFormatted.Enabled)
                td.SuperIntendentFormatted = cbSuperIntendentFormatted.Text.Trim();

            td.VTEORisk = cbVTEORisk.SelectedIndex >= 0 ? cbVTEORisk.SelectedIndex : (int?)null;
            td.GORisk = cbGORisk.SelectedIndex >= 0 ? cbGORisk.SelectedIndex : (int?)null;

            List<int> items = new List<int>();

            for (int i = 0; i < clbVTEOPrevention.Items.Count; i++)
                if (clbVTEOPrevention.GetItemChecked(i))
                    items.Add(i);

            td.VTEOPrevention = items.Count > 0 ? string.Join<int>(",", items) : null;
        }

        public void AnamnesisFieldsToPatient()
        {
            CommonFieldsToPatient();

            Patient p = Patient.Instance;
            TemporaryData td = p.PatientData.TemporaryFields;
            
            p.DateExam = DateTime.TryParse(tbDateExam.Text, out DateTime date) ? date : (DateTime?)null;
            td.TimeExam = tbTimeExam.Text;

            td.GenCondition = cbGenCondition.SelectedIndex >= 0 ? cbGenCondition.SelectedIndex : (int?)null;
            td.Nutrition = cbNutrition.SelectedIndex >= 0 ? cbNutrition.SelectedIndex : (int?)null;

            string temp = rtbComplaints.Text.Trim();
            p.Complaints = temp == Static.ComplaintsDefault ? null : temp;

            temp = rtbDiagnosisPre.Text.Trim();
            p.Diagnosis = td.DiagnosisPre = !string.IsNullOrEmpty(temp) ? temp : null;

            p.Complications = td.ComplicationsPre = rtbComplicationsPre.IsCueOn ? null : rtbComplicationsPre.Text.Trim();

            temp = rtbStatusLocalisPre.Text.Trim();
            p.StatusLocalis = td.StatusLocalisPre = temp == Static.StatusLocalisPreCue[p.Department.Value] ? null : temp;

            p.DiseaseMoreInfo = !string.IsNullOrWhiteSpace(rtbDiseaseMoreInfo.Text) ? rtbDiseaseMoreInfo.Text.Trim() : null;
            td.PlannedExamination = !string.IsNullOrWhiteSpace(rtbPlannedExamination.Text) ? rtbPlannedExamination.Text.Trim() : null;
            td.Assignments = !string.IsNullOrWhiteSpace(rtbAssignments.Text) ? rtbAssignments.Text.Trim() : null;

            td.IsChemTherapy = cbChemTherapy.Checked ? true : (bool?)null;

            temp = !string.IsNullOrWhiteSpace(rtbPlannedOperationScope.Text) ? rtbPlannedOperationScope.Text.Trim() : null;

            if (!cbVMP.Checked)
            {

                if (cbChemTherapy.Checked || !Static.DoesOperativeTreatment[Patient.Instance.Department.Value])
                    p.Medication = temp;
                else
                    p.Treatment = temp;

                td.PlannedOperationScope = temp;
                td.IsVMP = null;
            }
            else
            {
                td.IsVMP = true;
            }

            p.Accompanying = td.AccompDiagnosisPre = rtbAccompanying.IsCueOn ? null : rtbAccompanying.Text.Trim();
            td.AccompMedicationPre = rtbAccompMedicationPre.IsCueOn ? null : rtbAccompMedicationPre.Text.Trim();
            p.AccompMedication = rtbAccompMedicationPost.IsCueOn ? null : rtbAccompMedicationPost.Text.Trim();

            td.MedicalAllergy = rtbMedicalAllergy.IsCueOn ? null : rtbMedicalAllergy.Text.Trim();

            if(string.IsNullOrEmpty(td.LifeAnamnesisPre))
            {
                td.LifeAnamnesisPre = p.PatientData.LifeAnamnesisData.Serialize();
            }

            if (Static.DoesOperativeTreatment[Patient.Instance.Department.Value] && !td.IsChemTherapy.HasValue)
                p.Stage = (int)MainFrameState.PreoperativeEpicrisis;
            else
                p.Stage = (int)MainFrameState.DischargeSummary;

        }
        public void PreoperativeEpicrisisFieldsToPatient()
        {
            CommonFieldsToPatient();

            Patient p = Patient.Instance;
            TemporaryData td = p.PatientData.TemporaryFields;
            td.MedicalAllergy = rtbMedicalAllergy.IsCueOn ? null : rtbMedicalAllergy.Text.Trim();

            Patient.Instance.Stage = (int)MainFrameState.DischargeSummary;
        }
        public void DischargeSummaryFieldsToPatient()
        {
            CommonFieldsToPatient();

            Patient p = Patient.Instance;
            TemporaryData td = p.PatientData.TemporaryFields;

            string temp = rtbDiagnosisPost.Text.Trim();

            p.Diagnosis = !string.IsNullOrEmpty(temp) ? temp : null;
            p.Complications = !rtbComplicationsPost.IsCueOn ? rtbComplicationsPost.Text.Trim() : null;
            p.Accompanying = !rtbAccompanyingPost.IsCueOn ? rtbAccompanyingPost.Text.Trim() : null;

            p.MKBDiagnosis = !string.IsNullOrEmpty(tbMKBDiagnosis.Text) ? tbMKBDiagnosis.Text.Trim() : null;
            p.MKBComplications = !string.IsNullOrEmpty(tbMKBComplications.Text) ? tbMKBComplications.Text.Trim() : null;
            p.MKBAccompanying = !string.IsNullOrEmpty(tbMKBAccompanying.Text) ? tbMKBAccompanying.Text.Trim() : null;

            p.Medication = !rtbMedication.IsCueOn ? rtbMedication.Text.Trim() : null;
            p.Treatment = !rtbTreatment.IsCueOn ? rtbTreatment.Text.Trim() : null;

            td.TreatmentResult = cbTreatmentResult.SelectedIndex >= 0 ? cbTreatmentResult.SelectedIndex : (int?)null;
            td.ConditionPost = cbCondition.SelectedIndex >= 0 ? cbCondition.SelectedIndex : (int?)null;
            td.WorkCapacity = cbWorkCapacity.SelectedIndex >= 0 ? cbWorkCapacity.SelectedIndex : (int?)null;

            p.DateDischarge = DateTime.TryParse(tbDateDischarge.Text, out DateTime dt) ? dt : (DateTime?)null;

            if (cbSickLeave.Checked)
            {
                temp = tbSickLeaveNum.Text.Trim();
                td.SickLeaveNum = !string.IsNullOrEmpty(temp) ? temp : null;

                td.SickLeaveStart = DateTime.TryParse(tbSickLeaveStart.Text, out dt) ? dt : (DateTime?)null;
                td.SickLeaveEnd = DateTime.TryParse(tbSickLeaveEnd.Text, out dt) ? dt : (DateTime?)null;

                if (cbSickLeaveExt.Checked)
                {
                    td.SickLeaveExtStart = DateTime.TryParse(tbSickLeaveExtStart.Text, out dt) ? dt : (DateTime?)null;
                    td.SickLeaveExtEnd = DateTime.TryParse(tbSickLeaveExtEnd.Text, out dt) ? dt : (DateTime?)null;
                }
                else
                {
                    td.SickLeaveExtStart = null;
                    td.SickLeaveExtEnd = null;
                }

                td.SickLeaveEndType = cbSickLeaveEndType.SelectedIndex >= 0 ? cbSickLeaveEndType.SelectedIndex : (int?)null;
            }
            else
            {
                td.SickLeaveNum = null;

                td.SickLeaveStart = null;
                td.SickLeaveEnd = null;

                td.SickLeaveExtStart = null;
                td.SickLeaveExtEnd = null;

                td.SickLeaveEndType = null;
            }

            p.Stage = (int)MainFrameState.Discharge;
        }

        private void PatientToCommonFields()
        { 
            Patient p = Patient.Instance;
            TemporaryData td = p.PatientData.TemporaryFields;

            cbDepartment.SelectedIndex = p.Department.HasValue ? p.Department.Value : -1;
            cbDoctors.Text = p.Doctor.HasValue ? AppConfig.Instance.Staff[p.Doctor.Value].ShortName : string.Empty;

            tbClinicalRecordNum.Text = p.ClinicalRecordNum.EmptyIfNull();
            tbAmbuCardNum.Text = p.AmbuCardNum.EmptyIfNull();

            tbSecondName.Text = p.SecondName.EmptyIfNull();
            tbFirstName.Text = p.FirstName.EmptyIfNull();
            tbThirdName.Text = p.ThirdName.EmptyIfNull();
            cbSex.SelectedIndex = p.Sex.HasValue ? p.Sex.Value : -1;
            tbDateBirth.Text = p.DateBirth.HasValue ? Utility.DateToShortString(p.DateBirth.Value) : string.Empty;

            PersonalInfo pi = Patient.Instance.PatientData.PersonalInfo;

            tbIndex.Text = pi.Index.EmptyIfNull();
            tbOblast.Text = pi.Oblast ?? Static.OblastDefault;
            cbTown.Text = pi.Town.EmptyIfNull();
            rbVillage.Checked = pi.IsVillage.HasValue? true : false;

            if (pi.StreetType.HasValue)
                cbStreetType.SelectedIndex = pi.StreetType.Value;

            cbStreet.Text = pi.Street.EmptyIfNull();
            tbBuilding.Text = pi.Building.EmptyIfNull();
            tbKorpus.Text = pi.Korpus.EmptyIfNull();
            tbFlat.Text = pi.Flat.EmptyIfNull();

            cbBloodGroup.SelectedIndex = p.BloodGroup.HasValue ? p.BloodGroup.Value : -1;
            cbRh.SelectedIndex = p.Rh.HasValue ? p.Rh.Value : -1;

            int? dis = p.PatientData.PersonalInfo.Disability;
            cbDisability.SelectedIndex = dis.HasValue ? dis.Value : -1;

            tbDateExam.Text = p.DateExam.HasValue ? Utility.DateToShortString(p.DateExam.Value) : string.Empty;
            tbTimeExam.Text = td.TimeExam.EmptyIfNull();

            cbGenCondition.SelectedIndex = td.GenCondition.HasValue ? td.GenCondition.Value : -1;
            cbNutrition.SelectedIndex = td.Nutrition.HasValue ? td.Nutrition.Value : -1;

            cbIO.Checked = td.IO.HasValue;

            SetSuperIntendent(p.Department, td.SuperIntendent);

            if (!string.IsNullOrEmpty(td.SuperIntendentFormatted))
                cbSuperIntendentFormatted.Text = td.SuperIntendentFormatted;

            rtbComplaints.SetText(!string.IsNullOrEmpty(p.Complaints) ? p.Complaints : Static.ComplaintsDefault);
            rtbDiseaseMoreInfo.Text = p.DiseaseMoreInfo.EmptyIfNull();
            rtbPlannedExamination.Text = td.PlannedExamination.EmptyIfNull();
            rtbAssignments.Text = td.Assignments.EmptyIfNull();
            rtbMedicalAllergy.SetText(td.MedicalAllergy.EmptyIfNull());

            cbVTEORisk.SelectedIndex = td.VTEORisk.HasValue ? td.VTEORisk.Value : -1;
            cbGORisk.SelectedIndex = td.GORisk.HasValue ? td.GORisk.Value : -1;

            if (!string.IsNullOrEmpty(td.VTEOPrevention))
            {
                List<string> items = td.VTEOPrevention.Split(',').ToList<string>();

                foreach (string item in items)
                    if (int.TryParse(item, out int index))
                        clbVTEOPrevention.SetItemChecked(index, true);
            }

            tbDateDischarge.Text = p.DateDischarge.HasValue ? Utility.DateToShortString(p.DateDischarge.Value) : string.Empty;
        }
        public void PatientToAnamnesisFields()
        {
            PatientToCommonFields();

            Patient p = Patient.Instance;
            TemporaryData td = p.PatientData.TemporaryFields;

            rtbDiagnosisPre.Text = string.IsNullOrEmpty(td.DiagnosisPre) ? p.Diagnosis.EmptyIfNull() : td.DiagnosisPre;
            rtbComplicationsPre.SetText(string.IsNullOrEmpty(td.ComplicationsPre) ? p.Complications.EmptyIfNull() : td.ComplicationsPre.EmptyIfNull());

            string temp = string.Empty;

            if(string.IsNullOrEmpty(td.StatusLocalisPre))
            {
                rtbStatusLocalisPre.Text = !string.IsNullOrEmpty(p.StatusLocalis) ? p.StatusLocalis : Static.StatusLocalisPreCue[p.Department.Value];
            }
            else
                rtbStatusLocalisPre.Text = !string.IsNullOrEmpty(td.StatusLocalisPre) ? td.StatusLocalisPre : Static.StatusLocalisPreCue[p.Department.Value];

            rtbAccompMedicationPre.SetText(string.IsNullOrEmpty(td.TimeExam) ? p.AccompMedication.EmptyIfNull() : td.AccompMedicationPre.EmptyIfNull());
            rtbAccompMedicationPost.SetText(!string.IsNullOrEmpty(td.TimeExam) ? p.AccompMedication.EmptyIfNull() : string.Empty);

            cbChemTherapy.Checked = td.IsChemTherapy.HasValue;
            cbVMP.Checked = td.IsVMP.HasValue;
            rtbPlannedOperationScope.Text = td.PlannedOperationScope.EmptyIfNull();
        }
        public void PatientToPreoperativeEpicrisisFields()
        {
            PatientToCommonFields();

            Patient p = Patient.Instance;
            TemporaryData td = p.PatientData.TemporaryFields;

            rtbDiagnosisPre.Text = td.DiagnosisPre.EmptyIfNull();
            rtbComplicationsPre.SetText(td.ComplicationsPre.EmptyIfNull());
            rtbStatusLocalisPre.Text = !string.IsNullOrEmpty(td.StatusLocalisPre) ? td.StatusLocalisPre : Static.StatusLocalisPreCue[p.Department.Value];
            rtbAccompMedicationPre.SetText(td.AccompMedicationPre.EmptyIfNull());
            rtbAccompMedicationPost.SetText(p.AccompMedication.EmptyIfNull());
            rtbPlannedOperationScope.Text = td.PlannedOperationScope.EmptyIfNull();
        }
        public void PatientToDischargeSummaryFields()
        {
            PatientToPreoperativeEpicrisisFields();

            Patient p = Patient.Instance;
            TemporaryData td = p.PatientData.TemporaryFields;

            rtbDiagnosisPost.Text = p.Diagnosis.EmptyIfNull();
            rtbComplicationsPost.SetText(p.Complications.EmptyIfNull());
            rtbAccompanyingPost.SetText(p.Accompanying.EmptyIfNull());

            tbMKBDiagnosis.Text = p.MKBDiagnosis.EmptyIfNull();
            tbMKBComplications.Text = p.MKBComplications.EmptyIfNull();
            tbMKBAccompanying.Text = p.MKBAccompanying.EmptyIfNull();

            rtbMedication.SetText(p.Medication.EmptyIfNull());
            rtbTreatment.SetText(p.Treatment.EmptyIfNull());

            cbTreatmentResult.SelectedIndex = td.TreatmentResult.HasValue ? td.TreatmentResult.Value : -1; 
            cbCondition.SelectedIndex = td.ConditionPost.HasValue ? td.ConditionPost.Value : -1;
            cbWorkCapacity.SelectedIndex = td.WorkCapacity.HasValue ? td.WorkCapacity.Value : -1;

            if (!string.IsNullOrEmpty(td.SickLeaveNum))
            {
                tbSickLeaveNum.Text = td.SickLeaveNum;
                cbSickLeave.Checked = true;

                tbSickLeaveStart.Text = td.SickLeaveStart.HasValue ? Utility.DateToShortString(td.SickLeaveStart.Value) : string.Empty;
                tbSickLeaveEnd.Text = td.SickLeaveEnd.HasValue ? Utility.DateToShortString(td.SickLeaveEnd.Value) : string.Empty;

                if (td.SickLeaveExtStart.HasValue)
                {
                    cbSickLeaveExt.Checked = true;

                    tbSickLeaveExtStart.Text = td.SickLeaveExtStart.HasValue ? Utility.DateToShortString(td.SickLeaveExtStart.Value) : string.Empty;
                    tbSickLeaveExtEnd.Text = td.SickLeaveExtEnd.HasValue ? Utility.DateToShortString(td.SickLeaveExtEnd.Value) : string.Empty;
                }

                cbSickLeaveEndType.SelectedIndex = td.SickLeaveEndType.HasValue ? td.SickLeaveEndType.Value : -1;
            }
        }
        public void PatientToReadOnlyFields()
        {
            PatientToCommonFields();

            Patient p = Patient.Instance;

            rtbDiagnosisPre.Text = p.Diagnosis.EmptyIfNull();
            rtbComplicationsPre.SetText(p.Complications.EmptyIfNull());
            rtbAccompanying.SetText(p.Accompanying.EmptyIfNull());

            rtbStatusLocalisPre.Text = p.StatusLocalis.EmptyIfNull();
            rtbAccompMedicationPre.SetText(p.AccompMedication.EmptyIfNull());
            tbDateDischarge.Text = Utility.DateToShortString(p.DateDischarge);
        }
        public void LoadPatient(int dep_id, long doctorId)
        {
            Enabled = true;
            Department = dep_id;
            Doctor = doctorId;

            ClearAndReset();
            //Refresh();

            Patient p = Patient.Instance;

            int stage = p.Stage ?? 0;
            Type t = Static.MainFrameStateType[(MainFrameState)stage];

            state = (IMainFrameState) Activator.CreateInstance(t, this);

            Text = $"{p.SecondName.EmptyIfNull()} {p.FirstName?[0]}.{p.ThirdName?[0]}.";

            ucStatusBar.ShowOK("Пациент загружен");
        }
    }
}
