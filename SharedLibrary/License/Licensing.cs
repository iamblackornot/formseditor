﻿using AppSoftware.LicenceEngine.Common;
using AppSoftware.LicenceEngine.KeyVerification;
using Microsoft.Win32;
using System;
using System.IO;
using System.Windows.Forms;
using System.Management;
using System.Security.AccessControl;
using System.Security.Cryptography;
using System.Security.Principal;
using System.Text;

namespace SharedLibrary
{
    public static class Licensing
    {
        private static readonly DateTime Trial = new DateTime(2018, 10, 29, 22, 0, 0, DateTimeKind.Local);
        private static readonly DateTime Check = new DateTime(2018, 11, 13, 22, 0, 0, DateTimeKind.Local);
        public static int GetHardwareId(double date)
        {
            string cpuInfo = string.Empty;
            ManagementClass mc = new ManagementClass("win32_processor");
            ManagementObjectCollection moc = mc.GetInstances();

            foreach (ManagementObject mo in moc)
            {
                if (cpuInfo == "")
                {
                    //Get only the first CPU's ID
                    cpuInfo = mo.Properties["processorID"].Value.ToString();
                    break;
                }
            }

            //MessageBox.Show(cpuInfo);

            string drive = string.Empty;

            foreach (DriveInfo compDrive in DriveInfo.GetDrives())
            {
                if (compDrive.IsReady)
                {
                    drive = compDrive.RootDirectory.ToString();
                    break;
                }
            }

            if (drive.EndsWith(":\\"))
            {
                //C:\ -> C
                drive = drive.Substring(0, drive.Length - 2);
            }

            ManagementObject disk = new ManagementObject(@"win32_logicaldisk.deviceid=""" + drive + @":""");
            disk.Get();

            string volumeSerial = disk["VolumeSerialNumber"].ToString();
            disk.Dispose();

            //MessageBox.Show(volumeSerial);

            string guid = string.Empty;
            string install = string.Empty;

            using (RegistryKey key = Registry.LocalMachine.OpenSubKey("SOFTWARE\\Microsoft\\Cryptography"))
            {
                if (key != null)
                {
                    Object o = key.GetValue("dMachineGuid");
                    if (o != null)
                        guid = o.ToString();
                }
            }

            using (RegistryKey key = Registry.LocalMachine.OpenSubKey("SOFTWARE\\Microsoft\\Windows NT\\CurrentVersion\\"))
            {
                if (key != null)
                {
                    Object o = key.GetValue("InstallDate");
                    if (o != null)
                        install = o.ToString();
                }
            }

            MD5 md5Hasher = MD5.Create();
            var hashed = md5Hasher.ComputeHash(Encoding.UTF8.GetBytes(cpuInfo + volumeSerial + guid + install + date.ToString()));

            return BitConverter.ToInt32(hashed, 0);
        }
        public static void SaveActivation(string code, double date, string hklmSoftwareFolderName)
        {
            RegistryKey key;

            SecurityIdentifier sid = new SecurityIdentifier(WellKnownSidType.WorldSid, null);
            NTAccount account = sid.Translate(typeof(NTAccount)) as NTAccount;

            using (key = Registry.LocalMachine.CreateSubKey($"SOFTWARE\\{hklmSoftwareFolderName}\\", RegistryKeyPermissionCheck.ReadWriteSubTree))
            {
                RegistrySecurity rs = key.GetAccessControl();

                RegistryAccessRule rar = new RegistryAccessRule(
                    account.ToString(),
                    RegistryRights.FullControl,
                    InheritanceFlags.ContainerInherit | InheritanceFlags.ObjectInherit,
                    PropagationFlags.None,
                    AccessControlType.Allow);

                rs.AddAccessRule(rar);
                key.SetAccessControl(rs);

                key.SetValue("key", code, RegistryValueKind.String);
                key.SetValue("version", date, RegistryValueKind.DWord);
            }
        }
        public static PkvLicenceKeyResult CheckKey(int seed, string key)
        {
            KeyByteSet[] keyByteSets = new[]
                                    {
                                        new KeyByteSet(3, 11, 115, 69),
                                        new KeyByteSet(4, 2, 93, 41),
                                        new KeyByteSet(7, 89, 45,142)
                                    };

            var pkvKeyCheck = new PkvKeyCheck();

            return pkvKeyCheck.CheckKey(seed, key, keyByteSets, 8, null);
        }
        public static bool LicenseCheck(string hklmSoftwareFolderName)
        {
            bool res = false;

            if (DateTime.Now > Check)
            {
                if (DateTime.Now > Trial)
                {
                    string code = string.Empty;

                    SecurityIdentifier sid = new SecurityIdentifier(WellKnownSidType.WorldSid, null);
                    NTAccount account = sid.Translate(typeof(NTAccount)) as NTAccount;

                    using (RegistryKey key = Registry.LocalMachine.OpenSubKey($"SOFTWARE\\{hklmSoftwareFolderName}\\", RegistryKeyPermissionCheck.ReadWriteSubTree))
                    {
                        bool isActivated = false;

                        if (key != null)
                        {
                            RegistrySecurity rs = key.GetAccessControl();

                            // Creating registry access rule for 'Everyone' NT account
                            RegistryAccessRule rar = new RegistryAccessRule(
                                account.ToString(),
                                RegistryRights.FullControl,
                                InheritanceFlags.ContainerInherit | InheritanceFlags.ObjectInherit,
                                PropagationFlags.None,
                                AccessControlType.Allow);

                            rs.AddAccessRule(rar);
                            key.SetAccessControl(rs);

                            object o = key.GetValue("version");
                            if (o != null)
                            {
                                double date = Convert.ToDouble(o);

                                int seed = Licensing.GetHardwareId(date);

                                o = key.GetValue("key");
                                if (o != null)
                                {
                                    code = o.ToString();

                                    if (Licensing.CheckKey(seed, code) == PkvLicenceKeyResult.KeyGood)
                                    {
                                        isActivated = true;
                                        return true;
                                    }
                                }
                            }
                        }

                        if (!isActivated)
                        {
                            DialogResult dres;
                            using (LicenseForm licForm = new LicenseForm())
                            {
                                //licForm.Owner = this;
                                dres = licForm.ShowDialog();
                            }

                            if (dres == DialogResult.OK)
                                res = true;
                        }
                    }
                }
            }
            else
            {
                MessageBox.Show("Неправильно настроено время на данном ПК");
            }

            return res;
        }
    }
}
