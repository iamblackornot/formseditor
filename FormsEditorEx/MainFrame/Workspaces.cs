﻿using System.Drawing;
using System.Windows.Forms;

namespace FormsEditorEx
{
    public partial class MainFrame
    {
        public void SetAnamnesisWorkspace(string actionButtonText)
        {
            bool isPatientAvailable = CheckIfPatientAvailable();

            btAction.Enabled = isPatientAvailable;
            btAction.Text = actionButtonText;

            btRevert.Enabled = false;
            btVMP.Enabled = false;

            if (isPatientAvailable)
            {
                SetСommonFieldsAvailability();

                SetPreoperativeEpicrisisOnlyFielsAvailability(true);
                SetDischargeSummaryOnlyFielsAvailability(true);

                SetAnamnesisOnlyFielsAvailability();

                cbDisability.Enabled = true;
                pnChemTherapyToggle.Visible = Static.DoesOperativeTreatment[Patient.Instance.Department.Value];
                pnVMPToggle.Visible = true;
            }
            else
                SetReadOnlyFieldsAvailability();

            ActiveControl = null;
            SelectNextControl(this, true, true, true, true);
        }
        public void SetPreoperativeEpicrisisWorkspace(string actionButtonText)
        {
            bool isPatientAvailable = CheckIfPatientAvailable();

            btAction.Enabled = btRevert.Enabled = btVMP.Enabled = isPatientAvailable;
            btAction.Text = actionButtonText;

            if (isPatientAvailable)
            {
                SetСommonFieldsAvailability();

                SetAnamnesisOnlyFielsAvailability(true);
                SetDischargeSummaryOnlyFielsAvailability(true);

                SetPreoperativeEpicrisisOnlyFielsAvailability();

                SetTextBoxAvailability(rtbMedicalAllergy, false);
                cbBloodGroup.Enabled = false;
                cbRh.Enabled = false;
            }
            else
                SetReadOnlyFieldsAvailability();

            ActiveControl = null;
            SelectNextControl(this, true, true, true, true);
        }
        public void SetDischargeSummaryWorkspace(string actionButtonText)
        {
            bool isPatientAvailable = CheckIfPatientAvailable();

            btAction.Enabled = btRevert.Enabled = btVMP.Enabled = isPatientAvailable;
            btAction.Text = actionButtonText;

            if (isPatientAvailable)
            {
                SetСommonFieldsAvailability();

                SetAnamnesisOnlyFielsAvailability(true);
                SetPreoperativeEpicrisisOnlyFielsAvailability(true);

                SetDischargeSummaryOnlyFielsAvailability();
            }
            else
                SetReadOnlyFieldsAvailability();

            ActiveControl = null;
            SelectNextControl(this, true, true, true, true);
        }
        public void SetDischargeWorkspace(string actionButtonText)
        {
            bool isPatientAvailable = CheckIfPatientAvailable();

            btAction.Enabled = btRevert.Enabled = isPatientAvailable;
            btAction.Text = actionButtonText;

            btVMP.Enabled = false;
            SetReadOnlyFieldsAvailability();

            btTransfusion.Enabled = btStatusLocalisPost.Enabled =
                btLifeAnamnesisPost.Enabled = btAdditionalInfo.Enabled = btRecommendations.Enabled = true;

            ActiveControl = null;
        }
        public void SetReadOnlyWorkspace(string actionButtonText)
        {
            bool isPatientAvailable = CheckIfPatientAvailable();

            btAction.Enabled = isPatientAvailable;
            btAction.Text = actionButtonText;

            btRevert.Enabled = btVMP.Enabled = false;
            SetReadOnlyFieldsAvailability();

            ActiveControl = null;
        }
        private void SetReadOnlyFieldsAvailability()
        {
            SetСommonFieldsAvailability(true);

            SetAnamnesisOnlyFielsAvailability(true);
            SetPreoperativeEpicrisisOnlyFielsAvailability(true);
            SetDischargeSummaryOnlyFielsAvailability(true);

            int? dep = Patient.Instance.Department;

            btLifeAnamnesisPost.Enabled = btAdditionalInfo.Enabled = btRecommendations.Enabled 
                = (dep.HasValue && ((MainFrameState)Patient.Instance.Stage).In(MainFrameState.DischargeSummary, MainFrameState.Discharge)) 
                ? true : false;
        }
        private void SetTextBoxAvailability(TextBoxBase tb, bool toDisable = false)
        {
            if (tb is RichTextBox rtb)
            {
                rtb.BackColor = !toDisable ? SystemColors.Window : SystemColors.Control;
            }

            tb.ReadOnly = toDisable;
            tb.TabStop = !toDisable;
        }
        private void SetСommonFieldsAvailability(bool toDisable = false)
        {
            this.Enabled = true;

            Patient p = Patient.Instance;
            TemporaryData td = Patient.Instance.PatientData.TemporaryFields;

            SetTextBoxAvailability(tbClinicalRecordNum, toDisable || !string.IsNullOrEmpty(p.ClinicalRecordNum));
            SetTextBoxAvailability(tbAmbuCardNum, toDisable || !string.IsNullOrEmpty(p.AmbuCardNum));

            SetTextBoxAvailability(tbSecondName, toDisable || !string.IsNullOrEmpty(p.SecondName));
            SetTextBoxAvailability(tbFirstName, toDisable || !string.IsNullOrEmpty(p.FirstName));
            SetTextBoxAvailability(tbThirdName, toDisable || !string.IsNullOrEmpty(p.ThirdName));
            cbSex.Enabled = !toDisable && !p.Sex.HasValue;
            SetTextBoxAvailability(tbDateBirth, toDisable || p.DateBirth.HasValue);

            SetTextBoxAvailability(tbIndex, toDisable);
            SetTextBoxAvailability(tbOblast, toDisable);
            cbTown.Enabled = !toDisable;
            rbTown.Enabled = !toDisable;
            rbVillage.Enabled = !toDisable;
            cbStreetType.Enabled = !toDisable;
            cbStreet.Enabled = !toDisable;
            SetTextBoxAvailability(tbBuilding, toDisable);
            SetTextBoxAvailability(tbKorpus, toDisable);
            SetTextBoxAvailability(tbFlat, toDisable);

            cbBloodGroup.Enabled = !toDisable;
            cbRh.Enabled = !toDisable;
            cbDisability.Enabled = !toDisable && !p.PatientData.PersonalInfo.Disability.HasValue;
            btDisabilityListInfo.Enabled = !toDisable;

            cbIO.Enabled = !toDisable;
            tbSuperintendent.Enabled = !toDisable && td.IO.HasValue;
            cbSuperIntendentFormatted.Enabled = !toDisable && string.IsNullOrEmpty(td.SuperIntendentFormatted);

            cbVTEORisk.Enabled = !toDisable;
            cbGORisk.Enabled = !toDisable;
            clbVTEOPrevention.Enabled = !toDisable;

            pnVMPToggle.Visible = false;
            pnChemTherapyToggle.Visible = false;

            if (p.Department.HasValue && !Static.NeuroStatusEnabled[p.Department.Value])
                btOperTeamNeuro.Text = "Операционная бригада";
            else
                btOperTeamNeuro.Text = "Неврологический статус";

            btOperTeamNeuro.Enabled = (p.Department.HasValue ? Static.DoesOperativeTreatment[p.Department.Value] || Static.NeuroStatusEnabled[p.Department.Value] : true);

            btConclusions.TabStop = !toDisable;
            btCommissions.TabStop = !toDisable;
            btGenExam.TabStop = !toDisable;
            btInstrumentalExamination.TabStop = !toDisable;
        }
        private void SetAnamnesisOnlyFielsAvailability(bool toDisable = false)
        {
            SetTextBoxAvailability(tbDateExam, toDisable);
            SetTextBoxAvailability(tbTimeExam, toDisable);

            cbNutrition.Enabled = !toDisable;
            cbGenCondition.Enabled = !toDisable;

            btAnamnesis.TabStop = !toDisable;
            btLifeAnamnesis.TabStop = !toDisable;

            SetTextBoxAvailability(rtbComplaints, toDisable);
            SetTextBoxAvailability(rtbDiagnosisPre, toDisable);
            SetTextBoxAvailability(rtbComplicationsPre, toDisable);
            SetTextBoxAvailability(rtbStatusLocalisPre, toDisable);

            int? dep = Patient.Instance.Department;

            if (!toDisable && dep.HasValue && Static.DoesOperativeTreatment[dep.Value])
                btOperTeamNeuro.Enabled = false;

            btOperTeamNeuro.TabStop = !toDisable;

            cbVMP.Enabled = !toDisable;
            cbChemTherapy.Enabled = !toDisable;

            SetTextBoxAvailability(rtbDiseaseMoreInfo, toDisable);
            SetTextBoxAvailability(rtbPlannedExamination, toDisable);
            SetTextBoxAvailability(rtbPlannedOperationScope, toDisable);
            SetTextBoxAvailability(rtbAssignments, toDisable);

            SetTextBoxAvailability(rtbAccompanying, toDisable);
            SetTextBoxAvailability(rtbAccompMedicationPre, toDisable);
            SetTextBoxAvailability(rtbMedicalAllergy, toDisable);

            btAccompMedicationCopy.Enabled = !toDisable;

            SetTextBoxAvailability(rtbAccompMedicationPost, toDisable);

            cbSuperIntendentFormatted.Enabled = !toDisable;
            SetEnding();
        }
        private void SetPreoperativeEpicrisisOnlyFielsAvailability(bool toDisable = false)
        {
            int? dep = Patient.Instance.Department;
            btOperTeamNeuro.TabStop = !toDisable;
        }
        private void SetDischargeSummaryOnlyFielsAvailability(bool toDisable = false)
        {
            SetTextBoxAvailability(rtbDiagnosisPost, toDisable);
            SetTextBoxAvailability(rtbComplicationsPost, toDisable);
            SetTextBoxAvailability(rtbAccompanyingPost, toDisable);

            SetTextBoxAvailability(tbMKBDiagnosis, toDisable);
            SetTextBoxAvailability(tbMKBComplications, toDisable);
            SetTextBoxAvailability(tbMKBAccompanying, toDisable);


            SetTextBoxAvailability(rtbMedication, toDisable);
            SetTextBoxAvailability(rtbTreatment, toDisable);

            btTransfusion.Enabled = !toDisable;
            btStatusLocalisPost.Enabled = !toDisable;
            btLifeAnamnesisPost.Enabled = !toDisable;
            btAdditionalInfo.Enabled = !toDisable;
            btRecommendations.Enabled = !toDisable;

            btTransfusion.TabStop = !toDisable;
            btStatusLocalisPost.TabStop = !toDisable;
            btLifeAnamnesisPost.TabStop = !toDisable;
            btAdditionalInfo.TabStop = !toDisable;
            btRecommendations.TabStop = !toDisable;

            cbTreatmentResult.Enabled = !toDisable;
            cbCondition.Enabled = !toDisable;
            cbWorkCapacity.Enabled = !toDisable;

            SetTextBoxAvailability(tbDateDischarge, toDisable);

            cbSickLeave.Enabled = !toDisable;

            SetTextBoxAvailability(tbSickLeaveNum, toDisable);
            SetTextBoxAvailability(tbSickLeaveStart, toDisable);
            SetTextBoxAvailability(tbSickLeaveEnd, toDisable);

            cbSickLeaveExt.Enabled = cbSickLeave.Checked && !toDisable;
            SetTextBoxAvailability(tbSickLeaveExtStart, toDisable);
            SetTextBoxAvailability(tbSickLeaveExtEnd, toDisable);

            cbSickLeaveEndType.Enabled = cbSickLeave.Checked && !toDisable;
        }
    }
}
