﻿using System;
using FormsEditorEx.GenExam.Records;
using SharedLibrary;

namespace FormsEditorEx.GenExam.Tabs
{
    public partial class ucOAKTab : ucExamTabBase
    {
        public ucOAKTab()
        {
            InitializeComponent();
            ResizePanel();

            tbOAKDate.Validating += DateTextBox_Validating;
            tbOAKDate.KeyPress += DateTextBox_KeyPress;

            tbErythrocytes.KeyDown += TextBox_KeyDown;
            tbLeucocytes.KeyDown += TextBox_KeyDown;
            tbHemoglobin.KeyDown += TextBox_KeyDown;
            tbTrombocytes.KeyDown += TextBox_KeyDown;
            tbNeutrophils.KeyDown += TextBox_KeyDown;
        }

        private void ResizePanel()
        {
            pnContent.Location = ClientRectangle.Location;
            ClientSize = pnContent.Size;
        }

        public override bool AddExamRecord()
        {
            bool res = AddRecord(GenExamType.OAK, FieldsToRecord());

            if (res)
                Clear();

            return res;
        }
        public override void FillTestData()
        {
            tbOAKDate.Text = "20.03.2020";

            tbErythrocytes.Text = "4,6";
            tbLeucocytes.Text = "8,5";
            tbHemoglobin.Text = "131";
            tbTrombocytes.Text = "339";
            tbNeutrophils.Text = "47";
        }

        private void Clear()
        {
            tbOAKDate.Clear();
            tbErythrocytes.Clear();
            tbLeucocytes.Clear();
            tbHemoglobin.Clear();
            tbTrombocytes.Clear();
            tbNeutrophils.Clear();
        }

        private OAKRecord FieldsToRecord()
        {
            if (tbOAKDate.MaskCompleted && DateTime.TryParse(tbOAKDate.Text, out DateTime date))
            {
                return new OAKRecord()
                {
                    Date = date,
                    Erythrocytes = !string.IsNullOrWhiteSpace(tbErythrocytes.Text) ? tbErythrocytes.Text : null,
                    Leucocytes = !string.IsNullOrWhiteSpace(tbLeucocytes.Text) ? tbLeucocytes.Text : null,
                    Hemoglobin = !string.IsNullOrWhiteSpace(tbHemoglobin.Text) ? tbHemoglobin.Text : null,
                    Trombocytes = !string.IsNullOrWhiteSpace(tbTrombocytes.Text) ? tbTrombocytes.Text : null,
                    Neutrophils = !string.IsNullOrWhiteSpace(tbNeutrophils.Text) ? tbNeutrophils.Text : null
                };
            }
            else return null;
        }
        //public override void RecordToFields()
        //{
        //    OAKRecord oak = GenExamResult.Instance.OAK;

        //    if (oak.Date.HasValue)
        //    {
        //        tbOAKDate.Text = Utility.DateToShortString(oak.Date.Value);

        //        tbErythrocytes.Text = !string.IsNullOrWhiteSpace(oak.Erythrocytes) ? oak.Erythrocytes : string.Empty;
        //        tbLeucocytes.Text = !string.IsNullOrWhiteSpace(oak.Leucocytes) ? oak.Leucocytes : string.Empty;
        //        tbHemoglobin.Text = !string.IsNullOrWhiteSpace(oak.Hemoglobin) ? oak.Hemoglobin : string.Empty;
        //        tbTrombocytes.Text = !string.IsNullOrWhiteSpace(oak.Trombocytes) ? oak.Trombocytes : string.Empty;
        //        tbNeutrophils.Text = !string.IsNullOrWhiteSpace(oak.Neutrophils) ? oak.Neutrophils : string.Empty;
        //    }
        //}
    }
}
