﻿using System;
using System.Drawing;
using System.Data;
using System.Windows.Forms;
using FormsEditorEx.Conclusion;
using SharedLibrary;

namespace FormsEditorEx
{
    public partial class DatatableEditControl : UserControl
    {
        private bool HasChanged = false;
        private bool isReadOnly;
        private DataTable dt;
        public event EventHandler OnClose;
        public DatatableEditControl(DataTable dt, bool isReadOnly)
        {
            this.dt = dt;
            this.isReadOnly = isReadOnly;

            InitializeComponent();
            InitGridView();
            InitComponents();
            CenterButtons();
#if !DEBUG
                        btTestData.Visible = false;
#endif
        }

        private void InitComponents()
        {
            btOK.Enabled = isReadOnly;
            btCancel.Visible = !isReadOnly;
            btAdd.Enabled = !isReadOnly;
        }

        private void InitGridView()
        {
            dgvRecords.AlternatingRowsDefaultCellStyle.BackColor = ColorTranslator.FromHtml("#EFEFEF");
            dgvRecords.DataSource = new BindingSource(dt, string.Empty);
        }
        private void CenterButtons()
        {
            //btAdd.Left = (pnButtons.Width - btAdd.Width - btChange.Width - btRemove.Width - 12 * 2) / 2;
            //btChange.Left = btAdd.Left + btAdd.Width + 12;
            //btRemove.Left = btChange.Left + btChange.Width + 12;

            btOK.Left = (pnButtons.Width - btOK.Width - (!isReadOnly ? btCancel.Width + 12 : 0)) / 2;
            btCancel.Left = btOK.Left + btOK.Width + 12;
        }
        private void dgvRecords_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            OnCellFormatting(sender as DataGridView, e);
        }
        protected virtual void OnCellFormatting(DataGridView dgv, DataGridViewCellFormattingEventArgs e) { }
        private void btTestData_Click(object sender, EventArgs e)
        {
            FillTestData();
        }
        protected virtual void FillTestData() { }
        private void dgvRecords_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
            OnDataBindingComplete(sender as DataGridView);
        }
        protected virtual void OnDataBindingComplete(DataGridView dgv) { }
        protected void SetChangeRemoveButtonsAvailability(bool toEnable)
        {
            btChange.Enabled = btRemove.Enabled = toEnable & !isReadOnly;
        }
        private void btRemove_Click(object sender, EventArgs e)
        {
            if (dgvRecords.CurrentCell != null)
            {
                DialogResult result = MessageBox.Show("Подтверджение удаления", "", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2);
                if (result == DialogResult.OK)
                {
                    int id = dgvRecords.CurrentRow.Index;
                    dgvRecords.Rows.Remove(dgvRecords.CurrentRow);
                    Utility.ChangeSelectedRowOnRemove(dgvRecords, id);
                    SetChangedState();
                }
            }
        }
        private void btAdd_Click(object sender, EventArgs e)
        {
            OpenAddRowDialog(dt);
        }
        protected virtual void OpenAddRowDialog(DataTable dt)
        {
            throw new NotImplementedException();
        }
        private DataRow GetCurrentRow()
        {
            CurrencyManager xCM = (CurrencyManager)dgvRecords.BindingContext[dgvRecords.DataSource, dgvRecords.DataMember];
            DataRowView xDRV = (DataRowView)xCM.Current;
            return xDRV.Row;
        }
        private void btChange_Click(object sender, EventArgs e)
        {
            if (dgvRecords.CurrentCell != null)
            {
                //int id = dgvRecords.CurrentRow.Index;
                //DataRow row = dt.Rows[id];

                OpenRowEditor(GetCurrentRow(), false);

                //OpenRowEditor(row, false);
            }
        }
        protected virtual void OpenRowEditor(DataRow row, bool isReadOnly)
        {
            throw new NotImplementedException();
        }
        protected void SetChangedState()
        {
            HasChanged = true;
            btOK.Enabled = true;
        }
        private void btOK_Click(object sender, EventArgs e)
        {
            if (HasChanged)
                dt.AcceptChanges();

            OnClose?.Invoke(this, EventArgs.Empty);
        }

        private void btCancel_Click(object sender, EventArgs e)
        {
            if (HasChanged)
            {
                if (MessageBox.Show("Закрыть окно без сохранения изменений?", "Требуется подтверждение",
                    MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                {
                    dt.RejectChanges();
                    OnClose?.Invoke(this, EventArgs.Empty);
                }
            }
            else
                OnClose?.Invoke(this, EventArgs.Empty);
        }

        private void dgvRecords_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if(e.RowIndex > -1)
                OpenRowEditor(GetCurrentRow(), true);
        }

        private void dgvRecords_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                dgvRecords.ClearSelection();

                if (e.RowIndex > -1)
                {
                    dgvRecords.CurrentCell = dgvRecords.Rows[e.RowIndex].Cells[0];
                    dgvRecords.Rows[e.RowIndex].Selected = true;
                }
            }
        }

        private void cmsMenu_Opening(object sender, System.ComponentModel.CancelEventArgs e)
        {
            var dgv_rel_mousePos = dgvRecords.PointToClient(MousePosition);
            var hti = dgvRecords.HitTest(dgv_rel_mousePos.X, dgv_rel_mousePos.Y);
            if (hti.RowIndex == -1)
            {
                e.Cancel = true;
            }
        }
    }
}
