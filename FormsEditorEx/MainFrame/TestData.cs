﻿using SharedLibrary;
using System;

namespace FormsEditorEx
{
    public partial class MainFrame
    {
        public void FillAnamnesisTestData()
        {
            Patient p = Patient.Instance;

            tbDateExam.Text = "19.03.2020";
            tbTimeExam.Text = "10:40";

            cbTown.Text = "Ярославль";
            cbStreet.Text = "Архангельский";
            cbStreetType.SelectedIndex = 2;
            tbBuilding.Text = "3";
            tbFlat.Text = "56";
            tbKorpus.Text = "2";
            tbIndex.Text = "150020";

            cbBloodGroup.SelectedIndex = (int)BloodGroup.III;
            cbRh.SelectedIndex = (int)Rh.Positive;
            cbDisability.SelectedIndex = (int)Disability.None;
            p.PatientData.TemporaryFields.DisabilityListInfo = "--сведения о листке нетрудоспособности--";

            cbGenCondition.SelectedIndex = (int)Condition.Satisfactory;
            cbNutrition.SelectedIndex = (int)Nutrition.Sufficient;

            p.PatientData.LifeAnamnesisData.PressureA = "140";
            p.Anamnez = "04.12.19 в КБ №9 выполнена ДЭЭ шейки матки. Гист.заключ. №46357-66 от 16.12.19: дисплазия шейки матки HgSIL," +
                "без описания краев резекции. 22.01.20 выполнен пересмотр стекол в ЯОКОБ. Рег.№ 2Б 633к/20: HGSIL, в краях некоторых фрагментов" +
                "определяются участки дисплазии высокой степени. Учитывая данные гистологического исследования, ранее проведенное хирургическое" +
                " лечение пациентке показана высокая ножевая ампутация шейки матки";

            //rtbStatusLocalisPre.Text = "Слизистая влагалища не изменена. Шейка матки не изменена. Тело матки не увеличено. Придатки не пальпируются. Инфильтратов в малом тазу нет";
            rtbDiagnosisPre.Text = "Атипическая гиперплазия эндометрия, 1б кл.гр.";
            rtbAccompanying.SetText("Гипертоническая болезнь 2 ст, риск 3.");
            rtbAccompMedicationPre.SetText("Лозартан 50 мг 1 р/д.");
            rtbAccompMedicationPost.SetText("Лозартан 75 мг 1 р/д.");
            rtbDiseaseMoreInfo.Text = "--доп_сведения_о_заболевании--";
            rtbPlannedOperationScope.Text = "экстирпация матки с придатками.";
            rtbPlannedExamination.Text = "--план_обследования--";
            rtbAssignments.Text = "--назначения--";
            rtbMedicalAllergy.SetText("--аллергия на препараты--");


            cbVTEORisk.SelectedIndex = (int)Risk.Low;
            cbGORisk.SelectedIndex = (int)Risk.Moderate;
            clbVTEOPrevention.SetItemChecked(0, true);
            

            p.PatientData.Conclusions.FillTestData();
            p.PatientData.Commissions.FillTestData();
            p.InstrExam = "12.03.20  Колоноскопия: Без патологии." + Environment.NewLine +
                "05.03.20  ФГДС: Обострение хронического гастрита." + Environment.NewLine +
                "19.06.19  ФЛГ: В норме." + Environment.NewLine +
                "24.01.20  УЗИ малого таза: Патология энометрия.";
            p.PatientData.GenExam.FillTestData();
        }
        public void FillPreoperativeEpicrisisTestData()
        {
            AppConfig.Instance.OperTeam.Assistant = "Земскова М.Е.";
            AppConfig.Instance.OperTeam.Anesthetist = "Караяни И.В.";
            AppConfig.Instance.OperTeam.OperSister = "Цибизова В.И.";
            AppConfig.Instance.OperTeam.BloodTransSpecialist = "Лилеев Д.В.";
        }
        public void FillDischargeSummaryTestData()
        {
            Patient p = Patient.Instance;
            TemporaryData td = p.PatientData.TemporaryFields;

            rtbDiagnosisPost.Text = "HGSIL ш/ матки.Хирургическое лечение от 11.03.20.";

            tbMKBDiagnosis.SetText("D09.01");
            tbMKBComplications.SetText("C08");
            tbMKBAccompanying.SetText("A23.1");

            rtbMedication.SetText("--применение лекарственных препаратов--");
            rtbTreatment.SetText("11.03.20 ОПЕРАЦИЯ: Высокая ножевая ампутация шейки матки. Послеоперационный период без особенностей.");

            td.Transfusion = "--трансфузии--";
            p.StatusLocalis = "Слизистая влагалища без особенностей. Культя шейки матки состоятельна. Тело матки и придатки не изменены.";
            td.AdditionalInfo = "--дополнительные сведения--";
            p.PatientData.TemporaryFields.Recommendations = "- явка на осмотр к гинекологу по месту жительства через 3 мес.";

            cbTreatmentResult.SelectedIndex = (int)TreatmentResult.Recovery;
            cbCondition.SelectedIndex = (int)Condition.Satisfactory;
            cbWorkCapacity.SelectedIndex = (int)WorkCapacity.TemporarilyDisabled;
            tbDateDischarge.Text = "27.03.2020";
            cbSickLeave.Checked = true;
            tbSickLeaveNum.Text = "373862630981";
            tbSickLeaveStart.Text = "19.03.2020";
            tbSickLeaveEnd.Text = "03.04.2020";
            cbSickLeaveExt.Checked = true;
            tbSickLeaveExtStart.Text = "04.04.2020";
            tbSickLeaveExtEnd.Text = "06.04.2020";
            cbSickLeaveEndType.SelectedIndex = 0;

            

            p.PatientData.Conclusions.FillTestPostData();
            p.PatientData.Commissions.FillTestPostData();
        }
    }
}
