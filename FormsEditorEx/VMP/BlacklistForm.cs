﻿using System;
using System.Windows.Forms;

namespace FormsEditorEx.VMP
{
    public partial class BlacklistForm : Form
    {
        public BlacklistForm()
        {
            InitializeComponent();
            CenterToScreen();
        }

        private void btAdmin_Click(object sender, EventArgs e)
        {
            using(AdminForm adminForm = new AdminForm())
            {
                adminForm.ShowDialog();
            }
            this.Close();
        }
    }
}
