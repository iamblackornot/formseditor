﻿using System;
using System.Linq;
using System.Collections.Generic;
using Newtonsoft.Json;
using FormsEditorEx.GenExam.Records;
using Newtonsoft.Json.Linq;
using SharedLibrary;

namespace FormsEditorEx.GenExam
{
    public class GenExamResult
    {
        public bool RecordsAdded { get; private set; } = false;
        private Dictionary<GenExamType, List<GenExamRecord>> _records;

        public List<GenExamRecord> this[GenExamType type]
        {
            get
            {
                return _records[type];
            }
        }
        //public static GenExamResult Instance { get; private set; } = new GenExamResult();
        public GenExamResult()
        {
            _records = new Dictionary<GenExamType, List<GenExamRecord>>();

            Array enums = typeof(GenExamType).GetEnumValues();
            foreach (GenExamType type in enums)
            {
                _records.Add(type, new List<GenExamRecord>());
            }
        }
        public string Serialize()
        {
            RecordsAdded = false;

            JArray records = new JArray();

            foreach (var pair in _records)
            {
                if (pair.Value.Count > 0)
                {
                    JArray jarr = new JArray(
                            from r in pair.Value
                            select new JObject(
                                new JProperty("Date", Utility.DateToShortString(r.Date.Value)),
                                new JProperty("Result", r.Result)));

                    string key = ((int)pair.Key).ToString();

                    records.Add(new JObject(new JProperty(((int)pair.Key).ToString(), jarr)));
                }
            }

            return records.HasValues ? records.ToString(Formatting.None) : null;
        }
        public void Deserialize(string json)
        {
            Reset();

            if (!string.IsNullOrWhiteSpace(json))
            {
                JArray jarr = JArray.Parse(json);

                if (jarr is JArray types && jarr.HasValues)
                {
                    foreach (JObject t in types)
                    {
                        JProperty prop = t.First as JProperty;
                        GenExamType type = (GenExamType)int.Parse(prop.Name);

                        if (prop.Value is JArray recs)
                            foreach (JObject rec in recs)
                                _records[type].Add(new GenExamRecord() { Date = DateTime.Parse(rec["Date"].ToString()), Result = rec["Result"].ToString() });
                    }
                }
            }
        }

        /// <summary>
        /// Add GenExamRecord to Dictionary<GenExamType, List<GenExamRecord>> _records
        /// </summary>
        /// <param name="type"></param>
        /// <param name="record"></param>
        /// <returns>Index of inserted record in a list</returns>
        public int AddRecord(GenExamType type, GenExamRecord record)
        {
            if (!record.Date.HasValue)
                return -1;

            List<GenExamRecord> lst = this[type];
            DateTime DateToAdd = record.Date.Value;

            for (int i = 0; i < lst.Count; i++)
            {
                if (DateToAdd < lst[i].Date)
                {
                    lst.Insert(i, record);
                    return i;
                }
            }

            lst.Add(record);

            if (type != GenExamType.Disease)
                RecordsAdded = true;

            return lst.Count - 1;
        }
        public void RemoveRecord(GenExamType type, int index)
        {
            _records[type].RemoveAt(index);
        }

        public void ResetPost()
        {
            RecordsAdded = false;

            foreach (var key in _records.Keys)
            {
                if (!key.In(GenExamType.Disease, GenExamType.SARS))
                {
                    _records[key].Clear();
                }
            }
        }

        public void Reset()
        {
            RecordsAdded = false;

            foreach (var key in _records.Keys)
                _records[key].Clear();
        }
        public string GetDiseaseString()
        {
            string temp = string.Empty;

            List<GenExamRecord> lst = this[GenExamType.Disease];

            foreach (GenExamRecord ge in lst)
            {
                if (ge.Result.Length > 0)
                {
                    temp += ge.ToString() + Environment.NewLine;
                }
            }

            return temp;
        }
        public string GetLastDiseaseString()
        {
            string res = string.Empty;

            if (GetLastDiseaseRecord() is GenExamRecord ger)
                res = ger.ToString();

            return res;
        }

        public string GetString()
        {
            string temp = string.Empty;

            foreach (var pair in _records)
            {
                foreach (GenExamRecord ge in pair.Value)
                {
                    if (ge.Result.Length > 0)
                    {
                        if (temp.Length > 0)
                            temp += Environment.NewLine;

                        temp += ge.ToString();
                    }
                }
            }

            return temp;
        }
        public void FillTestData()
        {
            AddRecord(GenExamType.Disease, DiseaseRecord.GetTestRecord());
            AddRecord(GenExamType.OAK, OAKRecord.GetTestRecord());
            AddRecord(GenExamType.BAK, BAKRecord.GetTestRecord());
            AddRecord(GenExamType.Coagulogram, CoagulogramRecord.GetTestRecord());
            AddRecord(GenExamType.Urine, UrineRecord.GetTestRecord());
            AddRecord(GenExamType.SARS, SARSRecord.GetTestRecord());
        }
        public void Revert()
        {
            RecordsAdded = true;
        }
        public void ValidateDiseaseRecords(ValidateReportBuilder rb)
        {
            GenExamRecord lastRec = GetLastDiseaseRecord();

            if(lastRec is GenExamRecord ger)
            {
                if ((DateTime.Now - ger.Date.Value).TotalDays > 180)
                {
                    rb.AddWarning("последний анализ на заболевания (Туберкулез, RW, ВИЧ, HBsAg, HCV) был проведен более 180 дней назад");
                }
            }
            else
                rb.AddCriticialField("нет ни одного анализа на заболевания (Туберкулез, RW, ВИЧ, HBsAg, HCV)");
        }
        public bool HasNonDiseaseRecords()
        {
            foreach (var pair in _records)
            {
                if (pair.Key != GenExamType.Disease && pair.Value.Count > 0)
                    return true;
            }

            return false;
        }
        private GenExamRecord GetLastDiseaseRecord()
        {
            List<GenExamRecord> dis_records = _records[GenExamType.Disease];

            if (dis_records.Count > 0)
            {
                DateTime last_exam_date = dis_records[0].Date.Value;
                int last_id = 0;

                for (int i = 1; i < dis_records.Count; i++)
                {
                    if (dis_records[i].Date.Value >= last_exam_date)
                    {
                        last_exam_date = dis_records[i].Date.Value;
                        last_id = i;
                    }
                }

                return dis_records[last_id];
            }
            else
                return null;
        }
    }
}
