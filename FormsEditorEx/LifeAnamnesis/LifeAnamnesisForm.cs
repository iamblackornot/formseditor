﻿using SharedLibrary;
using System.Windows.Forms;

namespace FormsEditorEx
{
    public partial class LifeAnamnesisForm : Form
    {
        bool isReadOnly;
        LifeAnamnesisData lad;
        public LifeAnamnesisForm(ref LifeAnamnesisData lad, bool isReadOnly = true)
        {
            InitializeComponent();
            this.isReadOnly = isReadOnly;
            this.lad = lad;
            InitComponents(isReadOnly);
            CenterOKButton();
            LoadData();

            ActiveControl = btOK;
#if !DEBUG
            btTestData.Visible = false;
#endif
        }
        private void CenterOKButton()
        {
            btOK.Left = (pnButtons.Width - btOK.Width) / 2;
        }
        private void TextBox_KeyDown(object sender, KeyEventArgs e)
        {
            Utility.ReturnToTab(this, e);
        }
        private void InitComponents(bool isReadOnly)
        {
            SetCues();

            tbHeight.SetTextBoxAvailability(isReadOnly);
            tbWeight.SetTextBoxAvailability(isReadOnly);
            tbTemperature.SetTextBoxAvailability(isReadOnly);
            tbBloodPressureA.SetTextBoxAvailability(isReadOnly);
            tbBloodPressureD.SetTextBoxAvailability(isReadOnly);
            tbHeartRate.SetTextBoxAvailability(isReadOnly);

            rtbSkin.SetTextBoxAvailability(isReadOnly);
            rtbLymphNodes.SetTextBoxAvailability(isReadOnly);
            rtbMammary.SetTextBoxAvailability(isReadOnly);
            rtbThyroid.SetTextBoxAvailability(isReadOnly);
            rtbChest.SetTextBoxAvailability(isReadOnly);
            rtbDyspnea.SetTextBoxAvailability(isReadOnly);
            rtbLungsResp.SetTextBoxAvailability(isReadOnly);
            rtbPercussion.SetTextBoxAvailability(isReadOnly);
            rtbHeartSounds.SetTextBoxAvailability(isReadOnly);
            rtbPulse.SetTextBoxAvailability(isReadOnly);
            rtbStomach.SetTextBoxAvailability(isReadOnly);
            rtbLiver.SetTextBoxAvailability(isReadOnly);
            rtbUrination.SetTextBoxAvailability(isReadOnly);
            rtbStool.SetTextBoxAvailability(isReadOnly);

            rtbGenetics.SetTextBoxAvailability(isReadOnly);
            rtbAllergy.SetTextBoxAvailability(isReadOnly);
            rtbTransfusion.SetTextBoxAvailability(isReadOnly);
            rtbProfHarm.SetTextBoxAvailability(isReadOnly);
            rtbPastIllnesses.SetTextBoxAvailability(isReadOnly);
            rtbTuberculosis.SetTextBoxAvailability(isReadOnly);

            cbSmokes.Enabled = cbSmokes.TabStop = !isReadOnly;
            tbCiggsPerDay.SetTextBoxAvailability(isReadOnly);
        }
        private void btOK_Click(object sender, System.EventArgs e)
        {
            if(!isReadOnly)
                SaveData();
            this.Close();
        }
        private void FillTestData()
        {
            tbHeight.Text = "--175--";
            tbWeight.Text = "--65--";
            tbTemperature.SetText("--36.7--");
            tbBloodPressureA.SetText("--140--");
            tbBloodPressureD.SetText("--90--");
            tbHeartRate.SetText("--66--");

            rtbSkin.SetText("--Кожные_покровы--");
            rtbLymphNodes.SetText("--Периферические_лимфоузлы--");
            rtbMammary.SetText("--Молочные_железы--");
            rtbThyroid.SetText("--Щитовидная_железа--");
            rtbChest.SetText("--Грудная_клетка--");
            rtbDyspnea.SetText("--Одышка--");
            rtbLungsResp.SetText("--В_легких_дыхание--");
            rtbPercussion.SetText("--Перкуторно--");
            rtbHeartSounds.SetText("--Тоны сердца--");
            rtbPulse.SetText("--Пульс--");
            rtbStomach.SetText("--Живот--");
            rtbLiver.SetText("--Печень--");
            rtbUrination.SetText("--Мочеиспускание--");
            rtbStool.SetText("--Стул--");

            rtbGenetics.SetText("--Наследственность--");
            rtbAllergy.SetText("--Аллергия--");
            rtbTransfusion.SetText("--Гемотрансфузии--");
            rtbProfHarm.SetText("--Профессиональные_вредности--");
            rtbPastIllnesses.SetText("--Перенесенные_заболевания--");
            rtbTuberculosis.SetText("--Туберкулез--");

            cbSmokes.Checked = true;
            tbCiggsPerDay.SetText("--12--");
        }
        private void LoadData()
        {
            tbHeight.Text = !string.IsNullOrWhiteSpace(lad.Height) ? lad.Height : string.Empty;
            tbWeight.Text = !string.IsNullOrWhiteSpace(lad.Weight) ? lad.Weight : string.Empty;
            tbTemperature.SetText(!string.IsNullOrWhiteSpace(lad.Temperature) ? lad.Temperature : string.Empty);
            tbBloodPressureA.SetText(!string.IsNullOrWhiteSpace(lad.PressureA) ? lad.PressureA : string.Empty);
            tbBloodPressureD.SetText(!string.IsNullOrWhiteSpace(lad.PressureD) ? lad.PressureD : string.Empty);
            tbHeartRate.SetText(!string.IsNullOrWhiteSpace(lad.HeartRate) ? lad.HeartRate : string.Empty);

            rtbSkin.SetText(!string.IsNullOrWhiteSpace(lad.Skin) ? lad.Skin : string.Empty);
            rtbLymphNodes.SetText(!string.IsNullOrWhiteSpace(lad.LymphNodes) ? lad.LymphNodes : string.Empty);
            rtbMammary.SetText(!string.IsNullOrWhiteSpace(lad.Mammary) ? lad.Mammary : string.Empty);
            rtbThyroid.SetText(!string.IsNullOrWhiteSpace(lad.Thyroid) ? lad.Thyroid : string.Empty);
            rtbChest.SetText(!string.IsNullOrWhiteSpace(lad.Chest) ? lad.Chest : string.Empty);
            rtbDyspnea.SetText(!string.IsNullOrWhiteSpace(lad.Dyspnea) ? lad.Dyspnea : string.Empty);
            rtbLungsResp.SetText(!string.IsNullOrWhiteSpace(lad.LungsResp) ? lad.LungsResp : string.Empty);
            rtbPercussion.SetText(!string.IsNullOrWhiteSpace(lad.Percussion) ? lad.Percussion : string.Empty);
            rtbHeartSounds.SetText(!string.IsNullOrWhiteSpace(lad.HeartSounds) ? lad.HeartSounds : string.Empty);
            rtbPulse.SetText(!string.IsNullOrWhiteSpace(lad.Pulse) ? lad.Pulse : string.Empty);
            rtbStomach.SetText(!string.IsNullOrWhiteSpace(lad.Stomach) ? lad.Stomach : string.Empty);
            rtbLiver.SetText(!string.IsNullOrWhiteSpace(lad.Liver) ? lad.Liver : string.Empty);
            rtbUrination.SetText(!string.IsNullOrWhiteSpace(lad.Urination) ? lad.Urination : string.Empty);
            rtbStool.SetText(!string.IsNullOrWhiteSpace(lad.Stool) ? lad.Stool : string.Empty);

            rtbGenetics.SetText(!string.IsNullOrWhiteSpace(lad.Genetics) ? lad.Genetics : string.Empty);
            rtbAllergy.SetText(!string.IsNullOrWhiteSpace(lad.Allergy) ? lad.Allergy : string.Empty);
            rtbTransfusion.SetText(!string.IsNullOrWhiteSpace(lad.Transfusion) ? lad.Transfusion : string.Empty);
            rtbProfHarm.SetText(!string.IsNullOrWhiteSpace(lad.ProfHarm) ? lad.ProfHarm : string.Empty);
            rtbPastIllnesses.SetText(!string.IsNullOrWhiteSpace(lad.PastIllnesses) ? lad.PastIllnesses : string.Empty);
            rtbTuberculosis.SetText(!string.IsNullOrWhiteSpace(lad.Tuberculosis) ? lad.Tuberculosis : string.Empty);

            cbSmokes.Checked = lad.Smokes.HasValue ? true : false;
            tbCiggsPerDay.SetText(!string.IsNullOrWhiteSpace(lad.CiggsPerDay) ? lad.CiggsPerDay : string.Empty);
        }
        private void SaveData()
        {
            //LifeAnamnesisData lad = Patient.Instance.PatientData.LifeAnamnesisData;

            string temp = tbHeight.Text.Trim();
            lad.Height = !string.IsNullOrWhiteSpace(temp) ? temp : null;
            temp = tbWeight.Text.Trim();
            lad.Weight = !string.IsNullOrWhiteSpace(temp) ? temp : null;

            lad.Temperature = !tbTemperature.IsCueOn ? tbTemperature.Text.Trim() : null;
            lad.PressureA = !tbBloodPressureA.IsCueOn ? tbBloodPressureA.Text.Trim() : null;
            lad.PressureD = !tbBloodPressureD.IsCueOn ? tbBloodPressureD.Text.Trim() : null;
            lad.HeartRate = !tbHeartRate.IsCueOn ? tbHeartRate.Text.Trim() : null;

            lad.Skin = !rtbSkin.IsCueOn ? rtbSkin.Text.Trim() : null;
            lad.LymphNodes = !rtbLymphNodes.IsCueOn ? rtbLymphNodes.Text.Trim() : null;
            lad.Mammary = !rtbMammary.IsCueOn ? rtbMammary.Text.Trim() : null;
            lad.Thyroid = !rtbThyroid.IsCueOn ? rtbThyroid.Text.Trim() : null;
            lad.Chest = !rtbChest.IsCueOn ? rtbChest.Text.Trim() : null;
            lad.Dyspnea = !rtbDyspnea.IsCueOn ? rtbDyspnea.Text.Trim() : null;
            lad.LungsResp = !rtbLungsResp.IsCueOn ? rtbLungsResp.Text.Trim() : null;
            lad.Percussion = !rtbPercussion.IsCueOn ? rtbPercussion.Text.Trim() : null;
            lad.HeartSounds = !rtbHeartSounds.IsCueOn ? rtbHeartSounds.Text.Trim() : null;
            lad.Pulse = !rtbPulse.IsCueOn ? rtbPulse.Text.Trim() : null;
            lad.Stomach = !rtbStomach.IsCueOn ? rtbStomach.Text.Trim() : null;
            lad.Liver = !rtbLiver.IsCueOn ? rtbLiver.Text.Trim() : null;
            lad.Urination = !rtbUrination.IsCueOn ? rtbUrination.Text.Trim() : null;
            lad.Stool = !rtbStool.IsCueOn ? rtbStool.Text.Trim() : null;

            lad.Genetics = !rtbGenetics.IsCueOn ? rtbGenetics.Text.Trim() : null;
            lad.Allergy = !rtbAllergy.IsCueOn ? rtbAllergy.Text.Trim() : null;
            lad.Transfusion = !rtbTransfusion.IsCueOn ? rtbTransfusion.Text.Trim() : null;
            lad.ProfHarm = !rtbProfHarm.IsCueOn ? rtbProfHarm.Text.Trim() : null;
            lad.PastIllnesses = !rtbPastIllnesses.IsCueOn ? rtbPastIllnesses.Text.Trim() : null;
            lad.Tuberculosis = !rtbTuberculosis.IsCueOn ? rtbTuberculosis.Text.Trim() : null;

            lad.Smokes = cbSmokes.Checked ? 1 : (int?)null;
            lad.CiggsPerDay = cbSmokes.Checked && !tbCiggsPerDay.IsCueOn ? tbCiggsPerDay.Text.Trim() : null;
        }

        private void btTestData_Click(object sender, System.EventArgs e)
        {
            FillTestData();
        }

        private void SetCues()
        {
            tbTemperature.Cue = Static.Cues[FieldWithCue.Temperature];
            tbBloodPressureA.Cue = Static.Cues[FieldWithCue.PressureA];
            tbBloodPressureD.Cue = Static.Cues[FieldWithCue.PressureD];
            tbHeartRate.Cue = Static.Cues[FieldWithCue.HeartRate];
            rtbSkin.Cue = Static.Cues[FieldWithCue.Skin];
            rtbLymphNodes.Cue = Static.Cues[FieldWithCue.LymphNodes];
            rtbMammary.Cue = Static.Cues[FieldWithCue.Mammary];
            rtbThyroid.Cue = Static.Cues[FieldWithCue.Thyroid];
            rtbChest.Cue = Static.Cues[FieldWithCue.Chest];
            rtbDyspnea.Cue = Static.Cues[FieldWithCue.Dyspnea];
            rtbLungsResp.Cue = Static.Cues[FieldWithCue.LungsResp];
            rtbPercussion.Cue = Static.Cues[FieldWithCue.Percussion];
            rtbHeartSounds.Cue = Static.Cues[FieldWithCue.HeartSounds];
            rtbPulse.Cue = Static.Cues[FieldWithCue.Pulse];
            rtbStomach.Cue = Static.Cues[FieldWithCue.Stomach];
            rtbLiver.Cue = Static.Cues[FieldWithCue.Liver];
            rtbUrination.Cue = Static.Cues[FieldWithCue.Urination];
            rtbStool.Cue = Static.Cues[FieldWithCue.Stool];
            rtbGenetics.Cue = Static.Cues[FieldWithCue.Genetics];
            rtbAllergy.Cue = Static.Cues[FieldWithCue.Allergy];
            rtbTransfusion.Cue = Static.Cues[FieldWithCue.Transfusion];
            rtbProfHarm.Cue = Static.Cues[FieldWithCue.ProfHarm];
            rtbPastIllnesses.Cue = Static.Cues[FieldWithCue.PastIllnesses];
            rtbTuberculosis.Cue = Static.Cues[FieldWithCue.Tuberculosis];
        }

        private void cbSmokes_CheckedChanged(object sender, System.EventArgs e)
        {
            tbCiggsPerDay.Enabled = cbSmokes.Checked;
        }
    }
}
