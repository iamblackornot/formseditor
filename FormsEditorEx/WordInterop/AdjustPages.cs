﻿using Microsoft.Office.Interop.Word;
using System.Threading;
using System.Reflection;
using System;
using SharedLibrary;

namespace FormsEditorEx
{
    public static partial class WordInterop
    {
        public static readonly float MinTextSize = 9f; 
        public static readonly float Epsilon = 0.05f; 
        public static void AdjustPages(Document doc, DocFormat docFormat)
        {
            float working_area = doc.PageSetup.PageHeight - doc.PageSetup.TopMargin - doc.PageSetup.BottomMargin;
            Range range;

            for (int i = 1; i <= docFormat.Pages.Count; i++)
            {
                range = doc.Content;
                int line = 0;

                range = doc.Content;
                range.Find.Execute(string.Format("%p{0}s", i));

                int page = (int)range.Information[WdInformation.wdActiveEndAdjustedPageNumber];
                //bool isPageChanged = false;
                DateTime starttime = DateTime.Now;

                while (page < i)
                {
                    if((DateTime.Now - starttime).Seconds > 5)
                    {
                        string error = $"page break timeout: page = {page}, i = {i}, patient_id = {Patient.Instance.AmbuCardNum.EmptyIfNull()}";
                        Logger.AddToLogs(MethodBase.GetCurrentMethod().Name, error);
                        break;
                    }

                    doc.Range(range.Start, range.Start).InsertBreak(WdBreakType.wdPageBreak);

                    int timeout = 0;
                    int new_page = page + 1;

                    while (page != new_page && timeout <= 1000)
                    {
                        Thread.Sleep(50);
                        timeout += 50;
                        range = doc.Content;
                        range.Find.Execute(string.Format("%p{0}s", i));
                        page = (int)range.Information[WdInformation.wdActiveEndAdjustedPageNumber];
                    }
                }

                PageFormat pf = docFormat.Pages[i];

                if (pf.isScalable)
                {

                    range = doc.Content;
                    range.Find.Execute(string.Format("%p{0}e", i));

                    page = (int)doc.Range(range.Start - 1, range.Start - 1).Information[WdInformation.wdActiveEndAdjustedPageNumber];
                    starttime = DateTime.Now;

                    while (page > i)
                    {
                        if ((DateTime.Now - starttime).Seconds > 10)
                        {
                            string error = $"page scale timeout: page = {page}, i = {i}, patient_id = {Patient.Instance.AmbuCardNum.EmptyIfNull()}";
                            Logger.AddToLogs(MethodBase.GetCurrentMethod().Name, error);
                            break;
                        }

                        int timeout = 0;

                        range = doc.Content;
                        range.Find.Execute(string.Format("%p{0}s", i));
                        int start = range.End;

                        range = doc.Content;
                        range.Find.Execute(string.Format("%p{0}he", i));
                        int end = range.Start;

                        if (end - start > 0)
                        {
                            float header = doc.Range(start, start + 5).Font.Size;
                            header -= 0.5f;

                            if (header > MinTextSize - Epsilon)
                            {
                                doc.Range(start, end).Font.Size = header;

                                timeout = 0;

                                while (doc.Range(start, start + 5).Font.Size != header && timeout <= 1000)
                                {
                                    Thread.Sleep(50);
                                    timeout += 50;

                                    if (timeout >= 999)
                                    {
                                        string error = $"header timeout: " +
                                            $"header font size = {header}, " +
                                            $"first5 font size = {doc.Range(start, start + 5).Font.Size}, " +
                                            $"first5 text = {doc.Range(start, start + 5).Text}";
                                        Logger.AddToLogs(MethodBase.GetCurrentMethod().Name, error);
                                    }
                                }
                            }
                            else break;
                        }

                        range = doc.Content;
                        range.Find.Execute(string.Format("%p{0}c", i));
                        start = range.End;

                        range = doc.Content;
                        range.Find.Execute(string.Format("%p{0}e", i));
                        end = range.Start;

                        if (end - start > 0)
                        {
                            float content = doc.Range(end - 5, end).Font.Size;

                            content -= 0.5f;

                            if (content > MinTextSize - Epsilon)
                            {
                                doc.Range(start, end).Font.Size = content;

                                timeout = 0;

                                while (doc.Range(end - 5, end).Font.Size != content && timeout <= 1000)
                                {
                                    Thread.Sleep(50);
                                    timeout += 50;

                                    if (timeout >= 999)
                                    {
                                        string error = $"content timeout: " +
                                            $"content font size = {content}, " +
                                            $"last5 font size = {doc.Range(end - 5, end).Font.Size}, " +
                                            $"last5 text = {doc.Range(end - 5, end).Text}";
                                        Logger.AddToLogs(MethodBase.GetCurrentMethod().Name, error);
                                    }
                                }

                            }
                            else break;
                        }

                        range = doc.Content;
                        range.Find.Execute(string.Format("%p{0}e", i));

                        page = (int)doc.Range(range.Start - 1, range.Start - 1).Information[WdInformation.wdActiveEndAdjustedPageNumber];
                    }
                }

                //MessageBox.Show(string.Format("page height = {0}, top margin = {1}, bottom margin = {2}", doc.PageSetup.PageHeight.ToString(), doc.PageSetup.TopMargin.ToString(), doc.PageSetup.BottomMargin.ToString()));

                if (docFormat.Pages[i].MaxTopPadding > 0)
                {
                    range = doc.Content;
                    range.Find.Execute(string.Format("%p{0}s", i));

                    float text_top = (float)range.Information[WdInformation.wdVerticalPositionRelativeToPage];

                    range = doc.Content;
                    range.Find.Execute(string.Format("%p{0}e", i));
                    range = doc.Range(range.Start - 2, range.Start - 1);

                    float text_bottom = (float)range.Information[WdInformation.wdVerticalPositionRelativeToPage] + (float)range.Font.Size;
                    float text_heigth = text_bottom - text_top;

                    int padding = Convert.ToInt32(Math.Floor((working_area - text_heigth) / docFormat.LineBreakSize / 2));

                    if (padding > docFormat.Pages[i].MaxTopPadding)
                        padding = docFormat.Pages[i].MaxTopPadding;

                    if (padding > 0)
                    {

                        range = doc.Content;
                        range.Find.Execute(string.Format("%p{0}s", i));

                        line = (int)range.Information[WdInformation.wdFirstCharacterLineNumber];
                        int line_obj = line + padding;

                        for (int j = 0; j < padding; j++)
                            doc.Range(range.Start, range.Start).InsertBreak(WdBreakType.wdLineBreak);

                        int timeout = 0;
                        while (line < line_obj && timeout <= 1000)
                        {
                            Thread.Sleep(50);
                            range = doc.Content;
                            range.Find.Execute(string.Format("%p{0}s", i));
                            line = (int)range.Information[WdInformation.wdFirstCharacterLineNumber];

                            timeout += 50;

                            if (timeout >= 999)
                            {
                                string error = $"padding timeout: page = {i}, line = {line}, target_line = {line_obj}";
                                Logger.AddToLogs(MethodBase.GetCurrentMethod().Name, error);
                            }
                        }
                    }
                }
            } //for end

            for (int i = 1; i <= docFormat.Pages.Count; i++)
            {
                range = doc.Content;
                if (range.Find.Execute(string.Format("%p{0}s", i)))
                    range.Delete();

                range = doc.Content;
                if (range.Find.Execute(string.Format("%p{0}he", i)))
                    range.Delete();

                range = doc.Content;
                if (range.Find.Execute(string.Format("%p{0}c", i)))
                    range.Delete();

                range = doc.Content;
                if (range.Find.Execute(string.Format("%p{0}e", i)))
                    range.Delete();
            }
        }
    }
}
