﻿using System;
using System.Collections.Generic;
using FormsEditorEx.GenExam.Tabs;

namespace FormsEditorEx.GenExam
{
    public static class GenExamTabsFactory
    {
        private static List<string> _lstTabNames;
        public static List<IExamTab> CreateTabs()
        {
            List<IExamTab> tabs = new List<IExamTab>();

            tabs.Add(new ucDiseaseTab());     // 0
            tabs.Add(new ucOAKTab());         // 1
            tabs.Add(new ucBAKTab());         // 2
            tabs.Add(new ucUrineTab());       // 3
            tabs.Add(new ucCoagulogram());    // 4
            tabs.Add(new ucSARSTab());        // 5

            return tabs;
        }

        public static List<string> GetTabNames()
        {
            if (_lstTabNames != null)
                return _lstTabNames;
            else
            {
                _lstTabNames = new List<string>
                {
                    "Заболевания",     // 0
                    "ОАК",             // 1
                    "БАК",             // 2
                    "Общ. анализ мочи",// 3
                    "Коагулограмма",   // 4
                    "SARS-CoV-2"       // 5
                };

                return _lstTabNames;
            }
        }
    }
}
