﻿using SharedLibrary;

namespace FormsEditorEx.Database
{
    public class PatientRemoved : DatabaseResult
    {
        public PatientRemoved() : base(true, StatusBarIndicator.OK, "Запись удалена") { }
    }
    public class PatientRemoveError : DatabaseResult
    {
        public PatientRemoveError() : base(false, StatusBarIndicator.ERROR, "Ошибка при попытке удалить запись в базе данных") { }
    }
}
