﻿using System;
using System.Windows.Forms;
using SharedLibrary;

namespace FormsEditorEx.Conclusion
{
    public partial class ConclusionEditForm : Form
    {
        bool isReadOnly;
        bool hasChanged = false;
        public string Num { get; private set; }
        public string Conclusion { get; private set; }
        public int Type { get; private set; }
        public ConclusionEditForm(bool isReadOnly = true)
        {
            InitializeComponent();

            this.isReadOnly = isReadOnly;

            InitButtons();
            InitComponents();
            CenterToScreen();
        }
        public ConclusionEditForm(string num, string conclusion, int type, bool isReadOnly = true) : this(isReadOnly)
        {
            tbNum.Text = num;
            rtbConclusion.Text = conclusion;
            cbType.SelectedIndex = type;

            this.Text = isReadOnly ? "Посмотр записи" : this.Text = "Изменить запись";

            btOK.Enabled = isReadOnly;
            hasChanged = false;
        }
        private void InitButtons()
        {
            btCancel.Visible = !isReadOnly;
            btOK.Left = (pnButtons.Width - btOK.Width - (!isReadOnly ? btCancel.Width + 8 : 0)) / 2;
            btCancel.Left = btOK.Left + btOK.Width + 8;
        }
        private void InitComponents()
        {
            cbType.Enabled = !isReadOnly;
            tbNum.SetTextBoxAvailability(isReadOnly);
            rtbConclusion.SetTextBoxAvailability(isReadOnly);

            cbType.DataSource = Static.ConclusionTypes;
            cbType.SelectedIndex = -1;

            cbType.SelectedIndexChanged += InputChanged;
            tbNum.TextChanged += InputChanged;
            rtbConclusion.TextChanged += InputChanged;
        }

        private bool CheckFields()
        {
            if (tbNum.Text.Length == 0)
            {
                MessageBox.Show("Не указан номер заключения");
                return false;
            }
            if (cbType.SelectedIndex < 0)
            {
                MessageBox.Show("Не выбран тип заключения");
                return false;
            }
            return true;
        }

        private void btOK_Click(object sender, EventArgs e)
        {
            if (isReadOnly)
            {
                DialogResult = DialogResult.Cancel;
                Close();
            }
            else
            {
                if (CheckFields())
                {
                    Num = tbNum.Text.Trim();
                    Conclusion = rtbConclusion.Text.Trim();
                    Type = cbType.SelectedIndex;

                    DialogResult = DialogResult.OK;
                    Close();
                }
            }
        }
        private void TextBox_KeyDown(object sender, KeyEventArgs e)
        {
            Utility.ReturnToTab(this, e);
        }

        private void btCancel_Click(object sender, EventArgs e)
        {
            if (hasChanged)
            {
                if (MessageBox.Show("Закрыть окно без сохранения изменений?", "Требуется подтверждение",
                    MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                {
                    DialogResult = DialogResult.Cancel;
                    Close();
                }
            }
            else
            {
                DialogResult = DialogResult.Cancel;
                Close();
            }
        }
        private void InputChanged(object sender, EventArgs e)
        {
            hasChanged = true;
            btOK.Enabled = true;
        }
    }
}
