﻿namespace FormsEditorEx
{
    partial class OperTeamForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label68 = new System.Windows.Forms.Label();
            this.tbBloodTransSpecialist = new System.Windows.Forms.TextBox();
            this.label67 = new System.Windows.Forms.Label();
            this.tbOperSister = new System.Windows.Forms.TextBox();
            this.label66 = new System.Windows.Forms.Label();
            this.tbAnesthetist = new System.Windows.Forms.TextBox();
            this.label65 = new System.Windows.Forms.Label();
            this.tbAssistant = new System.Windows.Forms.TextBox();
            this.tbSurgeon = new SharedLibrary.TextBoxEx();
            this.label35 = new System.Windows.Forms.Label();
            this.btOK = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label68
            // 
            this.label68.AutoSize = true;
            this.label68.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label68.Location = new System.Drawing.Point(19, 136);
            this.label68.Name = "label68";
            this.label68.Size = new System.Drawing.Size(121, 15);
            this.label68.TabIndex = 95;
            this.label68.Text = "Отв. за перел. крови";
            // 
            // tbBloodTransSpecialist
            // 
            this.tbBloodTransSpecialist.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.tbBloodTransSpecialist.Location = new System.Drawing.Point(146, 133);
            this.tbBloodTransSpecialist.Name = "tbBloodTransSpecialist";
            this.tbBloodTransSpecialist.Size = new System.Drawing.Size(163, 23);
            this.tbBloodTransSpecialist.TabIndex = 91;
            this.tbBloodTransSpecialist.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.tbBloodTransSpecialist.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextBox_KeyDown);
            // 
            // label67
            // 
            this.label67.AutoSize = true;
            this.label67.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label67.Location = new System.Drawing.Point(47, 107);
            this.label67.Name = "label67";
            this.label67.Size = new System.Drawing.Size(93, 15);
            this.label67.TabIndex = 94;
            this.label67.Text = "Операц. сестра";
            // 
            // tbOperSister
            // 
            this.tbOperSister.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.tbOperSister.Location = new System.Drawing.Point(146, 104);
            this.tbOperSister.Name = "tbOperSister";
            this.tbOperSister.Size = new System.Drawing.Size(163, 23);
            this.tbOperSister.TabIndex = 89;
            this.tbOperSister.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.tbOperSister.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextBox_KeyDown);
            // 
            // label66
            // 
            this.label66.AutoSize = true;
            this.label66.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label66.Location = new System.Drawing.Point(57, 78);
            this.label66.Name = "label66";
            this.label66.Size = new System.Drawing.Size(83, 15);
            this.label66.TabIndex = 93;
            this.label66.Text = "Анестезиолог";
            // 
            // tbAnesthetist
            // 
            this.tbAnesthetist.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.tbAnesthetist.Location = new System.Drawing.Point(146, 75);
            this.tbAnesthetist.Name = "tbAnesthetist";
            this.tbAnesthetist.Size = new System.Drawing.Size(163, 23);
            this.tbAnesthetist.TabIndex = 88;
            this.tbAnesthetist.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.tbAnesthetist.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextBox_KeyDown);
            // 
            // label65
            // 
            this.label65.AutoSize = true;
            this.label65.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label65.Location = new System.Drawing.Point(78, 49);
            this.label65.Name = "label65";
            this.label65.Size = new System.Drawing.Size(62, 15);
            this.label65.TabIndex = 92;
            this.label65.Text = "Ассистент";
            // 
            // tbAssistant
            // 
            this.tbAssistant.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.tbAssistant.Location = new System.Drawing.Point(146, 46);
            this.tbAssistant.Name = "tbAssistant";
            this.tbAssistant.Size = new System.Drawing.Size(163, 23);
            this.tbAssistant.TabIndex = 87;
            this.tbAssistant.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.tbAssistant.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextBox_KeyDown);
            // 
            // tbSurgeon
            // 
            this.tbSurgeon.Cue = "";
            this.tbSurgeon.Font = new System.Drawing.Font("Calibri", 9.75F);
            this.tbSurgeon.ForeColor = System.Drawing.SystemColors.WindowText;
            this.tbSurgeon.Location = new System.Drawing.Point(146, 17);
            this.tbSurgeon.Name = "tbSurgeon";
            this.tbSurgeon.Size = new System.Drawing.Size(163, 23);
            this.tbSurgeon.TabIndex = 86;
            this.tbSurgeon.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.tbSurgeon.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextBox_KeyDown);
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label35.Location = new System.Drawing.Point(94, 20);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(46, 15);
            this.label35.TabIndex = 90;
            this.label35.Text = "Хирург";
            // 
            // btOK
            // 
            this.btOK.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btOK.Location = new System.Drawing.Point(125, 179);
            this.btOK.Name = "btOK";
            this.btOK.Size = new System.Drawing.Size(75, 32);
            this.btOK.TabIndex = 96;
            this.btOK.Text = "OK";
            this.btOK.UseVisualStyleBackColor = true;
            this.btOK.Click += new System.EventHandler(this.btOK_Click);
            // 
            // OperTeamForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(329, 228);
            this.ControlBox = false;
            this.Controls.Add(this.btOK);
            this.Controls.Add(this.label68);
            this.Controls.Add(this.tbBloodTransSpecialist);
            this.Controls.Add(this.label67);
            this.Controls.Add(this.tbOperSister);
            this.Controls.Add(this.label66);
            this.Controls.Add(this.tbAnesthetist);
            this.Controls.Add(this.label65);
            this.Controls.Add(this.tbAssistant);
            this.Controls.Add(this.tbSurgeon);
            this.Controls.Add(this.label35);
            this.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Name = "OperTeamForm";
            this.Padding = new System.Windows.Forms.Padding(14);
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.Text = "Операционная бригада";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label68;
        private System.Windows.Forms.TextBox tbBloodTransSpecialist;
        private System.Windows.Forms.Label label67;
        private System.Windows.Forms.TextBox tbOperSister;
        private System.Windows.Forms.Label label66;
        private System.Windows.Forms.TextBox tbAnesthetist;
        private System.Windows.Forms.Label label65;
        private System.Windows.Forms.TextBox tbAssistant;
        private SharedLibrary.TextBoxEx tbSurgeon;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Button btOK;
    }
}