﻿namespace FormsEditorEx.GenExam.Tabs
{
    partial class ucCoagulogram
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pnContent = new System.Windows.Forms.Panel();
            this.label13 = new System.Windows.Forms.Label();
            this.tbCoagulogramDate = new System.Windows.Forms.MaskedTextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.tbDdimer = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.tbThrombinTime = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.tbFibrinogen = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.tbA4TV = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.tbProthrombin = new System.Windows.Forms.TextBox();
            this.tbMNO = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.tbProthrombinTime = new System.Windows.Forms.TextBox();
            this.pnContent.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnContent
            // 
            this.pnContent.AutoSize = true;
            this.pnContent.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.pnContent.Controls.Add(this.label13);
            this.pnContent.Controls.Add(this.tbCoagulogramDate);
            this.pnContent.Controls.Add(this.label10);
            this.pnContent.Controls.Add(this.tbDdimer);
            this.pnContent.Controls.Add(this.label2);
            this.pnContent.Controls.Add(this.tbThrombinTime);
            this.pnContent.Controls.Add(this.label3);
            this.pnContent.Controls.Add(this.tbFibrinogen);
            this.pnContent.Controls.Add(this.label1);
            this.pnContent.Controls.Add(this.tbA4TV);
            this.pnContent.Controls.Add(this.label8);
            this.pnContent.Controls.Add(this.label6);
            this.pnContent.Controls.Add(this.tbProthrombin);
            this.pnContent.Controls.Add(this.tbMNO);
            this.pnContent.Controls.Add(this.label7);
            this.pnContent.Controls.Add(this.tbProthrombinTime);
            this.pnContent.Location = new System.Drawing.Point(23, 13);
            this.pnContent.Name = "pnContent";
            this.pnContent.Padding = new System.Windows.Forms.Padding(8);
            this.pnContent.Size = new System.Drawing.Size(251, 314);
            this.pnContent.TabIndex = 0;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(104, 14);
            this.label13.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(38, 18);
            this.label13.TabIndex = 55;
            this.label13.Text = "Дата";
            // 
            // tbCoagulogramDate
            // 
            this.tbCoagulogramDate.Culture = new System.Globalization.CultureInfo("");
            this.tbCoagulogramDate.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.tbCoagulogramDate.Location = new System.Drawing.Point(149, 11);
            this.tbCoagulogramDate.Mask = "00.00.0000";
            this.tbCoagulogramDate.Name = "tbCoagulogramDate";
            this.tbCoagulogramDate.Size = new System.Drawing.Size(91, 26);
            this.tbCoagulogramDate.TabIndex = 1;
            this.tbCoagulogramDate.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(111, 278);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(64, 18);
            this.label10.TabIndex = 50;
            this.label10.Text = "D-димер";
            // 
            // tbDdimer
            // 
            this.tbDdimer.Location = new System.Drawing.Point(177, 275);
            this.tbDdimer.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.tbDdimer.Name = "tbDdimer";
            this.tbDdimer.Size = new System.Drawing.Size(61, 26);
            this.tbDdimer.TabIndex = 8;
            this.tbDdimer.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(33, 241);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(138, 18);
            this.label2.TabIndex = 48;
            this.label2.Text = "Тромбиновое время";
            // 
            // tbThrombinTime
            // 
            this.tbThrombinTime.Location = new System.Drawing.Point(177, 238);
            this.tbThrombinTime.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.tbThrombinTime.Name = "tbThrombinTime";
            this.tbThrombinTime.Size = new System.Drawing.Size(61, 26);
            this.tbThrombinTime.TabIndex = 7;
            this.tbThrombinTime.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(84, 204);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(87, 18);
            this.label3.TabIndex = 46;
            this.label3.Text = "Фибриноген";
            // 
            // tbFibrinogen
            // 
            this.tbFibrinogen.Location = new System.Drawing.Point(177, 201);
            this.tbFibrinogen.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.tbFibrinogen.Name = "tbFibrinogen";
            this.tbFibrinogen.Size = new System.Drawing.Size(61, 26);
            this.tbFibrinogen.TabIndex = 6;
            this.tbFibrinogen.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(132, 167);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(39, 18);
            this.label1.TabIndex = 44;
            this.label1.Text = "АЧТВ";
            // 
            // tbA4TV
            // 
            this.tbA4TV.Location = new System.Drawing.Point(177, 164);
            this.tbA4TV.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.tbA4TV.Name = "tbA4TV";
            this.tbA4TV.Size = new System.Drawing.Size(61, 26);
            this.tbA4TV.TabIndex = 5;
            this.tbA4TV.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(13, 93);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(158, 18);
            this.label8.TabIndex = 38;
            this.label8.Text = "Протромбин (по Квику)";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(132, 130);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(39, 18);
            this.label6.TabIndex = 40;
            this.label6.Text = "МНО";
            // 
            // tbProthrombin
            // 
            this.tbProthrombin.Location = new System.Drawing.Point(177, 90);
            this.tbProthrombin.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.tbProthrombin.Name = "tbProthrombin";
            this.tbProthrombin.Size = new System.Drawing.Size(61, 26);
            this.tbProthrombin.TabIndex = 3;
            this.tbProthrombin.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tbMNO
            // 
            this.tbMNO.Location = new System.Drawing.Point(177, 127);
            this.tbMNO.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.tbMNO.Name = "tbMNO";
            this.tbMNO.Size = new System.Drawing.Size(61, 26);
            this.tbMNO.TabIndex = 4;
            this.tbMNO.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(8, 57);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(163, 18);
            this.label7.TabIndex = 39;
            this.label7.Text = "Протромбиновое время";
            // 
            // tbProthrombinTime
            // 
            this.tbProthrombinTime.Location = new System.Drawing.Point(177, 54);
            this.tbProthrombinTime.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.tbProthrombinTime.Name = "tbProthrombinTime";
            this.tbProthrombinTime.Size = new System.Drawing.Size(61, 26);
            this.tbProthrombinTime.TabIndex = 2;
            this.tbProthrombinTime.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // ucCoagulogram
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.pnContent);
            this.Name = "ucCoagulogram";
            this.Size = new System.Drawing.Size(340, 421);
            this.pnContent.ResumeLayout(false);
            this.pnContent.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel pnContent;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.MaskedTextBox tbCoagulogramDate;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox tbDdimer;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tbThrombinTime;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox tbFibrinogen;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tbA4TV;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox tbProthrombin;
        private System.Windows.Forms.TextBox tbMNO;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox tbProthrombinTime;
    }
}
