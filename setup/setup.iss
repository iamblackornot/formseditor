#define MyAppName "HMCpro"
#define MyAppExeName "HMCproEx.exe"
#define appVersion "HMCpro 1.2.18"

[Tasks]
Name: "desktopicon"; Description: "{cm:CreateDesktopIcon}"; \
    GroupDescription: "{cm:AdditionalIcons}"; 

[Icons]
Name: "{commondesktop}\{#MyAppName}"; Filename: "{app}\{#MyAppExeName}"; Tasks: desktopicon; \
    AfterInstall: SetElevationBit('{commondesktop}\{#MyAppName}.lnk')
;[Registry]
;Root: "HKLM"; Subkey: "SOFTWARE\Microsoft\Windows NT\CurrentVersion\AppCompatFlags\Layers"; \
    ValueType: String; ValueName: "{app}\{#MyAppExeName}"; ValueData: "RUNASADMIN"; \
    Flags: uninsdeletekeyifempty uninsdeletevalue; MinVersion: 0,6.1

[Setup]
DisableReadyPage=True
AppName=HMCpro
AppId=HMCpro
AppVersion={#appVersion}
RestartIfNeededByRun=False
AllowCancelDuringInstall=False
ExtraDiskSpaceRequired=2
DefaultDirName={pf}\HMCpro
AllowRootDirectory=True
ShowLanguageDialog=no
LanguageDetectionMethod=locale
UsePreviousGroup=True
DisableProgramGroupPage=yes
AppendDefaultGroupName=False
CloseApplications=True
RestartApplications=False
CreateUninstallRegKey=yes
UninstallDisplayName=HMCpro
UninstallDisplayIcon={uninstallexe}
MergeDuplicateFiles=False
TerminalServicesAware=False
OutputDir=D:\Documents\Visual Studio 2017\Projects\FormsEditor\setup
OutputBaseFilename={#appVersion}
Compression=lzma2/fast
DefaultGroupName=HMCpro
PrivilegesRequired = admin

[InstallDelete]
Type: files; Name: {commondesktop}\HMCpro.lnk
Type: files; Name: {commondesktop}\������� �������.lnk
Type: files; Name: {app}\HMCpro.exe

[Files]
Source: "..\FormsEditorEx\bin\Release\Confused\AppSoftware.LicenceEngine.Common.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "..\FormsEditorEx\bin\Release\Confused\AppSoftware.LicenceEngine.KeyVerification.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "..\FormsEditorEx\bin\Release\Microsoft.Office.Interop.Word.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "..\FormsEditorEx\bin\Release\Newtonsoft.Json.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "..\FormsEditorEx\bin\Release\Office.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "..\FormsEditorEx\bin\Release\HMCproEx.exe"; DestDir: "{app}"; Flags: ignoreversion
Source: "..\FormsEditorEx\bin\Release\System.Data.SQLite.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "..\FormsEditorEx\bin\Release\x64\SQLite.Interop.dll"; DestDir: "{app}\x64"; Flags: ignoreversion
Source: "..\FormsEditorEx\bin\Release\x86\SQLite.Interop.dll"; DestDir: "{app}\x86"; Flags: ignoreversion
Source: "..\FormsEditorEx\bin\Release\SharedLibrary.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "..\FormsEditorEx\.env\deploy\server_location.ini"; DestDir: "{app}"; Flags: ignoreversion

[Code]
procedure SetElevationBit(Filename: string);
var
  Buffer: string;
  Stream: TStream;
begin
  Filename := ExpandConstant(Filename);
  Log('Setting elevation bit for ' + Filename);

  Stream := TFileStream.Create(FileName, fmOpenReadWrite);
  try
    Stream.Seek(21, soFromBeginning);
    SetLength(Buffer, 1);
    Stream.ReadBuffer(Buffer, 1);
    Buffer[1] := Chr(Ord(Buffer[1]) or $20);
    Stream.Seek(-1, soFromCurrent);
    Stream.WriteBuffer(Buffer, 1);
  except
    Log('Setting elevation bit failed');
  finally
    Stream.Free;
  end;
end;

[Languages]
Name: "russian"; MessagesFile: "compiler:Languages\Russian.isl";
Name: "english"; MessagesFile: "compiler:Default.isl";