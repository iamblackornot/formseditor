﻿using System;
using System.Collections.Generic;
using System.Data;
using SharedLibrary;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using SharedLibrary.Settings;
using FormsEditorEx.Settings;

namespace FormsEditorEx.VMP
{
    public partial class CommissionSelect : Form
    {
        [DllImport("user32.dll", CharSet = CharSet.Auto)]
        static extern IntPtr SendMessage(IntPtr hWnd, int Msg, int wParam, int lParam);

        private const int WM_CHANGEUISTATE = 0x127;
        private const int UIS_SET = 1;
        private const int UISF_HIDEFOCUS = 0x1;

        public List<string> Members { get; set; }

        int dep;
        public CommissionSelect(int dep_id)
        {
            dep = dep_id;

            SendMessage(this.Handle, WM_CHANGEUISTATE, MakeLong(UIS_SET, UISF_HIDEFOCUS), 0);

            InitializeComponent();

            InitListBoxes();

            ActiveControl = null;

        }

        private void InitListBoxes()
        {
            ListViewItem item;
            ListViewItem.ListViewSubItem subItem0;
            ListViewItem.ListViewSubItem subItem1;

            List<Deputy> deputies = AppConfig.Instance.Staff.Administration.List;

            if (deputies.Count > 0)
            {
                item = new ListViewItem("");
                subItem0 = new ListViewItem.ListViewSubItem(item, "Гл врач");
                subItem1 = new ListViewItem.ListViewSubItem(item, AppConfig.Instance.Staff.Administration.CMO.ShortName);
                item.SubItems.Add(subItem0);
                item.SubItems.Add(subItem1);
                lvStaff.Items.Add(item);

                for(int i = 1; i < deputies.Count; i++)
                {
                    if (deputies[i].PersonnelId == Personnel.PLACEHOLDER_ID)
                    {
                        continue;
                    }
                    item = new ListViewItem("");
                    subItem0 = new ListViewItem.ListViewSubItem(item, "Зам гл врача");
                    subItem1 = new ListViewItem.ListViewSubItem(item, deputies[i].ShortName);
                    item.SubItems.Add(subItem0);
                    item.SubItems.Add(subItem1);
                    lvStaff.Items.Add(item);
                }
            }

            item = new ListViewItem("");
            subItem0 = new ListViewItem.ListViewSubItem(item, "Заведующий");
            subItem1 = new ListViewItem.ListViewSubItem(item, AppConfig.Instance.Staff.Departments.HeadByDepartment[dep].ShortName);
            item.SubItems.Add(subItem0);
            item.SubItems.Add(subItem1);
            lvStaff.Items.Add(item);

            var departmentDoctors = AppConfig.Instance.Staff.Departments.DoctorsByDepartment[dep];

            foreach (Personnel doc in departmentDoctors)
            {
                item = new ListViewItem("");
                subItem0 = new ListViewItem.ListViewSubItem(item, "Леч. врач");
                subItem1 = new ListViewItem.ListViewSubItem(item, doc.ShortName);
                item.SubItems.Add(subItem0);
                item.SubItems.Add(subItem1);
                lvStaff.Items.Add(item);
            }

            int height = lvStaff.TopItem.Bounds.Top;

            foreach (ListViewItem it in lvStaff.Items)
                height += it.Bounds.Height;

            lvCommission.Height = lvStaff.Height = height;

            btSelect.Top = btCancel.Top = lvStaff.Bottom + 12;
            btAdd.Top = lvStaff.Top + (height - btAdd.Height) / 2;

            this.Height = btSelect.Bottom + 12;
            this.Width = lvCommission.Right + 12;

            this.CenterToScreen();
        }

        private int MakeLong(int wLow, int wHigh)
        {
            int low = (int)IntLoWord(wLow);
            short high = IntLoWord(wHigh);
            int product = 0x10000 * (int)high;
            int mkLong = (int)(low | product);
            return mkLong;
        }

        private short IntLoWord(int word)
        {
            return (short)(word & short.MaxValue);
        }

        private void btAdd_Click(object sender, EventArgs e)
        {
            if(lvStaff.SelectedItems.Count > 0)
            {
                ListViewItem sitem = lvStaff.SelectedItems[0];

                ListViewItem item = new ListViewItem("");
                //ListViewItem.ListViewSubItem subItem0 = new ListViewItem.ListViewSubItem(item, sitem.Index.ToString());

                string temp;
                if(lvCommission.Items.Count > 0)
                    temp = "Член";
                else
                    temp = "Председатель";

                ListViewItem.ListViewSubItem subItem0 = new ListViewItem.ListViewSubItem(item, temp);
                ListViewItem.ListViewSubItem subItem1 = new ListViewItem.ListViewSubItem(item, sitem.SubItems[2].Text);
                item.SubItems.Add(subItem0);
                item.SubItems.Add(subItem1);
                //item.SubItems.Add(subItem2);
                lvCommission.Items.Add(item);

                int index = sitem.Index;
                if(lvStaff.Items.Count > 1)
                    index = sitem.Index;

                lvStaff.Items.RemoveAt(sitem.Index);

                if (index == lvStaff.Items.Count)
                    index--;
                if (index > -1)
                {
                    lvStaff.Select();
                    lvStaff.Items[index].Selected = true;
                }

                if (lvCommission.Items.Count > 0)
                    btSelect.Enabled = true;
            }
        }

        private void btCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private void btSelect_Click(object sender, EventArgs e)
        {
            Members = new List<string>(lvCommission.Items.Count);

            foreach (ListViewItem it in lvCommission.Items)
                Members.Add(it.SubItems[2].Text);

            DialogResult = DialogResult.OK;

            this.Close();
        }
    }
}
