﻿using System;
using FormsEditorEx.GenExam.Records;
using SharedLibrary;

namespace FormsEditorEx.GenExam.Tabs
{
    public partial class ucUrineTab : ucExamTabBase
    {
        public ucUrineTab()
        {
            InitializeComponent();
            ResizePanel();

            tbUrineDate.Validating += DateTextBox_Validating;
            tbUrineDate.KeyPress += DateTextBox_KeyPress;

            rtbUrine.KeyDown += TextBox_KeyDown;
        }

        private void ResizePanel()
        {
            pnContent.Location = ClientRectangle.Location;
            ClientSize = pnContent.Size;
        }
        public override bool AddExamRecord()
        {
            bool res = AddRecord(GenExamType.Urine, FieldsToRecord());

            if (res)
                Clear();

            return res;
        }
        public override void FillTestData()
        {
            tbUrineDate.Text = "23.01.2020";
        }

        private void Clear()
        {
            tbUrineDate.Clear();
            rtbUrine.SetText(string.Empty);
        }

        private UrineRecord FieldsToRecord()
        {
            if (tbUrineDate.MaskCompleted && DateTime.TryParse(tbUrineDate.Text, out DateTime date))
            {
                return new UrineRecord()
                {
                    Date = date,
                    Urine = !string.IsNullOrWhiteSpace(rtbUrine.Text) ? rtbUrine.Text.Trim() : null
                };
            }
            else return null;
        }
        //public override void RecordToFields()
        //{
        //    UrineRecord uri = GenExamResult.Instance.Urine;

        //    if (uri.Date.HasValue)
        //    {
        //        tbUrineDate.Text = Utility.DateToShortString(uri.Date.Value);

        //        rtbUrine.SetText(!string.IsNullOrWhiteSpace(uri.Urine) ? uri.Urine : string.Empty);
        //    }
        //}
    }
}
