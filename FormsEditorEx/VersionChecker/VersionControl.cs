﻿using System;
using FormsEditorEx.Database;
using System.IO;
using System.Reflection;
using System.Windows.Forms;

namespace FormsEditorEx.VersionChecker
{
    public static class VersionControl
    {
        public static CheckVersionResult Check(Version version)
        {

            if(version <= Static.AppVersion)
            {
                return CheckVersionResult.UPTODATE;
            }
            else
            {
                MessageBox.Show("Требуется обновление. Сейчас будет запущена установка актуальной версии.", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                RunInstaller(version);
                return CheckVersionResult.DEPRECATED;
            }
        }
        public static void DeleteIrrelevantFiles()
        {
            try
            {
                foreach(string file in Static.OutdatedFiles)
                {
                    File.Delete(Path.Combine(Application.StartupPath, file));
                }
            }
            catch(Exception ex)
            {
                Logger.AddToLogs(MethodBase.GetCurrentMethod().Name, ex.ToString());
            }
        }
        private static void RunInstaller(Version global)
        {
            string filename = Path.Combine(Paths.Instance.Solutions, "changelog.txt");

            if (File.Exists(filename))
                System.Diagnostics.Process.Start(filename);

            string path = Path.Combine(Paths.Instance.Solutions, $"HMCpro {global.ToString()}.exe");

            if (File.Exists(path))
            {
                System.Diagnostics.Process.Start(path);
            }
            else
                MessageBox.Show("Установочный файл не найден.", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }
        
    }
}
