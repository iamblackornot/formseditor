﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using Newtonsoft.Json.Linq;
using System.IO;
using System.Reflection;
using SharedLibrary;
using FormsEditorEx.Commission;
using FormsEditorEx.Conclusion;
using FormsEditorEx.GenExam;
using Newtonsoft.Json;
using FormsEditorEx.VMP;
using System.Linq;
using System.ComponentModel;
using SharedLibrary.Settings;
using FormsEditorEx.Database;

namespace FormsEditorEx
{
    public partial class MainFrame : Form
    {
        List<string> lastpath = new List<string>();
        public event EventHandler<LoadPatientEventArgs> PatientSearch;
        public int Department { get; private set; } = -1;
        public long? Doctor { get; private set; }
        public DatabaseAdapter dbAdapter { get; }

        ToolTip tt = new ToolTip();
        VMPForm vmpForm;

        IMainFrameState state;

        public MainFrame(DatabaseAdapter dbAdapter)
        {
            InitializeComponent();

            InitComponents(AppConfig.Instance.Info);
#if !DEBUG
                        btTestData.Visible = false;
                        btTest.Visible = false;
#endif
            SetWindowSize();

            this.Text = $"{Static.AppName} {Static.AppVersion}";
            this.dbAdapter = dbAdapter;

#if DEBUG
            btTest.MouseHover += MainFrame_MouseHover;
#endif
            vmpForm = new VMPForm(dbAdapter);
        }

        private void MainFrame_MouseHover(object sender, EventArgs e)
        {
            ucStatusBar.ShowWarning(ActiveControl?.Name);
        }

        private void SetWindowSize()
        {
            StartPosition = FormStartPosition.Manual;
            Location = new Point(0, 0);

            Screen screen = Screen.FromControl(this);
            Size = screen.WorkingArea.Size;
        }

        private void InitComponents(InfoClass info)
        {
            cbDepartment.DataSource = info.Departments;
            cbDepartment.SelectedIndex = -1;

            tbOblast.Text = Static.OblastDefault;

            cbDisability.DataSource = Static.DisabilityName.Values.ToList();
            cbDisability.SelectedIndex = -1;

            cbBloodGroup.DataSource = Static.BloodGroup;
            cbBloodGroup.SelectedIndex = -1;

            cbRh.DataSource = Static.Rh;
            cbRh.SelectedIndex = -1;

            cbGenCondition.DataSource = Static.GenCondition;
            cbGenCondition.SelectedIndex = -1;

            cbNutrition.DataSource = Static.Nutrition;
            cbNutrition.SelectedIndex = -1;

            cbVTEORisk.DataSource = Static.VTEO_Risk;
            cbVTEORisk.SelectedIndex = -1;

            cbGORisk.DataSource = Static.GO_Risk;
            cbGORisk.SelectedIndex = -1;

            cbTreatmentResult.DataSource = Static.TreatmentResult;
            cbTreatmentResult.SelectedIndex = -1;

            cbCondition.DataSource = Static.ConditionPost;
            cbCondition.SelectedIndex = -1;

            cbWorkCapacity.DataSource = Static.WorkCapacityName.Values.ToList();
            cbWorkCapacity.SelectedIndex = -1;

            cbSickLeaveEndType.DataSource = Static.SickLeaveEndTypeName.Values.ToList();
            cbSickLeaveEndType.SelectedIndex = -1;

            SetCues();

            InitToolTip();
        }
        private void InitToolTip()
        {
            tt.InitialDelay = 250;
            tt.AutoPopDelay = 60000;
            tt.SetToolTip(pbVMPHelp, Utility.WrapToolTipText("При активации данной опции создание листа анамнеза начнется с оформления ВМП" +
                " в соответствующем окне. При успешном оформлении ВМП документов начнется оформление непосредственно" +
                " самого листа анамнеза, где в качестве планируемого объема лечения будет указано лечение, выбранное" +
                " в справочнике ВМП на этапе создания ВМП документов. Если опция не активирована, то объем лечения" +
                " указывается вручную, при этом остается возможность оформить ВМП документы позже (до создания выписки)"));

            tt.SetToolTip(pbChemHelp, Utility.WrapToolTipText("При активации этап создания предоперационного эпикриза будет пропущен." +
                " Это необходимо в случае лечения химиотерапией."));
        }

        private void TextBox_KeyDown(object sender, KeyEventArgs e)
        {
            Utility.ReturnToTab(this, e);
        }
        public void ClearAndReset()
        {
            vmpForm.Reset();
            ClearForm();
        }

        private void ClearForm()
        {
            Utility.ClearControls(this, Static.NonClearControls);

            tbOblast.Text = Static.OblastDefault;
            rbTown.Checked = true;
            cbSickLeave.Checked = false;
            cbVMP.Checked = false;
            cbChemTherapy.Checked = false;

            for (int i = 0; i < clbVTEOPrevention.Items.Count; i++)
                clbVTEOPrevention.SetItemCheckState(i, CheckState.Unchecked);
        }
        private void btOpen_Click(object sender, EventArgs e)
        {
            foreach (string path in lastpath)
                System.Diagnostics.Process.Start(path);
        }
        private void btOpenDirectory_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start(Paths.Instance.GetSolutionsDirectory(Department, Doctor));
        }

        private void MainFrame_Load(object sender, EventArgs e)
        {
            SetControlPositions();
            this.Activate();
        }
        private void SetSuperIntendent(int? depId, long? personnelId = null)
        {
            if (depId.HasValue)
            {
                tbSuperintendent.DisplayMember = "ShortName";
                tbSuperintendent.ValueMember = "PersonnelId";

                var docList = AppConfig.Instance.Staff.Departments.DoctorsByDepartment[depId.Value];
                tbSuperintendent.DataSource = docList;

                if(!personnelId.HasValue)
                {
                    personnelId = AppConfig.Instance.Staff.Departments.HeadByDepartment[depId.Value].PersonnelId;
                }

                int index = docList.FindIndex((Personnel p) => p.PersonnelId == personnelId);
                tbSuperintendent.SelectedIndex = index;

                if(index < -1) 
                {
                    tbSuperintendent.Text = AppConfig.Instance.Staff[personnelId.Value].ShortName;
                }               
            }
            else
            {
                tbSuperintendent.DataSource = null;
            }

            SetEnding();
        }
        private void TbSuperintendent_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            SetEnding();
        }
        private void SetEnding()
        {
            string shortName = tbSuperintendent.Text.Trim();

            if (!string.IsNullOrWhiteSpace(shortName))
            {
                Regex expression = new Regex(RegexPattern.ShortName);

                Match match = expression.Match(shortName);
                if (match.Success && match.Groups.Count == 4)
                {
                    cbSuperIntendentFormatted.Text = string.Empty;
                    cbSuperIntendentFormatted.Items.Clear();

                    string firstName = match.Groups[1].Value;
                    string initials = match.Groups[3].Value;
                    string ending = string.Empty;
                    
                    switch(firstName.Substring(firstName.Length - 2, 2))
                    {
                        case "ев":
                        case "ов":
                        case "ын":
                        case "ин":
                        {
                                ending = Static.Endings[(int)Sex.Male];
                                break;
                        }
                        case "ва":
                        case "на":
                        {
                                firstName = firstName.Substring(0, firstName.Length - 1);
                                ending = Static.Endings[(int)Sex.Female];
                                break;
                        }
                    }

                    cbSuperIntendentFormatted.Items.Add($"{firstName}{ending} {initials}");

                    cbSuperIntendentFormatted.SelectedIndex = 0;
                }
            }
        }
        private void btTestData_Click(object sender, EventArgs e)
        {
            state.FillTestData();
        }

        private void tsAbout_Click(object sender, EventArgs e)
        {
            using (AboutForm aboutForm = new AboutForm())
                aboutForm.ShowDialog();
        }
        private void MainFrame_Click(object sender, EventArgs e)
        {
            this.ActiveControl = null;
        }

        private void btGenExamForm_Click(object sender, EventArgs e)
        {
            bool isReadOnly = !state.Type.In(MainFrameState.Anamnesis, MainFrameState.PreoperativeEpicrisis, MainFrameState.DischargeSummary) || !CheckIfPatientAvailable();

            if (isReadOnly)
            {
                using (var frm = new ExtendedSingleFieldEditForm("Общее обследование", Patient.Instance.PatientData.GenExam.GetString()))
                {
                    frm.ShowDialog(this);
                }
            }
            else
            {
                using (GenExamForm frm = new GenExamForm())
                {
                    frm.ShowDialog(this);
                }
            }

            SelectNextControl(ActiveControl, true, true, true, true);
        }

        private void cbSickLeave_CheckedChanged(object sender, EventArgs e)
        {
            bool isChecked = cbSickLeave.Checked;

            tbSickLeaveNum.Enabled = tbSickLeaveStart.Enabled = tbSickLeaveEnd.Enabled = cbSickLeaveExt.Enabled = cbSickLeaveEndType.Enabled = isChecked;

            if (isChecked)
            {
                cbSickLeave.ForeColor = lbSickLeave1.ForeColor = lbSickLeave2.ForeColor = SystemColors.ControlText;
                ActiveControl = tbSickLeaveNum;
            }
            else
            {
                cbSickLeave.ForeColor = lbSickLeave1.ForeColor = lbSickLeave2.ForeColor = SystemColors.ControlDark;
                cbSickLeaveExt.Checked = false;
            }
        }

        private void cbSickLeaveExt_CheckedChanged(object sender, EventArgs e)
        {
            bool isChecked = cbSickLeaveExt.Checked;

            tbSickLeaveExtStart.Enabled = tbSickLeaveExtEnd.Enabled = isChecked;

            if (isChecked)
            {
                cbSickLeaveExt.ForeColor = lbSickLeave3.ForeColor = lbSickLeave4.ForeColor = SystemColors.ControlText;
                ActiveControl = tbSickLeaveExtStart;
            }
            else
                cbSickLeaveExt.ForeColor = lbSickLeave3.ForeColor = lbSickLeave4.ForeColor = SystemColors.ControlDark;


        }

        private void btAccompMedicationCopy_Click(object sender, EventArgs e)
        {
            rtbAccompMedicationPost.SetText(!rtbAccompMedicationPre.IsCueOn ? rtbAccompMedicationPre.Text : string.Empty);
            rtbAccompMedicationPost.Focus();
        }

        private void btTest_Click(object sender, EventArgs e)
        {
            //MessageBox.Show(ActiveControl?.Name);
            //Dump();
            //Patient.Instance.PatientData.PersonalInfo.Disability = null;
            //cbDisability.SelectedIndex = -1;
            //btVMP.Enabled = true;
            //Middleware.Handle(delegate { Patient.Load("1111111"); }, ucStatusBar);
            //Patient.Instance.FirstName = "Борис";
            //Middleware.Handle(delegate { Patient.Save(); }, ucStatusBar);

            //Middleware.HandleDBAction(() => DbHandler.Instance.CreatePatient("1111111"), ucStatusBar);

            //CheckPatientAvailabilty("999999");

            //DBTweaks.TweakPersonalInfo();
            //DBTweaks.TweakLifeAnamnesis();
            //DBTweaks.TweakTemporaryFields();


            //string str = "Ch6/TmvSkFCfrPOZMLs/q1UmADHzUBvv1Qsu4jQYzO9jqkMqdmhDGEdsp9qlixzOJ/CT2Liu1liiqQjHBXQVYDkM3R2x0Iub718nPGSXTHndd3sb76qICJDmL1Uc2e/bLCeFco0rnhMo299sZXwdOXbEi4z3GXa7NFBGbVkV0Jpn+JLuXJNFKHRZ/o3kK9aW+7dEIFCTyKEOUPoucosEztuL8wCxnGJfo1bfRACTBPQUX6/xJrb/Sp4XbST818tpcUjeOr1ya1coUNmifj/qupSNOUfoiIkpvwXZldoOUfLlOZvVoipp018qZ6HBoA9b8ZZh9nTO5cgNRLrGLR0AB6kRJmN+L5xJnhdtJPzXy2nbi/MAsZxiXxWv8PK21+9m00CmO26qT6JtYcBgu03AkzaJXgYI6hQn/ilX4fM3BMbWRb81FI7xT+ma0PwELI58ybTApZE8VMiY/yYBUVDjGqkFR2naNxcAM/C8Rxq76+uIkNqbNlBlFuFVyDLAzg0/";
            //var json = Cryptography.DecryptString(str, Cryptography.ENC);
            //PersonalInfo pi = new PersonalInfo() { PassDate = DateTime.Parse("25/04/2002 12:20:03") };
            //string date = string.Format("{0:yyyy-MM-ddTHH:mm:ss}", pi.PassDate);

            //string json = JsonConvert.SerializeObject(pi);
        }

        private void RtbDiagnosisPre_ClientSizeChanged(object sender, EventArgs e)
        {
            if(sender is RichTextBox rtb)
            {
                rtb.ScrollBars = RichTextBoxScrollBars.None;
                rtb.ScrollBars = RichTextBoxScrollBars.Vertical;
            }
        }

        private void btPatients_Click(object sender, EventArgs e)
        {
            Owner?.Show();
            Hide();
        }

        private void tbAmbuCardNum_TextChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(tbAmbuCardNum.Text) && tbAmbuCardNum.Focused)
                btSearch.Show();
            else
                btSearch.Hide();
        }

        private void tbAmbuCardNum_Leave(object sender, EventArgs e)
        {
            if (btSearch.ContainsFocus)
                btSearch_Click(btSearch, EventArgs.Empty);
            btSearch.Hide();
        }

        private void tbAmbuCardNum_Enter(object sender, EventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(tbAmbuCardNum.Text))
                btSearch.Show();
        }

        private void btSearch_Click(object sender, EventArgs e)
        {
            tbAmbuCardNum.Focus();
            PatientSearch?.Invoke(this, new LoadPatientEventArgs() { PatientId = Patient.Instance.AmbuCardNum });
        }
        private void btCommissions_Click(object sender, EventArgs e)
        {
            bool isReadOnly = !state.Type.In(MainFrameState.Anamnesis, MainFrameState.PreoperativeEpicrisis, MainFrameState.DischargeSummary) || !CheckIfPatientAvailable();

            using (CommissionForm cf = new CommissionForm(isReadOnly))
            {
                cf.ShowDialog(this);
                SelectNextControl(ActiveControl, true, true, true, true);
            }
        }

        private void cbIO_CheckedChanged(object sender, EventArgs e)
        {
            if (cbIO.Checked)
            {
                tbSuperintendent.Enabled = true;
            }
            else
            {
                tbSuperintendent.Enabled = false;
                SetSuperIntendent(Patient.Instance.Department);
            }
        }

        public class FieldEntry
        {
            public string Name { get; set; }
            public string Value { get; set; }
            public FieldEntry(string _name, string _value)
            {
                Name = _name;
                Value = _value;
            }
        }

        public void Dump()
        {
            try
            {
                string dir = Path.Combine(Paths.Instance.Solutions, "logs");

                if (!Directory.Exists(dir))
                {
                    Directory.CreateDirectory(dir);
                }

                string logs_destination_path = Path.Combine(dir, Utility.RemoveSpecialCharacters($"{Utility.DateToExtendedShortString(DateTime.Now)} {cbDoctors.Text} dump.txt"));

                File.AppendAllText(logs_destination_path, FieldsToJSON());
            }
            catch (Exception ex)
            {
                Logger.AddToLogs(MethodBase.GetCurrentMethod().Name, ex.ToString());
            }
        }

        private string FieldsToJSON()
        {
            JObject jobj = new JObject();

            jobj.Add(new JProperty("Patient", JsonConvert.SerializeObject(Patient.Instance, Formatting.Indented)));

            List<FieldEntry> lst = new List<FieldEntry>();

            lst.Add(new FieldEntry(cbDepartment.Name, cbDepartment.SelectedIndex.ToString()));

            FieldsToList(this, ref lst);

            jobj.Add(new JProperty("Fields", JsonConvert.SerializeObject(lst, Formatting.Indented)));

            return jobj.ToString();
        }

        private void FieldsToList(Control ctrl, ref List<FieldEntry> lst)
        {
            string temp = null;
            foreach (Control c in ctrl.Controls)
            {
                if (c.Name != "")
                {
                    if (c is TextBoxBase tbb)
                    {
                        if (c is PlainRichTextBoxExt ext)
                        {
                            if (!ext.IsCueOn)
                                lst.Add(new FieldEntry(c.Name, c.Text));
                        }
                        else if (c is TextBoxEx ex)
                        {
                            if (!ex.IsCueOn)
                                lst.Add(new FieldEntry(c.Name, c.Text));
                        }
                        else
                        {
                            temp = tbb.Text.Trim();

                            if (c is MaskedTextBox)
                            {
                                if (!string.IsNullOrWhiteSpace(temp))
                                    temp = Regex.Replace(temp, "[^0-9]", "");
                            }
                            if (temp.Length > 0)
                                lst.Add(new FieldEntry(c.Name, temp));
                        }
                    }

                    else if (c is ComboBox cb)
                    {
                        int index = -1;
                        index = cb.SelectedIndex;
                        if (index >= 0)
                            lst.Add(new FieldEntry(c.Name, index.ToString()));
                        else
                        {
                            string text = text = cb.Text.Trim();

                            if (!string.IsNullOrWhiteSpace(text))
                                lst.Add(new FieldEntry(c.Name, text));
                        }
                    }

                    if (c.Controls.Count > 0)
                    {
                        FieldsToList(c, ref lst);
                    }
                }
            }
        }
        private void cbGORisk_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyData == Keys.Enter)
            {
                e.SuppressKeyPress = true;
                clbVTEOPrevention.Focus();
                clbVTEOPrevention.SetSelected(0, true);
            }
        }

        private void clbVTEOPrevention_Leave(object sender, EventArgs e)
        {
            clbVTEOPrevention.ClearSelected();
        }

        private void btConclusions_Click(object sender, EventArgs e)
        {
            bool isReadOnly = !state.Type.In(MainFrameState.Anamnesis, MainFrameState.PreoperativeEpicrisis, MainFrameState.DischargeSummary) || !CheckIfPatientAvailable();

            using (ConclusionForm f = new ConclusionForm(isReadOnly))
            {
                f.ShowDialog();
            }

            SelectNextControl(ActiveControl, true, true, true, true);
        }
        private void SetCues()
        {
            rtbComplicationsPre.Cue = Static.Cues[FieldWithCue.Complications];
            rtbComplicationsPost.Cue = Static.Cues[FieldWithCue.Complications];
            rtbAccompanying.Cue = Static.Cues[FieldWithCue.Accompanying];
            rtbAccompanyingPost.Cue = Static.Cues[FieldWithCue.Accompanying];
            rtbAccompMedicationPre.Cue = Static.Cues[FieldWithCue.AccompMedication];
            rtbAccompMedicationPost.Cue = Static.Cues[FieldWithCue.AccompMedicationPost];
            rtbMedicalAllergy.Cue = Static.Cues[FieldWithCue.MedicalAllergy];

            rtbMedication.Cue = Static.Cues[FieldWithCue.Medication];
            rtbTreatment.Cue = Static.Cues[FieldWithCue.Treatment];
        }
        public void SetLastPath(string path)
        {
            SetLastPath(new List<string>() { path });
        }
        public void SetLastPath(List<string> paths)
        {
            if (paths.Count > 0)
            {
                lastpath = paths;
                btOpen.Enabled = true;
            }
        }
        private void TextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            Utility.ReturnToTab(this, e);
        }

        private void btAnamnesis_Click(object sender, EventArgs e)
        {
            bool isReadOnly = !state.Type.In(MainFrameState.Anamnesis) || !CheckIfPatientAvailable();

            using (var frm = new ExtendedSingleFieldEditForm("Анамнез заболевания", Patient.Instance.Anamnez.EmptyIfNull(), isReadOnly))
            {
                DialogResult res = frm.ShowDialog();

                if(res == DialogResult.OK && !isReadOnly)
                {
                    Patient.Instance.Anamnez = !string.IsNullOrEmpty(frm.InputText) ? frm.InputText : null;
                }
            }

            SelectNextControl(ActiveControl, true, true, true, true);
        }

        private void btOperTeamNeuro_Click(object sender, EventArgs e)
        {
            Patient p = Patient.Instance;

            if (state.Type.In(MainFrameState.ReadOnly) || (p.Department.HasValue && Static.NeuroStatusEnabled[p.Department.Value]))
            {
                bool isReadOnly = !state.Type.In(MainFrameState.Anamnesis) || !CheckIfPatientAvailable();

                using (var frm = new ExtendedSingleFieldEditForm("Неврологический статус", !string.IsNullOrEmpty(p.NeuroStatus) ? p.NeuroStatus 
                    : Static.NeuroStatusDefault, isReadOnly))
                {
                    DialogResult res = frm.ShowDialog();

                    if (res == DialogResult.OK && !isReadOnly)
                    {
                        p.NeuroStatus = !string.IsNullOrEmpty(frm.InputText) && frm.InputText != Static.NeuroStatusDefault ? frm.InputText : null;
                    }
                }
            }
            else
            {
                bool isReadOnly = !state.Type.In(MainFrameState.PreoperativeEpicrisis) || !CheckIfPatientAvailable();

                using (var frm = new OperTeamForm(isReadOnly))
                {
                    frm.ShowDialog();
                }
            }

            SelectNextControl(ActiveControl, true, true, true, true);
        }

        private void btTreatment_Click(object sender, EventArgs e)
        {
            bool isReadOnly = !state.Type.In(MainFrameState.DischargeSummary) || !CheckIfPatientAvailable();

            using (var frm = new ExtendedSingleFieldEditForm("Лечение", Patient.Instance.Treatment.EmptyIfNull(), isReadOnly))
            {
                DialogResult res = frm.ShowDialog();

                if (res == DialogResult.OK && !isReadOnly)
                {
                    Patient.Instance.Treatment = !string.IsNullOrEmpty(frm.InputText) ? frm.InputText : null;
                }
            }

            SelectNextControl(ActiveControl, true, true, true, true);
        }
        private void btInstrumentalExamination_Click(object sender, EventArgs e)
        {
            bool isReadOnly = !state.Type.In(MainFrameState.Anamnesis, MainFrameState.PreoperativeEpicrisis, MainFrameState.DischargeSummary) || !CheckIfPatientAvailable();

            using (var frm = new ExtendedSingleFieldEditForm("Инструментальные методы обследования", Patient.Instance.InstrExam.EmptyIfNull(), isReadOnly))
            {
                DialogResult res = frm.ShowDialog();

                if (res == DialogResult.OK && !isReadOnly)
                {
                    Patient.Instance.InstrExam = !string.IsNullOrEmpty(frm.InputText) ? frm.InputText : null;
                }
            }

            SelectNextControl(ActiveControl, true, true, true, true);
        }
        private void btLifeAnamnesis_Click(object sender, EventArgs e)
        {
            bool isReadOnly = !state.Type.In(MainFrameState.Anamnesis) || !CheckIfPatientAvailable();

            Patient p = Patient.Instance;
            string preData = p.PatientData.TemporaryFields.LifeAnamnesisPre;
            bool usePreData = !string.IsNullOrEmpty(preData);

            LifeAnamnesisData lad = usePreData
                ? JsonConvert.DeserializeObject<LifeAnamnesisData>(preData)
                : p.PatientData.LifeAnamnesisData;

            using (var frm = new LifeAnamnesisForm(ref lad, isReadOnly))
            {
                frm.ShowDialog();

                if(usePreData)
                {
                    p.PatientData.TemporaryFields.LifeAnamnesisPre = lad.Serialize();
                }
            }

            SelectNextControl(ActiveControl, true, true, true, true);
        }

        private void MainFrame_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = true;
            Owner?.Show();
            Hide();
        }
        public bool CheckIfPatientAvailable()
        {
            Patient p = Patient.Instance;

            return (!p.Department.HasValue && !p.Doctor.HasValue) ||
                    (p.Department.HasValue && (p.Department.Value == Department) && Doctor.HasValue && (p.Doctor == Doctor));
        }
        protected void DateTextBox_Validating(object sender, CancelEventArgs e)
        {
            if (sender is MaskedTextBox tb)
            {
                if (!tb.MaskCompleted)
                {
                    if (tb.Text != "  .  .")
                    {
                        tt.Show("Неверная дата", tb, 25, 25, 3000);
                        e.Cancel = true;
                    }
                    //else
                }
                //else
            }
        }
        protected void TimeTextBox_Validating(object sender, CancelEventArgs e)
        {
            if (sender is MaskedTextBox tb)
            {
                if (!tb.MaskCompleted)
                {
                    if (tb.Text != "  :")
                    {
                        tt.Show("Неверное время", tb, 25, 25, 3000);
                        e.Cancel = true;
                    }
                    //else
                }
                //else
            }
        }

        private void btAction_Click(object sender, EventArgs e)
        {
            state = state.Action(ucStatusBar);
        }

        private void btRevert_Click(object sender, EventArgs e)
        {
            state = state.Revert(ucStatusBar);
        }

        private void cbVMP_CheckedChanged(object sender, EventArgs e)
        {
            rtbPlannedOperationScope.Enabled = !cbVMP.Checked;
        }

        private void btVMP_Click(object sender, EventArgs e)
        {
            vmpForm.ShowDialog(this);
        }
        public bool RunVMP()
        {
            vmpForm.ShowDialog(this);

            return vmpForm.IsDone;
        }

        private void btRecommendations_Click(object sender, EventArgs e)
        {
            bool isReadOnly = !state.Type.In(MainFrameState.DischargeSummary) || !CheckIfPatientAvailable();

            TemporaryData td = Patient.Instance.PatientData.TemporaryFields;

            using (var frm = new ExtendedSingleFieldEditForm("Рекомендации", td.Recommendations.EmptyIfNull(), isReadOnly))
            {
                DialogResult res = frm.ShowDialog();

                if (res == DialogResult.OK && !isReadOnly)
                {
                    td.Recommendations = !string.IsNullOrEmpty(frm.InputText) ? frm.InputText : null;
                }
            }

            SelectNextControl(ActiveControl, true, true, true, true);
        }

        private void MainFrame_Resize(object sender, EventArgs e)
        {
            SetControlPositions();
        }
        private void SetControlPositions()
        {
            pnCenteredResult.Left = (pnResult.Width - pnCenteredResult.Width) / 2;
            pnVTEO.Left = (gpVTEO.Width - pnVTEO.Width) / 2;
        }

        private void btStatusLocalisPost_Click(object sender, EventArgs e)
        {
            bool isReadOnly = !state.Type.In(MainFrameState.DischargeSummary) || !CheckIfPatientAvailable();

            Patient p = Patient.Instance;
            string text = !string.IsNullOrEmpty(p.StatusLocalis) ? p.StatusLocalis : Static.StatusLocalisPostCue[p.Department.Value];

            using (var frm = new ExtendedSingleFieldEditForm("Status Localis", text, isReadOnly))
            {
                DialogResult res = frm.ShowDialog();

                if (res == DialogResult.OK && !isReadOnly)
                {
                    p.StatusLocalis = !string.IsNullOrEmpty(frm.InputText) ? frm.InputText : null;
                }
            }

            SelectNextControl(ActiveControl, true, true, true, true);
        }

        private void btLifeAnamnesisPost_Click(object sender, EventArgs e)
        {
            bool isReadOnly = !state.Type.In(MainFrameState.DischargeSummary) || !CheckIfPatientAvailable();

            LifeAnamnesisData lad = Patient.Instance.PatientData.LifeAnamnesisData;

            using (var frm = new LifeAnamnesisForm(ref lad, isReadOnly))
            {
                frm.ShowDialog();
            }

            SelectNextControl(ActiveControl, true, true, true, true);
        }

        private void tbOblast_TextChanged(object sender, EventArgs e)
        {
            PopulateTownsIfNeeded();
            PopulateProspectsIfNeeded();
        }

        private void PopulateTownsIfNeeded()
        {
            if (rbTown.Checked && tbOblast.Text == "Ярославская")
                foreach (string town in AppConfig.Instance.Info.Cities)
                    cbTown.Items.Add(town);
            else
            {
                cbTown.SelectedIndex = -1;
                cbTown.Items.Clear();
            }
        }

        private void PopulateProspectsIfNeeded()
        {
            if (rbTown.Checked && cbTown.Text == "Ярославль" && cbStreetType.SelectedIndex == 1 && tbOblast.Text == "Ярославская")
            {
                cbStreet.SelectedIndex = -1;
                cbStreet.Items.Clear();

                foreach (InfoClass.Prospect pr in AppConfig.Instance.Info.Prospects)
                    cbStreet.Items.Add(pr.Name);
            }
            else
            {
                cbStreet.SelectedIndex = -1;
                cbStreet.Items.Clear();
            }
        }

        private void btDisabilityInfo_Click(object sender, EventArgs e)
        {
            bool isReadOnly = state.Type.In(MainFrameState.ReadOnly, MainFrameState.Discharge) || !CheckIfPatientAvailable();

            TemporaryData td = Patient.Instance.PatientData.TemporaryFields;
            string text = td.DisabilityListInfo.EmptyIfNull();

            using (var frm = new ExtendedSingleFieldEditForm("Сведения о листке нетрудоспособности", text, isReadOnly))
            {
                DialogResult res = frm.ShowDialog();

                if (res == DialogResult.OK && !isReadOnly)
                {
                    td.DisabilityListInfo = !string.IsNullOrEmpty(frm.InputText) ? frm.InputText : null;
                }
            }

            SelectNextControl(ActiveControl, true, true, true, true);
        }

        private void cbTown_TextChanged(object sender, EventArgs e)
        {
            PopulateProspectsIfNeeded();
        }

        private void cbStreetType_SelectedIndexChanged(object sender, EventArgs e)
        {
            PopulateProspectsIfNeeded();
        }

        private void rbTown_CheckedChanged(object sender, EventArgs e)
        {
            PopulateTownsIfNeeded();
            PopulateProspectsIfNeeded();

            if (!rbTown.Checked)
                cbStreetType.SelectedIndex = 0;
        }

        private void btTransfusion_Click(object sender, EventArgs e)
        {
            bool isReadOnly = !state.Type.In(MainFrameState.DischargeSummary) || !CheckIfPatientAvailable();

            TemporaryData td = Patient.Instance.PatientData.TemporaryFields;

            using (var frm = new ExtendedSingleFieldEditForm("Трансфузии", td.Transfusion.EmptyIfNull(), isReadOnly))
            {
                DialogResult res = frm.ShowDialog();

                if (res == DialogResult.OK && !isReadOnly)
                {
                    td.Transfusion = !string.IsNullOrEmpty(frm.InputText) ? frm.InputText : null;
                }
            }

            SelectNextControl(ActiveControl, true, true, true, true);
        }

        private void btAdditionalInfo_Click(object sender, EventArgs e)
        {
            bool isReadOnly = !state.Type.In(MainFrameState.DischargeSummary) || !CheckIfPatientAvailable();

            TemporaryData td = Patient.Instance.PatientData.TemporaryFields;

            using (var frm = new ExtendedSingleFieldEditForm("Дополнительные сведения", td.AdditionalInfo.EmptyIfNull(), isReadOnly))
            {
                DialogResult res = frm.ShowDialog();

                if (res == DialogResult.OK && !isReadOnly)
                {
                    td.AdditionalInfo = !string.IsNullOrEmpty(frm.InputText) ? frm.InputText : null;
                }
            }

            SelectNextControl(ActiveControl, true, true, true, true);
        }

        private void tbFlat_KeyPress(object sender, KeyPressEventArgs e)
        {
            ActiveControl = tbDateExam;
        }
    }
}
