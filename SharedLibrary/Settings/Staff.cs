﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;

namespace SharedLibrary.Settings
{
    public class Personnel
    {
        public static readonly long PLACEHOLDER_ID = 0;
        public long? PersonnelId { get; set; }
        public string ShortName { get; set; }
        public bool IsActive { get; set; }
        public long SpecialtyId { get; set; }
    }
    public class Deputy: Personnel
    {
        public long PositionId { get; set; }
        public string Description { get; set; }
    }
    public class PlaceholderPersonnel : Personnel
    {
        public PlaceholderPersonnel()
        {
            PersonnelId = PLACEHOLDER_ID;
            ShortName = "-- не определен --";
            IsActive = true;
        }
    }
    public class PlaceholderDeputy : Deputy
    {
        public PlaceholderDeputy(Deputy old)
        {
            PersonnelId = PLACEHOLDER_ID;
            ShortName = "-- не назначен --";
            IsActive = true;
            Description = old.Description;
            PositionId = old.PositionId;
        }
    }
    public class DoctorSpecialty
    {
        public static readonly long PLACEHOLDER_ID = 0;
        public long SpecialtyId { get; set; }
        public string Title { get; set; }
    }

    public class PersonnelCollection
    {
        public List<Personnel> AllPersonnel { get; private set; }
        private Dictionary<long, Personnel> _personnelById { get; }
        public Personnel this[long id]
            {
                get
                {
                    if (!_personnelById.ContainsKey(id)) return new PlaceholderPersonnel();
                    return _personnelById[id];
                }
            }
        public DepartmentCollection Departments { get; }
        public AdministrationCollection Administration { get; }
        public List<DoctorSpecialty> Specialties { get; }
        public PersonnelCollection(
            in IEnumerable<Personnel> allStaffList,
            in IEnumerable<DepartmentRecord> doctorList,
            in IEnumerable<DepartmentRecord> depHeadsList,
            in IEnumerable<Deputy> adminList,
            in IEnumerable<DoctorSpecialty> specialtyList)
        {
            AllPersonnel = new List<Personnel>(allStaffList);
            AllPersonnel.Sort(new PersonComparer());
            _personnelById = new Dictionary<long, Personnel>();
            Departments = new DepartmentCollection();

            BuildIndices();
            SetDepartmentData(doctorList, depHeadsList);

            Administration = new AdministrationCollection(adminList);
            Specialties = new List<DoctorSpecialty>(specialtyList);
        }

        public string GetPersonnelSpecialtyTitle(long personnelId)
        {
            if(!_personnelById.ContainsKey(personnelId))
            {
                return "--сотрудник не найден--";
            }

            long specialtyId = _personnelById[personnelId].SpecialtyId;

            if(specialtyId < 0 || specialtyId >= Specialties.Count) 
            {
                return "--специальность не найдена--";
            }

            return Specialties[(int)specialtyId].Title;
        }

        public string GetPersonnelPositionTitle(long personnelId)
        {
            if (!_personnelById.ContainsKey(personnelId))
            {
                return "--сотрудник не найден--";
            }

            int adminIndex = Administration.List.FindIndex((Deputy deputy) => deputy.PersonnelId == personnelId);

            if(adminIndex >= 0)
            {
                return Administration.List[adminIndex].Description;
            }

            var depHeads = Departments.HeadByDepartment.Values.ToList<Personnel>();
            int depHeadIndex = depHeads.FindIndex((Personnel depHead) => depHead.PersonnelId == personnelId);

            if(depHeadIndex >= 0)
            {
                return "Заведующий отделением";
            }

            return "Врач";
        }

        private void SetDepartmentData(
            in IEnumerable<DepartmentRecord> doctorList,
            in IEnumerable<DepartmentRecord> depHeadsList)
        {
            foreach (var doctor in doctorList)
            {
                Departments.AddDoctorToDepartment(
                    _personnelById[doctor.PersonnelId],
                    doctor.DepartmentId);
            }

            foreach (var head in depHeadsList)
            {
                Departments.SetDepartmentHead(
                    _personnelById[head.PersonnelId],
                    head.DepartmentId);
            }

            Departments.Sort();
        }

        private void BuildIndices()
        {
            foreach (Personnel person in AllPersonnel)
            {
                _personnelById[person.PersonnelId.Value] = person;
            }
        }
    }
    public class PersonComparer : IComparer<Personnel>
    {
        public int Compare(Personnel x, Personnel y)
        {
            return string.Compare(x.ShortName, y.ShortName);
        }
    }
    public class DepartmentRecord
    {
        public long DepartmentId { get; set; }
        public long PersonnelId { get; set; }
    }
    public class DepartmentCollection
    {
        public Dictionary<long, List<Personnel>> DoctorsByDepartment { get; }
        public Dictionary<long, Personnel> HeadByDepartment { get; }
        public DepartmentCollection() 
        {
            DoctorsByDepartment = new Dictionary<long, List<Personnel>>();
            HeadByDepartment = new Dictionary<long, Personnel>();
        }

        public void AddDoctorToDepartment(Personnel p, long depId)
        {
            if(!DoctorsByDepartment.ContainsKey(depId))
            {
                DoctorsByDepartment[depId] = new List<Personnel>();
            }

            DoctorsByDepartment[depId].Add(p);
        }
        public void RemoveDoctorFromDepartment(Personnel p, long depId)
        {
            DoctorsByDepartment[depId].Remove(p);
        }
        public void SetDepartmentHead(Personnel p, long depId)
        {
            HeadByDepartment[depId] = p;
        }
        public void Sort()
        {
            foreach(var value in DoctorsByDepartment.Values)
            {
                value.Sort(new PersonComparer());
            }
        }
    }
    public class AdministrationCollection
    {
        public readonly static int CMO_ID = 0;
        public readonly static int DEPUTY_SURGERY_ID = 1;
        public Deputy CMO
        { 
            get => List.Count > CMO_ID 
                ? List[CMO_ID] 
                : new PlaceholderDeputy(List[CMO_ID]);
        }
        public Deputy DeputySurgery
        {
            get => List.Count > DEPUTY_SURGERY_ID ? 
                List[DEPUTY_SURGERY_ID] 
                : new PlaceholderDeputy(List[DEPUTY_SURGERY_ID]);
        }
        public List<Deputy> List { get; private set; }
        public AdministrationCollection(in IEnumerable<Deputy> list)
        {
            List = new List<Deputy>(list);
        }
    }
}
