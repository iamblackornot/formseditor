﻿using System;
using SharedLibrary;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Data;
using Word = Microsoft.Office.Interop.Word;

namespace FormsEditorEx.Conclusion
{
    public class ConclusionRecords
    {
        public DataTable Data { get; private set; }
        public ConclusionRecords()
        {
            InitTable();
        }
        public void Reset()
        {
            Data.Clear();
        }
        private void InitTable()
        {
            Data = new DataTable();
            Data.Columns.Add("type", typeof(int));
            Data.Columns.Add("num", typeof(string));
            Data.Columns.Add("conclusion", typeof(string));
            Data.Columns.Add("ispost", typeof(bool));

            Data.AcceptChanges();
        }
        public string Serialize()
        {
            Data.AcceptChanges();

            string json = JsonConvert.SerializeObject(Data);
            return !json.Equals(Static.EmptyJArray) ? json : null; 
        }
        public void Deserialize(string json)
        {
            DataTable dt = Data;
            dt.Clear();

            if (!string.IsNullOrEmpty(json))
            {
                JArray jarr = JArray.Parse(json);

                if (jarr is JArray && jarr.HasValues)
                {
                    foreach (var jobj in jarr)
                    {
                        dt.Rows.Add(int.Parse(jobj["type"].ToString()), jobj["num"].ToString(), jobj["conclusion"].ToString(),
                            jobj["ispost"] is JToken ? bool.Parse(jobj["ispost"].ToString()) : false ///remove later
                            );
                    }
                    dt.AcceptChanges();
                }
            }
        }
        public void FillTestData()
        {
            Data.Rows.Add(0, "123", "заключение1 заключение1 заключение1 заключение1 заключение1 заключение1 заключение1 заключение1 заключение1", false);
            Data.Rows.Add(1, "124", "заключение2", false);

            Data.AcceptChanges();
        }
        public void FillTestPostData()
        {
            Data.Rows.Add(0, "125", "послеоперационное заключение", true);
            Data.Rows.Add(1, "126", "послеоперационное заключение 2", true);

            Data.AcceptChanges();
        }
        public string GetString()
        {
            Data.AcceptChanges();

            string temp = string.Empty;

            foreach(DataRow row in Data.Rows)
            {
                if (temp.Length > 0)
                    temp += Environment.NewLine;

                string type = int.TryParse(row["type"].ToString(), out int type_id) ? Static.ConclusionTypes[type_id] : string.Empty;

                temp += $"{type} заключение № {row["num"].ToString()}: {row["conclusion"].ToString()}";
            }

            return temp;
        }
        public void ResetPost()
        {
            foreach (DataRow row in Data.Rows)
            {
                row["ispost"] = false;
            }

            Data.AcceptChanges();
        }
        public bool ValidatePost()
        {
            foreach (DataRow row in Data.Rows)
            {
                if (bool.Parse(row["ispost"].ToString()) == true)
                {
                    return true;
                }
            }

            return false;
        }
        public void BuildDocFString(Word.Document doc, string tag)
        {
            Data.AcceptChanges();

            Word.Range range = doc.Content;
            if (range.Find.Execute(tag))
            {
                string temp = string.Empty;

                if (Data.Rows.Count > 0)
                {
                    temp = ", цитологического";

                    DataRowCollection rows = Data.Rows;
                    foreach(DataRow row in rows)
                    {
                        if(int.TryParse(row["type"].ToString(), out int type))
                        {
                            if(type != (int)ConclusionType.Сytological)
                            {
                                temp = ", гистологического";
                                break;
                            }
                        }
                    }
                }

                range.Text = temp + (temp.Length > 0 ? " заключения" : string.Empty);
            }
        }
        public void BuildDocString(Word.Document doc, string tag, bool ispost = false)
        {
            Data.AcceptChanges();

            Word.Range range = doc.Content;
            if (range.Find.Execute(tag))
            {
                range.Text = string.Empty;

                if (Data.Rows.Count > 0)
                {
                    DataRowCollection rows = Data.Rows;

                    bool rows_printed = false;

                    for (int i = 0; i < rows.Count; i++)
                    {
                        if (bool.TryParse(rows[i]["ispost"].ToString(), out bool post) && post == ispost)
                        {
                            rows_printed = true;

                            range = doc.Range(range.End, range.End);
                            range.Text = Environment.NewLine;
                            range = doc.Range(range.End, range.End);

                            string type = int.TryParse(rows[i]["type"].ToString(), out int type_id) ? Static.ConclusionTypes[type_id] : string.Empty;

                            range.Text = $"{type} заключение № {rows[i]["num"].ToString()}: ";
                            range.Bold = 1;

                            range = doc.Range(range.End, range.End);
                            range.Text = $"{ rows[i]["conclusion"].ToString()}";
                            range.Bold = 0;
                        }
                    }

                    if(rows_printed)
                    {
                        range = doc.Range(range.End, range.End);
                        range.Text = Environment.NewLine;
                    }
                }
            }
        }
    }
}
