﻿using System;
using System.Windows.Forms;
using FormsEditorEx.GenExam.Tabs;
using FormsEditorEx.GenExam.Records;
using System.ComponentModel;

namespace FormsEditorEx.GenExam
{
    [ToolboxItem(false)]
    public partial class ucExamTabBase : UserControl, IExamTab
    {
        private ToolTip tt = new ToolTip();
        public ucExamTabBase()
        {
            InitializeComponent();
            //Dock = DockStyle.Fill;
            Visible = false;
        }
        public virtual void Activate()
        {
            Show();
            SelectNextControl(this, true, true, true, true);
        }
        public virtual void Deactivate()
        {
            Hide();
        }

        public bool AddRecord(GenExamType type, IExamRecord rec)
        {
            if (rec is null)
                return false;

            int i = Patient.Instance.PatientData.GenExam.AddRecord(type, new GenExamRecord() { Date = rec.Date, Result = rec.GetString() });

            if (i > -1)
                return true;
            else
                return false;
        }
        private void InitializeComponent()
        {
            this.SuspendLayout();
            // 
            // ucExamTabBase
            // 
            this.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Name = "ucExamTabBase";
            this.Size = new System.Drawing.Size(795, 421);
            this.ResumeLayout(false);

        }

        public virtual bool AddExamRecord()
        {
            throw new NotImplementedException();
        }

        public virtual void FillTestData()
        {
            throw new NotImplementedException();
        }

        public virtual void RecordToFields()
        {
            throw new NotImplementedException();
        }
        protected void DateTextBox_Validating(object sender, CancelEventArgs e)
        {
            if (sender is MaskedTextBox tb)
            {
                if (!tb.MaskCompleted)
                {
                    if (tb.Text != "  .  .")
                    {
                        tt.Show("Неверная дата", tb, 0, -20, 5000);
                        e.Cancel = true;
                    }
                    //else
                }
                //else
            }
        }
        protected void DateTextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (sender is MaskedTextBox tb)
            {
                tt.Hide(tb);
            }

            ReturnToTab(e);
        }
        private void ReturnToTab(KeyPressEventArgs e)
        {
            if (e.KeyChar == Convert.ToChar(Keys.Return))
            {
                e.Handled = true;
                SelectNextControl(ActiveControl, true, true, true, true);
            }
        }
        private void ReturnToTab(KeyEventArgs e)
        {
            if (e.KeyData == Keys.Enter)
            {
                e.SuppressKeyPress = true;
                SelectNextControl(ActiveControl, true, true, true, true);
            }
        }

        protected void TextBox_KeyDown(object sender, KeyEventArgs e)
        {
            ReturnToTab(e);
        }
    }
}
