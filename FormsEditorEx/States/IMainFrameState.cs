﻿using SharedLibrary;

namespace FormsEditorEx
{
    public interface IMainFrameState
    {
        MainFrame Handler { get; }
        MainFrameState Type { get; }
        IMainFrameState Action(ucStatusBarControl ucStatusBar);
        IMainFrameState Revert(ucStatusBarControl ucStatusBar);
        void FillTestData();
    }
}
