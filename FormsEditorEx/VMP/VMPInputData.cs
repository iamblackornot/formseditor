﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FormsEditorEx.VMP
{
    public class VMPInputData
    {
        public int Department { get; set; }
        public string SuperIntendent { get; set; }
        public bool IO { get; set; }
        public string Doctor { get; set; }
        public string SecondName { get; set; }
        public string FirstName { get; set; }
        public string ThirdName { get; set; }
        public int Sex { get; set; }
        public string DateBirth { get; set; }
        public string AmbuCardNum { get; set; }
        public string Diagnosis { get; set; }
        public string Complications { get; set; }
        public string Accompanying { get; set; }
        public string Complaints { get; set; }
        public string Anamnez { get; set; }
        public string InstrumentalExamination { get; set; }     
    }
}
