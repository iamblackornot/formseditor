﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Windows.Forms;

namespace FormsEditorEx.Conclusion
{
    public class ConclusionTableEditControl : DatatableEditControl
    {
        public ConclusionTableEditControl(bool isReadOnly = true) : base(Patient.Instance.PatientData.Conclusions.Data, isReadOnly)
        {
                
        }
        protected override void OnCellFormatting(DataGridView dgv, DataGridViewCellFormattingEventArgs e)
        {
            if (dgv.Columns[e.ColumnIndex].Name == "type" && e.RowIndex >= 0 && e.Value is int)
            {
                e.Value = Static.ConclusionTypes[int.Parse(e.Value.ToString())];
                e.FormattingApplied = true;
            }
        }
        protected override void FillTestData()
        {
            Patient.Instance.PatientData.Conclusions.FillTestData();
        }
        protected override void OnDataBindingComplete(DataGridView dgv)
        {
            dgv.Columns[0].Width = 200;
            dgv.Columns[0].HeaderText = "Тип";
            dgv.Columns[1].Width = 200;
            dgv.Columns[1].HeaderText = "№";
            dgv.Columns[2].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            dgv.Columns[2].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            dgv.Columns[2].HeaderText = "Заключение";
            dgv.Columns[3].Visible = false;

            foreach (DataGridViewColumn col in dgv.Columns)
                col.SortMode = DataGridViewColumnSortMode.NotSortable;

            if (dgv.Rows.Count > 0)
            {
                SetChangeRemoveButtonsAvailability(true);
            }
            else
            {
                SetChangeRemoveButtonsAvailability(false);
            }
        }
        protected override void OpenRowEditor(DataRow row, bool isReadOnly)
        {
            using (ConclusionEditForm cef = new ConclusionEditForm(row["num"].ToString(), row["conclusion"].ToString(), int.Parse(row["type"].ToString()), isReadOnly))
            {
                if (cef.ShowDialog() == DialogResult.OK)
                {
                    row["num"] = cef.Num;
                    row["conclusion"] = cef.Conclusion;
                    row["type"] = cef.Type;

                    SetChangedState();
                }
            }
        }
        protected override void OpenAddRowDialog(DataTable dt)
        {
            using (ConclusionEditForm caf = new ConclusionEditForm(false))
            {
                if (caf.ShowDialog() == DialogResult.OK)
                {
                    Patient p = Patient.Instance;
                    dt.Rows.Add(caf.Type, caf.Num, caf.Conclusion, p.Stage.HasValue && p.Stage == (int)MainFrameState.DischargeSummary);
                    SetChangedState();
                }
            }
        }
    }
}
