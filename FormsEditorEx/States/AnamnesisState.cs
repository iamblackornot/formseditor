﻿using System.Collections.Generic;
using FormsEditorEx.Database;
using SharedLibrary;

namespace FormsEditorEx
{
    public class AnamnesisState : MainFrameStateBase
    {
        public AnamnesisState(MainFrame handler) : base(handler)
        {
            Type = MainFrameState.Anamnesis;
            Handler.PatientToAnamnesisFields();
            Handler.SetAnamnesisWorkspace(Static.ActionButtonText[Type]);
        }
        public override IMainFrameState Action(ucStatusBarControl ucStatusBar)
        {
            if (Handler.CheckAnamnesisData())
            {
                Handler.AnamnesisFieldsToPatient();

                Handler.Enabled = false;

                TemporaryData td = Patient.Instance.PatientData.TemporaryFields;

                if (td.IsVMP.HasValue)
                {
                    if(!Handler.RunVMP())
                    {
                        ucStatusBar.ShowError("Создание документов ВМП было отменено.");
                        Handler.Enabled = true;
                        return this;
                    }
                }

                if (Middleware.FillAndSavePatientAction(Static.FillStrategies[DocType.Anamnesis], Handler.dbAdapter, ucStatusBar, out string outputPath))
                {
                    List<string> paths = new List<string>();

                    if (!string.IsNullOrEmpty(outputPath))
                        paths.Add(outputPath);

                    ucStatusBar.ShowWarning("Создание титульного листа...");

                    if (Middleware.HandleFillAction(Static.FillStrategies[DocType.TitlePage], out outputPath))
                    {
                        if (!string.IsNullOrEmpty(outputPath))
                            paths.Add(outputPath);

                        ucStatusBar.ShowOK("Форма заполнена. Титульный лист создан. Данные пациента сохранены.");

                        Handler.SetLastPath(paths);

                        Handler.Enabled = true;

                        if (Static.DoesOperativeTreatment[Patient.Instance.Department.Value] && !td.IsChemTherapy.HasValue)
                            return new PreoperativeEpicrisisState(Handler);
                        else
                            return new DischargeSummaryState(Handler);
                    }
                    else
                        ucStatusBar.ShowError("Форма заполнена. Данные пациента сохранены. Но титульный лист создать не удалось.");
                }

                Handler.Enabled = true;
            }

            return this;
        }
        public override void FillTestData()
        {
            Handler.FillAnamnesisTestData();
        }
    }
}
