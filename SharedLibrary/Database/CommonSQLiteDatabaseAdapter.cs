﻿using System;
using System.Data.SQLite;
using SharedLibrary.Settings;
using System.Collections.Generic;
using SharedLibrary;
using SharedLibrary.Logger;
using Dapper;

namespace SharedLibrary.Database
{
    public class CommonSQLiteDatabaseAdapter : SQLiteAdapter, IHospitalDataProvider, IVersionInfoProvider
    {
        protected SQLiteConnection mainDbConn;
        protected SQLiteConnection configDbConn;

        public CommonSQLiteDatabaseAdapter(ILogger logger, string mainDbPath, string configDbPath) : base(logger)
        {
            this.logger = logger;
            mainDbConn = new SQLiteConnection(GetDatabaseConnectionString(mainDbPath));
            configDbConn = new SQLiteConnection(GetDatabaseConnectionString(configDbPath));
        }
        public DatabaseResult<PersonnelCollection> GetPersonnel()
        {
            return ExecuteDBAction(GetPersonnelAction, configDbConn);
        }
        private DatabaseResult<PersonnelCollection> GetPersonnelAction(SQLiteConnection dbConnection)
        {
            activeTransaction = dbConnection.BeginTransaction();

            var personnnelList = dbConnection.Query<Personnel>(
                "SELECT * FROM Personnel ",
                //+"WHERE IsActive = @IsActive",
                null,
                activeTransaction);

            var docList = dbConnection.Query<DepartmentRecord>(
                "SELECT DepartmentId, PersonnelId " +
                "FROM DepartmentStaff ",
                null,
                activeTransaction);

            var headList = dbConnection.Query<DepartmentRecord>(
                "SELECT DepartmentId, PersonnelId " +
                "FROM DepartmentHead ",
                null,
                activeTransaction);

            var adminList = dbConnection.Query<Deputy>(
                "SELECT p.*, a.* FROM Administration a LEFT JOIN Personnel p " +
                "ON a.PersonnelId = p.PersonnelId",
                null,
                activeTransaction);

            var specialtyList = dbConnection.Query<DoctorSpecialty>(
                "SELECT * FROM Specialty " +
                "WHERE Title is NOT NULL",
                null,
                activeTransaction);

            activeTransaction.Commit();

            return new DatabaseResult<PersonnelCollection>(true, 
                new PersonnelCollection(personnnelList, docList, headList, adminList, specialtyList));
        }
        public DatabaseResult<Version> GetAppVersion(string appName)
        {
            return ExecuteDBAction((SQLiteConnection conn) => GetAppVersionAction(appName, conn), configDbConn);
        }
        private DatabaseResult<Version> GetAppVersionAction(string appName, SQLiteConnection dbConnection)
        {
            string versionString = dbConnection.QuerySingleOrDefault<string>($"SELECT Version{appName} FROM Config");

            Version version;

            if(!Version.TryParse(versionString, out version))
            {
                return new DatabaseResult<Version>(false, null, "Неверный формат");
            }

            return new DatabaseResult<Version>(true, version);  
        }
    }
}
