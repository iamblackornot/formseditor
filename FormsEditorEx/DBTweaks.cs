﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SQLite;
using System.IO;
using SharedLibrary;
using FormsEditorEx.GenExam;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.Text.RegularExpressions;

namespace FormsEditorEx
{
    public class ConclusionInfo
    {
        public string AmbuCardNum { get; set; }
        public int? ConclusionType { get; set; }
        public string ConclusionNum { get; set; }
        public string Conclusion { get; set; }
    }
    public class PersonalData
    {
        public string AmbuCardNum { get; set; }
        public string PersonalInfo { get; set; }
    }
    public static class DBTweaks
    {
        public static List<PersonalData> GetCardPersInfoFromDB()
        {
            List<PersonalData> res = new List<PersonalData>();

            using (var dbConnection = new SQLiteConnection("Data Source =" + Path.Combine(Paths.Instance.Database, "patients.db") + "; Version = 3; "))
            using (var cmd = new SQLiteCommand($"SELECT AmbuCardNum, PersonalInfo FROM Patients", dbConnection))
            {
                try
                {
                    dbConnection.Open();

                    cmd.CommandType = CommandType.Text;

                    using (SQLiteDataReader rdr = cmd.ExecuteReader(CommandBehavior.CloseConnection))
                    {
                        while (rdr.Read())
                        {
                            string info = rdr.GetValue(1).ToString();

                            if (!string.IsNullOrWhiteSpace(info))
                                res.Add(new PersonalData()
                                {
                                    AmbuCardNum = rdr.GetValue(0).ToString(),
                                    PersonalInfo = info
                                });
                        }
                        dbConnection.Close();
                    }
                }
                catch (Exception ex)
                {

                }
                finally
                {
                    dbConnection.Close();
                }
            }

            return res;
        }
        public static void SavePersonalInfoChanges(string AmbuCardNum, string info)
        {
            using (var dbConnection = new SQLiteConnection("Data Source =" + Path.Combine(Paths.Instance.Database, "patients.db") + "; Version = 3; "))
            using (SQLiteCommand command = new SQLiteCommand("update Patients set PersonalInfo = @info where AmbuCardNum=@id", dbConnection))
            {
                dbConnection.Open();
                command.Parameters.Add("info", DbType.String).Value = info;
                command.Parameters.Add("id", DbType.String).Value = AmbuCardNum;
                command.ExecuteNonQuery();
            }
        }

        public static void TweakPersonalInfo()
        {
            List<PersonalData> records = GetCardPersInfoFromDB();

            foreach (var rec in records)
            {
                JObject jobj = JObject.Parse(Cryptography.DecryptString(rec.PersonalInfo, Cryptography.ENC));

                if (jobj.ContainsKey("pad"))
                {
                    DateTime date = DateTime.Parse(jobj["pad"].ToString());
                    string date_str = string.Format("{0:yyyy-MM-ddTHH:mm:ss}", date);
                    jobj["pad"] = date_str;
                    string json = jobj.ToString();

                    SavePersonalInfoChanges(rec.AmbuCardNum, Cryptography.EncryptString(json, Cryptography.ENC));
                }
            }
        }

        //public static void TweakLifeAnamnesis()
        //{
        //    try
        //    {
        //        List<string> ids = GetPatientIdsFromDB();

        //        foreach (string id in ids)
        //        {
        //            Patient.Load(id);

        //            Patient p = Patient.Instance;
        //            p.PatientData.LifeAnamnesisData = new LifeAnamnesisData()
        //            {
        //                PressureA = p.PressureA,
        //                PressureD = p.PressureD,
        //                HeartRate = p.HeartRate,
        //                Smokes = p.Smokes,

        //                Skin = p.Skin,
        //                LymphNodes = p.LymphNodes,
        //                Mammary = p.Mammary,
        //                Thyroid = p.Thyroid,
        //                Chest = p.Chest,
        //                Dyspnea = p.Dyspnea,
        //                LungsResp = p.LungsResp,
        //                Percussion = p.Percussion,
        //                HeartSounds = p.HeartSounds,
        //                Pulse = p.Pulse,
        //                Stomach = p.Stomach,
        //                Liver = p.Liver,
        //                Urination = p.Urination,
        //                Stool = p.Stool,

        //                Genetics = p.Genetics,
        //                Allergy = p.Allergy,
        //                Transfusion = p.Transfusion,
        //                ProfHarm = p.ProfHarm,
        //                PastIllnesses = p.PastIllnesses
        //            };

        //            Patient.Save();
        //        }
        //    }
        //    catch (Exception ex)
        //    {

        //    }
        //}

        //public static void TweakTemporaryFields()
        //{
        //    try
        //    {
        //        List<string> ids = GetPatientIdsFromDB();

        //        foreach (string id in ids)
        //        {
        //            Patient.Load(id);

        //            Patient p = Patient.Instance;

        //            if (!string.IsNullOrEmpty(p.ClinicalRecordNum))
        //            {
        //                p.PatientData.TemporaryFields = new TemporaryData()
        //                {
        //                    GenCondition = p.GenCondition,
        //                    Nutrition = p.Nutrition,
        //                    VTEORisk = p.VTEORisk,
        //                    GORisk = p.GORisk,
        //                    VTEOPrevention = p.VTEOPrevention,
        //                    DiagnosisPre = p.Diagnosis,
        //                    ComplicationsPre = p.Complications,
        //                    //AccompMedicationPre = p.AccompMedication,
        //                    PlannedOperationScope = p.Treatment,
        //                    StatusLocalisPre = p.StatusLocalis
        //                };

        //                Patient.Save();
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {

        //    }
        //}
        public static void TweakStage()
        {
            using (var dbConnection = new SQLiteConnection("Data Source =" + Path.Combine(Paths.Instance.Database, "patients.db") + "; Version = 3; "))
            using (SQLiteCommand command = new SQLiteCommand("UPDATE Patients SET Stage = 3 WHERE ClinicalRecordNum is NOT NULL;", dbConnection))
            {
                dbConnection.Open();
                command.ExecuteNonQuery();
                dbConnection.Close();
            }
        }

        //public static void TweakGenExams()
        //{
        //    Dictionary<string, string> records = GetCardExamInfoFromDB();

        //    //Dictionary<string, string> result = new Dictionary<string, string>();

        //    foreach (KeyValuePair<string,string> rec in records)
        //    {
        //        string AmbuCardNum = rec.Key;

        //        GenExamResult.Deserialize(rec.Value);
        //        //GenExamResult.Refactor();
        //        GenExamResult.Shorten();

        //        SaveGenExamChanges(AmbuCardNum, GenExamResult.Serialize());
        //    }
        //}

        public static void SaveGenExamChanges(string AmbuCardNum, string GenExam)
        {
            using (var dbConnection = new SQLiteConnection("Data Source =" + Path.Combine(Paths.Instance.Database, "patients.db") + "; Version = 3; "))
            using (SQLiteCommand command = new SQLiteCommand("update Patients set GenExam = @genexam where AmbuCardNum=@id", dbConnection))
            {
                dbConnection.Open();
                command.Parameters.Add("genexam", DbType.String).Value = GenExam;
                command.Parameters.Add("id", DbType.String).Value = AmbuCardNum;
                command.ExecuteNonQuery();
            }
        }
        public static Dictionary<string, string> GetCardExamInfoFromDB()
        {
            Dictionary<string, string> res = new Dictionary<string, string>();

            using (var dbConnection = new SQLiteConnection("Data Source =" + Path.Combine(Paths.Instance.Database, "patients.db") + "; Version = 3; "))
            using (var cmd = new SQLiteCommand($"SELECT AmbuCardNum, GenExam FROM Patients", dbConnection))
            {
                try
                {
                    dbConnection.Open();

                    cmd.CommandType = CommandType.Text;

                    using (SQLiteDataReader rdr = cmd.ExecuteReader(CommandBehavior.CloseConnection))
                    {
                        while (rdr.Read())
                        {
                            res.Add(rdr.GetValue(0).ToString(), rdr.GetValue(1).ToString());
                        }
                        dbConnection.Close();
                    }
                }
                catch (Exception ex)
                {

                }
                finally
                {
                    dbConnection.Close();
                }
            }

            return res;
        }

        public static List<string> GetPatientIdsFromDB()
        {
            List<string> ids = new List<string>();

            using (var dbConnection = new SQLiteConnection("Data Source =" + Path.Combine(Paths.Instance.Database, "patients.db") + "; Version = 3; "))
            using (var cmd = new SQLiteCommand($"SELECT AmbuCardNum FROM Patients", dbConnection))
            {
                try
                {
                    dbConnection.Open();

                    cmd.CommandType = CommandType.Text;

                    using (SQLiteDataReader rdr = cmd.ExecuteReader(CommandBehavior.CloseConnection))
                    {
                        while (rdr.Read())
                        {
                            ids.Add(rdr.GetValue(0).ToString());
                        }
                        dbConnection.Close();
                    }
                }
                catch (Exception ex)
                {

                }
                finally
                {
                    dbConnection.Close();
                }
            }

            return ids;
        }
        //public static void TweakTuber()
        //{
        //    string positiveRegex = @"(\;\sТуберкулез\:\sположит\.)|(\sТуберкулез\,)";
        //    string negativeRegex = @"(Туберкулез\,\s)|(Туберкулез\:\sотриц\.\;\s)";

        //    try
        //    {
        //        List<string> ids = GetPatientIdsFromDB();

        //        foreach (string id in ids)
        //        {
        //            Patient.Load(id);

        //            Patient p = Patient.Instance;

        //            List<GenExamRecord> lst = p.PatientData.GenExam[GenExamType.Disease];

        //            if (lst.Count > 0)
        //            {

        //                for (int i = 0; i < lst.Count; i++)
        //                {
        //                    bool positivefound = false;

        //                    string result = Regex.Replace(lst[i].Result, positiveRegex, match =>
        //                    {
        //                        positivefound = true;
        //                        return string.Empty;
        //                    });

        //                    if (!positivefound)
        //                    {
        //                        result = Regex.Replace(lst[i].Result, negativeRegex, string.Empty);
        //                    }
        //                    else
        //                    {
        //                        if (i == lst.Count - 1)
        //                        {
        //                            p.PatientData.LifeAnamnesisData.Tuberculosis = "положит.";
        //                        }
        //                    }

        //                    lst[i].Result = result;
        //                }

        //                Patient.Save();
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {

        //    }
        //}

        //public static void ReformatVMP(InfoClass info)
        //{
        //    DataSet ds = info.VMPData;
        //    List<DataSet> res = new List<DataSet>();

        //    for(int i = 0; i < 10; i++) 
        //    {
        //        res.Add(ds.Clone());
        //    }

        //    DataTable dtGroups = ds.Tables["groups"];

        //    Dictionary<int, int>groupTodep = new Dictionary<int, int>();

        //    foreach (DataRow dr in dtGroups.Rows) 
        //    { 
        //        int dep_id = Convert.ToInt32(dr["dep_id"]);
        //        int group_id = Convert.ToInt32(dr["group_id"]);

        //        res[dep_id].Tables["groups"].ImportRow(dr);
        //        groupTodep[group_id] = dep_id;
        //    }

        //    DataTable dtDesc= ds.Tables["desc"];

        //    Dictionary<int, int> descTodep = new Dictionary<int, int>();

        //    foreach (DataRow dr in dtDesc.Rows)
        //    {
        //        int group_id = Convert.ToInt32(dr["group_id"]);
        //        int desc_id = Convert.ToInt32(dr["desc_id"]);
        //        int dep_id = groupTodep[group_id];

        //        res[dep_id].Tables["desc"].ImportRow(dr);
        //        descTodep[desc_id] = dep_id;
        //    }

        //    DataTable dtMKBs = ds.Tables["MKBs"];

        //    Dictionary<int, int> mkbsTodep = new Dictionary<int, int>();

        //    foreach (DataRow dr in dtMKBs.Rows)
        //    {
        //        int desc_id = Convert.ToInt32(dr["desc_id"]);
        //        int mkb_id = Convert.ToInt32(dr["mkb_id"]);
        //        int dep_id = descTodep[desc_id];

        //        res[dep_id].Tables["MKBs"].ImportRow(dr);
        //        mkbsTodep[mkb_id] = dep_id;
        //    }

        //    DataTable dtModels = ds.Tables["models"];

        //    Dictionary<int, int> modelTodep = new Dictionary<int, int>();

        //    foreach (DataRow dr in dtModels.Rows)
        //    {
        //        int mkb_id = Convert.ToInt32(dr["mkb_id"]);
        //        int model_id = Convert.ToInt32(dr["model_id"]);
        //        int dep_id = mkbsTodep[mkb_id];

        //        res[dep_id].Tables["models"].ImportRow(dr);
        //        modelTodep[model_id] = dep_id;
        //    }

        //    DataTable dtTreatment = ds.Tables["treatment"];

        //    Dictionary<int, int> treatmentTodep = new Dictionary<int, int>();

        //    foreach (DataRow dr in dtTreatment.Rows)
        //    {
        //        int model_id = Convert.ToInt32(dr["model_id"]);
        //        int treat_id = Convert.ToInt32(dr["treat_id"]);
        //        int dep_id = modelTodep[model_id];

        //        res[dep_id].Tables["treatment"].ImportRow(dr);
        //        treatmentTodep[treat_id] = dep_id;
        //    }

        //    DataTable dtMethods = ds.Tables["methods"];


        //    foreach (DataRow dr in dtMethods.Rows)
        //    {
        //        int treat_id = Convert.ToInt32(dr["treat_id"]);
        //        int dep_id = treatmentTodep[treat_id];

        //        res[dep_id].Tables["methods"].ImportRow(dr);
        //    }

        //    for (int i = 0; i < 10; i++)
        //    {
        //        SaveVMPData(res[i], Path.Combine(Paths.Instance.Settings, "vmp_cfg"), i);
        //    }
        //}

        //static bool SaveVMPData(DataSet ds, string path, int dep_id)
        //{
        //    if(!Directory.Exists(path)) 
        //    {
        //        Directory.CreateDirectory(path);
        //    }

        //    bool isSaved = false;
        //    try
        //    {
        //        JObject jobj = new JObject();
        //        foreach (DataTable dt in ds.Tables)
        //        {
        //            if (dt.Rows.Count > 0)
        //            {
        //                JArray jrows = new JArray();

        //                foreach (DataRow row in dt.Rows)
        //                {
        //                    JArray jcells = new JArray();

        //                    foreach (object cell in row.ItemArray)
        //                        jcells.Add(new JValue(cell));

        //                    jrows.Add(jcells);
        //                }

        //                jobj.Add(new JProperty(dt.TableName, jrows));
        //            }
        //        }
        //        File.WriteAllText(Path.Combine(path, $"vmp_{dep_id}"), jobj.ToString());
        //        isSaved = true;
        //    }
        //    catch (Exception ex) {}

        //    return isSaved;
        //}

        //public static void MigrateStaffToDB()
        //{
        //    string path = @"D:\Documents\Visual Studio 2017\Projects\SurgerySchedule\";
        //    using (var dbConnection = new SQLiteConnection("Data Source =" + Path.Combine(path, "config.db") + "; Version = 3; "))
        //    {
        //        dbConnection.Open();

        //        var dtDoctors = AppConfig.Instance.Info.Staff.dtDoctors;

        //        foreach (DataRow row in dtDoctors.Rows)
        //        {
        //            using (SQLiteCommand command = new SQLiteCommand("insert into Doctors (ShortName, Department, IsHeadOfDepartment) " +
        //                    "VALUES(@shortname, @department, @isHead)", dbConnection))
        //            {

        //                command.Parameters.Add("shortname", DbType.String).Value = row[1];
        //                command.Parameters.Add("department", DbType.Byte).Value = row[0];
        //                command.Parameters.Add("isHead", DbType.Boolean).Value = false;
        //                command.ExecuteNonQuery();
        //            }
        //        }

        //        var dtSuperintendents = AppConfig.Instance.Info.Staff.dtSuperintendents;

        //        foreach (DataRow row in dtSuperintendents.Rows)
        //        {
        //            using (SQLiteCommand command = new SQLiteCommand("insert into Doctors (ShortName, Department, IsHeadOfDepartment) " +
        //                    "VALUES(@shortname, @department, @isHead)", dbConnection))
        //            {

        //                command.Parameters.Add("shortname", DbType.String).Value = row[1];
        //                command.Parameters.Add("department", DbType.Byte).Value = row[0];
        //                command.Parameters.Add("isHead", DbType.Boolean).Value = true;
        //                command.ExecuteNonQuery();
        //            }
        //        }

        //        dbConnection.Close();
        //    }
        //}
    }
}
