﻿using SharedLibrary;
using System.Text.RegularExpressions;

namespace FormsEditorEx
{
    public partial class MainFrame
    {
        private void CheckCommonData(ValidateReportBuilder rb)
        {
            Patient p = Patient.Instance;

            if (cbBloodGroup.SelectedIndex < 0)
                rb.AddWarning("Группа крови");

            if (cbRh.SelectedIndex < 0)
                rb.AddWarning("Резус-фактор");

            LifeAnamnesisData lad = Patient.Instance.PatientData.LifeAnamnesisData;

            if (string.IsNullOrWhiteSpace(lad.Height))
                rb.AddCriticialField("Рост");

            if (string.IsNullOrWhiteSpace(lad.Weight))
                rb.AddCriticialField("Вес");

            if (cbDisability.SelectedIndex < 0)
                rb.AddCriticialField("Инвалидность");

            if (string.IsNullOrEmpty(tbSuperintendent.Text.Trim()) || !Utility.CheckIfFullNameIsCorrect(tbSuperintendent.Text.Trim()))
                rb.AddCriticialField("Заведующий отделением");

            if (string.IsNullOrEmpty(cbSuperIntendentFormatted.Text.Trim()) || !Utility.CheckIfFullNameIsCorrect(cbSuperIntendentFormatted.Text.Trim()))
                rb.AddCriticialField("Осмотр с зав. отделением (склонение)");

            if (cbVTEORisk.SelectedIndex < 0)
                rb.AddCriticialField("Риск ВТЭО");
            if (cbGORisk.SelectedIndex < 0)
                rb.AddCriticialField("Риск ГО");

            bool hasAtLeastOneValue = false;

            for (int i = 0; i < clbVTEOPrevention.Items.Count; i++)
                if (clbVTEOPrevention.GetItemChecked(i))
                {
                    hasAtLeastOneValue = true;
                    break;
                }

            if (!hasAtLeastOneValue)
                rb.AddWarning("не выбрано ни одного споcоба профилактики ВТЭО");

            p.PatientData.GenExam.ValidateDiseaseRecords(rb);

            if (string.IsNullOrEmpty(p.InstrExam))
                rb.AddWarning("инструментальные методы обследования не указаны");
        }
        public bool CheckAnamnesisData()
        {
            ValidateReportBuilder rb = new ValidateReportBuilder();

            Patient p = Patient.Instance;

            if (!tbDateExam.MaskCompleted)
                rb.AddCriticialField("Дата осмотра");

            if (!tbTimeExam.MaskCompleted)
                rb.AddCriticialField("Время осмотра");

            if (cbGenCondition.SelectedIndex < 0)
                rb.AddCriticialField("Общее состояние");

            if (cbNutrition.SelectedIndex < 0)
                rb.AddCriticialField("Питание");

            if (string.IsNullOrEmpty(p.Anamnez))
                rb.AddCriticialField("Анамнез");

            if (string.IsNullOrEmpty(rtbDiagnosisPre.Text.Trim()))
                rb.AddCriticialField("Диагноз");

            if (string.IsNullOrEmpty(rtbStatusLocalisPre.Text.Trim()))
                rb.AddCriticialField("Status Localis");

            if (!cbVMP.Checked)
            {
                if (string.IsNullOrEmpty(rtbPlannedOperationScope.Text.Trim()))
                    rb.AddCriticialField("План лечения");
            }
            else
            {
                if (!p.PatientData.GenExam.RecordsAdded)
                    rb.AddWarning("была выбрана опция ВМП, но помимо тестов на заболевания (RW, ВИЧ, HBsAg, HCV) " +
                        "не было добавлено ни одного анализа в клинико-лабораторные данные, которые как правило указываются в выписке из " +
                        " амбулаторной карты");
            }

            if (string.IsNullOrEmpty(rtbPlannedExamination.Text.Trim()))
                rb.AddCriticialField("План обследования");

            if (string.IsNullOrEmpty(rtbAssignments.Text.Trim()))
                rb.AddCriticialField("Назначения");

            if (string.IsNullOrEmpty(p.PatientData.TemporaryFields.DisabilityListInfo))
                rb.AddWarning("Сведения о листке нетрудоспособности");

            CheckCommonData(rb);

            bool res = rb.ShowReportAndGetResult();

            if (!res)
                SelectNextControl(this, true, true, true, true);

            return res;
        }

        public bool CheckPreoperativeEpicrisisData()
        {
            ValidateReportBuilder rb = new ValidateReportBuilder();

            if (Static.DoesOperativeTreatment[Patient.Instance.Department.Value] && !AppConfig.Instance.OperTeam.ValidateOperTeam())
                rb.AddCriticialField("Операционная бригада");

            if (!Patient.Instance.PatientData.GenExam.HasNonDiseaseRecords())
                rb.AddWarning("В клинико-лабораторных данных отсутствуют какие-либо результаты анализов, не относящихся к тестам на заболевания (RW, ВИЧ, HBsAg, HCV)");

            CheckCommonData(rb);

            bool res = rb.ShowReportAndGetResult();

            if (!res)
                SelectNextControl(this, true, true, true, true);

            return res;
        }
        public bool CheckDischargeSummaryData()
        {
            ValidateReportBuilder rb = new ValidateReportBuilder();

            Patient p = Patient.Instance;

            if (!p.PatientData.GenExam.RecordsAdded)
                rb.AddWarning("на текущем этапе не было добавлено ни одного анализа из общего обследования помимо тестов на заболевания (RW, ВИЧ, HBsAg, HCV)");

            if (string.IsNullOrEmpty(rtbDiagnosisPost.Text.Trim()))
                rb.AddCriticialField("Диагноз");

            if (string.IsNullOrEmpty(tbMKBDiagnosis.Text.Trim()))
                rb.AddCriticialField("МКБ диагноза");

            if (!rtbComplicationsPost.IsCueOn && string.IsNullOrEmpty(tbMKBComplications.Text.Trim()))
                rb.AddCriticialField("МКБ осложнений");

            if (!rtbAccompanyingPost.IsCueOn && string.IsNullOrEmpty(tbMKBAccompanying.Text.Trim()))
                rb.AddCriticialField("МКБ сопутствующего диагноза");

            //if (!ValidateTreatment(p.Treatment))
            //    rb.AddCriticialField("Лечение: должно начинаться с даты в формате ДД.ММ.ГГГГ, например \"26.03.2021 Операция ...\"");

            if (string.IsNullOrEmpty(p.StatusLocalis))
                rb.AddCriticialField("Status Localis");

            if (cbTreatmentResult.SelectedIndex < 0)
                rb.AddCriticialField("Результат лечения");

            if (string.IsNullOrWhiteSpace(p.PatientData.TemporaryFields.Recommendations))
                rb.AddCriticialField("Рекомендации");

            if (cbCondition.SelectedIndex < 0)
                rb.AddCriticialField("Состояние");

            if (cbWorkCapacity.SelectedIndex < 0)
                rb.AddCriticialField("Трудоспособность");

            if (!tbDateDischarge.MaskCompleted)
                rb.AddCriticialField("Дата выписки");

            if (!tbIndex.MaskCompleted)
            {
                rb.AddCriticialField("Адрес регистрации - Индекс");
            }
            if (string.IsNullOrWhiteSpace(tbOblast.Text.Trim()))
            {
                rb.AddCriticialField("Адрес регистрации - Область");
            }
            if (string.IsNullOrWhiteSpace(cbTown.Text.Trim()))
            {
                rb.AddCriticialField("Адрес регистрации - Город");
            }
            if (string.IsNullOrWhiteSpace(cbStreet.Text.Trim()))
            {
                rb.AddCriticialField("Адрес регистрации - Улица");
            }
            if (string.IsNullOrWhiteSpace(tbBuilding.Text.Trim()))
            {
                rb.AddCriticialField("Адрес регистрации - Дом");
            }

            if (cbSickLeave.Checked)
            {
                if (string.IsNullOrEmpty(tbSickLeaveNum.Text.Trim()))
                    rb.AddCriticialField("Номер больничного листа");

                if (!tbSickLeaveStart.MaskCompleted || !tbSickLeaveEnd.MaskCompleted)
                    rb.AddCriticialField("Даты больничного");

                if (cbSickLeaveEndType.SelectedIndex < 0)
                    rb.AddCriticialField("Действие по окончанию больничного");

                if (cbSickLeaveExt.Checked)
                {
                    if (!tbSickLeaveExtStart.MaskCompleted || !tbSickLeaveExtEnd.MaskCompleted)
                        rb.AddCriticialField("Даты продленного больничного");
                }
            }

            if (!p.PatientData.Conclusions.ValidatePost())
                rb.AddWarning("на текущей стадии не было добавлено ни одного результата морфологического исследования");

            CheckCommonData(rb);

            bool res = rb.ShowReportAndGetResult();

            if (!res)
                SelectNextControl(this, true, true, true, true);

            return res;
        }
        public bool CheckDischargeData()
        {
            ValidateReportBuilder rb = new ValidateReportBuilder();

            Patient p = Patient.Instance;

            if (!p.PatientData.Conclusions.ValidatePost())
                rb.AddWarning("на этапе создания ВЫПИСКИ не было добавлено ни одного результата морфологического исследования");

            bool res = rb.ShowReportAndGetResult();

            if (!res)
                SelectNextControl(this, true, true, true, true);

            return res;
        }

        private bool ValidateTreatment(string text)
        {
            bool res = false;

            text = text?.Trim();

            if (!string.IsNullOrWhiteSpace(text))
            {
                Regex expression = new Regex(@"^(\d\d\.\d\d\.\d\d\d\d)");

                Match match = expression.Match(text);
                res = match.Success && match.Groups.Count == 2;
            }

            return res;
        }
    }
}
