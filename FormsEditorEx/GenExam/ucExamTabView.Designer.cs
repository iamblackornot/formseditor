﻿namespace FormsEditorEx.GenExam
{
    partial class ucExamTabView
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.pnTabs = new System.Windows.Forms.Panel();
            this.dgvTabs = new System.Windows.Forms.DataGridView();
            this.pnTabContent = new System.Windows.Forms.Panel();
            this.pnAddButton = new System.Windows.Forms.Panel();
            this.btAddRecord = new System.Windows.Forms.Button();
            this.pnTabs.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvTabs)).BeginInit();
            this.pnAddButton.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnTabs
            // 
            this.pnTabs.Controls.Add(this.dgvTabs);
            this.pnTabs.Dock = System.Windows.Forms.DockStyle.Left;
            this.pnTabs.Location = new System.Drawing.Point(0, 0);
            this.pnTabs.Name = "pnTabs";
            this.pnTabs.Size = new System.Drawing.Size(216, 609);
            this.pnTabs.TabIndex = 0;
            // 
            // dgvTabs
            // 
            this.dgvTabs.AllowUserToAddRows = false;
            this.dgvTabs.AllowUserToDeleteRows = false;
            this.dgvTabs.AllowUserToResizeColumns = false;
            this.dgvTabs.AllowUserToResizeRows = false;
            this.dgvTabs.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvTabs.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dgvTabs.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dgvTabs.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgvTabs.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dgvTabs.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dgvTabs.ColumnHeadersVisible = false;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle1.Padding = new System.Windows.Forms.Padding(0, 2, 0, 5);
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvTabs.DefaultCellStyle = dataGridViewCellStyle1;
            this.dgvTabs.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvTabs.Location = new System.Drawing.Point(0, 0);
            this.dgvTabs.MultiSelect = false;
            this.dgvTabs.Name = "dgvTabs";
            this.dgvTabs.ReadOnly = true;
            this.dgvTabs.RowHeadersVisible = false;
            this.dgvTabs.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dgvTabs.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvTabs.Size = new System.Drawing.Size(216, 609);
            this.dgvTabs.TabIndex = 0;
            // 
            // pnTabContent
            // 
            this.pnTabContent.BackColor = System.Drawing.SystemColors.Control;
            this.pnTabContent.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnTabContent.Location = new System.Drawing.Point(216, 0);
            this.pnTabContent.Name = "pnTabContent";
            this.pnTabContent.Size = new System.Drawing.Size(790, 500);
            this.pnTabContent.TabIndex = 1;
            // 
            // pnAddButton
            // 
            this.pnAddButton.BackColor = System.Drawing.SystemColors.Control;
            this.pnAddButton.Controls.Add(this.btAddRecord);
            this.pnAddButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnAddButton.Location = new System.Drawing.Point(216, 500);
            this.pnAddButton.Name = "pnAddButton";
            this.pnAddButton.Padding = new System.Windows.Forms.Padding(16);
            this.pnAddButton.Size = new System.Drawing.Size(790, 109);
            this.pnAddButton.TabIndex = 2;
            // 
            // btAddRecord
            // 
            this.btAddRecord.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btAddRecord.Location = new System.Drawing.Point(310, 8);
            this.btAddRecord.Margin = new System.Windows.Forms.Padding(0);
            this.btAddRecord.Name = "btAddRecord";
            this.btAddRecord.Size = new System.Drawing.Size(153, 36);
            this.btAddRecord.TabIndex = 0;
            this.btAddRecord.Text = "Добавить анализ";
            this.btAddRecord.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btAddRecord.UseVisualStyleBackColor = true;
            this.btAddRecord.Click += new System.EventHandler(this.btAddRecord_Click);
            // 
            // ucExamTabView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.Controls.Add(this.pnAddButton);
            this.Controls.Add(this.pnTabContent);
            this.Controls.Add(this.pnTabs);
            this.Name = "ucExamTabView";
            this.Size = new System.Drawing.Size(1006, 609);
            this.Load += new System.EventHandler(this.ucExamTabView_Load);
            this.pnTabs.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvTabs)).EndInit();
            this.pnAddButton.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnTabs;
        private System.Windows.Forms.Panel pnTabContent;
        private System.Windows.Forms.DataGridView dgvTabs;
        private System.Windows.Forms.Panel pnAddButton;
        private System.Windows.Forms.Button btAddRecord;
    }
}
