﻿using Microsoft.Office.Interop.Word;
using SharedLibrary;

namespace FormsEditorEx
{
    public class AnamnesisFillStrategy : IFillStrategy
    {
        public DocType DocType { get; } = DocType.Anamnesis;
        public string Fill(Application wordApp, Document doc)
        {
            string temp = string.Empty;
            Patient p = Patient.Instance;
            TemporaryData td = p.PatientData.TemporaryFields;
            LifeAnamnesisData lad = p.PatientData.LifeAnamnesisData;

            if (!td.IO.HasValue)
                temp = "заведующим";
            else
                temp = "и.о. заведующего";

            WordInterop.FindAndReplace(wordApp, "%fsi_st", temp);

            WordInterop.FindAndReplace(wordApp, "%fdprt", Static.DepartmentsFormatted[p.Department.Value]);

            WordInterop.FindAndReplace(wordApp, "%nendng", td.SuperIntendentFormatted.EmptyIfNull().Replace(" ", Symbols.NBSpace).Trim());

            WordInterop.FindAndReplace(wordApp, "%dtexam", Utility.DateToFullString(p.DateExam.Value));

            WordInterop.FindAndReplace(wordApp, "%tmexam", td.TimeExam.EmptyIfNull());
            WordInterop.FindAndReplace(wordApp, "%cmplnts", !string.IsNullOrEmpty(p.Complaints) ? p.Complaints : Static.ComplaintsDefault);
            WordInterop.FindAndReplace(wordApp, "%anmnz", p.Anamnez.EmptyIfNull());

            p.PatientData.Conclusions.BuildDocString(doc, "%cncl");
            p.PatientData.Commissions.BuildDocString(doc, "%cmssn");

            WordInterop.FindAndReplace(wordApp, "%dss_short", Patient.Instance.PatientData.GenExam.GetDiseaseString());

            WordInterop.FindAndReplace(wordApp, "%tbr", lad.Tuberculosis.ValueOrDefault(FieldWithCue.Tuberculosis));
            WordInterop.FindAndReplace(wordApp, "%gntcs", lad.Genetics.ValueOrDefault(FieldWithCue.Genetics));
            WordInterop.FindAndReplace(wordApp, "%pstlnss", lad.PastIllnesses.ValueOrDefault(FieldWithCue.PastIllnesses));
            WordInterop.FindAndReplace(wordApp, "%accmp_text", p.Accompanying.ValueOrDefault(FieldWithCue.Accompanying));

            WordInterop.BuildAccompMedicationPreString(doc);
            WordInterop.BuildAccompMedicationPlannedString(doc);

            WordInterop.FindAndReplace(wordApp, "%allrgy", lad.Allergy.ValueOrDefault(FieldWithCue.Allergy));
            WordInterop.FindAndReplace(wordApp, "%trnsfsn", lad.Transfusion.ValueOrDefault(FieldWithCue.Transfusion));
            WordInterop.FindAndReplace(wordApp, "%prfhrm", lad.ProfHarm.ValueOrDefault(FieldWithCue.ProfHarm));

            temp = string.Empty;

            if (lad.Smokes.HasValue)
                temp = "Курит";
            else
                temp = "Не курит";

            WordInterop.FindAndReplace(wordApp, "%smks", temp);
            WordInterop.FindAndReplace(wordApp, "%cgg", !string.IsNullOrEmpty(lad.CiggsPerDay) ? $" {lad.CiggsPerDay} сиг/д" : string.Empty);

            if (td.GenCondition.HasValue)
                temp = Static.GenCondition[td.GenCondition.Value];
            else
                temp = string.Empty;

            WordInterop.FillGeneralStatusPre(wordApp, p);

            WordInterop.FindAndReplace(wordApp, "%disinf", td.DisabilityListInfo.EmptyIfNull());

            WordInterop.BuildNeuroStatusString(doc, p);

            WordInterop.FindAndReplace(wordApp, "%statloc", !string.IsNullOrEmpty(p.StatusLocalis) ? p.StatusLocalis : Static.StatusLocalisPreCue[p.Department.Value]);

            WordInterop.FindAndReplace(wordApp, "%instr_exam", p.InstrExam.EmptyIfNull());

            Patient.Instance.PatientData.Conclusions.BuildDocFString(doc, "%fcncl");

            //WordInterop.FindAndReplace(wordApp, "%dgnz", p.Diagnosis.EmptyIfNull());
            WordInterop.FindAndReplace(wordApp, "%dgnz", td.DiagnosisPre.EmptyIfNull());
            WordInterop.FindAndReplace(wordApp, "%cmplc", td.ComplicationsPre.ValueOrDefault(FieldWithCue.Complications));
            WordInterop.FindAndReplace(wordApp, "%dismr", p.DiseaseMoreInfo.EmptyIfNull());

            if (td.VTEORisk.HasValue)
                temp = Static.VTEO_Risk[td.VTEORisk.Value];
            else
                temp = string.Empty;

            WordInterop.FindAndReplace(wordApp, "%vteo_risk", temp);

            if (td.GORisk.HasValue)
                temp = Static.GO_Risk[td.GORisk.Value];
            else
                temp = string.Empty;

            WordInterop.FindAndReplace(wordApp, "%go_risk", temp);

            WordInterop.FindAndReplace(wordApp, "%vteo_prev", WordInterop.GetVTEOPreventionString());

            WordInterop.FindAndReplace(wordApp, "%expln", td.PlannedExamination.EmptyIfNull());
            WordInterop.FindAndReplace(wordApp, "%opr_scp", td.PlannedOperationScope.EmptyIfNull());
            WordInterop.FindAndReplace(wordApp, "%assgn", td.Assignments.EmptyIfNull());

            WordInterop.FillDoctor(wordApp, p);
            WordInterop.FillSuperIntendent(wordApp, td);

            WordInterop.AdjustPages(doc, AppConfig.Instance.DocFormatMain);

            return Paths.Instance.GetOutputPath(DocType); 
        }
    }
}
