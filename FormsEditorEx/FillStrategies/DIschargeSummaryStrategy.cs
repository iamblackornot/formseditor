﻿using Microsoft.Office.Interop.Word;
using SharedLibrary;

namespace FormsEditorEx
{
    public class DischargeSummaryFillStrategy : IFillStrategy
    {
        public DocType DocType { get; } = DocType.DischargeSummary;
        public string Fill(Application wordApp, Document doc)
        {
            string temp = string.Empty;
            Patient p = Patient.Instance;
            TemporaryData td = p.PatientData.TemporaryFields;

            WordInterop.FindAndReplace(wordApp, "%dprt", Static.DepartmentNames[p.Department.Value].ToUpper());
            WordInterop.FindAndReplace(wordApp, "%addr", Static.DepartmentAddresses[p.Department.Value]);
            WordInterop.FindAndReplace(wordApp, "%phn", AppConfig.Instance.Info.Phones.TryGetValue(p.Department.Value, out string phone)
                ? phone : string.Empty);

            WordInterop.FindAndReplace(wordApp, "%clrec_num", p.ClinicalRecordNum);
            WordInterop.FindAndReplace(wordApp, "%crd_num", p.AmbuCardNum);

            WordInterop.FindAndReplace(wordApp, "%sname", p.SecondName);
            WordInterop.FindAndReplace(wordApp, "%fname", p.FirstName);
            WordInterop.FindAndReplace(wordApp, "%tname", p.ThirdName.EmptyIfNull());

            WordInterop.FindAndReplace(wordApp, "%dbirth", Utility.DateToFullString(p.DateBirth));
            WordInterop.FindAndReplace(wordApp, "%age", Utility.DateBirthToAgeFullString(p.DateBirth));

            WordInterop.FindAndReplace(wordApp, "%sex", p.Sex.HasValue ? Static.SexTitle[(Sex)p.Sex.Value] : string.Empty);

            WordInterop.FindAndReplace(wordApp, "%paddr", WordInterop.GetAddressString(p.PatientData.PersonalInfo));

            WordInterop.FindAndReplace(wordApp, "%dtexam", Utility.DateToFullString(p.DateExam.Value));
            WordInterop.FindAndReplace(wordApp, "%dtdischrg", Utility.DateToFullString(p.DateDischarge.Value));
            WordInterop.FindAndReplace(wordApp, "%daysc", Utility.GetTreatmentDaysCountString(p.DateExam.Value, p.DateDischarge.Value));

            WordInterop.FindAndReplace(wordApp, "%dgnz", p.Diagnosis.EmptyIfNull());
            WordInterop.FindAndReplace(wordApp, "%dimkb", WordInterop.GetMKBString(p.MKBDiagnosis));
            WordInterop.FindAndReplace(wordApp, "%cmplc", p.Complications.ValueOrDefault(FieldWithCue.Complications));
            WordInterop.FindAndReplace(wordApp, "%cmmkb", WordInterop.GetMKBString(p.MKBComplications));
            WordInterop.FindAndReplace(wordApp, "%accmp_text", p.Accompanying.ValueOrDefault(FieldWithCue.Accompanying));
            WordInterop.FindAndReplace(wordApp, "%acmkb", WordInterop.GetMKBString(p.MKBAccompanying));

            WordInterop.FindAndReplace(wordApp, "%dismr", p.DiseaseMoreInfo.EmptyIfNull());

            WordInterop.BuildAccompMedicationDoneString(doc);

            WordInterop.FillGeneralStatusPre(wordApp, p);

            WordInterop.FindAndReplace(wordApp, "%anmnz", p.Anamnez.EmptyIfNull());

            Patient.Instance.PatientData.Conclusions.BuildDocString(doc, "%cncl");
            Patient.Instance.PatientData.Commissions.BuildDocString(doc, "%cmssn");

            WordInterop.FindAndReplace(wordApp, "%medc", p.Medication.EmptyIfNull());
            WordInterop.FindAndReplace(wordApp, "%trtmnt", p.Treatment.EmptyIfNull());

            WordInterop.FindAndReplace(wordApp, "%trnsfsn", td.Transfusion.EmptyIfNull());

            Patient.Instance.PatientData.Conclusions.BuildDocString(doc, "%pstcncl", true);
            Patient.Instance.PatientData.Commissions.BuildDocString(doc, "%pstcmssn", true);

            WordInterop.FindAndReplace(wordApp, "%instr_exam", p.InstrExam.EmptyIfNull());
            WordInterop.FindAndReplace(wordApp, "%gen_exam", Patient.Instance.PatientData.GenExam.GetString());

            if (td.VTEORisk.HasValue)
                temp = Static.VTEO_Risk[td.VTEORisk.Value];
            else
                temp = string.Empty;

            WordInterop.FindAndReplace(wordApp, "%vteo_risk", temp);

            if (td.GORisk.HasValue)
                temp = Static.GO_Risk[td.GORisk.Value];
            else
                temp = string.Empty;

            WordInterop.FindAndReplace(wordApp, "%go_risk", temp);

            WordInterop.FindAndReplace(wordApp, "%vteo_prev", WordInterop.GetVTEOPreventionString());

            WordInterop.BuildBloodString(doc, p);

            WordInterop.FindAndReplace(wordApp, "%statloc", p.StatusLocalis.EmptyIfNull());

            WordInterop.FillGeneralStatusPost(wordApp, p);

            WordInterop.FindAndReplace(wordApp, "%trtres", td.TreatmentResult.HasValue ? Static.TreatmentResult[td.TreatmentResult.Value] : string.Empty);
            WordInterop.FindAndReplace(wordApp, "%cndtn", td.ConditionPost.HasValue ? Static.ConditionPost[td.ConditionPost.Value] : string.Empty);

            int? disability = p.PatientData.PersonalInfo.Disability;
            WordInterop.FindAndReplace(wordApp, "%dsbl", disability.HasValue ? Static.DisabilityName[(Disability)disability.Value] : string.Empty);
            WordInterop.FindAndReplace(wordApp, "%wrkc", td.WorkCapacity.HasValue ? Static.WorkCapacityName[(WorkCapacity)td.WorkCapacity.Value] : string.Empty);

            WordInterop.BuildRecommendationsString(doc);
            WordInterop.BuildSickLeaveString(doc);

            WordInterop.FillDoctor(wordApp, p);
            WordInterop.FillSuperIntendent(wordApp, td);

            WordInterop.AdjustPages(doc, AppConfig.Instance.DocFormatMain);

            return Paths.Instance.GetOutputPath(DocType);
        }
    }
}
