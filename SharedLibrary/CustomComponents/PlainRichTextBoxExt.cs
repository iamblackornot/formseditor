﻿using System;
using System.Drawing;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using System.ComponentModel;

namespace SharedLibrary
{
    public class PlainRichTextBoxExt : RichTextBox
    {
        const int WM_USER = 0x400;
        const int EM_SETTEXTMODE = WM_USER + 89;
        const int EM_GETTEXTMODE = WM_USER + 90;

        // EM_SETTEXTMODE/EM_GETTEXTMODE flags
        const int TM_PLAINTEXT = 1;
        const int TM_RICHTEXT = 2;          // Default behavior 
        const int TM_SINGLELEVELUNDO = 4;
        const int TM_MULTILEVELUNDO = 8;    // Default behavior 
        const int TM_SINGLECODEPAGE = 16;
        const int TM_MULTICODEPAGE = 32;    // Default behavior 

        [DllImport("user32.dll")]
        static extern IntPtr SendMessage(IntPtr hWnd, int wMsg, IntPtr wParam, IntPtr lParam);

        bool m_PlainTextMode = true;

        // If this property doesn't work for you from the designer for some reason
        // (for example framework version...) then set this property from outside
        // the designer then uncomment the Browsable and DesignerSerializationVisibility
        // attributes and set the Property from your component initializer code
        // that runs after the designer's code.
        [DefaultValue(false)]
        //[Browsable(false)]
        //[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public bool PlainTextMode
        {
            get
            {
                return m_PlainTextMode;
            }
            set
            {
                m_PlainTextMode = value;
                if (IsHandleCreated)
                {
                    IntPtr mode = value ? (IntPtr)TM_PLAINTEXT : (IntPtr)TM_RICHTEXT;
                    SendMessage(Handle, EM_SETTEXTMODE, mode, IntPtr.Zero);
                }
            }
        }
        //[Browsable(true)]
        [DefaultValue(false)]
        public bool ScrollParentOnMouseWheel { get; set; } = false;

        const int WM_MOUSEWHEEL = 0x020A;

        protected override void WndProc(ref Message m)
        {
            if (ScrollParentOnMouseWheel && m.Msg == WM_MOUSEWHEEL)
            {
                //directly send the message to parent without processing it
                //according to https://stackoverflow.com/a/19618100
                SendMessage(this.Parent.Handle, m.Msg, m.WParam, m.LParam);
                m.Result = IntPtr.Zero;
            }
            else base.WndProc(ref m);
        }

        protected override void OnHandleCreated(EventArgs e)
        {
            // For some reason it worked for me only if I manipulated the created
            // handle before calling the base method.
            PlainTextMode = m_PlainTextMode;
            base.OnHandleCreated(e);
            AddContextMenu();
        }

        public void AddContextMenu()
        {
            if (ContextMenuStrip == null)
            {
                ContextMenuStrip cms = new ContextMenuStrip { ShowImageMargin = false };
                ToolStripMenuItem tsmiCut = new ToolStripMenuItem("Вырезать");
                tsmiCut.Click += (sender, e) => Cut();
                cms.Items.Add(tsmiCut);
                ToolStripMenuItem tsmiCopy = new ToolStripMenuItem("Копировать");
                tsmiCopy.Click += (sender, e) => Copy();
                cms.Items.Add(tsmiCopy);
                ToolStripMenuItem tsmiPaste = new ToolStripMenuItem("Вставить");
                tsmiPaste.Click += (sender, e) => Paste();
                cms.Items.Add(tsmiPaste);
                ContextMenuStrip = cms;
            }
        }
        public string Cue
        {
            get { return cue; }
            set
            {
                showCue(false);
                cue = value;
                //SetText(Text);
                if (!this.Focused && !String.IsNullOrWhiteSpace(cue)) showCue(true);
            }
        }
        private string cue;

        protected override void OnEnter(EventArgs e)
        {
            showCue(false);
            base.OnEnter(e);
        }

        protected override void OnLeave(EventArgs e)
        {
            showCue(true);
            base.OnLeave(e);
        }

        protected override void OnVisibleChanged(EventArgs e)
        {
            if (!this.Focused && !String.IsNullOrWhiteSpace(cue)) showCue(true);
            base.OnVisibleChanged(e);
        }

        public bool IsCueOn { get; protected set; } = true;

        private void showCue(bool visible)
        {
            if (this.DesignMode) visible = false;
            if (visible)
            {
                if (this.Text.Length == 0)
                {
                    IsCueOn = true;
                    this.Text = cue;
                    //this.SelectAll();
                    //this.SelectionColor = Color.Gray;
                    this.ForeColor = Color.Gray;
                    this.Font = new Font(this.Font, FontStyle.Italic);
                }
            }
            else
            {
                if (this.Text == cue) //this check is needed for enter/leave events
                {
                    IsCueOn = false;
                    //this.SelectionColor = this.ForeColor;
                    this.Clear();
                    this.ForeColor = SystemColors.WindowText;
                    this.Font = new Font(this.Font, FontStyle.Regular);
                }
            }
        }

        public void SetText(string text)
        {
            if (String.IsNullOrWhiteSpace(text))
            {
                if (!String.IsNullOrWhiteSpace(Cue) && !IsCueOn)
                {
                    Clear();
                    showCue(true);
                }
            }
            else
            {
                if (!string.Equals(text, cue))
                {
                    if (IsCueOn)
                        showCue(false);
                    Text = text;
                }
            }
        }
    }
}