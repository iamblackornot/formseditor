﻿using System;
using System.IO;

namespace SharedLibrary.Settings
{
    public class PathsBase
    {
        public static readonly string SERVER_LOCATION_FILENAME = "server_location.ini";
        public static readonly string LOCAL_LOGS_FILENAME = "logs.txt";

        public string Settings { get; private set;  }
        public string ConfigDatabase { get; private set; }
        public string DedicatedLogs { get; private set; }
        public string ServerPath { get; private set; }

        protected PathsBase() { }

        public virtual SimpleResult Init()
        {
            if (!File.Exists(SERVER_LOCATION_FILENAME))
                return new SimpleResult(false, $"{SERVER_LOCATION_FILENAME} - файл не найден");

            string serverPath = File.ReadAllText(SERVER_LOCATION_FILENAME).Trim();

            if (!Directory.Exists(serverPath))
                return new SimpleResult(false, $"Неверный путь к серверной папке приложения, укажите правильный путь в файле {SERVER_LOCATION_FILENAME}");

            Settings = Path.Combine(serverPath, "cfg");
            ConfigDatabase = Path.Combine(serverPath, "config.db");
            DedicatedLogs = Path.Combine(serverPath, "logs");

            ServerPath = serverPath;

            return new SimpleResult(true, string.Empty);
        }
    }
}
