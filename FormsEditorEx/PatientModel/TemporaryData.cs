﻿using System;
using SharedLibrary;
using Newtonsoft.Json;

namespace FormsEditorEx
{
    public class TemporaryData
    {
        public TemporaryData() { }
        public void Reset()
        {
            Utility.ClearWritableProperties(this);
        }
        [JsonProperty("te")]
        public string TimeExam { get; set; }

        [JsonProperty("dli")]
        public string DisabilityListInfo { get; set; }

        [JsonProperty("io")]
        public int? IO { get; set; }
        [JsonProperty("supint")]
        public long? SuperIntendent { get; set; }
        [JsonProperty("sintf")]
        public string SuperIntendentFormatted { get; set; }

        [JsonProperty("gc")]
        public int? GenCondition { get; set; }
        [JsonProperty("nu")]
        public int? Nutrition { get; set; }
        [JsonProperty("lfan")]
        public string LifeAnamnesisPre { get; set; }

        [JsonProperty("vtr")]
        public int? VTEORisk { get; set; }
        [JsonProperty("gor")]
        public int? GORisk { get; set; }
        [JsonProperty("vtp")]
        public string VTEOPrevention { get; set; }

        [JsonProperty("diag")]
        public string DiagnosisPre { get; set; }
        [JsonProperty("cmpl")]
        public string ComplicationsPre { get; set; }
        [JsonProperty("accdiag")]
        public string AccompDiagnosisPre { get; set; }
        [JsonProperty("acc")]
        public string AccompMedicationPre { get; set; }
        [JsonProperty("pex")]
        public string PlannedExamination { get; set; }
        [JsonProperty("pos")]
        public string PlannedOperationScope { get; set; }
        [JsonProperty("assign")]
        public string Assignments { get; set; }
        [JsonProperty("cht")]
        public bool? IsChemTherapy { get; set; }
        [JsonProperty("vmp")]
        public bool? IsVMP { get; set; }
        [JsonProperty("sloc")]
        public string StatusLocalisPre { get; set; }
        [JsonProperty("malle")]
        public string MedicalAllergy { get; set; }
        [JsonProperty("trnf")]
        public string Transfusion { get; set; }
        [JsonProperty("addinf")]
        public string AdditionalInfo { get; set; }

        [JsonProperty("rcmn")]
        public string Recommendations { get; set; }
        [JsonProperty("tres")]
        public int? TreatmentResult { get; set; }
        [JsonProperty("cnd")]
        public int? ConditionPost { get; set; }
        [JsonProperty("wc")]
        public int? WorkCapacity { get; set; }

        [JsonProperty("sln")]
        public string SickLeaveNum { get; set; }
        [JsonProperty("sls")]
        public DateTime? SickLeaveStart { get; set; }
        [JsonProperty("sle")]
        public DateTime? SickLeaveEnd { get; set; }
        [JsonProperty("sles")]
        public DateTime? SickLeaveExtStart { get; set; }
        [JsonProperty("slee")]
        public DateTime? SickLeaveExtEnd { get; set; }
        [JsonProperty("slet")]
        public int? SickLeaveEndType { get; set; }
        
        public string Serialize()
        {
            var json = Utility.SerializeDataObject(this);

            return json != Static.EmptyJObject ? json : null; 
        }
    }
}
