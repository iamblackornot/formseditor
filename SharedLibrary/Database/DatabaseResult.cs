﻿namespace SharedLibrary.Database
{
    public class DatabaseResult<T>
    {
        public T Data { get; }
        public bool Completed { get; }
        public string Message { get; }
        public DatabaseResult(bool completed, T data = default(T), string message = "")
        {
            this.Data = data;
            this.Completed = completed;
            this.Message = message;
        }
    }

    public struct Nothing { }
    public class DefaultDatabaseError<T> : DatabaseResult<T>
    {
        public DefaultDatabaseError() : base(false, default(T), "Не удалось обратиться к базе данных") { }
    }
    public class NoConnectionError<T> : DatabaseResult<T>
    {
        public NoConnectionError() : base(false, default(T), "Нет подключения к базе данных") { }
    }
}
