﻿using System;
using SharedLibrary;

namespace FormsEditorEx.GenExam
{
    public class GenExamRecord
    {
        public DateTime? Date { get; set; }
        public string Result { get; set; }
        public override string ToString()
        {
            return (Date.HasValue ? Utility.DateToShortString(Date.Value) + Symbols.NBSpace + Symbols.NBSpace : string.Empty) + Result.EmptyIfNull();
        }
    }
}
