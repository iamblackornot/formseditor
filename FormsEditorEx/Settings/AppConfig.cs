﻿using SharedLibrary;
using System.Collections.Generic;
using System.IO;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Reflection;
using FormsEditorEx.Settings;
using SharedLibrary.Settings;
using System.Data.Common;
using FormsEditorEx.Database;

namespace FormsEditorEx
{
    public class AppConfig
    {
        public InfoClass Info { get; private set; }
        public PersonnelCollection Staff { get; private set; }
        public DocFormat DocFormatVMP { get; private set; }
        public DocFormat DocFormatMain { get; private set; }
        public OperTeam OperTeam { get; private set; }
        public int LastDepartment { get; private set; } = -1;
        public long? LastDoctor { get; private set; }
        public static AppConfig Instance { get; private set; } = new AppConfig();

        private AppConfig() { }

        public static LoadSettingsResult Load(DatabaseAdapter dbAdapter)
        {
            try
            {
                Instance.Info = new InfoClass();

                if (!Instance.Info.Load(Paths.Instance.Settings))
                    return new LoadSettingsResult(false, "общая информация cfg/info");

                var staffRes = dbAdapter.GetPersonnel();

                if (!staffRes.Completed)
                    return new LoadSettingsResult(false, "персонал");

                Instance.Staff = staffRes.Data;

                Instance.DocFormatMain = CreateMainFormatting();

                if (!LoadVMPFormatting())
                    return new LoadSettingsResult(false, "форматирование");

                //LoadLocalSettings();
                if (!LoadLocalSettings())
                    return new LoadSettingsResult(false, "локальные");

                return new LoadSettingsResult(true);
            }
            catch (Exception ex)
            {
                Logger.AddToLogs(MethodBase.GetCurrentMethod().Name, ex.ToString());

                return new LoadSettingsResult(false);
            }
        }
        private static DocFormat CreateMainFormatting()
        {
            DocFormat docFormat = new DocFormat();

            docFormat.LineBreakSize = 9;

            docFormat.Pages = new Dictionary<int, PageFormat>
            {
                { 1, new PageFormat() { MaxTopPadding = 0, isScalable = true } }
            };

            return docFormat;
        }
        private static bool LoadVMPFormatting()
        {
            if (File.Exists(Path.Combine(Paths.Instance.Settings, "format")))
            {
                string json = File.ReadAllText(Path.Combine(Paths.Instance.Settings, "format"));
                Instance.DocFormatVMP = JsonConvert.DeserializeObject<DocFormat>(json);

                return true;
            }

            return false;
        }
        private static bool LoadLocalSettings()
        {
            return Middleware.HandleAction(LoadLocalSettingsAction);
        }
        private static void LoadLocalSettingsAction()
        {
            Instance.OperTeam = new OperTeam();

            if (File.Exists(Paths.LocalSettings))
            {
                string json = File.ReadAllText(Paths.LocalSettings);

                if (!string.IsNullOrWhiteSpace(json))
                {
                    JObject settings = JObject.Parse(json);

                    if (settings.ContainsKey("department"))
                    {
                        int dep_id = (int)settings["department"];

                        if(dep_id <= Enum.GetNames(typeof(Departments)).Length - 1 && dep_id >= -2)
                        {
                            Instance.LastDepartment = dep_id;
                        }
                    }

                    if (settings.ContainsKey("doctor"))
                    {
                        if(long.TryParse(settings["doctor"].ToString(), out long value))
                        {
                            Instance.LastDoctor = value;
                        }
                    }

                    if (settings["operteam"] is JObject jobj)
                    {
                        if(jobj.ToString().TryJsonDeserealize<OperTeam>(out OperTeam ot))
                            Instance.OperTeam = ot;
                    }
                }
            }
        }
        public static void SaveLocalSettings(int dep, long? doc)
        {
            JObject settings = new JObject();
            settings.Add("department", new JValue(dep));

            if(doc.HasValue) 
            {
                settings.Add("doctor", new JValue(doc.Value));
            }

            settings.Add("operteam", Instance.OperTeam.Serialize());

            if(!Directory.Exists(Paths.LocalSettingsFolder))
            {
                Directory.CreateDirectory(Paths.LocalSettingsFolder);
            }

            File.WriteAllText(Paths.LocalSettings, settings.ToString());
        }
    }
}
