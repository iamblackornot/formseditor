﻿using System.Windows.Forms;
using SharedLibrary;

namespace FormsEditorEx
{
    public class MainFrameStateBase : IMainFrameState
    {
        public MainFrame Handler { get; }
        public MainFrameState Type { get; protected set; }
        public MainFrameStateBase(MainFrame handler)
        {
            Handler = handler;
        }
        public virtual IMainFrameState Action(ucStatusBarControl ucStatusBar)
        {
            return this;
        }
        public virtual void FillTestData()
        {
            
        }
        public virtual IMainFrameState Revert(ucStatusBarControl ucStatusBar)
        {
            return this;
        }
        protected bool ShowRevertPrompt()
        {
            return MessageBox.Show("Подтвердите откат на предыдущий этап", string.Empty, MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation) == DialogResult.OK;
        }
    }
}

