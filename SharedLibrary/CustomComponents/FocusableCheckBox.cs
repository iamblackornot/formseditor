﻿using System.Windows.Forms;

namespace SharedLibrary
{
    public class FocusableCheckBox : CheckBox
    {
        protected override bool ShowFocusCues => true; 
    }
}
