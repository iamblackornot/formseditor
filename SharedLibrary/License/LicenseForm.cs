﻿using System;
using System.Windows.Forms;

namespace SharedLibrary
{
    public partial class LicenseForm : Form
    {
        double date;
        int hrd_id;
        public LicenseForm()
        {
            InitializeComponent();
        }

        private void LicenseForm_Load(object sender, EventArgs e)
        {
            date = TimeNow();
            hrd_id = Licensing.GetHardwareId(date);
            tbSeed.Text = hrd_id.ToString();
        }

        private void btActivate_Click(object sender, EventArgs e)
        {
            if (Licensing.CheckKey(hrd_id, tbKey.Text.Trim()) == AppSoftware.LicenceEngine.KeyVerification.PkvLicenceKeyResult.KeyGood)
            {
                DialogResult = DialogResult.OK;
                Licensing.SaveActivation(tbKey.Text.Trim(), date, "frmsdtr");
                this.Close();
            }
            else
            {
                ToolTip tt = new ToolTip();
                tt.Show("Неверный ключ", this, btActivate.Right + 6, btActivate.Bottom, 2000);  
            }
        }
        public static double TimeNow()
        {
            TimeSpan epochTicks = new TimeSpan(new DateTime(1970, 1, 1).Ticks);
            TimeSpan unixTicks = new TimeSpan(DateTime.UtcNow.Ticks) - epochTicks;
            return Math.Floor(unixTicks.TotalSeconds);
        }
    }
}
