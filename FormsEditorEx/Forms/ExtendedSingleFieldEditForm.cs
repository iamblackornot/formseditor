﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace FormsEditorEx
{
    public partial class ExtendedSingleFieldEditForm : Form
    {
        bool hasChanged = false;
        bool isReadOnly;
        public string InputText { get; private set; } = string.Empty;
        public ExtendedSingleFieldEditForm(string caption, string initValue, bool isReadOnly = true)
        {
            InitializeComponent();

            Text = caption;

            this.isReadOnly = isReadOnly;
            rtbInput.ReadOnly = isReadOnly;
            btOK.Enabled = isReadOnly;

            rtbInput.Text = initValue;
            rtbInput.SelectionStart = rtbInput.Text.Length;

            if (isReadOnly)
            {
                pnButtons.Controls.Remove(btCancel);
                rtbInput.TabStop = false;
            }

            ExtendedSingleFieldEditForm_Resize(null, EventArgs.Empty);
        }

        private void btCancel_Click(object sender, EventArgs e)
        {
            if (hasChanged)
            {
                if (MessageBox.Show("Закрыть окно без сохранения изменений?", "Требуется подтверждение",
                    MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                {
                    DialogResult = DialogResult.Cancel;
                    Close();
                }
            }
            else
                Close();
        }

        private void ExtendedSingleFieldEditForm_Resize(object sender, EventArgs e)
        {
            pnButtons.Margin = new Padding((pnContent.Width - pnButtons.Width) / 2 - pnContent.Padding.Left,
                pnButtons.Margin.Top, pnButtons.Margin.Right, pnButtons.Margin.Bottom);
        }

        private void rtbInput_TextChanged(object sender, EventArgs e)
        {
            hasChanged = true;
            btOK.Enabled = true;
        }

        private void btOK_Click(object sender, EventArgs e)
        {
            if (!isReadOnly)
            {
                InputText = rtbInput.Text.Trim();
                DialogResult = DialogResult.OK;
            }
            else
                DialogResult = DialogResult.Cancel;

            Close();
        }
    }
}
