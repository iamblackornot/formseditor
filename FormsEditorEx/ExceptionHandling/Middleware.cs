﻿using System;
using System.Data.SQLite;
using System.Windows.Forms;
using FormsEditorEx.Database;
using Microsoft.Office.Interop.Word;
using Word = Microsoft.Office.Interop.Word;
using System.Runtime.InteropServices;
using System.Reflection;
using SharedLibrary;
using System.IO;

namespace FormsEditorEx
{
    public class Middleware
    {
        public delegate DatabaseResult DBAction();

        public static bool HandleAction(Action action, ucStatusBarControl usStatusBar = null)
        {
            try
            {
                action();
                return true;
            }
            catch (Exception ex)
            {
                Logger.AddToLogs(ex.ToString());
                return false;
            }
        }
        public static bool HandleDBAction(DBAction action, ucStatusBarControl ucStatusBar)
        {
            return HandleDBActionBase(action, ucStatusBar).Completed;
        }
        public static DatabaseResult HandleDBAction(DBAction action)
        {
            return HandleDBActionBase(action);
        }

        private static DatabaseResult HandleDBActionBase(DBAction action, ucStatusBarControl ucStatusBar = null)
        {
            DatabaseResult dbres = new DefaultDatabaseError();

            try
            {
                ucStatusBar?.ShowWarning("Обработка запроса...");

                dbres = action();
                //ucStatusBar?.ShowStatus(dbres);
            }
            catch (Exception ex)
            {
                Logger.AddToLogs(ex.ToString());
            }

            ucStatusBar?.ShowStatus(dbres);
            return dbres;
        }
        public static bool HandleFillAction(IFillStrategy strategy, out string outputPath)
        {
            bool res = false;
            outputPath = string.Empty;

            Word.Application wordApp = null;
            Word.Document doc = null;

            try
            {
                wordApp = new Word.Application();
                wordApp.ScreenUpdating = false;
                doc = wordApp.Documents.Open(Path.Combine(Paths.Instance.Settings, Static.TemplateFileNames[strategy.DocType]), ReadOnly: true);
                doc.Activate();

                wordApp.ActiveWindow.View.ReadingLayout = false;

                outputPath = strategy.Fill(wordApp, doc); 

                doc.SaveAs(outputPath);
                doc?.Close(WdSaveOptions.wdDoNotSaveChanges);

                res = true;
            }

            catch (COMException comex) when (comex.ErrorCode == -2146822932)
            {
                    MessageBox.Show("Файл " + outputPath + " недоступен для записи. Скорее всего, открыт в текстовом редакторе");
            }
            catch (Exception ex)
            {
                //Dump();
                try
                {
                    doc?.Close(WdSaveOptions.wdDoNotSaveChanges);
                }
                catch { }

                Logger.AddToLogs(MethodBase.GetCurrentMethod().Name, ex.ToString());
            }
            finally
            {
                try
                {
                    ReleaseCOM(doc);
                    wordApp?.Quit(WdSaveOptions.wdDoNotSaveChanges);
                    ReleaseCOM(wordApp);
                }
                catch (Exception ex)
                {
                    Logger.AddToLogs(MethodBase.GetCurrentMethod().Name, ex.ToString());
                }
            }

            return res;
        }
        private static void ReleaseCOM(object o)
        {
            if (o == null) return;
            try
            {
                while (Marshal.ReleaseComObject(o) > 0);
            }
            finally
            {
                o = null;
            }
        }
        public static bool FillAndSavePatientAction(IFillStrategy strategy, DatabaseAdapter dbAdapter, ucStatusBarControl ucStatusBar, out string outputPath)
        {
            ucStatusBar.ShowWarning("Заполнение формы...");

            if (Middleware.HandleFillAction(strategy, out outputPath))
            {
                ucStatusBar.ShowWarning("Сохранение пациента в БД...");

                if (Patient.Save(dbAdapter, ucStatusBar))
                {
                    ucStatusBar.ShowOK("Форма заполнена. Данные пациента сохранены.");

                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                ucStatusBar.ShowError("Заполнить форму не удалось. Данные пациента не сохранены.");

                return false;
            }
        }
    }
}
