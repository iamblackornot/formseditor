﻿namespace FormsEditorEx
{
    partial class NewClinicalRecordForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.tbEdit = new System.Windows.Forms.MaskedTextBox();
            this.pnButtons = new System.Windows.Forms.Panel();
            this.btTest = new System.Windows.Forms.Button();
            this.btCancel = new System.Windows.Forms.Button();
            this.btOK = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.pnButtons.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.panel1.Controls.Add(this.tbEdit);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(16, 16);
            this.panel1.Margin = new System.Windows.Forms.Padding(3, 3, 3, 15);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(198, 74);
            this.panel1.TabIndex = 4;
            // 
            // tbEdit
            // 
            this.tbEdit.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbEdit.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.tbEdit.Location = new System.Drawing.Point(0, 0);
            this.tbEdit.Mask = "0000000/00";
            this.tbEdit.Name = "tbEdit";
            this.tbEdit.Size = new System.Drawing.Size(198, 26);
            this.tbEdit.TabIndex = 0;
            this.tbEdit.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.tbEdit.TextChanged += new System.EventHandler(this.tbEdit_TextChanged);
            // 
            // pnButtons
            // 
            this.pnButtons.Controls.Add(this.btTest);
            this.pnButtons.Controls.Add(this.btCancel);
            this.pnButtons.Controls.Add(this.btOK);
            this.pnButtons.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnButtons.Location = new System.Drawing.Point(16, 60);
            this.pnButtons.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.pnButtons.Name = "pnButtons";
            this.pnButtons.Size = new System.Drawing.Size(198, 30);
            this.pnButtons.TabIndex = 5;
            // 
            // btTest
            // 
            this.btTest.Location = new System.Drawing.Point(66, 0);
            this.btTest.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btTest.Name = "btTest";
            this.btTest.Size = new System.Drawing.Size(24, 30);
            this.btTest.TabIndex = 7;
            this.btTest.TabStop = false;
            this.btTest.Text = "T";
            this.btTest.UseVisualStyleBackColor = true;
            this.btTest.Click += new System.EventHandler(this.btTest_Click);
            // 
            // btCancel
            // 
            this.btCancel.Location = new System.Drawing.Point(109, 0);
            this.btCancel.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btCancel.Name = "btCancel";
            this.btCancel.Size = new System.Drawing.Size(90, 30);
            this.btCancel.TabIndex = 6;
            this.btCancel.TabStop = false;
            this.btCancel.Text = "Отмена";
            this.btCancel.UseVisualStyleBackColor = true;
            this.btCancel.Click += new System.EventHandler(this.btCancel_Click);
            // 
            // btOK
            // 
            this.btOK.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btOK.Location = new System.Drawing.Point(-1, 0);
            this.btOK.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btOK.Name = "btOK";
            this.btOK.Size = new System.Drawing.Size(90, 30);
            this.btOK.TabIndex = 5;
            this.btOK.TabStop = false;
            this.btOK.Text = "ОК";
            this.btOK.UseVisualStyleBackColor = true;
            this.btOK.Click += new System.EventHandler(this.btOK_Click);
            // 
            // NewClinicalRecordForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(230, 106);
            this.ControlBox = false;
            this.Controls.Add(this.pnButtons);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Name = "NewClinicalRecordForm";
            this.Padding = new System.Windows.Forms.Padding(16);
            this.ShowInTaskbar = false;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Введите номер истории болезни";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.pnButtons.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.MaskedTextBox tbEdit;
        private System.Windows.Forms.Panel pnButtons;
        private System.Windows.Forms.Button btTest;
        private System.Windows.Forms.Button btCancel;
        private System.Windows.Forms.Button btOK;
    }
}