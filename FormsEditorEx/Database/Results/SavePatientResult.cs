﻿using SharedLibrary;

namespace FormsEditorEx.Database
{
    public class PatientSaved : DatabaseResult
    {
        public PatientSaved() : base(true, StatusBarIndicator.OK, "Данные пациента сохранены") { }
    }
    public class PatientSaveError : DatabaseResult
    {
        public PatientSaveError() : base(false, StatusBarIndicator.ERROR, "Не удалось сохранить данные пациента") { }
    }
}
