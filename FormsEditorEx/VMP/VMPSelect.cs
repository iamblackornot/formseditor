﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using SharedLibrary;
using System.Windows.Forms;
using System.Text.RegularExpressions;
using System.ComponentModel;
using SharedLibrary.Settings;

namespace FormsEditorEx.VMP
{
    public partial class VMPSelect : Form
    {
        Finance vmp_fin;

        BindingSource bsGroups;
        BindingSource bsDesc;
        BindingSource bsMKBs;
        BindingSource bsTreatment;
        BindingSource bsModels;

        DataTable dtMKB;

        const int GRID_HEIGHT = 600;

        int min_height;

        public int FinanceType { get; set; }
        public string Group { get; set; }
        public string VMPCode { get; set; }
        public string VMPDesc { get; set; }
        public string MKB { get; set; }
        public string ModelCode { get; set; }
        public string Model { get; set; }
        public int TreatmentType { get; set; }
        public string MethodCode { get; set; }
        public string Method { get; set; }

        DataSet ds;
        List<string> TreatmentTypes;
        bool isMethodsJustSet;

        int dep;
        public VMPSelect(DataSet _ds, List<string> _TreatmentTypes, int _dep = 0)
        {
            dep = _dep;
            ds = _ds;
            TreatmentTypes = _TreatmentTypes;
            InitializeComponent();
            CenterToScreen();
            ActiveControl = null;
            TransparencyKey = BackColor = Color.Magenta;
            btCancel.Left = this.ClientSize.Width - btCancel.Width - 6;
            btSelect.Left = btCancel.Left - btSelect.Width - 6;

            System.Drawing.Rectangle rect = Screen.FromControl(this).Bounds;
            if (this.Width > rect.Width)
                this.Width = rect.Width;

            InitVMPPanel();
        }

        private void InitVMPPanel()
        {
            btRegion.Top = btOMS.Top;
            btRegion.Left = btOMS.Right + 16;
            pnVMP.Size = new Size(3 * 16 + btOMS.Width + btRegion.Width, 2 * 16 + btOMS.Height );

            pnVMP.Location = new Point(
            this.ClientSize.Width / 2 - pnVMP.Size.Width / 2,
            this.ClientSize.Height / 2 - pnVMP.Size.Height / 2);
            pnVMP.Anchor = AnchorStyles.None;
        }

        private void btOMS_Click(object sender, EventArgs e)
        {
            SetFinance(Finance.OMS);
        }

        private void btRegion_Click(object sender, EventArgs e)
        {
            SetFinance(Finance.Region);
        }
        private void SetFinance(Finance fin)
        {
            vmp_fin = fin;
            lbFinance.Text += fin == Finance.OMS ? "ОМС" : "РЕГИОН";
            pnVMP.Visible = false;
            lbFinance.Visible = true;
            btCancel.Visible = true;
            btSelect.Visible = true;
            SetGroups();

            this.BackColor = SystemColors.Control;
            AllowTransparency = false;
            this.FormBorderStyle = FormBorderStyle.FixedSingle;
            BringToFront();
        }

        private void SetGroups()
        {
            bsGroups = new BindingSource(ds, "groups");
            bsGroups.Filter = String.Format("dep_id = {0} AND vmp_fin_id = {1}", dep, (int)vmp_fin);
            dgvGroups.DataSource = bsGroups;

            dgvGroups.Columns["group_id"].Visible = false;
            dgvGroups.Columns["dep_id"].Visible = false;
            dgvGroups.Columns["vmp_fin_id"].Visible = false;

            dgvGroups.BackgroundColor = SystemColors.Control;

            dgvGroups.Show();
        }

        private void btCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            Close();
        }

        private void dgvGroups_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
            int height = dgvGroups.ColumnHeadersHeight;
            foreach (DataGridViewRow row in dgvGroups.Rows)
                height += row.Height;

            if (height > GRID_HEIGHT)
                height = GRID_HEIGHT;

            dgvGroups.Height = height;

            if (height > min_height)
            {
                //this.Height = dgvGroups.Bottom + 6;
                this.ClientSize = new Size(ClientSize.Width, dgvGroups.Bottom + 6);

                if (this.HorizontalScroll.Visible)
                    this.Height += 20;

                min_height = height;
            }

            if (dgvDesc.DataSource == null)
            {
                if (dgvGroups.Rows.Count == 1)
                {
                    SetDesc();
                    dgvGroups.Rows[0].Selected = true;
                }
                else if (dgvGroups.Rows.Count > 1)
                    dgvGroups.ClearSelection();
            }
        }

        private void dgvGroups_SelectionChanged(object sender, EventArgs e)
        {
            if (dgvGroups.Focused && dgvDesc.DataSource == null)
                SetDesc();
        }

        private void SetDesc()
        {
            bsDesc = new BindingSource(bsGroups, "DescFiltered");
            dgvDesc.DataSource = bsDesc;

            dgvDesc.BackgroundColor = SystemColors.Control;

            dgvDesc.Show();
        }

        private void dgvDesc_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
            dgvDesc.Columns["desc_id"].Visible = false;
            dgvDesc.Columns["group_id"].Visible = false;

            dgvDesc.Columns["Код"].Width = 80;
            dgvDesc.Top = dgvGroups.Top;

            if (vmp_fin == Finance.Region)
                dgvDesc.Columns["Код"].Visible = false;

            int height = dgvDesc.ColumnHeadersHeight;
            foreach (DataGridViewRow row in dgvDesc.Rows)
                height += row.Height;

            if (height > GRID_HEIGHT)
                height = GRID_HEIGHT;

            dgvDesc.Height = height;

            if (height > min_height)
            {
                this.ClientSize = new Size(ClientSize.Width, dgvDesc.Bottom + 6);

                if (this.HorizontalScroll.Visible)
                    this.Height += 20;

                min_height = height;
            }

            if (dgvMKBs.DataSource == null)
            {
                if (dgvDesc.Rows.Count == 1)
                {
                    dgvDesc.Rows[0].Selected = true;
                    SetMKBs();
                }
                else if (dgvDesc.Rows.Count > 1)
                    dgvDesc.ClearSelection();
            }
        }

        private void dgvDesc_SelectionChanged(object sender, EventArgs e)
        {
            if (dgvDesc.Focused && dgvMKBs.DataSource == null)
                SetMKBs();
        }

        private void SetMKBs()
        {
            dtMKB = new DataTable();
            dtMKB.Columns.Add("МКБ", typeof(string));

            bsMKBs = new BindingSource(bsDesc, "MKBsFiltered");
            dgvMKBs.DataSource = bsMKBs;

            dgvMKBs.BackgroundColor = SystemColors.Control;

            dgvMKBs.Show();
        }

        private void dgvMKBs_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
            dgvMKBs.Columns["mkb_id"].Visible = false;
            dgvMKBs.Columns["desc_id"].Visible = false;

            dgvMKBs.Width = 120;
            dgvMKBs.Left = dgvDesc.Right + 6;

            int height = dgvMKBs.ColumnHeadersHeight;
            foreach (DataGridViewRow row in dgvMKBs.Rows)
                height += row.Height;

            if (height > GRID_HEIGHT)
                height = GRID_HEIGHT;

            dgvMKBs.Height = height;

            if (height > min_height)
            {
                this.ClientSize = new Size(this.ClientSize.Width, dgvMKBs.Bottom + 6);

                if (this.HorizontalScroll.Visible)
                    this.Height += 20;

                min_height = height;
            }

            dgvMKBs.Sort(dgvMKBs.Columns["Коды МКБ"], ListSortDirection.Ascending);

            if (dgvTreatment.DataSource == null && dgvMKBs.Rows.Count > 1)
            {
                dgvMKBs.ClearSelection();
            }
            else
            {
                dgvMKBs.Rows[0].Selected = true;
                dgvMKBs.CurrentCell = dgvMKBs.Rows[0].Cells["Коды МКБ"];
                SetMKB();
                SetModels();
            }
        }

        private void dgvMKBs_SelectionChanged(object sender, EventArgs e)
        {
            if (dgvMKB.DataSource != null && dgvMKBs.CurrentRow != null)
                MKBPopulate(dgvMKBs.CurrentRow.Cells["Коды МКБ"].Value.ToString());

            if (dgvMKBs.Focused && dgvMKB.DataSource == null)
            {
                SetMKB();
                SetModels();
            }
        }

        private void MKBPopulate(string text)
        {
            dtMKB.Clear();

            string[] split = text.Split(',');
            Regex regexSingle = new Regex(@"[A-ZА-Я]\d{2}(\.\d{1,2})?");
            Regex regexRange = new Regex(@"[A-ZА-Я]\d{2}(\.\d{1,2})?(\s+)?\-(\s+)?[A-ZА-Я]\d{2}(\.\d{1,2})?");

            foreach (string s in split)
            {
                Match match = regexRange.Match(s);

                if (match.Success)
                {
                    MatchCollection matches = regexSingle.Matches(s);
                    if (matches.Count != 2)
                        MessageBox.Show("Преобразование диапазона (например, C44.0 - C44.9) в список кодов не удалось. Проверьте значение текущей строки таблицы \"Коды МКБ\", скорее всего, оно задано в неверном формате либо с опечатками");
                    string num = matches[1].Value.Substring(1, matches[1].Value.Length - 1);
                    float end;
                    if (float.TryParse(num, out end))
                    {
                        float step = 1;

                        if (end - Math.Floor(end) > 0.01f)
                            step = 0.1f;

                        num = matches[0].Value.Substring(1, matches[1].Value.Length - 1);
                        float start;
                        if (float.TryParse(num, out start))
                        {
                            char ch = matches[0].Value[0];

                            for (float i = start; i <= end; )
                            {
                                string number = i.ToString();
                                if (i < 10)
                                    number = number.Insert(0, "0");

                                if ((i % (float)Math.Floor(i) < 0.01f) && (end % (float)Math.Floor(end) > 0.01f))
                                    number += ".0";

                                dtMKB.Rows.Add(ch + number);
                                i += step;
                                if (step < 1)
                                    i = (float)Math.Round(i, 1, MidpointRounding.ToEven);
                            }
                        }
                    }
                }
                else
                {
                    match = regexSingle.Match(s);

                    if (match.Success)
                        dtMKB.Rows.Add(match.Value);
                }
            }
        }

        private void SetMKB()
        {
            dgvMKB.DataSource = new BindingSource(dtMKB, "");

            MKBPopulate(dgvMKBs.CurrentCell.Value.ToString());

            dgvMKB.BackgroundColor = SystemColors.Control;

            dgvMKB.Show();
        }
        private void dgvMKB_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
            dgvMKB.Left = dgvMKBs.Right + 6;

            int height = dgvMKB.ColumnHeadersHeight;
            foreach (DataGridViewRow row in dgvMKB.Rows)
                height += row.Height;

            if (height > GRID_HEIGHT)
                height = GRID_HEIGHT;

            dgvMKB.Height = height;

            if (height > min_height)
            {
                this.ClientSize = new Size(this.ClientSize.Width, dgvMKB.Bottom + 6);

                if (this.HorizontalScroll.Visible)
                    this.Height += 20;

                min_height = height;
            }

            dgvMKB.Sort(dgvMKB.Columns["МКБ"], ListSortDirection.Ascending);

            if (dgvMethods.DataSource == null)
            {
                if (dgvMKB.Rows.Count == 1)
                {
                    //dgvMKB.Focus();
                    dgvMKB.Rows[0].Selected = true;
                }
                else if (dgvMKB.Rows.Count > 1)
                    dgvMKB.ClearSelection();
            }
        }

        private void SetTreatment()
        {
            bsTreatment = new BindingSource(bsModels, "TreatmentFiltered");
            dgvTreatment.DataSource = bsTreatment;

            DataGridViewComboBoxColumn dcCombo = new DataGridViewComboBoxColumn();

            DataTable dtCombo = new DataTable();
            dtCombo.Columns.Add("type_id", typeof(Int32));
            dtCombo.Columns.Add("type_name", typeof(string));

            for (int i = 0; i < TreatmentTypes.Count; i++)
                dtCombo.Rows.Add(new object[] { i, TreatmentTypes[i] });

            dcCombo.HeaderText = "Вид лечения";
            dcCombo.DataSource = dtCombo;
            dcCombo.ValueMember = "type_id";
            dcCombo.DisplayMember = "type_name";
            dcCombo.DataPropertyName = "type";
            dcCombo.DisplayStyle = DataGridViewComboBoxDisplayStyle.Nothing;

            dgvTreatment.Columns.Add(dcCombo);

            dgvTreatment.BackgroundColor = SystemColors.Control;

            dgvTreatment.Show();
        }

        private void dgvTreatment_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
            dgvTreatment.Columns["treat_id"].Visible = false;
            dgvTreatment.Columns["model_id"].Visible = false;
            dgvTreatment.Columns["type"].Visible = false;

            dgvTreatment.Left = dgvMKB.Right + 6;
            dgvTreatment.Top = dgvModels.Bottom + 6;

            if (dgvMethods.DataSource == null)
            {
                if (dgvTreatment.Rows.Count == 1)
                {
                    SetMethods();
                    dgvTreatment.Rows[0].Selected = true;
                }
                else if (dgvTreatment.Rows.Count > 1)
                    dgvTreatment.ClearSelection();
            }
        }

        private void SetModels()
        {
            bsModels = new BindingSource(bsMKBs, "ModelsFiltered");
            dgvModels.DataSource = bsModels;

            dgvModels.BackgroundColor = SystemColors.Control;

            dgvModels.Show();
        }

        private void dgvModels_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
            dgvModels.Columns["model_id"].Visible = false;
            dgvModels.Columns["mkb_id"].Visible = false;
            dgvModels.Columns["Код"].Width = 45;
            if (vmp_fin == Finance.Region)
                dgvModels.Columns["Код"].Visible = false;

            dgvModels.Left = dgvMKB.Right + 6;
            dgvModels.Top = dgvMKB.Top;

            int height = dgvModels.ColumnHeadersHeight;
            foreach (DataGridViewRow row in dgvModels.Rows)
                height += row.Height;

            int count = 0;

            if(dgvModels.CurrentRow != null)
            {
                int model_id = (int)dgvModels.CurrentRow.Cells["model_id"].Value;

                DataTable dtTreatment = ds.Tables["treatment"];
                foreach(DataRow row in dtTreatment.Rows)
                    if ((int)row["model_id"] == model_id)
                        count++;
            }

            int t_height = dgvTreatment.ColumnHeadersHeight + count * 22;

            if (t_height + height > GRID_HEIGHT)
                height = GRID_HEIGHT - t_height;

            dgvModels.Height = height;

            dgvTreatment.Top = dgvModels.Bottom + 6;
            dgvTreatment.Height = t_height;

            height += t_height;

            if (height > min_height)
            {
                this.ClientSize = new Size(this.ClientSize.Width, dgvTreatment.Bottom + 6);

                if (this.HorizontalScroll.Visible)
                    this.Height += 20;

                min_height = height;
            }

            if (dgvTreatment.DataSource == null)
            {
                if (dgvModels.Rows.Count == 1)
                {
                    dgvModels.Rows[0].Selected = true;
                    SetTreatment();
                }
                else if (dgvModels.Rows.Count > 1)
                    dgvModels.ClearSelection();
            }
        }

        private void dgvTreatment_SelectionChanged(object sender, EventArgs e)
        {
            if (dgvTreatment.Focused && dgvMethods.DataSource == null)
                SetMethods();
        }

        private void SetMethods()
        {
            isMethodsJustSet = true;
            dgvMethods.DataSource = new BindingSource(bsTreatment, "MethodsFiltered");

            dgvMethods.BackgroundColor = SystemColors.Control;

            dgvMethods.Show();
        }

        private void dgvMethods_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
            dgvMethods.Left = dgvModels.Right + 6;
            dgvMethods.Columns["Код"].Width = 35;

            if (vmp_fin == Finance.Region)
                dgvMethods.Columns["Код"].Visible = false;
            dgvMethods.Columns["treat_id"].Visible = false;

            int height = dgvMethods.ColumnHeadersHeight;
            foreach (DataGridViewRow row in dgvMethods.Rows)
                height += row.Height;

            if (height > GRID_HEIGHT - 12 - btCancel.Height)
                height = GRID_HEIGHT - 12 - btCancel.Height;

            dgvMethods.Height = height;

            height += 12 + btCancel.Height;

            if (height > min_height)
            {
                this.ClientSize = new Size(this.ClientSize.Width, dgvMethods.Bottom + 12 + btCancel.Height);

                if (this.HorizontalScroll.Visible)
                    this.Height += 20;

                min_height = height;
            }

            dgvMethods.Sort(dgvMethods.Columns["Метод лечения"], ListSortDirection.Ascending);

            if (isMethodsJustSet)
            {
                if (dgvMethods.Rows.Count == 1)
                {
                    dgvMethods.Rows[0].Selected = true;
                    isMethodsJustSet = false;
                }
                else if (dgvMethods.Rows.Count > 1)
                {
                    dgvMethods.ClearSelection();
                    dgvMethods.CurrentCell = null;
                }
            }
            else
            {
                dgvMethods.Rows[0].Selected = true;
            }
        }

        private void VMPSelect_Resize(object sender, EventArgs e)
        {
            if (this.HorizontalScroll.Visible)
            {
                btCancel.Left = 1250 - btCancel.Width - 12;
                btCancel.Top = this.ClientSize.Height - btCancel.Height - 18;
            }
            else
            {
                btCancel.Left = this.ClientSize.Width - btCancel.Width - 12;                
                btCancel.Top = this.ClientSize.Height - btCancel.Height - 6;
                
            }
            btSelect.Left = btCancel.Left - btSelect.Width - 6;
            btSelect.Top = btCancel.Top;
        }

        private void dgvModels_SelectionChanged(object sender, EventArgs e)
        {
            if (dgvModels.Focused && dgvTreatment.DataSource == null)
                SetTreatment();
        }

        private void VMPSelect_FormClosed(object sender, FormClosedEventArgs e)
        {
            Owner.Show();
            Owner.BringToFront();
        }

        private void VMPSelect_Load(object sender, EventArgs e)
        {
            pnVMP.BringToFront();
            Owner.Hide();
        }

        private void dgvMethods_SelectionChanged(object sender, EventArgs e)
        {
            if(dgvMethods.Focused)
                isMethodsJustSet = false;
        }

        private void btSelect_Click(object sender, EventArgs e)
        {
            if (dgvGroups.CurrentCell != null
                && dgvDesc.CurrentRow != null
                && dgvMKBs.CurrentCell != null
                && dgvMKB.CurrentCell != null
                && dgvModels.CurrentRow != null
                && dgvTreatment.CurrentCell != null
                && dgvMethods.CurrentRow != null)
            {
                FinanceType = (int)vmp_fin;
                Group = dgvGroups.CurrentCell.Value.ToString();
                VMPCode = dgvDesc.CurrentRow.Cells["Код"].Value.ToString();
                VMPDesc = dgvDesc.CurrentRow.Cells["Наименование"].Value.ToString();
                MKB = dgvMKB.CurrentCell.Value.ToString();
                ModelCode = dgvModels.CurrentRow.Cells["Код"].Value.ToString();
                Model = dgvModels.CurrentRow.Cells["Модель пациента"].Value.ToString();
                TreatmentType = (int)dgvTreatment.CurrentCell.Value;
                MethodCode = dgvMethods.CurrentRow.Cells["Код"].Value.ToString();
                Method = dgvMethods.CurrentRow.Cells["Метод лечения"].Value.ToString();

                DialogResult = DialogResult.OK;
                this.Close();
            }
            else
                MessageBox.Show("Нужно выбрать все параметры");
        }
    }
}
