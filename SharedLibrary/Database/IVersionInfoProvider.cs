﻿using System;

namespace SharedLibrary.Database
{
    public interface IVersionInfoProvider
    {
        DatabaseResult<Version> GetAppVersion(string appName);
    }
}
