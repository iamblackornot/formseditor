#define MyAppName "HMCpro"
#define MyAppExeName "HMCproEx.exe"
#define appVersion "HMCpro 1.3.0"

[Tasks]
Name: "desktopicon"; Description: "{cm:CreateDesktopIcon}"; \
    GroupDescription: "{cm:AdditionalIcons}"; 

[Icons]
Name: "{commondesktop}\{#MyAppName}"; Filename: "{app}\{#MyAppExeName}"; Tasks: desktopicon; \
    AfterInstall: SetElevationBit('{commondesktop}\{#MyAppName}.lnk')
;[Registry]
;Root: "HKLM"; Subkey: "SOFTWARE\Microsoft\Windows NT\CurrentVersion\AppCompatFlags\Layers"; \
    ValueType: String; ValueName: "{app}\{#MyAppExeName}"; ValueData: "RUNASADMIN"; \
    Flags: uninsdeletekeyifempty uninsdeletevalue; MinVersion: 0,6.1

[Setup]
DisableReadyPage=True
AppName=HMCpro
AppId=HMCpro
AppVersion={#appVersion}
RestartIfNeededByRun=False
AllowCancelDuringInstall=False
ExtraDiskSpaceRequired=2
DefaultDirName={pf}\HMCpro
AllowRootDirectory=True
ShowLanguageDialog=no
LanguageDetectionMethod=locale
UsePreviousGroup=True
DisableProgramGroupPage=yes
AppendDefaultGroupName=False
CloseApplications=True
RestartApplications=False
CreateUninstallRegKey=yes
UninstallDisplayName=HMCpro
UninstallDisplayIcon={uninstallexe}
MergeDuplicateFiles=False
TerminalServicesAware=False
OutputDir=D:\Documents\Visual Studio 2017\Projects\FormsEditor\setup
OutputBaseFilename={#appVersion}
Compression=lzma2/fast
DefaultGroupName=HMCpro
PrivilegesRequired = admin

[InstallDelete]
Type: files; Name: {commondesktop}\HMCpro.lnk
Type: files; Name: {commondesktop}\������� �������.lnk
Type: files; Name: {app}\HMCpro.exe

[Files]
Source: "..\FormsEditorEx\bin\Release\Confused\AppSoftware.LicenceEngine.Common.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "..\FormsEditorEx\bin\Release\Confused\AppSoftware.LicenceEngine.KeyVerification.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "..\FormsEditorEx\bin\Release\Microsoft.Office.Interop.Word.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "..\FormsEditorEx\bin\Release\Newtonsoft.Json.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "..\FormsEditorEx\bin\Release\Office.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "..\FormsEditorEx\bin\Release\HMCproEx.exe"; DestDir: "{app}"; Flags: ignoreversion
Source: "..\FormsEditorEx\bin\Release\System.Data.SQLite.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "..\FormsEditorEx\bin\Release\x64\SQLite.Interop.dll"; DestDir: "{app}\x64"; Flags: ignoreversion
Source: "..\FormsEditorEx\bin\Release\x86\SQLite.Interop.dll"; DestDir: "{app}\x86"; Flags: ignoreversion
Source: "..\FormsEditorEx\bin\Release\SharedLibrary.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "..\FormsEditorEx\.env\deploy\server_location.ini"; DestDir: "{app}"; Flags: ignoreversion
Source: "redist\NDP461-KB3102436-x86-x64-AllOS-RUS.exe"; DestDir: "{app}\redist"; Flags: ignoreversion
Source: "..\FormsEditorEx\bin\Release\Dapper.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "..\FormsEditorEx\bin\Release\System.ValueTuple.dll"; DestDir: "{app}"; Flags: ignoreversion

[Code]
function IsDotNetDetected(version: string; service: cardinal): boolean;
// Indicates whether the specified version and service pack of the .NET Framework is installed.
//
// version -- Specify one of these strings for the required .NET Framework version:
//    'v1.1'          .NET Framework 1.1
//    'v2.0'          .NET Framework 2.0
//    'v3.0'          .NET Framework 3.0
//    'v3.5'          .NET Framework 3.5
//    'v4\Client'     .NET Framework 4.0 Client Profile
//    'v4\Full'       .NET Framework 4.0 Full Installation
//    'v4.5'          .NET Framework 4.5
//    'v4.5.1'        .NET Framework 4.5.1
//    'v4.5.2'        .NET Framework 4.5.2
//    'v4.6'          .NET Framework 4.6
//    'v4.6.1'        .NET Framework 4.6.1
//    'v4.6.2'        .NET Framework 4.6.2
//    'v4.7'          .NET Framework 4.7
//
// service -- Specify any non-negative integer for the required service pack level:
//    0               No service packs required
//    1, 2, etc.      Service pack 1, 2, etc. required
var
    key, versionKey: string;
    install, release, serviceCount, versionRelease: cardinal;
    success: boolean;
begin
    versionKey := version;
    versionRelease := 0;

    // .NET 1.1 and 2.0 embed release number in version key
    if version = 'v1.1' then begin
        versionKey := 'v1.1.4322';
    end else if version = 'v2.0' then begin
        versionKey := 'v2.0.50727';
    end

    // .NET 4.5 and newer install as update to .NET 4.0 Full
    else if Pos('v4.', version) = 1 then begin
        versionKey := 'v4\Full';
        case version of
          'v4.5':   versionRelease := 378389;
          'v4.5.1': versionRelease := 378675; // 378758 on Windows 8 and older
          'v4.5.2': versionRelease := 379893;
          'v4.6':   versionRelease := 393295; // 393297 on Windows 8.1 and older
          'v4.6.1': versionRelease := 394254; // 394271 before Win10 November Update
          'v4.6.2': versionRelease := 394802; // 394806 before Win10 Anniversary Update
          'v4.7':   versionRelease := 460798; // 460805 before Win10 Creators Update
        end;
    end;

    // installation key group for all .NET versions
    key := 'SOFTWARE\Microsoft\NET Framework Setup\NDP\' + versionKey;

    // .NET 3.0 uses value InstallSuccess in subkey Setup
    if Pos('v3.0', version) = 1 then begin
        success := RegQueryDWordValue(HKLM, key + '\Setup', 'InstallSuccess', install);
    end else begin
        success := RegQueryDWordValue(HKLM, key, 'Install', install);
    end;

    // .NET 4.0 and newer use value Servicing instead of SP
    if Pos('v4', version) = 1 then begin
        success := success and RegQueryDWordValue(HKLM, key, 'Servicing', serviceCount);
    end else begin
        success := success and RegQueryDWordValue(HKLM, key, 'SP', serviceCount);
    end;

    // .NET 4.5 and newer use additional value Release
    if versionRelease > 0 then begin
        success := success and RegQueryDWordValue(HKLM, key, 'Release', release);
        success := success and (release >= versionRelease);
    end;

    result := success and (install = 1) and (serviceCount >= service);
end;




procedure CurStepChanged(CurStep: TSetupStep);
var
  ResultCode: Integer;
begin
  if CurStep = ssPostInstall then
        if not IsDotNetDetected('v4.6.1', 0) then begin
        MsgBox('������ ����� �������� ��������� Microsoft .NET Framework v4.6.1', mbInformation, MB_OK);
        ExecAsOriginalUser(ExpandConstant('{app}\redist\NDP461-KB3102436-x86-x64-AllOS-RUS.exe'), '', '', SW_SHOWNORMAL, ewNoWait, ResultCode);
  end;
end;

procedure SetElevationBit(Filename: string);
var
  Buffer: string;
  Stream: TStream;
begin
  Filename := ExpandConstant(Filename);
  Log('Setting elevation bit for ' + Filename);

  Stream := TFileStream.Create(FileName, fmOpenReadWrite);
  try
    Stream.Seek(21, soFromBeginning);
    SetLength(Buffer, 1);
    Stream.ReadBuffer(Buffer, 1);
    Buffer[1] := Chr(Ord(Buffer[1]) or $20);
    Stream.Seek(-1, soFromCurrent);
    Stream.WriteBuffer(Buffer, 1);
  except
    Log('Setting elevation bit failed');
  finally
    Stream.Free;
  end;
end;

[Languages]
Name: "russian"; MessagesFile: "compiler:Languages\Russian.isl";
Name: "english"; MessagesFile: "compiler:Default.isl";