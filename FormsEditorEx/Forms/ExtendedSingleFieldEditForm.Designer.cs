﻿namespace FormsEditorEx
{
    partial class ExtendedSingleFieldEditForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.rtbInput = new SharedLibrary.PlainRichTextBox();
            this.pnContent = new System.Windows.Forms.TableLayoutPanel();
            this.pnButtons = new System.Windows.Forms.FlowLayoutPanel();
            this.btOK = new System.Windows.Forms.Button();
            this.btCancel = new System.Windows.Forms.Button();
            this.pnContent.SuspendLayout();
            this.pnButtons.SuspendLayout();
            this.SuspendLayout();
            // 
            // rtbInput
            // 
            this.rtbInput.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rtbInput.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.rtbInput.ForeColor = System.Drawing.Color.Black;
            this.rtbInput.Location = new System.Drawing.Point(12, 12);
            this.rtbInput.Margin = new System.Windows.Forms.Padding(0);
            this.rtbInput.Name = "rtbInput";
            this.rtbInput.PlainTextMode = true;
            this.rtbInput.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.Vertical;
            this.rtbInput.ShowSelectionMargin = true;
            this.rtbInput.Size = new System.Drawing.Size(603, 395);
            this.rtbInput.TabIndex = 4;
            this.rtbInput.Text = "";
            this.rtbInput.TextChanged += new System.EventHandler(this.rtbInput_TextChanged);
            // 
            // pnContent
            // 
            this.pnContent.ColumnCount = 1;
            this.pnContent.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.pnContent.Controls.Add(this.pnButtons, 0, 1);
            this.pnContent.Controls.Add(this.rtbInput, 0, 0);
            this.pnContent.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnContent.Location = new System.Drawing.Point(0, 0);
            this.pnContent.Name = "pnContent";
            this.pnContent.Padding = new System.Windows.Forms.Padding(12, 12, 12, 8);
            this.pnContent.RowCount = 1;
            this.pnContent.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.pnContent.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.pnContent.Size = new System.Drawing.Size(627, 463);
            this.pnContent.TabIndex = 5;
            // 
            // pnButtons
            // 
            this.pnButtons.AutoSize = true;
            this.pnButtons.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.pnButtons.Controls.Add(this.btOK);
            this.pnButtons.Controls.Add(this.btCancel);
            this.pnButtons.Location = new System.Drawing.Point(12, 415);
            this.pnButtons.Margin = new System.Windows.Forms.Padding(0, 8, 0, 0);
            this.pnButtons.Name = "pnButtons";
            this.pnButtons.Size = new System.Drawing.Size(250, 40);
            this.pnButtons.TabIndex = 6;
            // 
            // btOK
            // 
            this.btOK.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btOK.Location = new System.Drawing.Point(3, 4);
            this.btOK.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btOK.Name = "btOK";
            this.btOK.Size = new System.Drawing.Size(119, 32);
            this.btOK.TabIndex = 0;
            this.btOK.Text = "OK";
            this.btOK.UseVisualStyleBackColor = true;
            this.btOK.Click += new System.EventHandler(this.btOK_Click);
            // 
            // btCancel
            // 
            this.btCancel.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btCancel.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btCancel.Location = new System.Drawing.Point(128, 4);
            this.btCancel.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btCancel.Name = "btCancel";
            this.btCancel.Size = new System.Drawing.Size(119, 32);
            this.btCancel.TabIndex = 1;
            this.btCancel.Text = "Отмена";
            this.btCancel.UseVisualStyleBackColor = true;
            this.btCancel.Click += new System.EventHandler(this.btCancel_Click);
            // 
            // ExtendedSingleFieldEditForm
            // 
            this.ClientSize = new System.Drawing.Size(627, 463);
            this.ControlBox = false;
            this.Controls.Add(this.pnContent);
            this.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(250, 400);
            this.Name = "ExtendedSingleFieldEditForm";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "text";
            this.Resize += new System.EventHandler(this.ExtendedSingleFieldEditForm_Resize);
            this.pnContent.ResumeLayout(false);
            this.pnContent.PerformLayout();
            this.pnButtons.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private SharedLibrary.PlainRichTextBox rtbInput;
        private System.Windows.Forms.TableLayoutPanel pnContent;
        private System.Windows.Forms.FlowLayoutPanel pnButtons;
        private System.Windows.Forms.Button btOK;
        private System.Windows.Forms.Button btCancel;
    }
}