﻿using System.Windows.Forms;
using SharedLibrary;

namespace FormsEditorEx
{
    public class ReadOnlyState : MainFrameStateBase
    {
        public ReadOnlyState(MainFrame handler) : base(handler)
        {
            Type = MainFrameState.ReadOnly;
            Handler.PatientToReadOnlyFields();
            Handler.SetReadOnlyWorkspace(Static.ActionButtonText[Type]);
        }

        public override IMainFrameState Action(ucStatusBarControl ucStatusBar)
        {
            using (NewClinicalRecordForm frm = new NewClinicalRecordForm())
            {
                if (frm.ShowDialog() == DialogResult.OK)
                {
                    Patient p = Patient.Instance;

                    if (Patient.Assign(Handler.dbAdapter, frm.ClinicalRecordNum, Handler.Department, Handler.Doctor.Value, ucStatusBar))
                    {
                        return new AnamnesisState(Handler);
                    }
                }

                return this;
            }
        }
    }
}