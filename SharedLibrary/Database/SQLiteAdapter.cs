﻿using System;
using System.Data.SQLite;
using System.Data;
using SharedLibrary.Logger;

namespace SharedLibrary.Database
{
    public class SQLiteAdapter
    {
        protected ILogger logger;
        protected SQLiteTransaction activeTransaction;
        protected static object locker = new object();

        public readonly int ForeignKeyConstraintError = -2147473489;

        public SQLiteAdapter(ILogger logger)
        {
            this.logger = logger;
        }
        protected static string GetDatabaseConnectionString(string filePath)
        {
            return "Data Source =\\\\" + filePath + "; Version = 3; foreign keys=true;";
        }
        protected DatabaseResult<T> ExecuteDBAction<T>(Func<SQLiteConnection, DatabaseResult<T>> func, SQLiteConnection dbConnection)
        {
            lock (locker)
            {
                try
                {
                    dbConnection.Open();

                    if (dbConnection.State != ConnectionState.Open)
                        return new NoConnectionError<T>();

                    return func(dbConnection);
                }
                catch (Exception ex)
                {
                    activeTransaction?.Rollback();
                    logger.AddToLogs(ex.ToString());
                    return new DefaultDatabaseError<T>();
                }
                finally
                {
                    activeTransaction?.Dispose();
                    activeTransaction = null;
                    dbConnection.Close();
                }
            }
        }
        protected void RollbackActiveTransaction()
        {
            activeTransaction?.Rollback();
            activeTransaction?.Dispose();
            activeTransaction = null;
        }
    }
}
