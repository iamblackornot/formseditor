﻿using System;

namespace FormsEditorEx.GenExam.Records
{
    public class SARSRecord : IExamRecord
    {
        public DateTime? Date { get; set; }
        public bool Positive { get; set; }

        public string GetString()
        {
            string temp = string.Empty;

            if (Date.HasValue)
            {
                temp += "SARS-CoV-2: ";
                temp += Positive ? "положит." : "отриц.";
            }

            return temp;
        }
        public static GenExamRecord GetTestRecord()
        {
            SARSRecord rec = new SARSRecord()
            {
                Date = new DateTime(2020, 1, 26),
                Positive = false
            };
            return new GenExamRecord() { Date = rec.Date, Result = rec.GetString() };
        }
    }
}
