﻿using System;
using System.Windows.Forms;
using SharedLibrary;
using FormsEditorEx.GenExam.Records;

namespace FormsEditorEx.GenExam.Tabs
{
    public class ucDiseaseTab : ucExamTabBase
    {
        private MaskedTextBox tbDiseaseDate;
        private CheckBox cbHC;
        private CheckBox cbHB;
        private CheckBox cbHIV;
        private CheckBox cbRW;
        private Label label1;
        private Panel pnContent;

        private const int SpaceMargin = 8;

        public ucDiseaseTab()
        {
            InitializeComponent();

            tbDiseaseDate.KeyPress += DateTextBox_KeyPress;

            ResizePanel();         
        }
        public override bool AddExamRecord()
        {
            bool res = AddRecord(GenExamType.Disease, FieldsToRecord());

            if (res)
                Clear();

            return res;
        }
        public DiseaseRecord FieldsToRecord()
        {
            if (tbDiseaseDate.MaskCompleted && DateTime.TryParse(tbDiseaseDate.Text, out DateTime date))
            {
                return new DiseaseRecord()
                {
                    Date = date,
                    RW = cbRW.Checked,
                    HIV = cbHIV.Checked,
                    HBsAg = cbHB.Checked,
                    HCV = cbHC.Checked
                };
            }
            else return null;
        }

        public override void FillTestData()
        {
            tbDiseaseDate.Text = "24.01.2020";
            cbRW.Checked = true;
        }
        private void Clear()
        {
            tbDiseaseDate.Clear();
            cbRW.Checked = false;
            cbHIV.Checked = false;
            cbHB.Checked = false;
            cbHC.Checked = false;
        }
        private void ResizePanel()
        {
            pnContent.Location = ClientRectangle.Location;
            ClientSize = pnContent.Size;
        }
        private void InitializeComponent()
        {
            this.pnContent = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.tbDiseaseDate = new System.Windows.Forms.MaskedTextBox();
            this.cbHC = new System.Windows.Forms.CheckBox();
            this.cbHB = new System.Windows.Forms.CheckBox();
            this.cbHIV = new System.Windows.Forms.CheckBox();
            this.cbRW = new System.Windows.Forms.CheckBox();
            this.pnContent.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnContent
            // 
            this.pnContent.AutoSize = true;
            this.pnContent.Controls.Add(this.label1);
            this.pnContent.Controls.Add(this.tbDiseaseDate);
            this.pnContent.Controls.Add(this.cbHC);
            this.pnContent.Controls.Add(this.cbHB);
            this.pnContent.Controls.Add(this.cbHIV);
            this.pnContent.Controls.Add(this.cbRW);
            this.pnContent.Location = new System.Drawing.Point(26, 3);
            this.pnContent.Name = "pnContent";
            this.pnContent.Padding = new System.Windows.Forms.Padding(8);
            this.pnContent.Size = new System.Drawing.Size(157, 178);
            this.pnContent.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 14);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(38, 18);
            this.label1.TabIndex = 1;
            this.label1.Text = "Дата";
            // 
            // tbDiseaseDate
            // 
            this.tbDiseaseDate.Culture = new System.Globalization.CultureInfo("");
            this.tbDiseaseDate.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.tbDiseaseDate.Location = new System.Drawing.Point(56, 11);
            this.tbDiseaseDate.Mask = "00.00.0000";
            this.tbDiseaseDate.Name = "tbDiseaseDate";
            this.tbDiseaseDate.Size = new System.Drawing.Size(90, 26);
            this.tbDiseaseDate.TabIndex = 1;
            this.tbDiseaseDate.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // cbHC
            // 
            this.cbHC.AutoSize = true;
            this.cbHC.Location = new System.Drawing.Point(35, 143);
            this.cbHC.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.cbHC.Name = "cbHC";
            this.cbHC.Size = new System.Drawing.Size(53, 22);
            this.cbHC.TabIndex = 28;
            this.cbHC.Text = "HCV";
            this.cbHC.UseVisualStyleBackColor = true;
            // 
            // cbHB
            // 
            this.cbHB.AutoSize = true;
            this.cbHB.Location = new System.Drawing.Point(35, 113);
            this.cbHB.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.cbHB.Name = "cbHB";
            this.cbHB.Size = new System.Drawing.Size(66, 22);
            this.cbHB.TabIndex = 27;
            this.cbHB.Text = "HBsAg";
            this.cbHB.UseVisualStyleBackColor = true;
            // 
            // cbHIV
            // 
            this.cbHIV.AutoSize = true;
            this.cbHIV.Location = new System.Drawing.Point(35, 83);
            this.cbHIV.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.cbHIV.Name = "cbHIV";
            this.cbHIV.Size = new System.Drawing.Size(53, 22);
            this.cbHIV.TabIndex = 26;
            this.cbHIV.Text = "ВИЧ";
            this.cbHIV.UseVisualStyleBackColor = true;
            // 
            // cbRW
            // 
            this.cbRW.AutoSize = true;
            this.cbRW.Location = new System.Drawing.Point(35, 53);
            this.cbRW.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.cbRW.Name = "cbRW";
            this.cbRW.Size = new System.Drawing.Size(48, 22);
            this.cbRW.TabIndex = 25;
            this.cbRW.Text = "RW";
            this.cbRW.UseVisualStyleBackColor = true;
            // 
            // ucDiseaseTab
            // 
            this.Controls.Add(this.pnContent);
            this.Name = "ucDiseaseTab";
            this.Size = new System.Drawing.Size(724, 616);
            this.pnContent.ResumeLayout(false);
            this.pnContent.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }
    }
}
