﻿using Microsoft.Office.Interop.Word;
using System.IO;
using SharedLibrary;

namespace FormsEditorEx
{
    public interface IFillStrategy
    {
        DocType DocType { get; }
        string Fill(Application wordApp, Document doc);
    }
}
