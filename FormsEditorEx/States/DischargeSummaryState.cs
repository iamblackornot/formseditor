﻿using SharedLibrary;

namespace FormsEditorEx
{
    public class DischargeSummaryState : MainFrameStateBase
    {
        public DischargeSummaryState(MainFrame handler) : base(handler)
        {
            Type = MainFrameState.DischargeSummary;
            Handler.PatientToDischargeSummaryFields();
            Handler.SetDischargeSummaryWorkspace(Static.ActionButtonText[Type]);
        }

        public override IMainFrameState Action(ucStatusBarControl ucStatusBar)
        {
            if (Handler.CheckDischargeSummaryData())
            {
                Handler.DischargeSummaryFieldsToPatient();

                Handler.Enabled = false;

                if (Middleware.FillAndSavePatientAction(
                    Static.FillStrategies[DocType.DischargeSummary], Handler.dbAdapter, ucStatusBar, out string outputPath))
                {
                    if (!string.IsNullOrEmpty(outputPath))
                        Handler.SetLastPath(outputPath);

                    Handler.Enabled = true;

                    return new DischargeState(Handler);
                }

                Handler.Enabled = true;
            }

            return this;
        }
        public override void FillTestData()
        {
            Handler.FillDischargeSummaryTestData();
        }
        public override IMainFrameState Revert(ucStatusBarControl ucStatusBar)
        {
            if (ShowRevertPrompt())
            {
                ucStatusBar.ShowOK("Произведен откат на предыдущий этап");

                Patient p = Patient.Instance;
                p.PatientData.GenExam.Revert();

                if (Static.DoesOperativeTreatment[p.Department.Value] && !p.PatientData.TemporaryFields.IsChemTherapy.HasValue)
                {
                    p.Stage = (int)MainFrameState.PreoperativeEpicrisis;
                    return new PreoperativeEpicrisisState(Handler);
                }
                else
                {
                    p.Stage = (int)MainFrameState.Anamnesis;
                    return new AnamnesisState(Handler);
                }
            }

            return this;
        }
    }
}

