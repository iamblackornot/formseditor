﻿using Microsoft.Office.Interop.Word;
using SharedLibrary;

namespace FormsEditorEx
{
    public class TitlePageStrategy : IFillStrategy
    {
        public DocType DocType { get; } = DocType.TitlePage;
        public string Fill(Application wordApp, Document doc)
        {
            //string diag = "(С43.5, С18.0) Основной: ПМЗНМ: 1. Меланома кожи передней грудной стенки T4bN0M0 IIc стадия." +
            //    " Хирургическое лечение от 2012 года. Иммунотерапия интерфероном альфа. Прогрессирование – мтс в м/ткани" +
            //    " передней брюшной стенки, хирургическое лечение в 2014 г. Прогрессирование в 2017 г – мтс в подмышечные" +
            //    " л/у, ЛАЭ в 2017г. 2. Рак слепой кишки T4N1M1 мтс в печень 4 ст. Хирургическое лечение от 01.04.2020 в " +
            //    "объеме правосторонней гемиколэктомии с расширенной лимфаденэктомией, метастазэктомия из С5, С6 печени." +
            //    " В процессе ППХТ FOLFOX. 2 кл.гр.ыыыыыыыыыыыыыыыыыыыыыыssssssssssssssssssssssssss";

            //string cmpl = "Асцит. Билиарная гипертензия. желтуха смешанного генеза. почечно-печеночная недостаточность. хронический болевой синдром 3 ст";

            //string accmp = "Состояние после хирургического лечения гнилостного пельвиоректального парапрактита, осложненного " +
            //    "флегмоной промежности, левого бендра, брюшной стенки с остеомиелитом лонных костей таза в 2017г., хроническое " +
            //    "воспаление в гипогастральной и лонной области с наружными гнойными  свищами брюшной стенки.  Сахарный диабет, 2 " +
            //    "тип, средней степени тяжести, субкомпенсация.  Гипертоническая болезнь II ст., риск 4. ХСН IIА. Ожирение II ст. " +
            //    "Посттромботическая болезнь глубоких вен ног, ХВН III.  ЖКБ, состояние после холецистэктомии.";

            //string dtex = "22.11.2020";

            //string diag = "diagdiagdiagdiagdiagdiagdiagdiagdiagdiagdiagdiagdiagdiagsssssssssssssss";
            //string cmpl = "cmpl";
            //string accmp = "accmp";

            //WordInterop.FindAndReplace(wordApp, "%diag", diag);
            //WordInterop.FindAndReplace(wordApp, "%dtex", dtex);
            //WordInterop.FindAndReplace(wordApp, "%cmpl", cmpl);
            //WordInterop.FindAndReplace(wordApp, "%accmp", accmp);
            Patient p = Patient.Instance;
            TemporaryData td = p.PatientData.TemporaryFields;

            WordInterop.FindAndReplace(wordApp, "%diag", td.DiagnosisPre.EmptyIfNull());
            WordInterop.FindAndReplace(wordApp, "%dtex", Utility.DateToShortString(p.DateExam));
            WordInterop.FindAndReplace(wordApp, "%cmpl", td.ComplicationsPre.ValueOrDefault(FieldWithCue.Complications));
            WordInterop.FindAndReplace(wordApp, "%accmp", p.Accompanying.ValueOrDefault(FieldWithCue.Accompanying));

            WordInterop.AdjustTitlePage(doc);

            //return System.IO.Path.Combine(Paths.Settings, "title.doc");
            return System.IO.Path.Combine(Paths.Instance.GetSolutionsDirectory(p.Department.Value, p.Doctor), $"{Static.DocNames[DocType.TitlePage]}.doc");
        }
    }
}
