﻿using System;
using SharedLibrary;

namespace FormsEditorEx.Database
{
    public class VersionCheckSuccess : DatabaseResult<Version>
    {
        public VersionCheckSuccess(Version version) : base(true, StatusBarIndicator.OK, "Актуальная версия", version) { }
    }
    public class VersionCheckError : DatabaseResult<Version>
    {
        public VersionCheckError() : base(false, StatusBarIndicator.ERROR, "Не удалось получить информацию об актуальности версии приложения.", null) { }
    }
}