﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FormsEditorEx
{
    public class DocFormat
    {
        public float LineBreakSize { get; set; }
        public Dictionary<int, PageFormat> Pages { get; set; }
    }

    public class PageFormat
    {
        public int MaxTopPadding { get; set; }
        public bool isScalable { get; set; }
    }
}
