﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace SharedLibrary
{
    public class ValidateReportBuilder
    {
        public string CriticalHeader { get; set; } = "Требуется указать следующую информацию";
        List<string> critical = new List<string>();
        List<string> warnings = new List<string>();

        public ValidateReportBuilder()
        {
        }
        public void AddCriticialField(string fieldName)
        {
            critical.Add(fieldName);
        }
        public void AddWarning(string text)
        {
            warnings.Add(text);
        }
        private string GetReportString()
        {
            string text = string.Empty;

            if (critical.Count > 0)
            {
                text += $"### {CriticalHeader} ###" + Environment.NewLine;

                foreach (string item in critical)
                {
                    text += Environment.NewLine + $"- {item}";
                }
            }

            if (warnings.Count > 0)
            {
                if (critical.Count > 0)
                    text += Environment.NewLine + Environment.NewLine;

                text += "### Возможные недочеты ###" + Environment.NewLine;

                foreach (string item in warnings)
                {
                    text += Environment.NewLine + $"- {item}";
                }

                if (critical.Count == 0)
                {
                    text += Environment.NewLine + Environment.NewLine + "OK - продолжить";
                    text += Environment.NewLine + "Отмена - вернуться к редактированию";
                }
            }

            return text;
        }
        public bool ShowReportAndGetResult()
        {
            if (critical.Count > 0)
            {
                MessageBox.Show(GetReportString(), string.Empty, MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            else if (warnings.Count > 0)
            {
                DialogResult res = MessageBox.Show(GetReportString(), string.Empty, MessageBoxButtons.OKCancel, MessageBoxIcon.Warning);
                return res == DialogResult.OK ? true : false;
            }
            else
                return true;
        }
    }
}
