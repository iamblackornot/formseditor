﻿using SharedLibrary;

namespace FormsEditorEx.Database
{
    public class PatientListLoaded : DatabaseResult
    {
        public PatientListLoaded(int count) : base(true, StatusBarIndicator.OK, $"Загружено {count} {Utility.GetEntriesString(count)}") { }
    }
}
