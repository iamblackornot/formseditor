﻿using Microsoft.Office.Interop.Word;
using SharedLibrary;
using System;

namespace FormsEditorEx
{
    public class PreoperativeEpicrisisFillStrategy : IFillStrategy
    {
        public DocType DocType { get; } = DocType.PreoperativeEpicrisis;
        public string Fill(Application wordApp, Document doc)
        {
            string temp = string.Empty;
            Patient p = Patient.Instance;
            TemporaryData td = p.PatientData.TemporaryFields;
            LifeAnamnesisData lad = p.PatientData.LifeAnamnesisData;

            WordInterop.FindAndReplace(wordApp, "%clrec_num", p.ClinicalRecordNum);
            WordInterop.FindAndReplace(wordApp, "%crd_num", p.AmbuCardNum);

            WordInterop.FindAndReplace(wordApp, "%dtexam", Utility.DateToFullString(p.DateExam.Value));

            WordInterop.FindAndReplace(wordApp, "%sname", p.SecondName);
            WordInterop.FindAndReplace(wordApp, "%fname", p.FirstName);
            WordInterop.FindAndReplace(wordApp, "%tname", p.ThirdName.EmptyIfNull());

            WordInterop.FindAndReplace(wordApp, "%dbirth", Utility.DateToFullString(p.DateBirth));
            WordInterop.FindAndReplace(wordApp, "%age", Utility.DateBirthToAgeFullString(p.DateBirth));
            WordInterop.FindAndReplace(wordApp, "%tmexam", td.TimeExam.EmptyIfNull());

            WordInterop.FindAndReplace(wordApp, "%sxe", p.Sex.HasValue && p.Sex.Value == (int)Sex.Female ? "a" : string.Empty);

            WordInterop.FindAndReplace(wordApp, "%dgnz", p.Diagnosis.EmptyIfNull());
            WordInterop.FindAndReplace(wordApp, "%accmp_text", p.Accompanying.ValueOrDefault(FieldWithCue.Accompanying));
            WordInterop.FindAndReplace(wordApp, "%cmplc", p.Complications.ValueOrDefault(FieldWithCue.Complications));
            WordInterop.FindAndReplace(wordApp, "%dismr", p.DiseaseMoreInfo.EmptyIfNull());

            WordInterop.FindAndReplace(wordApp, "%anmnz", p.Anamnez.EmptyIfNull());

            Patient.Instance.PatientData.Conclusions.BuildDocString(doc, "%cncl");
            Patient.Instance.PatientData.Commissions.BuildDocString(doc, "%cmssn");

            WordInterop.FindAndReplace(wordApp, "%medalle", td.MedicalAllergy.EmptyIfNull());
            WordInterop.FindAndReplace(wordApp, "%gntcs", lad.Genetics.ValueOrDefault(FieldWithCue.Genetics));
            WordInterop.FindAndReplace(wordApp, "%pstlnss", lad.PastIllnesses.ValueOrDefault(FieldWithCue.PastIllnesses));

            WordInterop.BuildAccompMedicationPreString(doc);
            WordInterop.BuildAccompMedicationPlannedString(doc);

            WordInterop.FindAndReplace(wordApp, "%instr_exam", p.InstrExam.EmptyIfNull());
            WordInterop.FindAndReplace(wordApp, "%gen_exam", Patient.Instance.PatientData.GenExam.GetString());

            WordInterop.FindAndReplace(wordApp, "%opr_scp", td.PlannedOperationScope.EmptyIfNull());

            if (td.VTEORisk.HasValue)
                temp = Static.VTEO_Risk[td.VTEORisk.Value];
            else
                temp = string.Empty;

            WordInterop.FindAndReplace(wordApp, "%vteo_risk", temp);

            if (td.GORisk.HasValue)
                temp = Static.GO_Risk[td.GORisk.Value];
            else
                temp = string.Empty;

            WordInterop.FindAndReplace(wordApp, "%go_risk", temp);

            WordInterop.FindAndReplace(wordApp, "%vteo_prev", WordInterop.GetVTEOPreventionString());

            WordInterop.BuildBloodString(doc, p);

            if (p.Sex == (int)Sex.Male)
            {
                WordInterop.FindAndReplace(wordApp, "%ptnt_sex", string.Empty);
                WordInterop.FindAndReplace(wordApp, "%agrd", "согласен");
                WordInterop.FindAndReplace(wordApp, "%illperson_sex", "ой");
            }
            else
            {
                WordInterop.FindAndReplace(wordApp, "%ptnt_sex", "ка");
                WordInterop.FindAndReplace(wordApp, "%agrd", "согласна");
                WordInterop.FindAndReplace(wordApp, "%illperson_sex", "ая");
            }
            WordInterop.FindAndReplace(wordApp, "%statloc", !string.IsNullOrEmpty(p.StatusLocalis) ? p.StatusLocalis : Static.StatusLocalisPreCue[p.Department.Value]);

            WordInterop.FindAndReplace(wordApp, "%srgn", AppConfig.Instance.OperTeam.Surgeon ?? string.Empty);
            WordInterop.FindAndReplace(wordApp, "%asst", AppConfig.Instance.OperTeam.Assistant ?? string.Empty);
            WordInterop.FindAndReplace(wordApp, "%ansth", AppConfig.Instance.OperTeam.Anesthetist ?? string.Empty);
            WordInterop.FindAndReplace(wordApp, "%oprsis", AppConfig.Instance.OperTeam.OperSister ?? string.Empty);
            WordInterop.FindAndReplace(wordApp, "%bld_spc", AppConfig.Instance.OperTeam.BloodTransSpecialist ?? string.Empty);

            WordInterop.FindAndReplace(wordApp, "%fdprt", Static.DepartmentsFormatted[p.Department.Value]);

            WordInterop.FillDoctor(wordApp, p);
            WordInterop.FillSuperIntendent(wordApp, td);

            WordInterop.AdjustPages(doc, AppConfig.Instance.DocFormatMain);

            return Paths.Instance.GetOutputPath(DocType);
        }
    }
}
