﻿namespace FormsEditorEx.GenExam.Tabs
{
    partial class ucOAKTab
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tbOAKDate = new System.Windows.Forms.MaskedTextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.tbTrombocytes = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.tbHemoglobin = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.tbLeucocytes = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.tbErythrocytes = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.tbNeutrophils = new System.Windows.Forms.TextBox();
            this.pnContent = new System.Windows.Forms.Panel();
            this.pnContent.SuspendLayout();
            this.SuspendLayout();
            // 
            // tbOAKDate
            // 
            this.tbOAKDate.Culture = new System.Globalization.CultureInfo("");
            this.tbOAKDate.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.tbOAKDate.Location = new System.Drawing.Point(82, 12);
            this.tbOAKDate.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.tbOAKDate.Mask = "00.00.0000";
            this.tbOAKDate.Name = "tbOAKDate";
            this.tbOAKDate.Size = new System.Drawing.Size(90, 26);
            this.tbOAKDate.TabIndex = 1;
            this.tbOAKDate.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(13, 170);
            this.label4.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(88, 18);
            this.label4.TabIndex = 16;
            this.label4.Text = "Тромбоциты";
            // 
            // tbTrombocytes
            // 
            this.tbTrombocytes.Location = new System.Drawing.Point(111, 167);
            this.tbTrombocytes.Margin = new System.Windows.Forms.Padding(5);
            this.tbTrombocytes.Name = "tbTrombocytes";
            this.tbTrombocytes.Size = new System.Drawing.Size(61, 26);
            this.tbTrombocytes.TabIndex = 5;
            this.tbTrombocytes.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(18, 133);
            this.label3.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(83, 18);
            this.label3.TabIndex = 15;
            this.label3.Text = "Гемоглобин";
            // 
            // tbHemoglobin
            // 
            this.tbHemoglobin.Location = new System.Drawing.Point(111, 130);
            this.tbHemoglobin.Margin = new System.Windows.Forms.Padding(5);
            this.tbHemoglobin.Name = "tbHemoglobin";
            this.tbHemoglobin.Size = new System.Drawing.Size(61, 26);
            this.tbHemoglobin.TabIndex = 4;
            this.tbHemoglobin.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(21, 96);
            this.label2.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(80, 18);
            this.label2.TabIndex = 11;
            this.label2.Text = "Лейкоциты";
            // 
            // tbLeucocytes
            // 
            this.tbLeucocytes.Location = new System.Drawing.Point(111, 93);
            this.tbLeucocytes.Margin = new System.Windows.Forms.Padding(5);
            this.tbLeucocytes.Name = "tbLeucocytes";
            this.tbLeucocytes.Size = new System.Drawing.Size(61, 26);
            this.tbLeucocytes.TabIndex = 3;
            this.tbLeucocytes.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(15, 59);
            this.label1.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(86, 18);
            this.label1.TabIndex = 9;
            this.label1.Text = "Эритроциты";
            // 
            // tbErythrocytes
            // 
            this.tbErythrocytes.Location = new System.Drawing.Point(111, 56);
            this.tbErythrocytes.Margin = new System.Windows.Forms.Padding(5);
            this.tbErythrocytes.Name = "tbErythrocytes";
            this.tbErythrocytes.Size = new System.Drawing.Size(61, 26);
            this.tbErythrocytes.TabIndex = 2;
            this.tbErythrocytes.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(34, 15);
            this.label5.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(38, 18);
            this.label5.TabIndex = 17;
            this.label5.Text = "Дата";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(11, 206);
            this.label6.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(90, 18);
            this.label6.TabIndex = 19;
            this.label6.Text = "Нейтрофилы";
            // 
            // tbNeutrophils
            // 
            this.tbNeutrophils.Location = new System.Drawing.Point(111, 203);
            this.tbNeutrophils.Margin = new System.Windows.Forms.Padding(5);
            this.tbNeutrophils.Name = "tbNeutrophils";
            this.tbNeutrophils.Size = new System.Drawing.Size(61, 26);
            this.tbNeutrophils.TabIndex = 6;
            this.tbNeutrophils.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // pnContent
            // 
            this.pnContent.AutoSize = true;
            this.pnContent.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.pnContent.BackColor = System.Drawing.SystemColors.Control;
            this.pnContent.Controls.Add(this.label5);
            this.pnContent.Controls.Add(this.label6);
            this.pnContent.Controls.Add(this.tbErythrocytes);
            this.pnContent.Controls.Add(this.tbNeutrophils);
            this.pnContent.Controls.Add(this.label1);
            this.pnContent.Controls.Add(this.tbLeucocytes);
            this.pnContent.Controls.Add(this.tbOAKDate);
            this.pnContent.Controls.Add(this.label2);
            this.pnContent.Controls.Add(this.label4);
            this.pnContent.Controls.Add(this.tbHemoglobin);
            this.pnContent.Controls.Add(this.tbTrombocytes);
            this.pnContent.Controls.Add(this.label3);
            this.pnContent.Location = new System.Drawing.Point(72, 37);
            this.pnContent.Name = "pnContent";
            this.pnContent.Padding = new System.Windows.Forms.Padding(8);
            this.pnContent.Size = new System.Drawing.Size(185, 242);
            this.pnContent.TabIndex = 1;
            // 
            // ucOAKTab
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.pnContent);
            this.Margin = new System.Windows.Forms.Padding(0);
            this.Name = "ucOAKTab";
            this.Size = new System.Drawing.Size(698, 419);
            this.pnContent.ResumeLayout(false);
            this.pnContent.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MaskedTextBox tbOAKDate;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox tbTrombocytes;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox tbHemoglobin;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tbLeucocytes;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tbErythrocytes;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox tbNeutrophils;
        private System.Windows.Forms.Panel pnContent;
    }
}
