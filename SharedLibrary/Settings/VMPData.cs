﻿using Newtonsoft.Json.Linq;
using System;
using System.Data;
using System.IO;

namespace SharedLibrary.Settings
{
    public class VMP
    {
        public DataSet Data { get; set; }
        public VMP() 
        {
            InitVMPData();
        }
        public SimpleResult Load(string vmpFolderPath, int depId)
        {
            try
            {
                string filePath = Path.Combine(vmpFolderPath, $"vmp_{depId}");
                if (!File.Exists(filePath))
                    return new SimpleResult(false, $"Файл не найден server/cfg/vmp_cfg/vmp_{depId}");

                InitVMPData();

                string json = File.ReadAllText(filePath);

                JObject jobj = JObject.Parse(json);

                foreach (var p in jobj)
                {
                    JArray jrows = p.Value as JArray;
                    DataTable dt = Data.Tables[p.Key.ToString()];

                    foreach (JArray jcells in jrows)
                    {
                        DataRow row = dt.NewRow();
                        for (int i = 0; i < jcells.Count; i++)
                        {
                            object c = jcells[i];
                            row[i] = string.IsNullOrWhiteSpace(c.ToString()) ? DBNull.Value : c;
                        }
                        dt.Rows.Add(row);
                    }

                }

                Data.AcceptChanges();

                return new SimpleResult(true, string.Empty);
            }
            catch (Exception ex)
            {
                return new SimpleResult(false, ex.ToString());
            }
        }
        public SimpleResult Save(string vmpFolderPath, int depId)
        {
            try
            {
                Data.AcceptChanges();

                string filePath = Path.Combine(vmpFolderPath, $"vmp_{depId}");

                JObject jobj = new JObject();
                foreach (DataTable dt in Data.Tables)
                {
                    if (dt.Rows.Count > 0)
                    {
                        JArray jrows = new JArray();

                        foreach (DataRow row in dt.Rows)
                        {
                            JArray jcells = new JArray();

                            foreach (object cell in row.ItemArray)
                                jcells.Add(new JValue(cell));

                            jrows.Add(jcells);
                        }

                        jobj.Add(new JProperty(dt.TableName, jrows));
                    }
                }
                File.WriteAllText(filePath, jobj.ToString());

                DateTime today = DateTime.Today;
                string backupFolder = Path.Combine(vmpFolderPath, "backup");

                if(!Directory.Exists(backupFolder))
                {
                    Directory.CreateDirectory(backupFolder);
                }

                string backupFilePath = Path.Combine(backupFolder,
                    $"vmp_{depId}_{today.ToString("dd-MM-yyyy")}");

                if(!File.Exists(backupFilePath))
                {
                    File.WriteAllText(backupFilePath, jobj.ToString());
                }

                return new SimpleResult(true, string.Empty);
            }
            catch (Exception ex)
            {
                InitVMPData();
                return new SimpleResult(false, ex.ToString());
            }
        }
        private void InitVMPData()
        {
            Data = new DataSet();

            DataTable dtGroups = new DataTable("groups");

            DataColumn dcIndex0 = new DataColumn("group_id", typeof(Int32));
            dcIndex0.AutoIncrement = true;
            dcIndex0.Unique = true;
            dtGroups.Columns.Add(dcIndex0);
            dtGroups.Columns.Add("dep_id", typeof(Int32));
            dtGroups.Columns.Add("vmp_fin_id", typeof(Int32));
            dtGroups.Columns.Add("№", typeof(Int32));

            Data.Tables.Add(dtGroups);

            DataTable dtDesc = new DataTable("desc");

            DataColumn dcIndex1 = new DataColumn("desc_id", typeof(Int32));
            dcIndex1.AutoIncrement = true;
            dcIndex1.Unique = true;
            dtDesc.Columns.Add(dcIndex1);
            dtDesc.Columns.Add("group_id", typeof(Int32));
            dtDesc.Columns.Add("Код", typeof(string));
            dtDesc.Columns.Add("Наименование", typeof(string));

            Data.Tables.Add(dtDesc);
            Data.Relations.Add("DescFiltered", dtGroups.Columns["group_id"], dtDesc.Columns["group_id"]);

            DataTable dtMKBs = new DataTable("MKBs");

            DataColumn dcIndex2 = new DataColumn("mkb_id", typeof(Int32));
            dcIndex2.AutoIncrement = true;
            dcIndex2.Unique = true;
            dtMKBs.Columns.Add(dcIndex2);
            dtMKBs.Columns.Add("desc_id", typeof(Int32));
            dtMKBs.Columns.Add("Коды МКБ", typeof(string));

            Data.Tables.Add(dtMKBs);
            Data.Relations.Add("MKBsFiltered", dtDesc.Columns["desc_id"], dtMKBs.Columns["desc_id"]);

            DataTable dtModels = new DataTable("models");
            DataColumn dcIndex4 = new DataColumn("model_id", typeof(Int32));
            dcIndex4.AutoIncrement = true;
            dcIndex4.Unique = true;
            dtModels.Columns.Add(dcIndex4);
            dtModels.Columns.Add("mkb_id", typeof(Int32));
            dtModels.Columns.Add("Код", typeof(Int32));
            dtModels.Columns.Add("Модель пациента", typeof(string));

            Data.Tables.Add(dtModels);
            Data.Relations.Add("ModelsFiltered", dtMKBs.Columns["mkb_id"], dtModels.Columns["mkb_id"]);

            DataTable dtTreatment = new DataTable("treatment");
            DataColumn dcIndex3 = new DataColumn("treat_id", typeof(Int32));
            dcIndex3.AutoIncrement = true;
            dcIndex3.Unique = true;
            dtTreatment.Columns.Add(dcIndex3);
            dtTreatment.Columns.Add("model_id", typeof(Int32));
            dtTreatment.Columns.Add("type", typeof(Int32));

            Data.Tables.Add(dtTreatment);
            Data.Relations.Add("TreatmentFiltered", dtModels.Columns["model_id"], dtTreatment.Columns["model_id"]);

            DataTable dtMethods = new DataTable("methods");

            dtMethods.Columns.Add("treat_id", typeof(Int32));
            dtMethods.Columns.Add("Код", typeof(Int32));
            dtMethods.Columns.Add("Метод лечения", typeof(string));

            Data.Tables.Add(dtMethods);
            Data.Relations.Add("MethodsFiltered", dtTreatment.Columns["treat_id"], dtMethods.Columns["treat_id"]);

            Data.AcceptChanges();
        }
    }
}
