﻿namespace FormsEditorEx.GenExam.Tabs
{
    partial class ucBAKTab
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pnContent = new System.Windows.Forms.Panel();
            this.label13 = new System.Windows.Forms.Label();
            this.tbBAKDate = new System.Windows.Forms.MaskedTextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.tbAlkalinePhosphatase = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.tbSodium = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.tbPotassium = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.tbGlucose = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.tbAlphaAmylase = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.tbProteinTotal = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.tbBilirubinDirect = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.tbUrea = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.tbCreatinine = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.tbAST = new System.Windows.Forms.TextBox();
            this.tbBilirubinTotal = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.tbALT = new System.Windows.Forms.TextBox();
            this.pnContent.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnContent
            // 
            this.pnContent.AutoSize = true;
            this.pnContent.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.pnContent.Controls.Add(this.label13);
            this.pnContent.Controls.Add(this.tbBAKDate);
            this.pnContent.Controls.Add(this.label11);
            this.pnContent.Controls.Add(this.tbAlkalinePhosphatase);
            this.pnContent.Controls.Add(this.label12);
            this.pnContent.Controls.Add(this.tbSodium);
            this.pnContent.Controls.Add(this.label4);
            this.pnContent.Controls.Add(this.tbPotassium);
            this.pnContent.Controls.Add(this.label10);
            this.pnContent.Controls.Add(this.tbGlucose);
            this.pnContent.Controls.Add(this.label2);
            this.pnContent.Controls.Add(this.tbAlphaAmylase);
            this.pnContent.Controls.Add(this.label3);
            this.pnContent.Controls.Add(this.tbProteinTotal);
            this.pnContent.Controls.Add(this.label1);
            this.pnContent.Controls.Add(this.tbBilirubinDirect);
            this.pnContent.Controls.Add(this.label9);
            this.pnContent.Controls.Add(this.tbUrea);
            this.pnContent.Controls.Add(this.label5);
            this.pnContent.Controls.Add(this.tbCreatinine);
            this.pnContent.Controls.Add(this.label8);
            this.pnContent.Controls.Add(this.label6);
            this.pnContent.Controls.Add(this.tbAST);
            this.pnContent.Controls.Add(this.tbBilirubinTotal);
            this.pnContent.Controls.Add(this.label7);
            this.pnContent.Controls.Add(this.tbALT);
            this.pnContent.Location = new System.Drawing.Point(75, 23);
            this.pnContent.Margin = new System.Windows.Forms.Padding(0);
            this.pnContent.Name = "pnContent";
            this.pnContent.Padding = new System.Windows.Forms.Padding(8);
            this.pnContent.Size = new System.Drawing.Size(364, 278);
            this.pnContent.TabIndex = 0;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(53, 14);
            this.label13.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(38, 18);
            this.label13.TabIndex = 37;
            this.label13.Text = "Дата";
            // 
            // tbBAKDate
            // 
            this.tbBAKDate.Culture = new System.Globalization.CultureInfo("");
            this.tbBAKDate.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.tbBAKDate.Location = new System.Drawing.Point(98, 11);
            this.tbBAKDate.Mask = "00.00.0000";
            this.tbBAKDate.Name = "tbBAKDate";
            this.tbBAKDate.Size = new System.Drawing.Size(90, 26);
            this.tbBAKDate.TabIndex = 1;
            this.tbBAKDate.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(12, 205);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(109, 18);
            this.label11.TabIndex = 33;
            this.label11.Text = "Щел. фосфатаза";
            // 
            // tbAlkalinePhosphatase
            // 
            this.tbAlkalinePhosphatase.Location = new System.Drawing.Point(127, 202);
            this.tbAlkalinePhosphatase.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.tbAlkalinePhosphatase.Name = "tbAlkalinePhosphatase";
            this.tbAlkalinePhosphatase.Size = new System.Drawing.Size(61, 26);
            this.tbAlkalinePhosphatase.TabIndex = 6;
            this.tbAlkalinePhosphatase.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(232, 131);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(54, 18);
            this.label12.TabIndex = 32;
            this.label12.Text = "Натрий";
            // 
            // tbSodium
            // 
            this.tbSodium.Location = new System.Drawing.Point(292, 128);
            this.tbSodium.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.tbSodium.Name = "tbSodium";
            this.tbSodium.Size = new System.Drawing.Size(61, 26);
            this.tbSodium.TabIndex = 10;
            this.tbSodium.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(239, 94);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(47, 18);
            this.label4.TabIndex = 29;
            this.label4.Text = "Калий";
            // 
            // tbPotassium
            // 
            this.tbPotassium.Location = new System.Drawing.Point(292, 91);
            this.tbPotassium.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.tbPotassium.Name = "tbPotassium";
            this.tbPotassium.Size = new System.Drawing.Size(61, 26);
            this.tbPotassium.TabIndex = 9;
            this.tbPotassium.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(226, 57);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(60, 18);
            this.label10.TabIndex = 28;
            this.label10.Text = "Глюкоза";
            // 
            // tbGlucose
            // 
            this.tbGlucose.Location = new System.Drawing.Point(292, 54);
            this.tbGlucose.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.tbGlucose.Name = "tbGlucose";
            this.tbGlucose.Size = new System.Drawing.Size(61, 26);
            this.tbGlucose.TabIndex = 8;
            this.tbGlucose.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(15, 242);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(106, 18);
            this.label2.TabIndex = 26;
            this.label2.Text = "Альфа-амилаза";
            // 
            // tbAlphaAmylase
            // 
            this.tbAlphaAmylase.Location = new System.Drawing.Point(127, 239);
            this.tbAlphaAmylase.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.tbAlphaAmylase.Name = "tbAlphaAmylase";
            this.tbAlphaAmylase.Size = new System.Drawing.Size(61, 26);
            this.tbAlphaAmylase.TabIndex = 7;
            this.tbAlphaAmylase.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(203, 168);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(83, 18);
            this.label3.TabIndex = 24;
            this.label3.Text = "Общ. белок";
            // 
            // tbProteinTotal
            // 
            this.tbProteinTotal.Location = new System.Drawing.Point(292, 165);
            this.tbProteinTotal.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.tbProteinTotal.Name = "tbProteinTotal";
            this.tbProteinTotal.Size = new System.Drawing.Size(61, 26);
            this.tbProteinTotal.TabIndex = 11;
            this.tbProteinTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(19, 168);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(102, 18);
            this.label1.TabIndex = 22;
            this.label1.Text = "Билирубин пр.";
            // 
            // tbBilirubinDirect
            // 
            this.tbBilirubinDirect.Location = new System.Drawing.Point(127, 165);
            this.tbBilirubinDirect.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.tbBilirubinDirect.Name = "tbBilirubinDirect";
            this.tbBilirubinDirect.Size = new System.Drawing.Size(61, 26);
            this.tbBilirubinDirect.TabIndex = 5;
            this.tbBilirubinDirect.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(213, 242);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(73, 18);
            this.label9.TabIndex = 18;
            this.label9.Text = "Мочевина";
            // 
            // tbUrea
            // 
            this.tbUrea.Location = new System.Drawing.Point(292, 239);
            this.tbUrea.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.tbUrea.Name = "tbUrea";
            this.tbUrea.Size = new System.Drawing.Size(61, 26);
            this.tbUrea.TabIndex = 13;
            this.tbUrea.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(209, 205);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(77, 18);
            this.label5.TabIndex = 15;
            this.label5.Text = "Креатинин";
            // 
            // tbCreatinine
            // 
            this.tbCreatinine.Location = new System.Drawing.Point(292, 202);
            this.tbCreatinine.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.tbCreatinine.Name = "tbCreatinine";
            this.tbCreatinine.Size = new System.Drawing.Size(61, 26);
            this.tbCreatinine.TabIndex = 12;
            this.tbCreatinine.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(89, 94);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(32, 18);
            this.label8.TabIndex = 12;
            this.label8.Text = "АСТ";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(8, 131);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(113, 18);
            this.label6.TabIndex = 14;
            this.label6.Text = "Билирубин общ.";
            // 
            // tbAST
            // 
            this.tbAST.Location = new System.Drawing.Point(127, 91);
            this.tbAST.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.tbAST.Name = "tbAST";
            this.tbAST.Size = new System.Drawing.Size(61, 26);
            this.tbAST.TabIndex = 3;
            this.tbAST.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tbBilirubinTotal
            // 
            this.tbBilirubinTotal.Location = new System.Drawing.Point(127, 128);
            this.tbBilirubinTotal.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.tbBilirubinTotal.Name = "tbBilirubinTotal";
            this.tbBilirubinTotal.Size = new System.Drawing.Size(61, 26);
            this.tbBilirubinTotal.TabIndex = 4;
            this.tbBilirubinTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(88, 57);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(33, 18);
            this.label7.TabIndex = 13;
            this.label7.Text = "АЛТ";
            // 
            // tbALT
            // 
            this.tbALT.Location = new System.Drawing.Point(127, 54);
            this.tbALT.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.tbALT.Name = "tbALT";
            this.tbALT.Size = new System.Drawing.Size(61, 26);
            this.tbALT.TabIndex = 2;
            this.tbALT.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // ucBAKTab
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.pnContent);
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "ucBAKTab";
            this.Size = new System.Drawing.Size(608, 544);
            this.pnContent.ResumeLayout(false);
            this.pnContent.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel pnContent;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox tbUrea;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox tbCreatinine;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox tbAST;
        private System.Windows.Forms.TextBox tbBilirubinTotal;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox tbALT;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tbAlphaAmylase;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox tbProteinTotal;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tbBilirubinDirect;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox tbAlkalinePhosphatase;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox tbSodium;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox tbPotassium;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox tbGlucose;
        private System.Windows.Forms.MaskedTextBox tbBAKDate;
        private System.Windows.Forms.Label label13;
    }
}
