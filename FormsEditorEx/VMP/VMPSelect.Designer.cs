﻿namespace FormsEditorEx.VMP
{
    partial class VMPSelect
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle15 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(VMPSelect));
            this.pnVMP = new System.Windows.Forms.Panel();
            this.btRegion = new System.Windows.Forms.Button();
            this.btOMS = new System.Windows.Forms.Button();
            this.lbFinance = new System.Windows.Forms.Label();
            this.dgvMethods = new System.Windows.Forms.DataGridView();
            this.dgvModels = new System.Windows.Forms.DataGridView();
            this.dgvTreatment = new System.Windows.Forms.DataGridView();
            this.dgvMKBs = new System.Windows.Forms.DataGridView();
            this.dgvDesc = new System.Windows.Forms.DataGridView();
            this.dgvGroups = new System.Windows.Forms.DataGridView();
            this.btCancel = new System.Windows.Forms.Button();
            this.btSelect = new System.Windows.Forms.Button();
            this.dgvMKB = new System.Windows.Forms.DataGridView();
            this.pnVMP.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvMethods)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvModels)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvTreatment)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvMKBs)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDesc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvGroups)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvMKB)).BeginInit();
            this.SuspendLayout();
            // 
            // pnVMP
            // 
            this.pnVMP.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.pnVMP.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnVMP.Controls.Add(this.btRegion);
            this.pnVMP.Controls.Add(this.btOMS);
            this.pnVMP.Location = new System.Drawing.Point(602, 9);
            this.pnVMP.Name = "pnVMP";
            this.pnVMP.Size = new System.Drawing.Size(254, 75);
            this.pnVMP.TabIndex = 0;
            // 
            // btRegion
            // 
            this.btRegion.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btRegion.Location = new System.Drawing.Point(132, 16);
            this.btRegion.Name = "btRegion";
            this.btRegion.Size = new System.Drawing.Size(100, 40);
            this.btRegion.TabIndex = 1;
            this.btRegion.TabStop = false;
            this.btRegion.Text = "РЕГИОН";
            this.btRegion.UseVisualStyleBackColor = true;
            this.btRegion.Click += new System.EventHandler(this.btRegion_Click);
            // 
            // btOMS
            // 
            this.btOMS.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btOMS.Location = new System.Drawing.Point(16, 16);
            this.btOMS.Name = "btOMS";
            this.btOMS.Size = new System.Drawing.Size(100, 40);
            this.btOMS.TabIndex = 0;
            this.btOMS.TabStop = false;
            this.btOMS.Text = "ОМС";
            this.btOMS.UseVisualStyleBackColor = true;
            this.btOMS.Click += new System.EventHandler(this.btOMS_Click);
            // 
            // lbFinance
            // 
            this.lbFinance.AutoSize = true;
            this.lbFinance.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lbFinance.Location = new System.Drawing.Point(460, 12);
            this.lbFinance.Name = "lbFinance";
            this.lbFinance.Size = new System.Drawing.Size(118, 16);
            this.lbFinance.TabIndex = 1;
            this.lbFinance.Text = "Финансирование: ";
            this.lbFinance.Visible = false;
            // 
            // dgvMethods
            // 
            this.dgvMethods.AllowUserToAddRows = false;
            this.dgvMethods.AllowUserToDeleteRows = false;
            this.dgvMethods.AllowUserToResizeColumns = false;
            this.dgvMethods.AllowUserToResizeRows = false;
            this.dgvMethods.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvMethods.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dgvMethods.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgvMethods.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvMethods.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvMethods.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvMethods.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgvMethods.Location = new System.Drawing.Point(871, 37);
            this.dgvMethods.MultiSelect = false;
            this.dgvMethods.Name = "dgvMethods";
            this.dgvMethods.ReadOnly = true;
            this.dgvMethods.RowHeadersVisible = false;
            this.dgvMethods.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvMethods.Size = new System.Drawing.Size(295, 430);
            this.dgvMethods.TabIndex = 27;
            this.dgvMethods.TabStop = false;
            this.dgvMethods.Visible = false;
            this.dgvMethods.DataBindingComplete += new System.Windows.Forms.DataGridViewBindingCompleteEventHandler(this.dgvMethods_DataBindingComplete);
            this.dgvMethods.SelectionChanged += new System.EventHandler(this.dgvMethods_SelectionChanged);
            // 
            // dgvModels
            // 
            this.dgvModels.AllowUserToAddRows = false;
            this.dgvModels.AllowUserToDeleteRows = false;
            this.dgvModels.AllowUserToResizeColumns = false;
            this.dgvModels.AllowUserToResizeRows = false;
            this.dgvModels.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvModels.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dgvModels.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgvModels.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvModels.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dgvModels.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvModels.DefaultCellStyle = dataGridViewCellStyle4;
            this.dgvModels.Location = new System.Drawing.Point(585, 133);
            this.dgvModels.MultiSelect = false;
            this.dgvModels.Name = "dgvModels";
            this.dgvModels.ReadOnly = true;
            this.dgvModels.RowHeadersVisible = false;
            this.dgvModels.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvModels.Size = new System.Drawing.Size(280, 334);
            this.dgvModels.TabIndex = 26;
            this.dgvModels.TabStop = false;
            this.dgvModels.Visible = false;
            this.dgvModels.DataBindingComplete += new System.Windows.Forms.DataGridViewBindingCompleteEventHandler(this.dgvModels_DataBindingComplete);
            this.dgvModels.SelectionChanged += new System.EventHandler(this.dgvModels_SelectionChanged);
            // 
            // dgvTreatment
            // 
            this.dgvTreatment.AllowUserToAddRows = false;
            this.dgvTreatment.AllowUserToDeleteRows = false;
            this.dgvTreatment.AllowUserToResizeColumns = false;
            this.dgvTreatment.AllowUserToResizeRows = false;
            this.dgvTreatment.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvTreatment.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dgvTreatment.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgvTreatment.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvTreatment.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle5;
            this.dgvTreatment.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvTreatment.DefaultCellStyle = dataGridViewCellStyle6;
            this.dgvTreatment.Location = new System.Drawing.Point(585, 37);
            this.dgvTreatment.MultiSelect = false;
            this.dgvTreatment.Name = "dgvTreatment";
            this.dgvTreatment.ReadOnly = true;
            this.dgvTreatment.RowHeadersVisible = false;
            this.dgvTreatment.Size = new System.Drawing.Size(280, 90);
            this.dgvTreatment.TabIndex = 25;
            this.dgvTreatment.TabStop = false;
            this.dgvTreatment.Visible = false;
            this.dgvTreatment.DataBindingComplete += new System.Windows.Forms.DataGridViewBindingCompleteEventHandler(this.dgvTreatment_DataBindingComplete);
            this.dgvTreatment.SelectionChanged += new System.EventHandler(this.dgvTreatment_SelectionChanged);
            // 
            // dgvMKBs
            // 
            this.dgvMKBs.AllowUserToAddRows = false;
            this.dgvMKBs.AllowUserToDeleteRows = false;
            this.dgvMKBs.AllowUserToResizeColumns = false;
            this.dgvMKBs.AllowUserToResizeRows = false;
            this.dgvMKBs.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvMKBs.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dgvMKBs.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgvMKBs.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle7.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle7.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvMKBs.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle7;
            this.dgvMKBs.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle8.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle8.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle8.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvMKBs.DefaultCellStyle = dataGridViewCellStyle8;
            this.dgvMKBs.Location = new System.Drawing.Point(447, 37);
            this.dgvMKBs.MultiSelect = false;
            this.dgvMKBs.Name = "dgvMKBs";
            this.dgvMKBs.ReadOnly = true;
            this.dgvMKBs.RowHeadersVisible = false;
            this.dgvMKBs.Size = new System.Drawing.Size(146, 430);
            this.dgvMKBs.TabIndex = 24;
            this.dgvMKBs.TabStop = false;
            this.dgvMKBs.Visible = false;
            this.dgvMKBs.DataBindingComplete += new System.Windows.Forms.DataGridViewBindingCompleteEventHandler(this.dgvMKBs_DataBindingComplete);
            this.dgvMKBs.SelectionChanged += new System.EventHandler(this.dgvMKBs_SelectionChanged);
            // 
            // dgvDesc
            // 
            this.dgvDesc.AllowUserToAddRows = false;
            this.dgvDesc.AllowUserToDeleteRows = false;
            this.dgvDesc.AllowUserToResizeColumns = false;
            this.dgvDesc.AllowUserToResizeRows = false;
            this.dgvDesc.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvDesc.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dgvDesc.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgvDesc.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle9.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle9.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle9.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle9.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvDesc.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle9;
            this.dgvDesc.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle10.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle10.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle10.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle10.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle10.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle10.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvDesc.DefaultCellStyle = dataGridViewCellStyle10;
            this.dgvDesc.Location = new System.Drawing.Point(86, 37);
            this.dgvDesc.MultiSelect = false;
            this.dgvDesc.Name = "dgvDesc";
            this.dgvDesc.ReadOnly = true;
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle11.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle11.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle11.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle11.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle11.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvDesc.RowHeadersDefaultCellStyle = dataGridViewCellStyle11;
            this.dgvDesc.RowHeadersVisible = false;
            this.dgvDesc.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvDesc.ShowCellToolTips = false;
            this.dgvDesc.Size = new System.Drawing.Size(355, 430);
            this.dgvDesc.TabIndex = 23;
            this.dgvDesc.TabStop = false;
            this.dgvDesc.Visible = false;
            this.dgvDesc.DataBindingComplete += new System.Windows.Forms.DataGridViewBindingCompleteEventHandler(this.dgvDesc_DataBindingComplete);
            this.dgvDesc.SelectionChanged += new System.EventHandler(this.dgvDesc_SelectionChanged);
            // 
            // dgvGroups
            // 
            this.dgvGroups.AllowUserToAddRows = false;
            this.dgvGroups.AllowUserToDeleteRows = false;
            this.dgvGroups.AllowUserToResizeColumns = false;
            this.dgvGroups.AllowUserToResizeRows = false;
            this.dgvGroups.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvGroups.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgvGroups.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle12.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle12.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle12.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle12.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle12.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvGroups.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle12;
            this.dgvGroups.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle13.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle13.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle13.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle13.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle13.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle13.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvGroups.DefaultCellStyle = dataGridViewCellStyle13;
            this.dgvGroups.Location = new System.Drawing.Point(12, 37);
            this.dgvGroups.MultiSelect = false;
            this.dgvGroups.Name = "dgvGroups";
            this.dgvGroups.ReadOnly = true;
            this.dgvGroups.RowHeadersVisible = false;
            this.dgvGroups.Size = new System.Drawing.Size(68, 430);
            this.dgvGroups.TabIndex = 22;
            this.dgvGroups.TabStop = false;
            this.dgvGroups.Visible = false;
            this.dgvGroups.DataBindingComplete += new System.Windows.Forms.DataGridViewBindingCompleteEventHandler(this.dgvGroups_DataBindingComplete);
            this.dgvGroups.SelectionChanged += new System.EventHandler(this.dgvGroups_SelectionChanged);
            // 
            // btCancel
            // 
            this.btCancel.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btCancel.Location = new System.Drawing.Point(924, 473);
            this.btCancel.Name = "btCancel";
            this.btCancel.Size = new System.Drawing.Size(75, 32);
            this.btCancel.TabIndex = 28;
            this.btCancel.TabStop = false;
            this.btCancel.Text = "Отмена";
            this.btCancel.UseVisualStyleBackColor = true;
            this.btCancel.Visible = false;
            this.btCancel.Click += new System.EventHandler(this.btCancel_Click);
            // 
            // btSelect
            // 
            this.btSelect.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btSelect.Location = new System.Drawing.Point(843, 473);
            this.btSelect.Name = "btSelect";
            this.btSelect.Size = new System.Drawing.Size(75, 32);
            this.btSelect.TabIndex = 29;
            this.btSelect.TabStop = false;
            this.btSelect.Text = "Выбрать";
            this.btSelect.UseVisualStyleBackColor = true;
            this.btSelect.Visible = false;
            this.btSelect.Click += new System.EventHandler(this.btSelect_Click);
            // 
            // dgvMKB
            // 
            this.dgvMKB.AllowUserToAddRows = false;
            this.dgvMKB.AllowUserToDeleteRows = false;
            this.dgvMKB.AllowUserToResizeColumns = false;
            this.dgvMKB.AllowUserToResizeRows = false;
            this.dgvMKB.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvMKB.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgvMKB.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle14.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle14.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle14.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle14.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle14.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvMKB.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle14;
            this.dgvMKB.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle15.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle15.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle15.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle15.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle15.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle15.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle15.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvMKB.DefaultCellStyle = dataGridViewCellStyle15;
            this.dgvMKB.Location = new System.Drawing.Point(1160, 37);
            this.dgvMKB.MultiSelect = false;
            this.dgvMKB.Name = "dgvMKB";
            this.dgvMKB.ReadOnly = true;
            this.dgvMKB.RowHeadersVisible = false;
            this.dgvMKB.Size = new System.Drawing.Size(78, 430);
            this.dgvMKB.TabIndex = 30;
            this.dgvMKB.TabStop = false;
            this.dgvMKB.Visible = false;
            this.dgvMKB.DataBindingComplete += new System.Windows.Forms.DataGridViewBindingCompleteEventHandler(this.dgvMKB_DataBindingComplete);
            // 
            // VMPSelect
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.AutoSize = true;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(1250, 600);
            this.ControlBox = false;
            this.Controls.Add(this.dgvMKB);
            this.Controls.Add(this.btSelect);
            this.Controls.Add(this.btCancel);
            this.Controls.Add(this.dgvMethods);
            this.Controls.Add(this.dgvModels);
            this.Controls.Add(this.dgvTreatment);
            this.Controls.Add(this.dgvMKBs);
            this.Controls.Add(this.dgvDesc);
            this.Controls.Add(this.dgvGroups);
            this.Controls.Add(this.lbFinance);
            this.Controls.Add(this.pnVMP);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "VMPSelect";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.Text = "Выбор ВМП";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.VMPSelect_FormClosed);
            this.Load += new System.EventHandler(this.VMPSelect_Load);
            this.Resize += new System.EventHandler(this.VMPSelect_Resize);
            this.pnVMP.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvMethods)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvModels)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvTreatment)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvMKBs)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDesc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvGroups)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvMKB)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel pnVMP;
        private System.Windows.Forms.Button btRegion;
        private System.Windows.Forms.Button btOMS;
        private System.Windows.Forms.Label lbFinance;
        private System.Windows.Forms.DataGridView dgvMethods;
        private System.Windows.Forms.DataGridView dgvModels;
        private System.Windows.Forms.DataGridView dgvTreatment;
        private System.Windows.Forms.DataGridView dgvMKBs;
        private System.Windows.Forms.DataGridView dgvDesc;
        private System.Windows.Forms.DataGridView dgvGroups;
        private System.Windows.Forms.Button btCancel;
        private System.Windows.Forms.Button btSelect;
        private System.Windows.Forms.DataGridView dgvMKB;
    }
}