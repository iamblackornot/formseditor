﻿using System;
using FormsEditorEx.GenExam.Records;
using System.Linq;
using System.Text;

namespace FormsEditorEx.GenExam.Tabs
{
    public interface IExamTab
    {
        bool AddExamRecord();
        void Activate();
        void Deactivate();
        void FillTestData();
        void RecordToFields();
    }
}
