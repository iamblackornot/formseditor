﻿using System;
using System.Drawing;
using System.Windows.Forms;
using Microsoft.Win32;

namespace FormsEditorEx.VMP
{
    public partial class AdminForm : Form
    {
        public AdminForm()
        {
            InitializeComponent();

            pnPass.Location = pnSettings.Location = new Point(0, 0);
            this.ClientSize = pnPass.Size;
            CenterToScreen();
        }

        private void tbPass_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.KeyCode == Keys.Enter)
            {
                if (tbPass.Text == "foxfrog93")
                {
                    pnPass.Visible = false;
                    pnSettings.Visible = true;
                }
                else
                    this.Close();
            }
        }

        private void btBlack_Click(object sender, EventArgs e)
        {
            RegistryKey key;

            using (key = Registry.LocalMachine.OpenSubKey("SOFTWARE\\frmsdtr\\", true))
            {
                if (key != null)
                {
                    Object o = key.GetValue("version");
                    if (o != null)
                    {
                        double date = Convert.ToDouble(o);
                        date++;
                        key.SetValue("version", date, RegistryValueKind.DWord);
                    }
                }
            }
            this.Close();
        }
    }
}
