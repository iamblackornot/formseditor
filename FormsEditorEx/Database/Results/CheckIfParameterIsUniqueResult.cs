﻿using SharedLibrary;

namespace FormsEditorEx.Database
{
    public class ParameterIsUnique : DatabaseResult
    {
        public ParameterIsUnique() : base(true, StatusBarIndicator.OK, string.Empty) { }
    }
    public class ParameterIsNotUnique : DatabaseResult
    {
        public ParameterIsNotUnique() : base(false, StatusBarIndicator.ERROR, string.Empty) { }
    }
}

