﻿using System;
using FormsEditorEx.PatientModel;
using Newtonsoft.Json;
using SharedLibrary;
using FormsEditorEx.Database;

namespace FormsEditorEx
{
    public class Patient
    {
        public static event EventHandler PatientAssigned;
        public static event EventHandler PatientDischarged;
        public static Patient Instance { get; private set; } = new Patient();
        public PatientData PatientData { get; } = new PatientData();


        private Patient() { }
        public static bool Load(string id, DatabaseAdapter dbAdapter, ucStatusBarControl ucStatusBar = null)
        {
            bool res = Middleware.HandleDBAction(() => dbAdapter.LoadPatient(id), ucStatusBar);

            if(res)
            {
                DeserializeData();
            }

            return res;
        }
        public static bool Save(DatabaseAdapter dbAdapter, ucStatusBarControl ucStatusBar = null)
        {
            SerializeData();

            return Middleware.HandleDBAction(() => dbAdapter.SavePatient(), ucStatusBar);
        }
        private static void SerializeData()
        {
            Patient p = Instance;

            p.GenExam = p.PatientData.GenExam.Serialize();
            p.Conclusions = p.PatientData.Conclusions.Serialize();
            p.Commissions = p.PatientData.Commissions.Serialize();
            p.PersonalInfo = p.PatientData.PersonalInfo.Serialize();
            p.TemporaryFields = p.PatientData.TemporaryFields.Serialize();
            p.LifeAnamnesisData = p.PatientData.LifeAnamnesisData.Serialize();
        }
        private static void DeserializeData()
        {
            Patient p = Instance;

            p.PatientData.GenExam.Deserialize(p.GenExam);
            p.PatientData.Conclusions.Deserialize(p.Conclusions);
            p.PatientData.Commissions.Deserialize(p.Commissions);

            if (!string.IsNullOrEmpty(p.PersonalInfo))
                p.PatientData.PersonalInfo = JsonConvert.DeserializeObject<PersonalInfo>(Cryptography.DecryptString(p.PersonalInfo, Cryptography.ENC));
            else
                p.PatientData.PersonalInfo = new PersonalInfo();

            if (!string.IsNullOrEmpty(p.TemporaryFields))
                p.PatientData.TemporaryFields = JsonConvert.DeserializeObject<TemporaryData>(p.TemporaryFields);
            else
                p.PatientData.TemporaryFields = new TemporaryData();

            if (!string.IsNullOrEmpty(p.LifeAnamnesisData))
                p.PatientData.LifeAnamnesisData = JsonConvert.DeserializeObject<LifeAnamnesisData>(p.LifeAnamnesisData);
            else
                p.PatientData.LifeAnamnesisData = new LifeAnamnesisData();
        }
        public static void Reset()
        {
            Instance = new Patient();
        }
        public static bool Assign(DatabaseAdapter dbAdapter, string ClinicalRecordNum, int dep_id, long doc, ucStatusBarControl ucStatusBar = null)
        {
            Patient p = Instance;

            p.ClinicalRecordNum = ClinicalRecordNum;
            p.DateExam = null;
            p.DateDischarge = null;
            p.Department = dep_id;
            p.Doctor = doc;
            p.Stage = (int)MainFrameState.Anamnesis;

            bool res =  Middleware.HandleDBAction(() => dbAdapter.AssignPatient(p.AmbuCardNum,
                        ClinicalRecordNum, dep_id, doc), ucStatusBar);

            if (res)
                PatientAssigned?.Invoke(Instance, EventArgs.Empty);

            return res;
        }
        public static bool Discharge(DatabaseAdapter dbAdapter, ucStatusBarControl ucStatusBar = null)
        {
            Patient p = Instance;

            if (!string.IsNullOrEmpty(p.Treatment))
            {
                p.Anamnez += Environment.NewLine + "    " + p.Treatment;
            }

            if (!string.IsNullOrEmpty(p.Medication))
            {
                p.Anamnez += Environment.NewLine + "    " + p.Medication;
            }

            p.ClinicalRecordNum = null;
            p.Treatment = null;
            p.Medication = null;
            p.Department = null;
            p.Doctor = null;

            p.Stage = null;

            p.PatientData.ResetPost();

            bool res = Save(dbAdapter, ucStatusBar);

            if (res)
                PatientDischarged?.Invoke(Instance, EventArgs.Empty);

            return res;
        }
        public string ClinicalRecordNum { get; set; }
        public string AmbuCardNum { get; set; }

        public string SecondName { get; set; }
        public string FirstName { get; set; }
        public string ThirdName { get; set; }
        public int? Sex { get; set; }
        public DateTime? DateBirth { get; set; }

        public DateTime? DateExam { get; set; }

        public int? BloodGroup { get; set; }
        public int? Rh { get; set; }

        public string Complaints { get; set; }

        public string Anamnez { get; set; }
        public string StatusLocalis { get; set; }

        public string Diagnosis { get; set; }
        public string Complications { get; set; }
        public string Accompanying { get; set; }

        public string MKBDiagnosis { get; set; }
        public string MKBComplications { get; set; }
        public string MKBAccompanying { get; set; }

        public string DiseaseMoreInfo { get; set; }
        public string AccompMedication { get; set; }

        public string InstrExam { get; set; }
        public string GenExam { get; set; }
        public string Conclusions { get; set; }
        public string Commissions { get; set; }

        public string NeuroStatus { get; set; }
        public string Medication { get; set; }
        public string Treatment { get; set; }
        public DateTime? DateDischarge { get; set; }

        public string PersonalInfo { get; set; }
        public string TemporaryFields { get; set; }
        public string LifeAnamnesisData { get; set; }

        public int? Stage { get; set; }
        public int? Department { get; set; }
        public long? Doctor { get; set; }

    }
}

