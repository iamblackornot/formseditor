﻿using SharedLibrary;

namespace FormsEditorEx
{
    partial class PatientsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PatientsForm));
            this.pnFilters = new System.Windows.Forms.Panel();
            this.btLast100 = new System.Windows.Forms.Button();
            this.gbSearch = new System.Windows.Forms.GroupBox();
            this.tbThirdName = new SharedLibrary.TextBoxEx();
            this.tbFirstName = new SharedLibrary.TextBoxEx();
            this.tbSecondName = new SharedLibrary.TextBoxEx();
            this.tbResetFilters = new System.Windows.Forms.Button();
            this.tbAmbuCardNum = new SharedLibrary.TextBoxEx();
            this.btSearch = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.dpEnd = new System.Windows.Forms.DateTimePicker();
            this.dpStart = new System.Windows.Forms.DateTimePicker();
            this.btRefresh = new System.Windows.Forms.Button();
            this.pnBottomBar = new System.Windows.Forms.Panel();
            this.pnButtons = new System.Windows.Forms.Panel();
            this.btEdit = new System.Windows.Forms.Button();
            this.btBack = new System.Windows.Forms.Button();
            this.btTest1 = new System.Windows.Forms.Button();
            this.btLoadEntry = new System.Windows.Forms.Button();
            this.btNewPatient = new System.Windows.Forms.Button();
            this.btDelete = new System.Windows.Forms.Button();
            this.dgvPatients = new System.Windows.Forms.DataGridView();
            this.cmsMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.miLoad = new System.Windows.Forms.ToolStripMenuItem();
            this.miEdit = new System.Windows.Forms.ToolStripMenuItem();
            this.miDelete = new System.Windows.Forms.ToolStripMenuItem();
            this.pnTop = new System.Windows.Forms.TableLayoutPanel();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.label27 = new System.Windows.Forms.Label();
            this.cbDepartment = new SharedLibrary.ComboBoxEx();
            this.btAbout = new System.Windows.Forms.Button();
            this.btChangelog = new System.Windows.Forms.Button();
            this.btGuide = new System.Windows.Forms.Button();
            this.btDiagnostics = new System.Windows.Forms.Button();
            this.flowLayoutPanel2 = new System.Windows.Forms.FlowLayoutPanel();
            this.label3 = new System.Windows.Forms.Label();
            this.cbDoctors = new SharedLibrary.ComboBoxEx();
            this.ucStatusBar = new SharedLibrary.ucStatusBarControl();
            this.pnFilters.SuspendLayout();
            this.gbSearch.SuspendLayout();
            this.pnBottomBar.SuspendLayout();
            this.pnButtons.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPatients)).BeginInit();
            this.cmsMenu.SuspendLayout();
            this.pnTop.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            this.flowLayoutPanel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnFilters
            // 
            this.pnFilters.BackColor = System.Drawing.SystemColors.Control;
            this.pnFilters.Controls.Add(this.btLast100);
            this.pnFilters.Controls.Add(this.gbSearch);
            this.pnFilters.Controls.Add(this.btRefresh);
            this.pnFilters.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnFilters.Location = new System.Drawing.Point(0, 44);
            this.pnFilters.Name = "pnFilters";
            this.pnFilters.Size = new System.Drawing.Size(1451, 143);
            this.pnFilters.TabIndex = 0;
            // 
            // btLast100
            // 
            this.btLast100.FlatAppearance.BorderSize = 0;
            this.btLast100.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btLast100.Location = new System.Drawing.Point(52, 80);
            this.btLast100.Name = "btLast100";
            this.btLast100.Size = new System.Drawing.Size(380, 36);
            this.btLast100.TabIndex = 5;
            this.btLast100.TabStop = false;
            this.btLast100.Text = "Последние 100 поступивших";
            this.btLast100.UseVisualStyleBackColor = true;
            this.btLast100.Click += new System.EventHandler(this.btLast100_Click);
            // 
            // gbSearch
            // 
            this.gbSearch.Controls.Add(this.tbThirdName);
            this.gbSearch.Controls.Add(this.tbFirstName);
            this.gbSearch.Controls.Add(this.tbSecondName);
            this.gbSearch.Controls.Add(this.tbResetFilters);
            this.gbSearch.Controls.Add(this.tbAmbuCardNum);
            this.gbSearch.Controls.Add(this.btSearch);
            this.gbSearch.Controls.Add(this.label2);
            this.gbSearch.Controls.Add(this.label1);
            this.gbSearch.Controls.Add(this.dpEnd);
            this.gbSearch.Controls.Add(this.dpStart);
            this.gbSearch.Location = new System.Drawing.Point(52, 12);
            this.gbSearch.Name = "gbSearch";
            this.gbSearch.Size = new System.Drawing.Size(1076, 62);
            this.gbSearch.TabIndex = 3;
            this.gbSearch.TabStop = false;
            this.gbSearch.Text = "Поиск";
            // 
            // tbThirdName
            // 
            this.tbThirdName.Cue = "Отчество";
            this.tbThirdName.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.tbThirdName.Location = new System.Drawing.Point(287, 21);
            this.tbThirdName.Name = "tbThirdName";
            this.tbThirdName.Size = new System.Drawing.Size(135, 26);
            this.tbThirdName.TabIndex = 11;
            this.tbThirdName.TabStop = false;
            this.tbThirdName.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.tbThirdName.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextBox_KeyDown);
            // 
            // tbFirstName
            // 
            this.tbFirstName.Cue = "Имя";
            this.tbFirstName.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.tbFirstName.Location = new System.Drawing.Point(156, 21);
            this.tbFirstName.Name = "tbFirstName";
            this.tbFirstName.Size = new System.Drawing.Size(125, 26);
            this.tbFirstName.TabIndex = 10;
            this.tbFirstName.TabStop = false;
            this.tbFirstName.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.tbFirstName.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextBox_KeyDown);
            // 
            // tbSecondName
            // 
            this.tbSecondName.Cue = "Фамилия";
            this.tbSecondName.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.tbSecondName.Location = new System.Drawing.Point(15, 21);
            this.tbSecondName.Name = "tbSecondName";
            this.tbSecondName.Size = new System.Drawing.Size(135, 26);
            this.tbSecondName.TabIndex = 9;
            this.tbSecondName.TabStop = false;
            this.tbSecondName.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.tbSecondName.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextBox_KeyDown);
            // 
            // tbResetFilters
            // 
            this.tbResetFilters.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.tbResetFilters.Image = global::FormsEditorEx.Properties.Resources.blank16;
            this.tbResetFilters.Location = new System.Drawing.Point(942, 20);
            this.tbResetFilters.Name = "tbResetFilters";
            this.tbResetFilters.Size = new System.Drawing.Size(28, 28);
            this.tbResetFilters.TabIndex = 8;
            this.tbResetFilters.TabStop = false;
            this.tbResetFilters.UseVisualStyleBackColor = true;
            this.tbResetFilters.Click += new System.EventHandler(this.btResetFilters_Click);
            // 
            // tbAmbuCardNum
            // 
            this.tbAmbuCardNum.Cue = "№ амбул. карты";
            this.tbAmbuCardNum.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.tbAmbuCardNum.Location = new System.Drawing.Point(446, 21);
            this.tbAmbuCardNum.Name = "tbAmbuCardNum";
            this.tbAmbuCardNum.Size = new System.Drawing.Size(146, 26);
            this.tbAmbuCardNum.TabIndex = 4;
            this.tbAmbuCardNum.TabStop = false;
            this.tbAmbuCardNum.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.tbAmbuCardNum.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextBox_KeyDown);
            // 
            // btSearch
            // 
            this.btSearch.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btSearch.Location = new System.Drawing.Point(976, 20);
            this.btSearch.Name = "btSearch";
            this.btSearch.Size = new System.Drawing.Size(87, 28);
            this.btSearch.TabIndex = 4;
            this.btSearch.TabStop = false;
            this.btSearch.Text = "Поиск";
            this.btSearch.UseVisualStyleBackColor = true;
            this.btSearch.Click += new System.EventHandler(this.btSearch_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.Location = new System.Drawing.Point(614, 25);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(14, 18);
            this.label2.TabIndex = 7;
            this.label2.Text = "с";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(773, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(24, 18);
            this.label1.TabIndex = 6;
            this.label1.Text = "по";
            // 
            // dpEnd
            // 
            this.dpEnd.CalendarFont = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.dpEnd.Checked = false;
            this.dpEnd.CustomFormat = "dd.MM.yyyy";
            this.dpEnd.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.dpEnd.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dpEnd.Location = new System.Drawing.Point(803, 21);
            this.dpEnd.Name = "dpEnd";
            this.dpEnd.ShowCheckBox = true;
            this.dpEnd.Size = new System.Drawing.Size(124, 26);
            this.dpEnd.TabIndex = 5;
            this.dpEnd.TabStop = false;
            this.dpEnd.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextBox_KeyDown);
            // 
            // dpStart
            // 
            this.dpStart.CalendarFont = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.dpStart.Checked = false;
            this.dpStart.CustomFormat = "dd.MM.yyyy";
            this.dpStart.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.dpStart.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dpStart.Location = new System.Drawing.Point(634, 21);
            this.dpStart.Name = "dpStart";
            this.dpStart.ShowCheckBox = true;
            this.dpStart.Size = new System.Drawing.Size(126, 26);
            this.dpStart.TabIndex = 4;
            this.dpStart.TabStop = false;
            this.dpStart.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextBox_KeyDown);
            // 
            // btRefresh
            // 
            this.btRefresh.BackColor = System.Drawing.SystemColors.Control;
            this.btRefresh.FlatAppearance.BorderSize = 0;
            this.btRefresh.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btRefresh.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btRefresh.Image = global::FormsEditorEx.Properties.Resources.refresh2;
            this.btRefresh.Location = new System.Drawing.Point(438, 81);
            this.btRefresh.Name = "btRefresh";
            this.btRefresh.Size = new System.Drawing.Size(690, 34);
            this.btRefresh.TabIndex = 0;
            this.btRefresh.TabStop = false;
            this.btRefresh.Text = " Проходят лечение";
            this.btRefresh.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btRefresh.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btRefresh.UseVisualStyleBackColor = false;
            this.btRefresh.Click += new System.EventHandler(this.btRefresh_Click);
            // 
            // pnBottomBar
            // 
            this.pnBottomBar.Controls.Add(this.pnButtons);
            this.pnBottomBar.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnBottomBar.Location = new System.Drawing.Point(0, 701);
            this.pnBottomBar.Name = "pnBottomBar";
            this.pnBottomBar.Size = new System.Drawing.Size(1451, 48);
            this.pnBottomBar.TabIndex = 1;
            // 
            // pnButtons
            // 
            this.pnButtons.BackColor = System.Drawing.SystemColors.Control;
            this.pnButtons.Controls.Add(this.btEdit);
            this.pnButtons.Controls.Add(this.btBack);
            this.pnButtons.Controls.Add(this.btTest1);
            this.pnButtons.Controls.Add(this.btLoadEntry);
            this.pnButtons.Controls.Add(this.btNewPatient);
            this.pnButtons.Controls.Add(this.btDelete);
            this.pnButtons.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnButtons.Location = new System.Drawing.Point(0, 0);
            this.pnButtons.Margin = new System.Windows.Forms.Padding(0);
            this.pnButtons.Name = "pnButtons";
            this.pnButtons.Size = new System.Drawing.Size(1451, 48);
            this.pnButtons.TabIndex = 3;
            // 
            // btEdit
            // 
            this.btEdit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btEdit.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btEdit.Image = global::FormsEditorEx.Properties.Resources.edit24;
            this.btEdit.Location = new System.Drawing.Point(1229, 6);
            this.btEdit.Name = "btEdit";
            this.btEdit.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.btEdit.Size = new System.Drawing.Size(111, 36);
            this.btEdit.TabIndex = 12;
            this.btEdit.TabStop = false;
            this.btEdit.Text = " Изменить";
            this.btEdit.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btEdit.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btEdit.UseVisualStyleBackColor = true;
            this.btEdit.Click += new System.EventHandler(this.btEdit_Click);
            // 
            // btBack
            // 
            this.btBack.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btBack.Image = global::FormsEditorEx.Properties.Resources.gobackarrow;
            this.btBack.Location = new System.Drawing.Point(5, 6);
            this.btBack.Name = "btBack";
            this.btBack.Padding = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this.btBack.Size = new System.Drawing.Size(88, 36);
            this.btBack.TabIndex = 11;
            this.btBack.TabStop = false;
            this.btBack.Text = "  Назад";
            this.btBack.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btBack.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btBack.UseVisualStyleBackColor = true;
            this.btBack.Visible = false;
            this.btBack.Click += new System.EventHandler(this.btBack_Click);
            // 
            // btTest1
            // 
            this.btTest1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btTest1.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btTest1.Image = global::FormsEditorEx.Properties.Resources.lookup24;
            this.btTest1.Location = new System.Drawing.Point(644, 6);
            this.btTest1.Name = "btTest1";
            this.btTest1.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.btTest1.Size = new System.Drawing.Size(112, 36);
            this.btTest1.TabIndex = 10;
            this.btTest1.TabStop = false;
            this.btTest1.Text = "Test";
            this.btTest1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btTest1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btTest1.UseVisualStyleBackColor = true;
            this.btTest1.Click += new System.EventHandler(this.btTest1_Click);
            // 
            // btLoadEntry
            // 
            this.btLoadEntry.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btLoadEntry.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btLoadEntry.Image = global::FormsEditorEx.Properties.Resources.lookup24;
            this.btLoadEntry.Location = new System.Drawing.Point(956, 6);
            this.btLoadEntry.Name = "btLoadEntry";
            this.btLoadEntry.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.btLoadEntry.Size = new System.Drawing.Size(112, 36);
            this.btLoadEntry.TabIndex = 9;
            this.btLoadEntry.TabStop = false;
            this.btLoadEntry.Text = " Загрузить";
            this.btLoadEntry.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btLoadEntry.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btLoadEntry.UseVisualStyleBackColor = true;
            this.btLoadEntry.Click += new System.EventHandler(this.btLoadEntry_Click);
            // 
            // btNewPatient
            // 
            this.btNewPatient.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btNewPatient.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btNewPatient.Image = global::FormsEditorEx.Properties.Resources.add24;
            this.btNewPatient.Location = new System.Drawing.Point(1074, 6);
            this.btNewPatient.Name = "btNewPatient";
            this.btNewPatient.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.btNewPatient.Size = new System.Drawing.Size(149, 36);
            this.btNewPatient.TabIndex = 8;
            this.btNewPatient.TabStop = false;
            this.btNewPatient.Text = " Новый пациент";
            this.btNewPatient.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btNewPatient.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btNewPatient.UseVisualStyleBackColor = true;
            this.btNewPatient.Click += new System.EventHandler(this.btNewPatient_Click);
            // 
            // btDelete
            // 
            this.btDelete.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btDelete.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btDelete.Image = global::FormsEditorEx.Properties.Resources.delete24;
            this.btDelete.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btDelete.Location = new System.Drawing.Point(1346, 6);
            this.btDelete.Name = "btDelete";
            this.btDelete.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.btDelete.Size = new System.Drawing.Size(100, 36);
            this.btDelete.TabIndex = 7;
            this.btDelete.TabStop = false;
            this.btDelete.Text = " Удалить";
            this.btDelete.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btDelete.UseVisualStyleBackColor = true;
            this.btDelete.Click += new System.EventHandler(this.btDelete_Click);
            // 
            // dgvPatients
            // 
            this.dgvPatients.AllowUserToAddRows = false;
            this.dgvPatients.AllowUserToDeleteRows = false;
            this.dgvPatients.AllowUserToResizeColumns = false;
            this.dgvPatients.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.AliceBlue;
            this.dgvPatients.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvPatients.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvPatients.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.dgvPatients.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvPatients.ContextMenuStrip = this.cmsMenu;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvPatients.DefaultCellStyle = dataGridViewCellStyle3;
            this.dgvPatients.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvPatients.GridColor = System.Drawing.SystemColors.Control;
            this.dgvPatients.Location = new System.Drawing.Point(0, 187);
            this.dgvPatients.MultiSelect = false;
            this.dgvPatients.Name = "dgvPatients";
            this.dgvPatients.ReadOnly = true;
            this.dgvPatients.RowHeadersVisible = false;
            this.dgvPatients.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvPatients.Size = new System.Drawing.Size(1451, 514);
            this.dgvPatients.TabIndex = 2;
            this.dgvPatients.TabStop = false;
            this.dgvPatients.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.dgvPatients_CellFormatting);
            this.dgvPatients.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgvPatients_CellMouseDoubleClick);
            this.dgvPatients.CellMouseUp += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgvPatients_CellMouseUp);
            this.dgvPatients.DataBindingComplete += new System.Windows.Forms.DataGridViewBindingCompleteEventHandler(this.dgvPatients_DataBindingComplete);
            this.dgvPatients.SelectionChanged += new System.EventHandler(this.dgvPatients_SelectionChanged);
            this.dgvPatients.SortCompare += new System.Windows.Forms.DataGridViewSortCompareEventHandler(this.dgvPatients_SortCompare);
            // 
            // cmsMenu
            // 
            this.cmsMenu.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.cmsMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.miLoad,
            this.miEdit,
            this.miDelete});
            this.cmsMenu.Name = "cmsMenu";
            this.cmsMenu.Size = new System.Drawing.Size(140, 70);
            this.cmsMenu.Opening += new System.ComponentModel.CancelEventHandler(this.cmsMenu_Opening);
            // 
            // miLoad
            // 
            this.miLoad.Name = "miLoad";
            this.miLoad.Size = new System.Drawing.Size(139, 22);
            this.miLoad.Text = "Загрузить";
            this.miLoad.Click += new System.EventHandler(this.miLoad_Click);
            // 
            // miEdit
            // 
            this.miEdit.Name = "miEdit";
            this.miEdit.Size = new System.Drawing.Size(139, 22);
            this.miEdit.Text = "Изменить";
            this.miEdit.Click += new System.EventHandler(this.miEdit_Click);
            // 
            // miDelete
            // 
            this.miDelete.Name = "miDelete";
            this.miDelete.Size = new System.Drawing.Size(139, 22);
            this.miDelete.Text = "Удалить";
            this.miDelete.Click += new System.EventHandler(this.miDelete_Click);
            // 
            // pnTop
            // 
            this.pnTop.AutoSize = true;
            this.pnTop.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.pnTop.ColumnCount = 7;
            this.pnTop.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 98.32714F));
            this.pnTop.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.pnTop.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 1.672862F));
            this.pnTop.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.pnTop.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.pnTop.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.pnTop.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.pnTop.Controls.Add(this.flowLayoutPanel1, 0, 0);
            this.pnTop.Controls.Add(this.btAbout, 6, 0);
            this.pnTop.Controls.Add(this.btChangelog, 5, 0);
            this.pnTop.Controls.Add(this.btGuide, 4, 0);
            this.pnTop.Controls.Add(this.btDiagnostics, 3, 0);
            this.pnTop.Controls.Add(this.flowLayoutPanel2, 1, 0);
            this.pnTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnTop.Location = new System.Drawing.Point(0, 0);
            this.pnTop.Name = "pnTop";
            this.pnTop.Padding = new System.Windows.Forms.Padding(0, 8, 0, 0);
            this.pnTop.RowCount = 1;
            this.pnTop.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.pnTop.Size = new System.Drawing.Size(1451, 44);
            this.pnTop.TabIndex = 4;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.flowLayoutPanel1.Controls.Add(this.label27);
            this.flowLayoutPanel1.Controls.Add(this.cbDepartment);
            this.flowLayoutPanel1.Location = new System.Drawing.Point(3, 11);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(523, 30);
            this.flowLayoutPanel1.TabIndex = 0;
            // 
            // label27
            // 
            this.label27.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label27.Location = new System.Drawing.Point(3, 0);
            this.label27.Name = "label27";
            this.label27.Padding = new System.Windows.Forms.Padding(12, 0, 0, 0);
            this.label27.Size = new System.Drawing.Size(79, 30);
            this.label27.TabIndex = 47;
            this.label27.Text = "Отделение";
            this.label27.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cbDepartment
            // 
            this.cbDepartment.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cbDepartment.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbDepartment.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.cbDepartment.FormattingEnabled = true;
            this.cbDepartment.Location = new System.Drawing.Point(88, 3);
            this.cbDepartment.Name = "cbDepartment";
            this.cbDepartment.ReadOnly = false;
            this.cbDepartment.Size = new System.Drawing.Size(280, 24);
            this.cbDepartment.TabIndex = 46;
            this.cbDepartment.TabStop = false;
            // 
            // btAbout
            // 
            this.btAbout.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btAbout.BackColor = System.Drawing.SystemColors.Control;
            this.btAbout.FlatAppearance.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.btAbout.FlatAppearance.CheckedBackColor = System.Drawing.Color.Transparent;
            this.btAbout.FlatAppearance.MouseOverBackColor = System.Drawing.SystemColors.Window;
            this.btAbout.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btAbout.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btAbout.Location = new System.Drawing.Point(1310, 11);
            this.btAbout.Margin = new System.Windows.Forms.Padding(3, 3, 6, 3);
            this.btAbout.Name = "btAbout";
            this.btAbout.Size = new System.Drawing.Size(134, 30);
            this.btAbout.TabIndex = 2;
            this.btAbout.TabStop = false;
            this.btAbout.Text = "HMCpro <version>";
            this.btAbout.UseVisualStyleBackColor = false;
            this.btAbout.Click += new System.EventHandler(this.btAbout_Click);
            // 
            // btChangelog
            // 
            this.btChangelog.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btChangelog.BackColor = System.Drawing.SystemColors.Control;
            this.btChangelog.FlatAppearance.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.btChangelog.FlatAppearance.CheckedBackColor = System.Drawing.Color.Transparent;
            this.btChangelog.FlatAppearance.MouseOverBackColor = System.Drawing.SystemColors.Window;
            this.btChangelog.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btChangelog.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btChangelog.Location = new System.Drawing.Point(1156, 11);
            this.btChangelog.Name = "btChangelog";
            this.btChangelog.Size = new System.Drawing.Size(148, 30);
            this.btChangelog.TabIndex = 3;
            this.btChangelog.TabStop = false;
            this.btChangelog.Text = "История изменений";
            this.btChangelog.UseVisualStyleBackColor = false;
            this.btChangelog.Click += new System.EventHandler(this.btChangelog_Click);
            // 
            // btGuide
            // 
            this.btGuide.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btGuide.BackColor = System.Drawing.SystemColors.Control;
            this.btGuide.FlatAppearance.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.btGuide.FlatAppearance.CheckedBackColor = System.Drawing.Color.Transparent;
            this.btGuide.FlatAppearance.MouseOverBackColor = System.Drawing.SystemColors.Window;
            this.btGuide.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btGuide.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btGuide.Location = new System.Drawing.Point(956, 11);
            this.btGuide.Name = "btGuide";
            this.btGuide.Size = new System.Drawing.Size(194, 30);
            this.btGuide.TabIndex = 4;
            this.btGuide.TabStop = false;
            this.btGuide.Text = "Руководство пользователя";
            this.btGuide.UseVisualStyleBackColor = false;
            this.btGuide.Click += new System.EventHandler(this.btGuide_Click);
            // 
            // btDiagnostics
            // 
            this.btDiagnostics.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btDiagnostics.BackColor = System.Drawing.SystemColors.Control;
            this.btDiagnostics.FlatAppearance.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.btDiagnostics.FlatAppearance.CheckedBackColor = System.Drawing.Color.Transparent;
            this.btDiagnostics.FlatAppearance.MouseOverBackColor = System.Drawing.SystemColors.Window;
            this.btDiagnostics.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btDiagnostics.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btDiagnostics.Location = new System.Drawing.Point(845, 11);
            this.btDiagnostics.Name = "btDiagnostics";
            this.btDiagnostics.Size = new System.Drawing.Size(105, 30);
            this.btDiagnostics.TabIndex = 5;
            this.btDiagnostics.TabStop = false;
            this.btDiagnostics.Text = "Диагностика";
            this.btDiagnostics.UseVisualStyleBackColor = false;
            this.btDiagnostics.Click += new System.EventHandler(this.btDiagnostics_Click);
            // 
            // flowLayoutPanel2
            // 
            this.flowLayoutPanel2.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.flowLayoutPanel2.Controls.Add(this.label3);
            this.flowLayoutPanel2.Controls.Add(this.cbDoctors);
            this.flowLayoutPanel2.Location = new System.Drawing.Point(532, 11);
            this.flowLayoutPanel2.Name = "flowLayoutPanel2";
            this.flowLayoutPanel2.Size = new System.Drawing.Size(298, 30);
            this.flowLayoutPanel2.TabIndex = 1;
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.Location = new System.Drawing.Point(3, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(93, 30);
            this.label3.TabIndex = 47;
            this.label3.Text = "Лечащий врач";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cbDoctors
            // 
            this.cbDoctors.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cbDoctors.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbDoctors.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.cbDoctors.FormattingEnabled = true;
            this.cbDoctors.Location = new System.Drawing.Point(102, 3);
            this.cbDoctors.Name = "cbDoctors";
            this.cbDoctors.ReadOnly = false;
            this.cbDoctors.Size = new System.Drawing.Size(193, 24);
            this.cbDoctors.TabIndex = 46;
            this.cbDoctors.TabStop = false;
            this.cbDoctors.SelectedIndexChanged += new System.EventHandler(this.cbDoctors_SelectedIndexChanged);
            this.cbDoctors.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cbDoctors_KeyDown);
            // 
            // ucStatusBar
            // 
            this.ucStatusBar.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.ucStatusBar.Location = new System.Drawing.Point(0, 749);
            this.ucStatusBar.MaximumSize = new System.Drawing.Size(2240, 17);
            this.ucStatusBar.MinimumSize = new System.Drawing.Size(350, 17);
            this.ucStatusBar.Name = "ucStatusBar";
            this.ucStatusBar.Size = new System.Drawing.Size(1451, 17);
            this.ucStatusBar.TabIndex = 3;
            // 
            // PatientsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1451, 766);
            this.Controls.Add(this.dgvPatients);
            this.Controls.Add(this.pnFilters);
            this.Controls.Add(this.pnTop);
            this.Controls.Add(this.pnBottomBar);
            this.Controls.Add(this.ucStatusBar);
            this.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "PatientsForm";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "База данных";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.PatientsForm_FormClosing);
            this.Load += new System.EventHandler(this.PatientsForm_Load);
            this.MouseClick += new System.Windows.Forms.MouseEventHandler(this.PatientsForm_MouseClick);
            this.Resize += new System.EventHandler(this.PatientsForm_Resize);
            this.pnFilters.ResumeLayout(false);
            this.gbSearch.ResumeLayout(false);
            this.gbSearch.PerformLayout();
            this.pnBottomBar.ResumeLayout(false);
            this.pnButtons.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvPatients)).EndInit();
            this.cmsMenu.ResumeLayout(false);
            this.pnTop.ResumeLayout(false);
            this.flowLayoutPanel1.ResumeLayout(false);
            this.flowLayoutPanel2.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel pnFilters;
        private System.Windows.Forms.Panel pnBottomBar;
        private System.Windows.Forms.Panel pnButtons;
        private System.Windows.Forms.DataGridView dgvPatients;
        private System.Windows.Forms.Button btRefresh;
        private System.Windows.Forms.GroupBox gbSearch;
        private System.Windows.Forms.DateTimePicker dpStart;
        private System.Windows.Forms.Button btSearch;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DateTimePicker dpEnd;
        private TextBoxEx tbAmbuCardNum;
        private System.Windows.Forms.Button tbResetFilters;
        private TextBoxEx tbThirdName;
        private TextBoxEx tbFirstName;
        private TextBoxEx tbSecondName;
        private ucStatusBarControl ucStatusBar;
        private System.Windows.Forms.Button btLast100;
        private System.Windows.Forms.TableLayoutPanel pnTop;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.Label label27;
        private ComboBoxEx cbDepartment;
        private System.Windows.Forms.Button btDelete;
        private System.Windows.Forms.Button btNewPatient;
        private System.Windows.Forms.Button btLoadEntry;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel2;
        private System.Windows.Forms.Label label3;
        private ComboBoxEx cbDoctors;
        private System.Windows.Forms.Button btChangelog;
        private System.Windows.Forms.Button btTest1;
        private System.Windows.Forms.Button btBack;
        private System.Windows.Forms.ContextMenuStrip cmsMenu;
        private System.Windows.Forms.ToolStripMenuItem miLoad;
        private System.Windows.Forms.ToolStripMenuItem miDelete;
        private System.Windows.Forms.Button btEdit;
        private System.Windows.Forms.ToolStripMenuItem miEdit;
        private System.Windows.Forms.Button btAbout;
        private System.Windows.Forms.Button btGuide;
        private System.Windows.Forms.Button btDiagnostics;
    }
}