﻿using FormsEditorEx.GenExam;
using FormsEditorEx.Conclusion;
using FormsEditorEx.Commission;

namespace FormsEditorEx.PatientModel
{
    public class PatientData
    { 
        public GenExamResult GenExam { get; set; } = new GenExamResult();
        public ConclusionRecords Conclusions { get; set; } = new ConclusionRecords();
        public CommissionRecords Commissions { get; set; } = new CommissionRecords();
        public PersonalInfo PersonalInfo { get; set; } = new PersonalInfo();
        public TemporaryData TemporaryFields { get; set; } = new TemporaryData();
        public LifeAnamnesisData LifeAnamnesisData { get; set; } = new LifeAnamnesisData();
        public PatientData()
        {

        }
        public void Reset()
        {
            GenExam = new GenExamResult();
            Conclusions = new ConclusionRecords();
            Commissions = new CommissionRecords();
            PersonalInfo = new PersonalInfo();
            TemporaryFields = new TemporaryData();
            LifeAnamnesisData = new LifeAnamnesisData();
        }
        public void ResetPost()
        {
            GenExam.ResetPost();
            TemporaryFields = new TemporaryData();

            Commissions.ResetPost();
            Conclusions.ResetPost();
        }
    }
}
