﻿using Newtonsoft.Json.Linq;
using Newtonsoft.Json;

namespace FormsEditorEx
{
    public class OperTeam
    {
        public string Surgeon { get; set; }
        public string Assistant { get; set; }
        public string Anesthetist { get; set; }
        public string OperSister { get; set; }
        public string BloodTransSpecialist { get; set; }

        public OperTeam() { }

        public JObject Serialize()
        {
            JObject jobj = new JObject();

            if (!string.IsNullOrWhiteSpace(Assistant))
                jobj.Add(new JProperty(nameof(Assistant), Assistant));
            if (!string.IsNullOrWhiteSpace(Anesthetist))
                jobj.Add(new JProperty(nameof(Anesthetist), Anesthetist));
            if (!string.IsNullOrWhiteSpace(OperSister))
                jobj.Add(new JProperty(nameof(OperSister), OperSister));
            if (!string.IsNullOrWhiteSpace(BloodTransSpecialist))
                jobj.Add(new JProperty(nameof(BloodTransSpecialist), BloodTransSpecialist));

            return jobj;
        }
        //public static void Deserialize(string json)
        //{
        //    Instance = JsonConvert.DeserializeObject<OperTeam>(json);
        //}
        public bool ValidateOperTeam()
        {
            return !string.IsNullOrWhiteSpace(Surgeon)
                && !string.IsNullOrWhiteSpace(Assistant)
                && !string.IsNullOrWhiteSpace(Anesthetist)
                && !string.IsNullOrWhiteSpace(OperSister)
                && !string.IsNullOrWhiteSpace(BloodTransSpecialist);
        }
    }
}
