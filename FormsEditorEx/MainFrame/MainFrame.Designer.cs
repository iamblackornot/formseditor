﻿using SharedLibrary;

namespace FormsEditorEx
{
    partial class MainFrame
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainFrame));
            this.btVMP = new System.Windows.Forms.Button();
            this.tbSecondName = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.tbFirstName = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.tbThirdName = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.gbPersonal = new System.Windows.Forms.GroupBox();
            this.tbDateBirth = new System.Windows.Forms.MaskedTextBox();
            this.label28 = new System.Windows.Forms.Label();
            this.cbSex = new SharedLibrary.ComboBoxEx();
            this.label23 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.btAccompMedicationCopy = new System.Windows.Forms.Button();
            this.gbDiagnoz = new System.Windows.Forms.GroupBox();
            this.rtbDiagnosisPre = new SharedLibrary.PlainRichTextBox();
            this.label33 = new System.Windows.Forms.Label();
            this.rtbComplicationsPre = new SharedLibrary.PlainRichTextBoxExt();
            this.label8 = new System.Windows.Forms.Label();
            this.rtbStatusLocalisPre = new SharedLibrary.PlainRichTextBox();
            this.label31 = new System.Windows.Forms.Label();
            this.rtbDiseaseMoreInfo = new SharedLibrary.PlainRichTextBox();
            this.label30 = new System.Windows.Forms.Label();
            this.rtbPlannedExamination = new SharedLibrary.PlainRichTextBox();
            this.pnSplitter4 = new System.Windows.Forms.Panel();
            this.pnPlannedTreatment = new System.Windows.Forms.Panel();
            this.pnChemTherapyToggle = new System.Windows.Forms.Panel();
            this.cbChemTherapy = new SharedLibrary.FocusableCheckBox();
            this.pbChemHelp = new System.Windows.Forms.PictureBox();
            this.pnSplitter3 = new System.Windows.Forms.Panel();
            this.label54 = new System.Windows.Forms.Label();
            this.pnVMPToggle = new System.Windows.Forms.Panel();
            this.cbVMP = new SharedLibrary.FocusableCheckBox();
            this.pbVMPHelp = new System.Windows.Forms.PictureBox();
            this.rtbPlannedOperationScope = new SharedLibrary.PlainRichTextBox();
            this.label29 = new System.Windows.Forms.Label();
            this.rtbAssignments = new SharedLibrary.PlainRichTextBox();
            this.pnAnamnesis = new System.Windows.Forms.Panel();
            this.btAnamnesis = new System.Windows.Forms.Button();
            this.pnSplitter1 = new System.Windows.Forms.Panel();
            this.btLifeAnamnesis = new System.Windows.Forms.Button();
            this.pnSplitter2 = new System.Windows.Forms.Panel();
            this.rtbComplaints = new SharedLibrary.PlainRichTextBoxExt();
            this.label10 = new System.Windows.Forms.Label();
            this.btOperTeamNeuro = new System.Windows.Forms.Button();
            this.btConclusions = new System.Windows.Forms.Button();
            this.label42 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.btGenExam = new System.Windows.Forms.Button();
            this.tbDateExam = new System.Windows.Forms.MaskedTextBox();
            this.label39 = new System.Windows.Forms.Label();
            this.btTestData = new System.Windows.Forms.Button();
            this.tbAmbuCardNum = new System.Windows.Forms.TextBox();
            this.tbClinicalRecordNum = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.btTest = new System.Windows.Forms.Button();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this.btDisabilityListInfo = new System.Windows.Forms.Button();
            this.label9 = new System.Windows.Forms.Label();
            this.cbDisability = new SharedLibrary.ComboBoxEx();
            this.cbRh = new SharedLibrary.ComboBoxEx();
            this.label53 = new System.Windows.Forms.Label();
            this.cbBloodGroup = new SharedLibrary.ComboBoxEx();
            this.label52 = new System.Windows.Forms.Label();
            this.groupBox10 = new System.Windows.Forms.GroupBox();
            this.cbGenCondition = new SharedLibrary.ComboBoxEx();
            this.cbNutrition = new SharedLibrary.ComboBoxEx();
            this.tbTimeExam = new System.Windows.Forms.MaskedTextBox();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.rtbTreatment = new SharedLibrary.PlainRichTextBoxExt();
            this.panel3 = new System.Windows.Forms.Panel();
            this.btTransfusion = new System.Windows.Forms.Button();
            this.panel4 = new System.Windows.Forms.Panel();
            this.btStatusLocalisPost = new System.Windows.Forms.Button();
            this.label21 = new System.Windows.Forms.Label();
            this.rtbMedication = new SharedLibrary.PlainRichTextBoxExt();
            this.label22 = new System.Windows.Forms.Label();
            this.rtbAccompanyingPost = new SharedLibrary.PlainRichTextBoxExt();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label32 = new System.Windows.Forms.Label();
            this.tbMKBAccompanying = new SharedLibrary.TextBoxEx();
            this.label35 = new System.Windows.Forms.Label();
            this.rtbComplicationsPost = new SharedLibrary.PlainRichTextBoxExt();
            this.pnComplications = new System.Windows.Forms.Panel();
            this.label25 = new System.Windows.Forms.Label();
            this.tbMKBComplications = new SharedLibrary.TextBoxEx();
            this.label26 = new System.Windows.Forms.Label();
            this.rtbDiagnosisPost = new SharedLibrary.PlainRichTextBox();
            this.pnDiagnosisHeader = new System.Windows.Forms.Panel();
            this.label24 = new System.Windows.Forms.Label();
            this.tbMKBDiagnosis = new SharedLibrary.TextBoxEx();
            this.label55 = new System.Windows.Forms.Label();
            this.pnMiscInfo = new System.Windows.Forms.Panel();
            this.btLifeAnamnesisPost = new System.Windows.Forms.Button();
            this.pnMiscSplitter1 = new System.Windows.Forms.Panel();
            this.btAdditionalInfo = new System.Windows.Forms.Button();
            this.pnMiscSplitter2 = new System.Windows.Forms.Panel();
            this.btRecommendations = new System.Windows.Forms.Button();
            this.pnResult = new System.Windows.Forms.Panel();
            this.pnCenteredResult = new System.Windows.Forms.Panel();
            this.label59 = new System.Windows.Forms.Label();
            this.cbWorkCapacity = new SharedLibrary.ComboBoxEx();
            this.cbTreatmentResult = new SharedLibrary.ComboBoxEx();
            this.label11 = new System.Windows.Forms.Label();
            this.label58 = new System.Windows.Forms.Label();
            this.cbCondition = new SharedLibrary.ComboBoxEx();
            this.cbSickLeaveEndType = new SharedLibrary.ComboBoxEx();
            this.tbDateDischarge = new System.Windows.Forms.MaskedTextBox();
            this.label57 = new System.Windows.Forms.Label();
            this.cbSickLeave = new SharedLibrary.FocusableCheckBox();
            this.tbSickLeaveNum = new System.Windows.Forms.TextBox();
            this.lbSickLeave1 = new System.Windows.Forms.Label();
            this.tbSickLeaveStart = new System.Windows.Forms.MaskedTextBox();
            this.cbSickLeaveExt = new SharedLibrary.FocusableCheckBox();
            this.tbSickLeaveEnd = new System.Windows.Forms.MaskedTextBox();
            this.lbSickLeave4 = new System.Windows.Forms.Label();
            this.lbSickLeave2 = new System.Windows.Forms.Label();
            this.tbSickLeaveExtEnd = new System.Windows.Forms.MaskedTextBox();
            this.lbSickLeave3 = new System.Windows.Forms.Label();
            this.tbSickLeaveExtStart = new System.Windows.Forms.MaskedTextBox();
            this.tbSuperintendent = new SharedLibrary.ComboBoxEx();
            this.label45 = new System.Windows.Forms.Label();
            this.label64 = new System.Windows.Forms.Label();
            this.gbSuperIntendent = new System.Windows.Forms.GroupBox();
            this.cbSuperIntendentFormatted = new SharedLibrary.ComboBoxEx();
            this.cbIO = new System.Windows.Forms.CheckBox();
            this.btAction = new System.Windows.Forms.Button();
            this.groupBox12 = new System.Windows.Forms.GroupBox();
            this.rtbMedicalAllergy = new SharedLibrary.PlainRichTextBoxExt();
            this.label34 = new System.Windows.Forms.Label();
            this.rtbAccompMedicationPost = new SharedLibrary.PlainRichTextBoxExt();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label38 = new System.Windows.Forms.Label();
            this.rtbAccompMedicationPre = new SharedLibrary.PlainRichTextBoxExt();
            this.rtbAccompanying = new SharedLibrary.PlainRichTextBoxExt();
            this.btCommissions = new System.Windows.Forms.Button();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.btInstrumentalExamination = new System.Windows.Forms.Button();
            this.pnBottom = new System.Windows.Forms.Panel();
            this.cbDepartment = new SharedLibrary.ComboBoxEx();
            this.label7 = new System.Windows.Forms.Label();
            this.btPatients = new System.Windows.Forms.Button();
            this.btRevert = new System.Windows.Forms.Button();
            this.btOpen = new System.Windows.Forms.Button();
            this.btOpenDirectory = new System.Windows.Forms.Button();
            this.cbDoctors = new SharedLibrary.ComboBoxEx();
            this.btSearch = new System.Windows.Forms.Button();
            this.label47 = new System.Windows.Forms.Label();
            this.label49 = new System.Windows.Forms.Label();
            this.clbVTEOPrevention = new System.Windows.Forms.CheckedListBox();
            this.label50 = new System.Windows.Forms.Label();
            this.gpVTEO = new System.Windows.Forms.GroupBox();
            this.pnVTEO = new System.Windows.Forms.Panel();
            this.cbVTEORisk = new SharedLibrary.ComboBoxEx();
            this.cbGORisk = new SharedLibrary.ComboBoxEx();
            this.pnStatusBar = new System.Windows.Forms.Panel();
            this.ucStatusBar = new SharedLibrary.ucStatusBarControl();
            this.pnWorkingArea = new System.Windows.Forms.Panel();
            this.pnSection4 = new System.Windows.Forms.Panel();
            this.pnSection3 = new System.Windows.Forms.Panel();
            this.pnSection2 = new System.Windows.Forms.Panel();
            this.pnSection1 = new System.Windows.Forms.Panel();
            this.gpAddress = new System.Windows.Forms.GroupBox();
            this.cbStreet = new SharedLibrary.ComboBoxEx();
            this.cbTown = new SharedLibrary.ComboBoxEx();
            this.cbStreetType = new SharedLibrary.ComboBoxEx();
            this.rbVillage = new System.Windows.Forms.RadioButton();
            this.rbTown = new System.Windows.Forms.RadioButton();
            this.label12 = new System.Windows.Forms.Label();
            this.tbBuilding = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.tbFlat = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.tbKorpus = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.tbOblast = new System.Windows.Forms.TextBox();
            this.tbIndex = new System.Windows.Forms.MaskedTextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.pnRecord = new System.Windows.Forms.Panel();
            this.gbPersonal.SuspendLayout();
            this.gbDiagnoz.SuspendLayout();
            this.pnPlannedTreatment.SuspendLayout();
            this.pnChemTherapyToggle.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbChemHelp)).BeginInit();
            this.pnVMPToggle.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbVMPHelp)).BeginInit();
            this.pnAnamnesis.SuspendLayout();
            this.groupBox9.SuspendLayout();
            this.groupBox10.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel1.SuspendLayout();
            this.pnComplications.SuspendLayout();
            this.pnDiagnosisHeader.SuspendLayout();
            this.pnMiscInfo.SuspendLayout();
            this.pnResult.SuspendLayout();
            this.pnCenteredResult.SuspendLayout();
            this.gbSuperIntendent.SuspendLayout();
            this.groupBox12.SuspendLayout();
            this.panel2.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.pnBottom.SuspendLayout();
            this.gpVTEO.SuspendLayout();
            this.pnVTEO.SuspendLayout();
            this.pnStatusBar.SuspendLayout();
            this.pnWorkingArea.SuspendLayout();
            this.pnSection4.SuspendLayout();
            this.pnSection3.SuspendLayout();
            this.pnSection2.SuspendLayout();
            this.pnSection1.SuspendLayout();
            this.gpAddress.SuspendLayout();
            this.pnRecord.SuspendLayout();
            this.SuspendLayout();
            // 
            // btVMP
            // 
            this.btVMP.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btVMP.Enabled = false;
            this.btVMP.Location = new System.Drawing.Point(1549, 4);
            this.btVMP.Name = "btVMP";
            this.btVMP.Size = new System.Drawing.Size(90, 40);
            this.btVMP.TabIndex = 0;
            this.btVMP.TabStop = false;
            this.btVMP.Text = "ВМП";
            this.btVMP.UseVisualStyleBackColor = true;
            this.btVMP.Click += new System.EventHandler(this.btVMP_Click);
            // 
            // tbSecondName
            // 
            this.tbSecondName.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.tbSecondName.Location = new System.Drawing.Point(134, 19);
            this.tbSecondName.Name = "tbSecondName";
            this.tbSecondName.Size = new System.Drawing.Size(140, 26);
            this.tbSecondName.TabIndex = 1;
            this.tbSecondName.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.tbSecondName.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextBox_KeyDown);
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.Location = new System.Drawing.Point(5, 19);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(117, 26);
            this.label1.TabIndex = 2;
            this.label1.Text = "Фамилия";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.Location = new System.Drawing.Point(5, 51);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(117, 26);
            this.label2.TabIndex = 4;
            this.label2.Text = "Имя";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tbFirstName
            // 
            this.tbFirstName.AcceptsReturn = true;
            this.tbFirstName.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.tbFirstName.Location = new System.Drawing.Point(134, 51);
            this.tbFirstName.Name = "tbFirstName";
            this.tbFirstName.Size = new System.Drawing.Size(140, 26);
            this.tbFirstName.TabIndex = 2;
            this.tbFirstName.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.tbFirstName.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextBox_KeyDown);
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.Location = new System.Drawing.Point(5, 83);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(117, 26);
            this.label3.TabIndex = 6;
            this.label3.Text = "Отчество";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tbThirdName
            // 
            this.tbThirdName.AcceptsReturn = true;
            this.tbThirdName.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.tbThirdName.Location = new System.Drawing.Point(134, 83);
            this.tbThirdName.Name = "tbThirdName";
            this.tbThirdName.Size = new System.Drawing.Size(140, 26);
            this.tbThirdName.TabIndex = 3;
            this.tbThirdName.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.tbThirdName.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextBox_KeyDown);
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label4.Location = new System.Drawing.Point(5, 148);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(117, 26);
            this.label4.TabIndex = 8;
            this.label4.Text = "Дата рождения";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // gbPersonal
            // 
            this.gbPersonal.BackColor = System.Drawing.SystemColors.Control;
            this.gbPersonal.Controls.Add(this.tbDateBirth);
            this.gbPersonal.Controls.Add(this.label1);
            this.gbPersonal.Controls.Add(this.tbSecondName);
            this.gbPersonal.Controls.Add(this.tbFirstName);
            this.gbPersonal.Controls.Add(this.label2);
            this.gbPersonal.Controls.Add(this.tbThirdName);
            this.gbPersonal.Controls.Add(this.label3);
            this.gbPersonal.Controls.Add(this.label4);
            this.gbPersonal.Controls.Add(this.label28);
            this.gbPersonal.Controls.Add(this.cbSex);
            this.gbPersonal.Dock = System.Windows.Forms.DockStyle.Top;
            this.gbPersonal.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.gbPersonal.Location = new System.Drawing.Point(8, 86);
            this.gbPersonal.Name = "gbPersonal";
            this.gbPersonal.Size = new System.Drawing.Size(283, 185);
            this.gbPersonal.TabIndex = 2;
            this.gbPersonal.TabStop = false;
            // 
            // tbDateBirth
            // 
            this.tbDateBirth.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.tbDateBirth.Culture = new System.Globalization.CultureInfo("");
            this.tbDateBirth.Location = new System.Drawing.Point(134, 148);
            this.tbDateBirth.Mask = "00.00.0000";
            this.tbDateBirth.Name = "tbDateBirth";
            this.tbDateBirth.Size = new System.Drawing.Size(140, 26);
            this.tbDateBirth.TabIndex = 5;
            this.tbDateBirth.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.tbDateBirth.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TextBox_KeyPress);
            // 
            // label28
            // 
            this.label28.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label28.Location = new System.Drawing.Point(5, 115);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(117, 26);
            this.label28.TabIndex = 54;
            this.label28.Text = "Пол";
            this.label28.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cbSex
            // 
            this.cbSex.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cbSex.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cbSex.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbSex.FormattingEnabled = true;
            this.cbSex.Items.AddRange(new object[] {
            "мужской",
            "женский"});
            this.cbSex.Location = new System.Drawing.Point(134, 115);
            this.cbSex.Name = "cbSex";
            this.cbSex.ReadOnly = false;
            this.cbSex.Size = new System.Drawing.Size(140, 27);
            this.cbSex.TabIndex = 4;
            this.cbSex.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextBox_KeyDown);
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Dock = System.Windows.Forms.DockStyle.Top;
            this.label23.Location = new System.Drawing.Point(8, 156);
            this.label23.Name = "label23";
            this.label23.Padding = new System.Windows.Forms.Padding(8, 4, 0, 0);
            this.label23.Size = new System.Drawing.Size(68, 22);
            this.label23.TabIndex = 34;
            this.label23.Text = "Диагноз";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(332, 17);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(98, 18);
            this.label27.TabIndex = 45;
            this.label27.Text = "Лечащий врач";
            // 
            // btAccompMedicationCopy
            // 
            this.btAccompMedicationCopy.Dock = System.Windows.Forms.DockStyle.Right;
            this.btAccompMedicationCopy.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btAccompMedicationCopy.Location = new System.Drawing.Point(339, 0);
            this.btAccompMedicationCopy.Name = "btAccompMedicationCopy";
            this.btAccompMedicationCopy.Size = new System.Drawing.Size(59, 26);
            this.btAccompMedicationCopy.TabIndex = 53;
            this.btAccompMedicationCopy.TabStop = false;
            this.btAccompMedicationCopy.Text = "↓ copy";
            this.btAccompMedicationCopy.UseVisualStyleBackColor = true;
            this.btAccompMedicationCopy.Click += new System.EventHandler(this.btAccompMedicationCopy_Click);
            // 
            // gbDiagnoz
            // 
            this.gbDiagnoz.BackColor = System.Drawing.SystemColors.Control;
            this.gbDiagnoz.Controls.Add(this.rtbDiagnosisPre);
            this.gbDiagnoz.Controls.Add(this.label33);
            this.gbDiagnoz.Controls.Add(this.rtbComplicationsPre);
            this.gbDiagnoz.Controls.Add(this.label8);
            this.gbDiagnoz.Controls.Add(this.rtbStatusLocalisPre);
            this.gbDiagnoz.Controls.Add(this.label31);
            this.gbDiagnoz.Controls.Add(this.rtbDiseaseMoreInfo);
            this.gbDiagnoz.Controls.Add(this.label30);
            this.gbDiagnoz.Controls.Add(this.rtbPlannedExamination);
            this.gbDiagnoz.Controls.Add(this.pnSplitter4);
            this.gbDiagnoz.Controls.Add(this.pnPlannedTreatment);
            this.gbDiagnoz.Controls.Add(this.rtbPlannedOperationScope);
            this.gbDiagnoz.Controls.Add(this.label29);
            this.gbDiagnoz.Controls.Add(this.rtbAssignments);
            this.gbDiagnoz.Controls.Add(this.label23);
            this.gbDiagnoz.Controls.Add(this.pnAnamnesis);
            this.gbDiagnoz.Controls.Add(this.pnSplitter2);
            this.gbDiagnoz.Controls.Add(this.rtbComplaints);
            this.gbDiagnoz.Controls.Add(this.label10);
            this.gbDiagnoz.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gbDiagnoz.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.gbDiagnoz.Location = new System.Drawing.Point(4, 0);
            this.gbDiagnoz.Name = "gbDiagnoz";
            this.gbDiagnoz.Padding = new System.Windows.Forms.Padding(8, 4, 8, 9);
            this.gbDiagnoz.Size = new System.Drawing.Size(490, 928);
            this.gbDiagnoz.TabIndex = 6;
            this.gbDiagnoz.TabStop = false;
            // 
            // rtbDiagnosisPre
            // 
            this.rtbDiagnosisPre.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rtbDiagnosisPre.ForeColor = System.Drawing.Color.Black;
            this.rtbDiagnosisPre.Location = new System.Drawing.Point(8, 178);
            this.rtbDiagnosisPre.Name = "rtbDiagnosisPre";
            this.rtbDiagnosisPre.PlainTextMode = true;
            this.rtbDiagnosisPre.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.ForcedVertical;
            this.rtbDiagnosisPre.ShowSelectionMargin = true;
            this.rtbDiagnosisPre.Size = new System.Drawing.Size(474, 126);
            this.rtbDiagnosisPre.TabIndex = 4;
            this.rtbDiagnosisPre.Text = "";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.label33.Location = new System.Drawing.Point(8, 304);
            this.label33.Name = "label33";
            this.label33.Padding = new System.Windows.Forms.Padding(8, 4, 0, 0);
            this.label33.Size = new System.Drawing.Size(97, 22);
            this.label33.TabIndex = 49;
            this.label33.Text = "Осложнения";
            // 
            // rtbComplicationsPre
            // 
            this.rtbComplicationsPre.Cue = "нет.";
            this.rtbComplicationsPre.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.rtbComplicationsPre.ForeColor = System.Drawing.Color.Black;
            this.rtbComplicationsPre.Location = new System.Drawing.Point(8, 326);
            this.rtbComplicationsPre.Name = "rtbComplicationsPre";
            this.rtbComplicationsPre.PlainTextMode = true;
            this.rtbComplicationsPre.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.ForcedVertical;
            this.rtbComplicationsPre.ShowSelectionMargin = true;
            this.rtbComplicationsPre.Size = new System.Drawing.Size(474, 69);
            this.rtbComplicationsPre.TabIndex = 5;
            this.rtbComplicationsPre.Text = "";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.label8.Location = new System.Drawing.Point(8, 395);
            this.label8.Name = "label8";
            this.label8.Padding = new System.Windows.Forms.Padding(8, 4, 0, 0);
            this.label8.Size = new System.Drawing.Size(98, 22);
            this.label8.TabIndex = 47;
            this.label8.Text = "Status Localis";
            // 
            // rtbStatusLocalisPre
            // 
            this.rtbStatusLocalisPre.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.rtbStatusLocalisPre.ForeColor = System.Drawing.Color.Black;
            this.rtbStatusLocalisPre.Location = new System.Drawing.Point(8, 417);
            this.rtbStatusLocalisPre.Name = "rtbStatusLocalisPre";
            this.rtbStatusLocalisPre.PlainTextMode = true;
            this.rtbStatusLocalisPre.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.ForcedVertical;
            this.rtbStatusLocalisPre.ShowSelectionMargin = true;
            this.rtbStatusLocalisPre.Size = new System.Drawing.Size(474, 83);
            this.rtbStatusLocalisPre.TabIndex = 6;
            this.rtbStatusLocalisPre.Text = "";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.label31.Location = new System.Drawing.Point(8, 500);
            this.label31.Name = "label31";
            this.label31.Padding = new System.Windows.Forms.Padding(8, 4, 0, 0);
            this.label31.Size = new System.Drawing.Size(289, 22);
            this.label31.TabIndex = 86;
            this.label31.Text = "Дополнительные сведения о заболевании";
            // 
            // rtbDiseaseMoreInfo
            // 
            this.rtbDiseaseMoreInfo.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.rtbDiseaseMoreInfo.ForeColor = System.Drawing.Color.Black;
            this.rtbDiseaseMoreInfo.Location = new System.Drawing.Point(8, 522);
            this.rtbDiseaseMoreInfo.Name = "rtbDiseaseMoreInfo";
            this.rtbDiseaseMoreInfo.PlainTextMode = true;
            this.rtbDiseaseMoreInfo.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.ForcedVertical;
            this.rtbDiseaseMoreInfo.ShowSelectionMargin = true;
            this.rtbDiseaseMoreInfo.Size = new System.Drawing.Size(474, 72);
            this.rtbDiseaseMoreInfo.TabIndex = 7;
            this.rtbDiseaseMoreInfo.Text = "";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.label30.Location = new System.Drawing.Point(8, 594);
            this.label30.Name = "label30";
            this.label30.Padding = new System.Windows.Forms.Padding(8, 4, 0, 0);
            this.label30.Size = new System.Drawing.Size(142, 22);
            this.label30.TabIndex = 84;
            this.label30.Text = "План обследования";
            // 
            // rtbPlannedExamination
            // 
            this.rtbPlannedExamination.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.rtbPlannedExamination.ForeColor = System.Drawing.Color.Black;
            this.rtbPlannedExamination.Location = new System.Drawing.Point(8, 616);
            this.rtbPlannedExamination.Name = "rtbPlannedExamination";
            this.rtbPlannedExamination.PlainTextMode = true;
            this.rtbPlannedExamination.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.ForcedVertical;
            this.rtbPlannedExamination.ShowSelectionMargin = true;
            this.rtbPlannedExamination.Size = new System.Drawing.Size(474, 88);
            this.rtbPlannedExamination.TabIndex = 8;
            this.rtbPlannedExamination.Text = "";
            // 
            // pnSplitter4
            // 
            this.pnSplitter4.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnSplitter4.Location = new System.Drawing.Point(8, 704);
            this.pnSplitter4.Name = "pnSplitter4";
            this.pnSplitter4.Size = new System.Drawing.Size(474, 8);
            this.pnSplitter4.TabIndex = 83;
            // 
            // pnPlannedTreatment
            // 
            this.pnPlannedTreatment.Controls.Add(this.pnChemTherapyToggle);
            this.pnPlannedTreatment.Controls.Add(this.pnSplitter3);
            this.pnPlannedTreatment.Controls.Add(this.label54);
            this.pnPlannedTreatment.Controls.Add(this.pnVMPToggle);
            this.pnPlannedTreatment.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnPlannedTreatment.Location = new System.Drawing.Point(8, 712);
            this.pnPlannedTreatment.Name = "pnPlannedTreatment";
            this.pnPlannedTreatment.Size = new System.Drawing.Size(474, 21);
            this.pnPlannedTreatment.TabIndex = 81;
            // 
            // pnChemTherapyToggle
            // 
            this.pnChemTherapyToggle.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.pnChemTherapyToggle.Controls.Add(this.cbChemTherapy);
            this.pnChemTherapyToggle.Controls.Add(this.pbChemHelp);
            this.pnChemTherapyToggle.Dock = System.Windows.Forms.DockStyle.Right;
            this.pnChemTherapyToggle.Location = new System.Drawing.Point(304, 0);
            this.pnChemTherapyToggle.Margin = new System.Windows.Forms.Padding(0);
            this.pnChemTherapyToggle.Name = "pnChemTherapyToggle";
            this.pnChemTherapyToggle.Size = new System.Drawing.Size(69, 21);
            this.pnChemTherapyToggle.TabIndex = 75;
            // 
            // cbChemTherapy
            // 
            this.cbChemTherapy.AutoSize = true;
            this.cbChemTherapy.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cbChemTherapy.Location = new System.Drawing.Point(0, 0);
            this.cbChemTherapy.Name = "cbChemTherapy";
            this.cbChemTherapy.Size = new System.Drawing.Size(51, 21);
            this.cbChemTherapy.TabIndex = 8;
            this.cbChemTherapy.Text = "ПХТ";
            this.cbChemTherapy.UseVisualStyleBackColor = true;
            // 
            // pbChemHelp
            // 
            this.pbChemHelp.BackgroundImage = global::FormsEditorEx.Properties.Resources.information;
            this.pbChemHelp.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.pbChemHelp.Dock = System.Windows.Forms.DockStyle.Right;
            this.pbChemHelp.Location = new System.Drawing.Point(51, 0);
            this.pbChemHelp.Name = "pbChemHelp";
            this.pbChemHelp.Size = new System.Drawing.Size(18, 21);
            this.pbChemHelp.TabIndex = 1;
            this.pbChemHelp.TabStop = false;
            // 
            // pnSplitter3
            // 
            this.pnSplitter3.Dock = System.Windows.Forms.DockStyle.Right;
            this.pnSplitter3.Location = new System.Drawing.Point(373, 0);
            this.pnSplitter3.Name = "pnSplitter3";
            this.pnSplitter3.Size = new System.Drawing.Size(24, 21);
            this.pnSplitter3.TabIndex = 56;
            // 
            // label54
            // 
            this.label54.Dock = System.Windows.Forms.DockStyle.Left;
            this.label54.Location = new System.Drawing.Point(0, 0);
            this.label54.Name = "label54";
            this.label54.Padding = new System.Windows.Forms.Padding(8, 0, 0, 0);
            this.label54.Size = new System.Drawing.Size(179, 21);
            this.label54.TabIndex = 55;
            this.label54.Text = "План лечения";
            this.label54.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // pnVMPToggle
            // 
            this.pnVMPToggle.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.pnVMPToggle.Controls.Add(this.cbVMP);
            this.pnVMPToggle.Controls.Add(this.pbVMPHelp);
            this.pnVMPToggle.Dock = System.Windows.Forms.DockStyle.Right;
            this.pnVMPToggle.Location = new System.Drawing.Point(397, 0);
            this.pnVMPToggle.Margin = new System.Windows.Forms.Padding(0);
            this.pnVMPToggle.Name = "pnVMPToggle";
            this.pnVMPToggle.Size = new System.Drawing.Size(77, 21);
            this.pnVMPToggle.TabIndex = 8;
            // 
            // cbVMP
            // 
            this.cbVMP.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cbVMP.Location = new System.Drawing.Point(0, 0);
            this.cbVMP.Name = "cbVMP";
            this.cbVMP.Size = new System.Drawing.Size(59, 21);
            this.cbVMP.TabIndex = 8;
            this.cbVMP.Text = "ВМП";
            this.cbVMP.UseVisualStyleBackColor = true;
            this.cbVMP.CheckedChanged += new System.EventHandler(this.cbVMP_CheckedChanged);
            this.cbVMP.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextBox_KeyDown);
            // 
            // pbVMPHelp
            // 
            this.pbVMPHelp.BackgroundImage = global::FormsEditorEx.Properties.Resources.information;
            this.pbVMPHelp.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.pbVMPHelp.Dock = System.Windows.Forms.DockStyle.Right;
            this.pbVMPHelp.Location = new System.Drawing.Point(59, 0);
            this.pbVMPHelp.Name = "pbVMPHelp";
            this.pbVMPHelp.Size = new System.Drawing.Size(18, 21);
            this.pbVMPHelp.TabIndex = 1;
            this.pbVMPHelp.TabStop = false;
            // 
            // rtbPlannedOperationScope
            // 
            this.rtbPlannedOperationScope.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.rtbPlannedOperationScope.ForeColor = System.Drawing.Color.Black;
            this.rtbPlannedOperationScope.Location = new System.Drawing.Point(8, 733);
            this.rtbPlannedOperationScope.Name = "rtbPlannedOperationScope";
            this.rtbPlannedOperationScope.PlainTextMode = true;
            this.rtbPlannedOperationScope.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.ForcedVertical;
            this.rtbPlannedOperationScope.ShowSelectionMargin = true;
            this.rtbPlannedOperationScope.Size = new System.Drawing.Size(474, 82);
            this.rtbPlannedOperationScope.TabIndex = 9;
            this.rtbPlannedOperationScope.Text = "";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.label29.Location = new System.Drawing.Point(8, 815);
            this.label29.Name = "label29";
            this.label29.Padding = new System.Windows.Forms.Padding(8, 4, 0, 0);
            this.label29.Size = new System.Drawing.Size(91, 22);
            this.label29.TabIndex = 80;
            this.label29.Text = "Назначения";
            // 
            // rtbAssignments
            // 
            this.rtbAssignments.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.rtbAssignments.ForeColor = System.Drawing.Color.Black;
            this.rtbAssignments.Location = new System.Drawing.Point(8, 837);
            this.rtbAssignments.Name = "rtbAssignments";
            this.rtbAssignments.PlainTextMode = true;
            this.rtbAssignments.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.ForcedVertical;
            this.rtbAssignments.ShowSelectionMargin = true;
            this.rtbAssignments.Size = new System.Drawing.Size(474, 82);
            this.rtbAssignments.TabIndex = 10;
            this.rtbAssignments.Text = "";
            // 
            // pnAnamnesis
            // 
            this.pnAnamnesis.Controls.Add(this.btAnamnesis);
            this.pnAnamnesis.Controls.Add(this.pnSplitter1);
            this.pnAnamnesis.Controls.Add(this.btLifeAnamnesis);
            this.pnAnamnesis.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnAnamnesis.Location = new System.Drawing.Point(8, 118);
            this.pnAnamnesis.Name = "pnAnamnesis";
            this.pnAnamnesis.Size = new System.Drawing.Size(474, 38);
            this.pnAnamnesis.TabIndex = 78;
            // 
            // btAnamnesis
            // 
            this.btAnamnesis.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btAnamnesis.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btAnamnesis.Location = new System.Drawing.Point(0, 0);
            this.btAnamnesis.Margin = new System.Windows.Forms.Padding(3, 8, 3, 3);
            this.btAnamnesis.Name = "btAnamnesis";
            this.btAnamnesis.Size = new System.Drawing.Size(249, 38);
            this.btAnamnesis.TabIndex = 2;
            this.btAnamnesis.Text = "Анамнез заболевания";
            this.btAnamnesis.UseVisualStyleBackColor = true;
            this.btAnamnesis.Click += new System.EventHandler(this.btAnamnesis_Click);
            // 
            // pnSplitter1
            // 
            this.pnSplitter1.Dock = System.Windows.Forms.DockStyle.Right;
            this.pnSplitter1.Location = new System.Drawing.Point(249, 0);
            this.pnSplitter1.Name = "pnSplitter1";
            this.pnSplitter1.Size = new System.Drawing.Size(8, 38);
            this.pnSplitter1.TabIndex = 76;
            // 
            // btLifeAnamnesis
            // 
            this.btLifeAnamnesis.Dock = System.Windows.Forms.DockStyle.Right;
            this.btLifeAnamnesis.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btLifeAnamnesis.Location = new System.Drawing.Point(257, 0);
            this.btLifeAnamnesis.Name = "btLifeAnamnesis";
            this.btLifeAnamnesis.Size = new System.Drawing.Size(217, 38);
            this.btLifeAnamnesis.TabIndex = 3;
            this.btLifeAnamnesis.Text = "Общий осмотр";
            this.btLifeAnamnesis.UseVisualStyleBackColor = true;
            this.btLifeAnamnesis.Click += new System.EventHandler(this.btLifeAnamnesis_Click);
            // 
            // pnSplitter2
            // 
            this.pnSplitter2.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnSplitter2.Location = new System.Drawing.Point(8, 110);
            this.pnSplitter2.Name = "pnSplitter2";
            this.pnSplitter2.Size = new System.Drawing.Size(474, 8);
            this.pnSplitter2.TabIndex = 77;
            // 
            // rtbComplaints
            // 
            this.rtbComplaints.Cue = "";
            this.rtbComplaints.Dock = System.Windows.Forms.DockStyle.Top;
            this.rtbComplaints.Location = new System.Drawing.Point(8, 45);
            this.rtbComplaints.Name = "rtbComplaints";
            this.rtbComplaints.PlainTextMode = true;
            this.rtbComplaints.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.ForcedVertical;
            this.rtbComplaints.ShowSelectionMargin = true;
            this.rtbComplaints.Size = new System.Drawing.Size(474, 65);
            this.rtbComplaints.TabIndex = 1;
            this.rtbComplaints.Text = "";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Dock = System.Windows.Forms.DockStyle.Top;
            this.label10.Location = new System.Drawing.Point(8, 23);
            this.label10.Name = "label10";
            this.label10.Padding = new System.Windows.Forms.Padding(8, 4, 0, 0);
            this.label10.Size = new System.Drawing.Size(69, 22);
            this.label10.TabIndex = 74;
            this.label10.Text = "Жалобы";
            this.label10.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // btOperTeamNeuro
            // 
            this.btOperTeamNeuro.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btOperTeamNeuro.Location = new System.Drawing.Point(7, 492);
            this.btOperTeamNeuro.Name = "btOperTeamNeuro";
            this.btOperTeamNeuro.Size = new System.Drawing.Size(400, 38);
            this.btOperTeamNeuro.TabIndex = 7;
            this.btOperTeamNeuro.Text = "Операционная бригада";
            this.btOperTeamNeuro.UseVisualStyleBackColor = true;
            this.btOperTeamNeuro.Click += new System.EventHandler(this.btOperTeamNeuro_Click);
            // 
            // btConclusions
            // 
            this.btConclusions.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btConclusions.Location = new System.Drawing.Point(8, 15);
            this.btConclusions.Name = "btConclusions";
            this.btConclusions.Size = new System.Drawing.Size(398, 38);
            this.btConclusions.TabIndex = 1;
            this.btConclusions.Text = "Морфологические исследования";
            this.btConclusions.UseVisualStyleBackColor = true;
            this.btConclusions.Click += new System.EventHandler(this.btConclusions_Click);
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Dock = System.Windows.Forms.DockStyle.Top;
            this.label42.Location = new System.Drawing.Point(8, 130);
            this.label42.Name = "label42";
            this.label42.Padding = new System.Windows.Forms.Padding(8, 4, 0, 0);
            this.label42.Size = new System.Drawing.Size(278, 22);
            this.label42.TabIndex = 53;
            this.label42.Text = "Пo сопутствующей патологии принимает";
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Dock = System.Windows.Forms.DockStyle.Top;
            this.label36.Location = new System.Drawing.Point(8, 23);
            this.label36.Name = "label36";
            this.label36.Padding = new System.Windows.Forms.Padding(8, 4, 0, 0);
            this.label36.Size = new System.Drawing.Size(170, 22);
            this.label36.TabIndex = 40;
            this.label36.Text = "Сопутствующий диагноз";
            // 
            // label14
            // 
            this.label14.Location = new System.Drawing.Point(13, 85);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(86, 27);
            this.label14.TabIndex = 37;
            this.label14.Text = "Питание";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label13
            // 
            this.label13.Location = new System.Drawing.Point(13, 52);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(86, 27);
            this.label13.TabIndex = 35;
            this.label13.Text = "Состояние";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // btGenExam
            // 
            this.btGenExam.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btGenExam.Location = new System.Drawing.Point(8, 147);
            this.btGenExam.Name = "btGenExam";
            this.btGenExam.Size = new System.Drawing.Size(398, 38);
            this.btGenExam.TabIndex = 4;
            this.btGenExam.Text = "Клинико-лабораторные данные";
            this.btGenExam.UseVisualStyleBackColor = true;
            this.btGenExam.Click += new System.EventHandler(this.btGenExamForm_Click);
            // 
            // tbDateExam
            // 
            this.tbDateExam.Culture = new System.Globalization.CultureInfo("");
            this.tbDateExam.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.tbDateExam.Location = new System.Drawing.Point(105, 20);
            this.tbDateExam.Mask = "00.00.0000";
            this.tbDateExam.Name = "tbDateExam";
            this.tbDateExam.Size = new System.Drawing.Size(94, 26);
            this.tbDateExam.TabIndex = 1;
            this.tbDateExam.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.tbDateExam.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TextBox_KeyPress);
            this.tbDateExam.Validating += new System.ComponentModel.CancelEventHandler(this.DateTextBox_Validating);
            // 
            // label39
            // 
            this.label39.Location = new System.Drawing.Point(13, 20);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(86, 26);
            this.label39.TabIndex = 47;
            this.label39.Text = "Дата/время";
            this.label39.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // btTestData
            // 
            this.btTestData.Location = new System.Drawing.Point(828, 10);
            this.btTestData.Name = "btTestData";
            this.btTestData.Size = new System.Drawing.Size(72, 32);
            this.btTestData.TabIndex = 71;
            this.btTestData.TabStop = false;
            this.btTestData.Text = "TestData";
            this.btTestData.UseVisualStyleBackColor = true;
            this.btTestData.Click += new System.EventHandler(this.btTestData_Click);
            // 
            // tbAmbuCardNum
            // 
            this.tbAmbuCardNum.AcceptsReturn = true;
            this.tbAmbuCardNum.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.tbAmbuCardNum.Location = new System.Drawing.Point(154, 42);
            this.tbAmbuCardNum.Name = "tbAmbuCardNum";
            this.tbAmbuCardNum.Size = new System.Drawing.Size(120, 26);
            this.tbAmbuCardNum.TabIndex = 2;
            this.tbAmbuCardNum.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.tbAmbuCardNum.TextChanged += new System.EventHandler(this.tbAmbuCardNum_TextChanged);
            this.tbAmbuCardNum.Enter += new System.EventHandler(this.tbAmbuCardNum_Enter);
            this.tbAmbuCardNum.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextBox_KeyDown);
            this.tbAmbuCardNum.Leave += new System.EventHandler(this.tbAmbuCardNum_Leave);
            // 
            // tbClinicalRecordNum
            // 
            this.tbClinicalRecordNum.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.tbClinicalRecordNum.Location = new System.Drawing.Point(154, 9);
            this.tbClinicalRecordNum.Name = "tbClinicalRecordNum";
            this.tbClinicalRecordNum.Size = new System.Drawing.Size(120, 26);
            this.tbClinicalRecordNum.TabIndex = 1;
            this.tbClinicalRecordNum.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.tbClinicalRecordNum.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextBox_KeyDown);
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label5.Location = new System.Drawing.Point(2, 41);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(140, 26);
            this.label5.TabIndex = 77;
            this.label5.Text = "Амбулат. карта №";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label6
            // 
            this.label6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label6.Location = new System.Drawing.Point(2, 9);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(140, 26);
            this.label6.TabIndex = 76;
            this.label6.Text = "История болезни №";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // btTest
            // 
            this.btTest.Location = new System.Drawing.Point(911, 10);
            this.btTest.Name = "btTest";
            this.btTest.Size = new System.Drawing.Size(62, 32);
            this.btTest.TabIndex = 79;
            this.btTest.TabStop = false;
            this.btTest.Text = "Test";
            this.btTest.UseVisualStyleBackColor = true;
            this.btTest.Click += new System.EventHandler(this.btTest_Click);
            // 
            // groupBox9
            // 
            this.groupBox9.BackColor = System.Drawing.SystemColors.Control;
            this.groupBox9.Controls.Add(this.btDisabilityListInfo);
            this.groupBox9.Controls.Add(this.label9);
            this.groupBox9.Controls.Add(this.cbDisability);
            this.groupBox9.Controls.Add(this.cbRh);
            this.groupBox9.Controls.Add(this.label53);
            this.groupBox9.Controls.Add(this.cbBloodGroup);
            this.groupBox9.Controls.Add(this.label52);
            this.groupBox9.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox9.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.groupBox9.Location = new System.Drawing.Point(8, 612);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Size = new System.Drawing.Size(283, 169);
            this.groupBox9.TabIndex = 5;
            this.groupBox9.TabStop = false;
            // 
            // btDisabilityListInfo
            // 
            this.btDisabilityListInfo.Location = new System.Drawing.Point(8, 119);
            this.btDisabilityListInfo.Name = "btDisabilityListInfo";
            this.btDisabilityListInfo.Size = new System.Drawing.Size(267, 40);
            this.btDisabilityListInfo.TabIndex = 4;
            this.btDisabilityListInfo.TabStop = false;
            this.btDisabilityListInfo.Text = "Свед. о листке нетрудоспособности";
            this.btDisabilityListInfo.UseVisualStyleBackColor = true;
            this.btDisabilityListInfo.Click += new System.EventHandler(this.btDisabilityInfo_Click);
            // 
            // label9
            // 
            this.label9.Location = new System.Drawing.Point(19, 86);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(103, 27);
            this.label9.TabIndex = 81;
            this.label9.Text = "Инвалидность";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cbDisability
            // 
            this.cbDisability.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cbDisability.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbDisability.FormattingEnabled = true;
            this.cbDisability.Items.AddRange(new object[] {
            "(+) полож.",
            "(-) отриц."});
            this.cbDisability.Location = new System.Drawing.Point(134, 86);
            this.cbDisability.Name = "cbDisability";
            this.cbDisability.ReadOnly = false;
            this.cbDisability.Size = new System.Drawing.Size(140, 27);
            this.cbDisability.TabIndex = 3;
            this.cbDisability.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextBox_KeyDown);
            // 
            // cbRh
            // 
            this.cbRh.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cbRh.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbRh.FormattingEnabled = true;
            this.cbRh.Items.AddRange(new object[] {
            "(+) полож.",
            "(-) отриц."});
            this.cbRh.Location = new System.Drawing.Point(134, 53);
            this.cbRh.Name = "cbRh";
            this.cbRh.ReadOnly = false;
            this.cbRh.Size = new System.Drawing.Size(140, 27);
            this.cbRh.TabIndex = 2;
            this.cbRh.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextBox_KeyDown);
            // 
            // label53
            // 
            this.label53.Location = new System.Drawing.Point(19, 53);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(103, 27);
            this.label53.TabIndex = 79;
            this.label53.Text = "Резус-фактор";
            this.label53.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cbBloodGroup
            // 
            this.cbBloodGroup.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cbBloodGroup.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbBloodGroup.FormattingEnabled = true;
            this.cbBloodGroup.Items.AddRange(new object[] {
            "0 (I)",
            "A (II)",
            "B (III)",
            "AB (IV)"});
            this.cbBloodGroup.Location = new System.Drawing.Point(134, 20);
            this.cbBloodGroup.Name = "cbBloodGroup";
            this.cbBloodGroup.ReadOnly = false;
            this.cbBloodGroup.Size = new System.Drawing.Size(140, 27);
            this.cbBloodGroup.TabIndex = 1;
            this.cbBloodGroup.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextBox_KeyDown);
            // 
            // label52
            // 
            this.label52.Location = new System.Drawing.Point(19, 20);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(103, 27);
            this.label52.TabIndex = 77;
            this.label52.Text = "Группа крови";
            this.label52.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // groupBox10
            // 
            this.groupBox10.BackColor = System.Drawing.SystemColors.Control;
            this.groupBox10.Controls.Add(this.cbGenCondition);
            this.groupBox10.Controls.Add(this.cbNutrition);
            this.groupBox10.Controls.Add(this.label14);
            this.groupBox10.Controls.Add(this.tbTimeExam);
            this.groupBox10.Controls.Add(this.label13);
            this.groupBox10.Controls.Add(this.label39);
            this.groupBox10.Controls.Add(this.tbDateExam);
            this.groupBox10.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox10.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.groupBox10.Location = new System.Drawing.Point(8, 488);
            this.groupBox10.Name = "groupBox10";
            this.groupBox10.Size = new System.Drawing.Size(283, 124);
            this.groupBox10.TabIndex = 4;
            this.groupBox10.TabStop = false;
            // 
            // cbGenCondition
            // 
            this.cbGenCondition.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cbGenCondition.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbGenCondition.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.cbGenCondition.FormattingEnabled = true;
            this.cbGenCondition.Items.AddRange(new object[] {
            "удовлетворительное",
            "средней ст. тяжести",
            "тяжелое",
            "крайне тяжелое"});
            this.cbGenCondition.Location = new System.Drawing.Point(105, 52);
            this.cbGenCondition.Name = "cbGenCondition";
            this.cbGenCondition.ReadOnly = false;
            this.cbGenCondition.Size = new System.Drawing.Size(169, 27);
            this.cbGenCondition.TabIndex = 3;
            this.cbGenCondition.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextBox_KeyDown);
            // 
            // cbNutrition
            // 
            this.cbNutrition.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cbNutrition.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbNutrition.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.cbNutrition.FormattingEnabled = true;
            this.cbNutrition.Items.AddRange(new object[] {
            "избыточное",
            "достаточное",
            "кахексия"});
            this.cbNutrition.Location = new System.Drawing.Point(105, 85);
            this.cbNutrition.Name = "cbNutrition";
            this.cbNutrition.ReadOnly = false;
            this.cbNutrition.Size = new System.Drawing.Size(169, 27);
            this.cbNutrition.TabIndex = 4;
            this.cbNutrition.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextBox_KeyDown);
            // 
            // tbTimeExam
            // 
            this.tbTimeExam.Culture = new System.Globalization.CultureInfo("");
            this.tbTimeExam.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.tbTimeExam.Location = new System.Drawing.Point(205, 20);
            this.tbTimeExam.Mask = "00:00";
            this.tbTimeExam.Name = "tbTimeExam";
            this.tbTimeExam.Size = new System.Drawing.Size(69, 26);
            this.tbTimeExam.TabIndex = 2;
            this.tbTimeExam.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.tbTimeExam.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TextBox_KeyPress);
            this.tbTimeExam.Validating += new System.ComponentModel.CancelEventHandler(this.TimeTextBox_Validating);
            // 
            // groupBox5
            // 
            this.groupBox5.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.groupBox5.Controls.Add(this.rtbTreatment);
            this.groupBox5.Controls.Add(this.panel3);
            this.groupBox5.Controls.Add(this.label21);
            this.groupBox5.Controls.Add(this.rtbMedication);
            this.groupBox5.Controls.Add(this.label22);
            this.groupBox5.Controls.Add(this.rtbAccompanyingPost);
            this.groupBox5.Controls.Add(this.panel1);
            this.groupBox5.Controls.Add(this.rtbComplicationsPost);
            this.groupBox5.Controls.Add(this.pnComplications);
            this.groupBox5.Controls.Add(this.rtbDiagnosisPost);
            this.groupBox5.Controls.Add(this.pnDiagnosisHeader);
            this.groupBox5.Controls.Add(this.pnMiscInfo);
            this.groupBox5.Controls.Add(this.pnResult);
            this.groupBox5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox5.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.groupBox5.ForeColor = System.Drawing.Color.Black;
            this.groupBox5.Location = new System.Drawing.Point(4, 0);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Padding = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.groupBox5.Size = new System.Drawing.Size(677, 928);
            this.groupBox5.TabIndex = 11;
            this.groupBox5.TabStop = false;
            // 
            // rtbTreatment
            // 
            this.rtbTreatment.Cue = "не проводились.";
            this.rtbTreatment.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rtbTreatment.Location = new System.Drawing.Point(8, 447);
            this.rtbTreatment.Name = "rtbTreatment";
            this.rtbTreatment.PlainTextMode = true;
            this.rtbTreatment.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.ForcedVertical;
            this.rtbTreatment.ShowSelectionMargin = true;
            this.rtbTreatment.Size = new System.Drawing.Size(661, 124);
            this.rtbTreatment.TabIndex = 8;
            this.rtbTreatment.Text = "";
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.btTransfusion);
            this.panel3.Controls.Add(this.panel4);
            this.panel3.Controls.Add(this.btStatusLocalisPost);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel3.Location = new System.Drawing.Point(8, 571);
            this.panel3.Name = "panel3";
            this.panel3.Padding = new System.Windows.Forms.Padding(0, 8, 0, 0);
            this.panel3.Size = new System.Drawing.Size(661, 50);
            this.panel3.TabIndex = 8;
            // 
            // btTransfusion
            // 
            this.btTransfusion.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btTransfusion.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btTransfusion.Location = new System.Drawing.Point(0, 8);
            this.btTransfusion.Name = "btTransfusion";
            this.btTransfusion.Size = new System.Drawing.Size(317, 42);
            this.btTransfusion.TabIndex = 8;
            this.btTransfusion.TabStop = false;
            this.btTransfusion.Text = "Трансфузии";
            this.btTransfusion.UseVisualStyleBackColor = true;
            this.btTransfusion.Click += new System.EventHandler(this.btTransfusion_Click);
            // 
            // panel4
            // 
            this.panel4.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel4.Location = new System.Drawing.Point(317, 8);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(10, 42);
            this.panel4.TabIndex = 108;
            // 
            // btStatusLocalisPost
            // 
            this.btStatusLocalisPost.Dock = System.Windows.Forms.DockStyle.Right;
            this.btStatusLocalisPost.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btStatusLocalisPost.Location = new System.Drawing.Point(327, 8);
            this.btStatusLocalisPost.Name = "btStatusLocalisPost";
            this.btStatusLocalisPost.Size = new System.Drawing.Size(334, 42);
            this.btStatusLocalisPost.TabIndex = 8;
            this.btStatusLocalisPost.TabStop = false;
            this.btStatusLocalisPost.Text = "Status Localis";
            this.btStatusLocalisPost.UseVisualStyleBackColor = true;
            this.btStatusLocalisPost.Click += new System.EventHandler(this.btStatusLocalisPost_Click);
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Dock = System.Windows.Forms.DockStyle.Top;
            this.label21.Location = new System.Drawing.Point(8, 421);
            this.label21.Name = "label21";
            this.label21.Padding = new System.Windows.Forms.Padding(8, 8, 0, 0);
            this.label21.Size = new System.Drawing.Size(306, 26);
            this.label21.TabIndex = 112;
            this.label21.Text = "Оперативные и медицинские вмешательства";
            // 
            // rtbMedication
            // 
            this.rtbMedication.Cue = "не применялись.";
            this.rtbMedication.Dock = System.Windows.Forms.DockStyle.Top;
            this.rtbMedication.Location = new System.Drawing.Point(8, 322);
            this.rtbMedication.Name = "rtbMedication";
            this.rtbMedication.PlainTextMode = true;
            this.rtbMedication.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.ForcedVertical;
            this.rtbMedication.ShowSelectionMargin = true;
            this.rtbMedication.Size = new System.Drawing.Size(661, 99);
            this.rtbMedication.TabIndex = 7;
            this.rtbMedication.Text = "";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Dock = System.Windows.Forms.DockStyle.Top;
            this.label22.Location = new System.Drawing.Point(8, 296);
            this.label22.Name = "label22";
            this.label22.Padding = new System.Windows.Forms.Padding(8, 8, 0, 0);
            this.label22.Size = new System.Drawing.Size(356, 26);
            this.label22.TabIndex = 114;
            this.label22.Text = "Лекарственные препараты (химиотерапия и прочие)";
            // 
            // rtbAccompanyingPost
            // 
            this.rtbAccompanyingPost.Cue = "нет.";
            this.rtbAccompanyingPost.Dock = System.Windows.Forms.DockStyle.Top;
            this.rtbAccompanyingPost.ForeColor = System.Drawing.Color.Black;
            this.rtbAccompanyingPost.Location = new System.Drawing.Point(8, 239);
            this.rtbAccompanyingPost.Name = "rtbAccompanyingPost";
            this.rtbAccompanyingPost.PlainTextMode = true;
            this.rtbAccompanyingPost.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.ForcedVertical;
            this.rtbAccompanyingPost.ShowSelectionMargin = true;
            this.rtbAccompanyingPost.Size = new System.Drawing.Size(661, 57);
            this.rtbAccompanyingPost.TabIndex = 5;
            this.rtbAccompanyingPost.Text = "";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.label32);
            this.panel1.Controls.Add(this.tbMKBAccompanying);
            this.panel1.Controls.Add(this.label35);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(8, 213);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(661, 26);
            this.panel1.TabIndex = 5;
            // 
            // label32
            // 
            this.label32.Dock = System.Windows.Forms.DockStyle.Right;
            this.label32.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label32.Location = new System.Drawing.Point(482, 0);
            this.label32.Name = "label32";
            this.label32.Padding = new System.Windows.Forms.Padding(8, 0, 0, 0);
            this.label32.Size = new System.Drawing.Size(79, 26);
            this.label32.TabIndex = 102;
            this.label32.Text = "Код МКБ  ";
            this.label32.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tbMKBAccompanying
            // 
            this.tbMKBAccompanying.Cue = null;
            this.tbMKBAccompanying.Dock = System.Windows.Forms.DockStyle.Right;
            this.tbMKBAccompanying.Location = new System.Drawing.Point(561, 0);
            this.tbMKBAccompanying.Name = "tbMKBAccompanying";
            this.tbMKBAccompanying.Size = new System.Drawing.Size(100, 26);
            this.tbMKBAccompanying.TabIndex = 6;
            this.tbMKBAccompanying.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label35
            // 
            this.label35.Dock = System.Windows.Forms.DockStyle.Left;
            this.label35.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label35.Location = new System.Drawing.Point(0, 0);
            this.label35.Name = "label35";
            this.label35.Padding = new System.Windows.Forms.Padding(8, 0, 0, 0);
            this.label35.Size = new System.Drawing.Size(183, 26);
            this.label35.TabIndex = 99;
            this.label35.Text = "Сопутствующий диагноз";
            this.label35.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // rtbComplicationsPost
            // 
            this.rtbComplicationsPost.Cue = "отсутствуют.";
            this.rtbComplicationsPost.Dock = System.Windows.Forms.DockStyle.Top;
            this.rtbComplicationsPost.ForeColor = System.Drawing.Color.Black;
            this.rtbComplicationsPost.Location = new System.Drawing.Point(8, 156);
            this.rtbComplicationsPost.Name = "rtbComplicationsPost";
            this.rtbComplicationsPost.PlainTextMode = true;
            this.rtbComplicationsPost.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.ForcedVertical;
            this.rtbComplicationsPost.ShowSelectionMargin = true;
            this.rtbComplicationsPost.Size = new System.Drawing.Size(661, 57);
            this.rtbComplicationsPost.TabIndex = 3;
            this.rtbComplicationsPost.Text = "";
            // 
            // pnComplications
            // 
            this.pnComplications.Controls.Add(this.label25);
            this.pnComplications.Controls.Add(this.tbMKBComplications);
            this.pnComplications.Controls.Add(this.label26);
            this.pnComplications.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnComplications.Location = new System.Drawing.Point(8, 130);
            this.pnComplications.Name = "pnComplications";
            this.pnComplications.Size = new System.Drawing.Size(661, 26);
            this.pnComplications.TabIndex = 3;
            // 
            // label25
            // 
            this.label25.Dock = System.Windows.Forms.DockStyle.Right;
            this.label25.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label25.Location = new System.Drawing.Point(482, 0);
            this.label25.Name = "label25";
            this.label25.Padding = new System.Windows.Forms.Padding(8, 0, 0, 0);
            this.label25.Size = new System.Drawing.Size(79, 26);
            this.label25.TabIndex = 102;
            this.label25.Text = "Код МКБ  ";
            this.label25.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tbMKBComplications
            // 
            this.tbMKBComplications.Cue = null;
            this.tbMKBComplications.Dock = System.Windows.Forms.DockStyle.Right;
            this.tbMKBComplications.Location = new System.Drawing.Point(561, 0);
            this.tbMKBComplications.Name = "tbMKBComplications";
            this.tbMKBComplications.Size = new System.Drawing.Size(100, 26);
            this.tbMKBComplications.TabIndex = 4;
            this.tbMKBComplications.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label26
            // 
            this.label26.Dock = System.Windows.Forms.DockStyle.Left;
            this.label26.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label26.Location = new System.Drawing.Point(0, 0);
            this.label26.Name = "label26";
            this.label26.Padding = new System.Windows.Forms.Padding(8, 0, 0, 0);
            this.label26.Size = new System.Drawing.Size(132, 26);
            this.label26.TabIndex = 99;
            this.label26.Text = "Осложнения";
            this.label26.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // rtbDiagnosisPost
            // 
            this.rtbDiagnosisPost.BackColor = System.Drawing.SystemColors.Window;
            this.rtbDiagnosisPost.Dock = System.Windows.Forms.DockStyle.Top;
            this.rtbDiagnosisPost.ForeColor = System.Drawing.Color.Black;
            this.rtbDiagnosisPost.Location = new System.Drawing.Point(8, 45);
            this.rtbDiagnosisPost.Name = "rtbDiagnosisPost";
            this.rtbDiagnosisPost.PlainTextMode = true;
            this.rtbDiagnosisPost.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.ForcedVertical;
            this.rtbDiagnosisPost.ShowSelectionMargin = true;
            this.rtbDiagnosisPost.Size = new System.Drawing.Size(661, 85);
            this.rtbDiagnosisPost.TabIndex = 1;
            this.rtbDiagnosisPost.Text = "";
            // 
            // pnDiagnosisHeader
            // 
            this.pnDiagnosisHeader.Controls.Add(this.label24);
            this.pnDiagnosisHeader.Controls.Add(this.tbMKBDiagnosis);
            this.pnDiagnosisHeader.Controls.Add(this.label55);
            this.pnDiagnosisHeader.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnDiagnosisHeader.Location = new System.Drawing.Point(8, 19);
            this.pnDiagnosisHeader.Name = "pnDiagnosisHeader";
            this.pnDiagnosisHeader.Size = new System.Drawing.Size(661, 26);
            this.pnDiagnosisHeader.TabIndex = 1;
            // 
            // label24
            // 
            this.label24.Dock = System.Windows.Forms.DockStyle.Right;
            this.label24.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label24.Location = new System.Drawing.Point(482, 0);
            this.label24.Name = "label24";
            this.label24.Padding = new System.Windows.Forms.Padding(8, 0, 0, 0);
            this.label24.Size = new System.Drawing.Size(79, 26);
            this.label24.TabIndex = 102;
            this.label24.Text = "Код МКБ  ";
            this.label24.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tbMKBDiagnosis
            // 
            this.tbMKBDiagnosis.Cue = null;
            this.tbMKBDiagnosis.Dock = System.Windows.Forms.DockStyle.Right;
            this.tbMKBDiagnosis.Location = new System.Drawing.Point(561, 0);
            this.tbMKBDiagnosis.Name = "tbMKBDiagnosis";
            this.tbMKBDiagnosis.Size = new System.Drawing.Size(100, 26);
            this.tbMKBDiagnosis.TabIndex = 2;
            this.tbMKBDiagnosis.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label55
            // 
            this.label55.Dock = System.Windows.Forms.DockStyle.Left;
            this.label55.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label55.Location = new System.Drawing.Point(0, 0);
            this.label55.Name = "label55";
            this.label55.Padding = new System.Windows.Forms.Padding(8, 0, 0, 0);
            this.label55.Size = new System.Drawing.Size(132, 26);
            this.label55.TabIndex = 99;
            this.label55.Text = "Основной диагноз";
            this.label55.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // pnMiscInfo
            // 
            this.pnMiscInfo.Controls.Add(this.btLifeAnamnesisPost);
            this.pnMiscInfo.Controls.Add(this.pnMiscSplitter1);
            this.pnMiscInfo.Controls.Add(this.btAdditionalInfo);
            this.pnMiscInfo.Controls.Add(this.pnMiscSplitter2);
            this.pnMiscInfo.Controls.Add(this.btRecommendations);
            this.pnMiscInfo.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnMiscInfo.Location = new System.Drawing.Point(8, 621);
            this.pnMiscInfo.Name = "pnMiscInfo";
            this.pnMiscInfo.Padding = new System.Windows.Forms.Padding(0, 8, 0, 4);
            this.pnMiscInfo.Size = new System.Drawing.Size(661, 50);
            this.pnMiscInfo.TabIndex = 8;
            // 
            // btLifeAnamnesisPost
            // 
            this.btLifeAnamnesisPost.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btLifeAnamnesisPost.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btLifeAnamnesisPost.Location = new System.Drawing.Point(0, 8);
            this.btLifeAnamnesisPost.Name = "btLifeAnamnesisPost";
            this.btLifeAnamnesisPost.Size = new System.Drawing.Size(317, 38);
            this.btLifeAnamnesisPost.TabIndex = 8;
            this.btLifeAnamnesisPost.TabStop = false;
            this.btLifeAnamnesisPost.Text = "Состояние при выписке";
            this.btLifeAnamnesisPost.UseVisualStyleBackColor = true;
            this.btLifeAnamnesisPost.Click += new System.EventHandler(this.btLifeAnamnesisPost_Click);
            // 
            // pnMiscSplitter1
            // 
            this.pnMiscSplitter1.Dock = System.Windows.Forms.DockStyle.Right;
            this.pnMiscSplitter1.Location = new System.Drawing.Point(317, 8);
            this.pnMiscSplitter1.Name = "pnMiscSplitter1";
            this.pnMiscSplitter1.Size = new System.Drawing.Size(10, 38);
            this.pnMiscSplitter1.TabIndex = 108;
            // 
            // btAdditionalInfo
            // 
            this.btAdditionalInfo.Dock = System.Windows.Forms.DockStyle.Right;
            this.btAdditionalInfo.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btAdditionalInfo.Location = new System.Drawing.Point(327, 8);
            this.btAdditionalInfo.Name = "btAdditionalInfo";
            this.btAdditionalInfo.Size = new System.Drawing.Size(162, 38);
            this.btAdditionalInfo.TabIndex = 8;
            this.btAdditionalInfo.TabStop = false;
            this.btAdditionalInfo.Text = "Доп. сведения";
            this.btAdditionalInfo.UseVisualStyleBackColor = true;
            this.btAdditionalInfo.Click += new System.EventHandler(this.btAdditionalInfo_Click);
            // 
            // pnMiscSplitter2
            // 
            this.pnMiscSplitter2.Dock = System.Windows.Forms.DockStyle.Right;
            this.pnMiscSplitter2.Location = new System.Drawing.Point(489, 8);
            this.pnMiscSplitter2.Name = "pnMiscSplitter2";
            this.pnMiscSplitter2.Size = new System.Drawing.Size(10, 38);
            this.pnMiscSplitter2.TabIndex = 109;
            // 
            // btRecommendations
            // 
            this.btRecommendations.Dock = System.Windows.Forms.DockStyle.Right;
            this.btRecommendations.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btRecommendations.Location = new System.Drawing.Point(499, 8);
            this.btRecommendations.Name = "btRecommendations";
            this.btRecommendations.Size = new System.Drawing.Size(162, 38);
            this.btRecommendations.TabIndex = 8;
            this.btRecommendations.TabStop = false;
            this.btRecommendations.Text = "Рекомендации";
            this.btRecommendations.UseVisualStyleBackColor = true;
            this.btRecommendations.Click += new System.EventHandler(this.btRecommendations_Click);
            // 
            // pnResult
            // 
            this.pnResult.Controls.Add(this.pnCenteredResult);
            this.pnResult.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnResult.Location = new System.Drawing.Point(8, 671);
            this.pnResult.Name = "pnResult";
            this.pnResult.Size = new System.Drawing.Size(661, 257);
            this.pnResult.TabIndex = 25;
            // 
            // pnCenteredResult
            // 
            this.pnCenteredResult.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pnCenteredResult.Controls.Add(this.label59);
            this.pnCenteredResult.Controls.Add(this.cbWorkCapacity);
            this.pnCenteredResult.Controls.Add(this.cbTreatmentResult);
            this.pnCenteredResult.Controls.Add(this.label11);
            this.pnCenteredResult.Controls.Add(this.label58);
            this.pnCenteredResult.Controls.Add(this.cbCondition);
            this.pnCenteredResult.Controls.Add(this.cbSickLeaveEndType);
            this.pnCenteredResult.Controls.Add(this.tbDateDischarge);
            this.pnCenteredResult.Controls.Add(this.label57);
            this.pnCenteredResult.Controls.Add(this.cbSickLeave);
            this.pnCenteredResult.Controls.Add(this.tbSickLeaveNum);
            this.pnCenteredResult.Controls.Add(this.lbSickLeave1);
            this.pnCenteredResult.Controls.Add(this.tbSickLeaveStart);
            this.pnCenteredResult.Controls.Add(this.cbSickLeaveExt);
            this.pnCenteredResult.Controls.Add(this.tbSickLeaveEnd);
            this.pnCenteredResult.Controls.Add(this.lbSickLeave4);
            this.pnCenteredResult.Controls.Add(this.lbSickLeave2);
            this.pnCenteredResult.Controls.Add(this.tbSickLeaveExtEnd);
            this.pnCenteredResult.Controls.Add(this.lbSickLeave3);
            this.pnCenteredResult.Controls.Add(this.tbSickLeaveExtStart);
            this.pnCenteredResult.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.pnCenteredResult.Location = new System.Drawing.Point(77, 0);
            this.pnCenteredResult.Name = "pnCenteredResult";
            this.pnCenteredResult.Size = new System.Drawing.Size(549, 257);
            this.pnCenteredResult.TabIndex = 25;
            // 
            // label59
            // 
            this.label59.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label59.Location = new System.Drawing.Point(40, 13);
            this.label59.Name = "label59";
            this.label59.Size = new System.Drawing.Size(87, 27);
            this.label59.TabIndex = 63;
            this.label59.Text = "Результат";
            this.label59.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cbWorkCapacity
            // 
            this.cbWorkCapacity.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cbWorkCapacity.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cbWorkCapacity.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbWorkCapacity.FormattingEnabled = true;
            this.cbWorkCapacity.Items.AddRange(new object[] {
            "удовлетворительное",
            "средней ст. тяжести",
            "тяжелое"});
            this.cbWorkCapacity.Location = new System.Drawing.Point(133, 79);
            this.cbWorkCapacity.Name = "cbWorkCapacity";
            this.cbWorkCapacity.ReadOnly = false;
            this.cbWorkCapacity.Size = new System.Drawing.Size(193, 27);
            this.cbWorkCapacity.TabIndex = 16;
            this.cbWorkCapacity.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextBox_KeyDown);
            // 
            // cbTreatmentResult
            // 
            this.cbTreatmentResult.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cbTreatmentResult.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cbTreatmentResult.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbTreatmentResult.FormattingEnabled = true;
            this.cbTreatmentResult.Items.AddRange(new object[] {
            "выздоровление",
            "улучшение",
            "ремиссия",
            "без изменений"});
            this.cbTreatmentResult.Location = new System.Drawing.Point(133, 13);
            this.cbTreatmentResult.Name = "cbTreatmentResult";
            this.cbTreatmentResult.ReadOnly = false;
            this.cbTreatmentResult.Size = new System.Drawing.Size(193, 27);
            this.cbTreatmentResult.TabIndex = 14;
            this.cbTreatmentResult.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextBox_KeyDown);
            // 
            // label11
            // 
            this.label11.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label11.Location = new System.Drawing.Point(3, 79);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(124, 27);
            this.label11.TabIndex = 104;
            this.label11.Text = "Трудоспособность";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label58
            // 
            this.label58.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label58.Location = new System.Drawing.Point(41, 45);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(86, 27);
            this.label58.TabIndex = 64;
            this.label58.Text = "Состояние";
            this.label58.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cbCondition
            // 
            this.cbCondition.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cbCondition.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cbCondition.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbCondition.FormattingEnabled = true;
            this.cbCondition.Items.AddRange(new object[] {
            "удовлетворительное",
            "средней ст. тяжести",
            "тяжелое"});
            this.cbCondition.Location = new System.Drawing.Point(133, 46);
            this.cbCondition.Name = "cbCondition";
            this.cbCondition.ReadOnly = false;
            this.cbCondition.Size = new System.Drawing.Size(237, 27);
            this.cbCondition.TabIndex = 15;
            this.cbCondition.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextBox_KeyDown);
            // 
            // cbSickLeaveEndType
            // 
            this.cbSickLeaveEndType.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cbSickLeaveEndType.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cbSickLeaveEndType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbSickLeaveEndType.Enabled = false;
            this.cbSickLeaveEndType.FormattingEnabled = true;
            this.cbSickLeaveEndType.Items.AddRange(new object[] {
            "удовлетворительное",
            "средней ст. тяжести",
            "тяжелое"});
            this.cbSickLeaveEndType.Location = new System.Drawing.Point(133, 219);
            this.cbSickLeaveEndType.Name = "cbSickLeaveEndType";
            this.cbSickLeaveEndType.ReadOnly = false;
            this.cbSickLeaveEndType.Size = new System.Drawing.Size(413, 27);
            this.cbSickLeaveEndType.TabIndex = 25;
            // 
            // tbDateDischarge
            // 
            this.tbDateDischarge.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.tbDateDischarge.Culture = new System.Globalization.CultureInfo("");
            this.tbDateDischarge.Location = new System.Drawing.Point(133, 112);
            this.tbDateDischarge.Mask = "00.00.0000";
            this.tbDateDischarge.Name = "tbDateDischarge";
            this.tbDateDischarge.Size = new System.Drawing.Size(96, 26);
            this.tbDateDischarge.TabIndex = 17;
            this.tbDateDischarge.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.tbDateDischarge.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TextBox_KeyPress);
            this.tbDateDischarge.Validating += new System.ComponentModel.CancelEventHandler(this.DateTextBox_Validating);
            // 
            // label57
            // 
            this.label57.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label57.Location = new System.Drawing.Point(25, 112);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(102, 26);
            this.label57.TabIndex = 66;
            this.label57.Text = "Дата выписки";
            this.label57.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cbSickLeave
            // 
            this.cbSickLeave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cbSickLeave.AutoSize = true;
            this.cbSickLeave.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.cbSickLeave.Location = new System.Drawing.Point(2, 155);
            this.cbSickLeave.Name = "cbSickLeave";
            this.cbSickLeave.Size = new System.Drawing.Size(125, 22);
            this.cbSickLeave.TabIndex = 18;
            this.cbSickLeave.Text = "Больничный №";
            this.cbSickLeave.UseVisualStyleBackColor = true;
            this.cbSickLeave.CheckedChanged += new System.EventHandler(this.cbSickLeave_CheckedChanged);
            this.cbSickLeave.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextBox_KeyDown);
            // 
            // tbSickLeaveNum
            // 
            this.tbSickLeaveNum.AcceptsReturn = true;
            this.tbSickLeaveNum.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.tbSickLeaveNum.Enabled = false;
            this.tbSickLeaveNum.Location = new System.Drawing.Point(133, 153);
            this.tbSickLeaveNum.Name = "tbSickLeaveNum";
            this.tbSickLeaveNum.Size = new System.Drawing.Size(193, 26);
            this.tbSickLeaveNum.TabIndex = 19;
            this.tbSickLeaveNum.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.tbSickLeaveNum.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextBox_KeyDown);
            // 
            // lbSickLeave1
            // 
            this.lbSickLeave1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lbSickLeave1.AutoSize = true;
            this.lbSickLeave1.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.lbSickLeave1.Location = new System.Drawing.Point(335, 156);
            this.lbSickLeave1.Name = "lbSickLeave1";
            this.lbSickLeave1.Size = new System.Drawing.Size(14, 18);
            this.lbSickLeave1.TabIndex = 78;
            this.lbSickLeave1.Text = "с";
            this.lbSickLeave1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tbSickLeaveStart
            // 
            this.tbSickLeaveStart.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.tbSickLeaveStart.Culture = new System.Globalization.CultureInfo("");
            this.tbSickLeaveStart.Enabled = false;
            this.tbSickLeaveStart.Location = new System.Drawing.Point(354, 153);
            this.tbSickLeaveStart.Mask = "00.00.0000";
            this.tbSickLeaveStart.Name = "tbSickLeaveStart";
            this.tbSickLeaveStart.Size = new System.Drawing.Size(80, 26);
            this.tbSickLeaveStart.TabIndex = 20;
            this.tbSickLeaveStart.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.tbSickLeaveStart.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TextBox_KeyPress);
            // 
            // cbSickLeaveExt
            // 
            this.cbSickLeaveExt.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cbSickLeaveExt.AutoSize = true;
            this.cbSickLeaveExt.Enabled = false;
            this.cbSickLeaveExt.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.cbSickLeaveExt.Location = new System.Drawing.Point(2, 187);
            this.cbSickLeaveExt.Name = "cbSickLeaveExt";
            this.cbSickLeaveExt.Size = new System.Drawing.Size(84, 22);
            this.cbSickLeaveExt.TabIndex = 22;
            this.cbSickLeaveExt.Text = "Продлен";
            this.cbSickLeaveExt.UseVisualStyleBackColor = true;
            this.cbSickLeaveExt.CheckedChanged += new System.EventHandler(this.cbSickLeaveExt_CheckedChanged);
            this.cbSickLeaveExt.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextBox_KeyDown);
            // 
            // tbSickLeaveEnd
            // 
            this.tbSickLeaveEnd.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.tbSickLeaveEnd.Culture = new System.Globalization.CultureInfo("");
            this.tbSickLeaveEnd.Enabled = false;
            this.tbSickLeaveEnd.Location = new System.Drawing.Point(467, 153);
            this.tbSickLeaveEnd.Mask = "00.00.0000";
            this.tbSickLeaveEnd.Name = "tbSickLeaveEnd";
            this.tbSickLeaveEnd.Size = new System.Drawing.Size(80, 26);
            this.tbSickLeaveEnd.TabIndex = 21;
            this.tbSickLeaveEnd.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.tbSickLeaveEnd.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TextBox_KeyPress);
            // 
            // lbSickLeave4
            // 
            this.lbSickLeave4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lbSickLeave4.AutoSize = true;
            this.lbSickLeave4.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.lbSickLeave4.Location = new System.Drawing.Point(440, 188);
            this.lbSickLeave4.Name = "lbSickLeave4";
            this.lbSickLeave4.Size = new System.Drawing.Size(24, 18);
            this.lbSickLeave4.TabIndex = 89;
            this.lbSickLeave4.Text = "по";
            // 
            // lbSickLeave2
            // 
            this.lbSickLeave2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lbSickLeave2.AutoSize = true;
            this.lbSickLeave2.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.lbSickLeave2.Location = new System.Drawing.Point(440, 156);
            this.lbSickLeave2.Name = "lbSickLeave2";
            this.lbSickLeave2.Size = new System.Drawing.Size(24, 18);
            this.lbSickLeave2.TabIndex = 85;
            this.lbSickLeave2.Text = "по";
            // 
            // tbSickLeaveExtEnd
            // 
            this.tbSickLeaveExtEnd.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.tbSickLeaveExtEnd.Culture = new System.Globalization.CultureInfo("");
            this.tbSickLeaveExtEnd.Enabled = false;
            this.tbSickLeaveExtEnd.Location = new System.Drawing.Point(467, 185);
            this.tbSickLeaveExtEnd.Mask = "00.00.0000";
            this.tbSickLeaveExtEnd.Name = "tbSickLeaveExtEnd";
            this.tbSickLeaveExtEnd.Size = new System.Drawing.Size(80, 26);
            this.tbSickLeaveExtEnd.TabIndex = 24;
            this.tbSickLeaveExtEnd.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.tbSickLeaveExtEnd.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TextBox_KeyPress);
            // 
            // lbSickLeave3
            // 
            this.lbSickLeave3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lbSickLeave3.AutoSize = true;
            this.lbSickLeave3.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.lbSickLeave3.Location = new System.Drawing.Point(335, 188);
            this.lbSickLeave3.Name = "lbSickLeave3";
            this.lbSickLeave3.Size = new System.Drawing.Size(14, 18);
            this.lbSickLeave3.TabIndex = 86;
            this.lbSickLeave3.Text = "с";
            // 
            // tbSickLeaveExtStart
            // 
            this.tbSickLeaveExtStart.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.tbSickLeaveExtStart.Culture = new System.Globalization.CultureInfo("");
            this.tbSickLeaveExtStart.Enabled = false;
            this.tbSickLeaveExtStart.Location = new System.Drawing.Point(354, 185);
            this.tbSickLeaveExtStart.Mask = "00.00.0000";
            this.tbSickLeaveExtStart.Name = "tbSickLeaveExtStart";
            this.tbSickLeaveExtStart.Size = new System.Drawing.Size(80, 26);
            this.tbSickLeaveExtStart.TabIndex = 23;
            this.tbSickLeaveExtStart.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.tbSickLeaveExtStart.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TextBox_KeyPress);
            // 
            // tbSuperintendent
            // 
            this.tbSuperintendent.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.tbSuperintendent.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.tbSuperintendent.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.tbSuperintendent.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.tbSuperintendent.Location = new System.Drawing.Point(74, 44);
            this.tbSuperintendent.Name = "tbSuperintendent";
            this.tbSuperintendent.ReadOnly = false;
            this.tbSuperintendent.Size = new System.Drawing.Size(200, 27);
            this.tbSuperintendent.TabIndex = 2;
            this.tbSuperintendent.TabStop = false;
            this.tbSuperintendent.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextBox_KeyDown);
            // 
            // label45
            // 
            this.label45.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label45.AutoSize = true;
            this.label45.Location = new System.Drawing.Point(84, 20);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(115, 18);
            this.label45.TabIndex = 69;
            this.label45.Text = "Зав. отделением";
            // 
            // label64
            // 
            this.label64.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label64.AutoSize = true;
            this.label64.Location = new System.Drawing.Point(84, 78);
            this.label64.Name = "label64";
            this.label64.Size = new System.Drawing.Size(135, 18);
            this.label64.TabIndex = 71;
            this.label64.Text = "Осмотр совместно с";
            // 
            // gbSuperIntendent
            // 
            this.gbSuperIntendent.BackColor = System.Drawing.SystemColors.Control;
            this.gbSuperIntendent.Controls.Add(this.cbSuperIntendentFormatted);
            this.gbSuperIntendent.Controls.Add(this.cbIO);
            this.gbSuperIntendent.Controls.Add(this.label64);
            this.gbSuperIntendent.Controls.Add(this.label45);
            this.gbSuperIntendent.Controls.Add(this.tbSuperintendent);
            this.gbSuperIntendent.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.gbSuperIntendent.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.gbSuperIntendent.Location = new System.Drawing.Point(8, 791);
            this.gbSuperIntendent.Name = "gbSuperIntendent";
            this.gbSuperIntendent.Size = new System.Drawing.Size(283, 137);
            this.gbSuperIntendent.TabIndex = 6;
            this.gbSuperIntendent.TabStop = false;
            // 
            // cbSuperIntendentFormatted
            // 
            this.cbSuperIntendentFormatted.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cbSuperIntendentFormatted.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cbSuperIntendentFormatted.FormattingEnabled = true;
            this.cbSuperIntendentFormatted.Location = new System.Drawing.Point(74, 99);
            this.cbSuperIntendentFormatted.Name = "cbSuperIntendentFormatted";
            this.cbSuperIntendentFormatted.ReadOnly = false;
            this.cbSuperIntendentFormatted.Size = new System.Drawing.Size(200, 27);
            this.cbSuperIntendentFormatted.TabIndex = 3;
            this.cbSuperIntendentFormatted.TabStop = false;
            this.cbSuperIntendentFormatted.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextBox_KeyDown);
            // 
            // cbIO
            // 
            this.cbIO.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cbIO.AutoSize = true;
            this.cbIO.Location = new System.Drawing.Point(13, 46);
            this.cbIO.Name = "cbIO";
            this.cbIO.Padding = new System.Windows.Forms.Padding(0, 1, 0, 0);
            this.cbIO.Size = new System.Drawing.Size(55, 23);
            this.cbIO.TabIndex = 1;
            this.cbIO.TabStop = false;
            this.cbIO.Text = "И.О.";
            this.cbIO.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.cbIO.UseVisualStyleBackColor = true;
            this.cbIO.CheckedChanged += new System.EventHandler(this.cbIO_CheckedChanged);
            // 
            // btAction
            // 
            this.btAction.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btAction.Location = new System.Drawing.Point(1155, 4);
            this.btAction.Name = "btAction";
            this.btAction.Size = new System.Drawing.Size(388, 40);
            this.btAction.TabIndex = 78;
            this.btAction.TabStop = false;
            this.btAction.Text = "Action";
            this.btAction.UseVisualStyleBackColor = true;
            this.btAction.Click += new System.EventHandler(this.btAction_Click);
            // 
            // groupBox12
            // 
            this.groupBox12.BackColor = System.Drawing.SystemColors.Control;
            this.groupBox12.Controls.Add(this.rtbMedicalAllergy);
            this.groupBox12.Controls.Add(this.label34);
            this.groupBox12.Controls.Add(this.rtbAccompMedicationPost);
            this.groupBox12.Controls.Add(this.panel2);
            this.groupBox12.Controls.Add(this.rtbAccompMedicationPre);
            this.groupBox12.Controls.Add(this.label42);
            this.groupBox12.Controls.Add(this.rtbAccompanying);
            this.groupBox12.Controls.Add(this.label36);
            this.groupBox12.Controls.Add(this.btOperTeamNeuro);
            this.groupBox12.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox12.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.groupBox12.Location = new System.Drawing.Point(4, 0);
            this.groupBox12.Name = "groupBox12";
            this.groupBox12.Padding = new System.Windows.Forms.Padding(8, 4, 8, 9);
            this.groupBox12.Size = new System.Drawing.Size(414, 536);
            this.groupBox12.TabIndex = 8;
            this.groupBox12.TabStop = false;
            // 
            // rtbMedicalAllergy
            // 
            this.rtbMedicalAllergy.Cue = "отсутствуют.";
            this.rtbMedicalAllergy.Dock = System.Windows.Forms.DockStyle.Top;
            this.rtbMedicalAllergy.ForeColor = System.Drawing.Color.Black;
            this.rtbMedicalAllergy.Location = new System.Drawing.Point(8, 370);
            this.rtbMedicalAllergy.Name = "rtbMedicalAllergy";
            this.rtbMedicalAllergy.PlainTextMode = true;
            this.rtbMedicalAllergy.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.ForcedVertical;
            this.rtbMedicalAllergy.ShowSelectionMargin = true;
            this.rtbMedicalAllergy.Size = new System.Drawing.Size(398, 117);
            this.rtbMedicalAllergy.TabIndex = 4;
            this.rtbMedicalAllergy.Text = "";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Dock = System.Windows.Forms.DockStyle.Top;
            this.label34.Location = new System.Drawing.Point(8, 348);
            this.label34.Name = "label34";
            this.label34.Padding = new System.Windows.Forms.Padding(8, 4, 0, 0);
            this.label34.Size = new System.Drawing.Size(364, 22);
            this.label34.TabIndex = 119;
            this.label34.Text = "Аллергические реакции на лекарственные препараты";
            // 
            // rtbAccompMedicationPost
            // 
            this.rtbAccompMedicationPost.Cue = "не планируется.";
            this.rtbAccompMedicationPost.Dock = System.Windows.Forms.DockStyle.Top;
            this.rtbAccompMedicationPost.ForeColor = System.Drawing.Color.Black;
            this.rtbAccompMedicationPost.Location = new System.Drawing.Point(8, 263);
            this.rtbAccompMedicationPost.Name = "rtbAccompMedicationPost";
            this.rtbAccompMedicationPost.PlainTextMode = true;
            this.rtbAccompMedicationPost.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.ForcedVertical;
            this.rtbAccompMedicationPost.ShowSelectionMargin = true;
            this.rtbAccompMedicationPost.Size = new System.Drawing.Size(398, 85);
            this.rtbAccompMedicationPost.TabIndex = 3;
            this.rtbAccompMedicationPost.Text = "";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.label38);
            this.panel2.Controls.Add(this.btAccompMedicationCopy);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(8, 237);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(398, 26);
            this.panel2.TabIndex = 118;
            // 
            // label38
            // 
            this.label38.Dock = System.Windows.Forms.DockStyle.Left;
            this.label38.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label38.Location = new System.Drawing.Point(0, 0);
            this.label38.Name = "label38";
            this.label38.Padding = new System.Windows.Forms.Padding(8, 0, 0, 0);
            this.label38.Size = new System.Drawing.Size(333, 26);
            this.label38.TabIndex = 99;
            this.label38.Text = "Планируемое лечение сопутств. патологии";
            this.label38.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // rtbAccompMedicationPre
            // 
            this.rtbAccompMedicationPre.Cue = "постоянно лекарств не принимает.";
            this.rtbAccompMedicationPre.Dock = System.Windows.Forms.DockStyle.Top;
            this.rtbAccompMedicationPre.ForeColor = System.Drawing.Color.Black;
            this.rtbAccompMedicationPre.Location = new System.Drawing.Point(8, 152);
            this.rtbAccompMedicationPre.Name = "rtbAccompMedicationPre";
            this.rtbAccompMedicationPre.PlainTextMode = true;
            this.rtbAccompMedicationPre.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.ForcedVertical;
            this.rtbAccompMedicationPre.ShowSelectionMargin = true;
            this.rtbAccompMedicationPre.Size = new System.Drawing.Size(398, 85);
            this.rtbAccompMedicationPre.TabIndex = 2;
            this.rtbAccompMedicationPre.Text = "";
            // 
            // rtbAccompanying
            // 
            this.rtbAccompanying.Cue = "нет.";
            this.rtbAccompanying.Dock = System.Windows.Forms.DockStyle.Top;
            this.rtbAccompanying.ForeColor = System.Drawing.Color.Black;
            this.rtbAccompanying.Location = new System.Drawing.Point(8, 45);
            this.rtbAccompanying.Name = "rtbAccompanying";
            this.rtbAccompanying.PlainTextMode = true;
            this.rtbAccompanying.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.ForcedVertical;
            this.rtbAccompanying.ShowSelectionMargin = true;
            this.rtbAccompanying.Size = new System.Drawing.Size(398, 85);
            this.rtbAccompanying.TabIndex = 1;
            this.rtbAccompanying.Text = "";
            // 
            // btCommissions
            // 
            this.btCommissions.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btCommissions.Location = new System.Drawing.Point(8, 59);
            this.btCommissions.Name = "btCommissions";
            this.btCommissions.Size = new System.Drawing.Size(398, 38);
            this.btCommissions.TabIndex = 2;
            this.btCommissions.Text = "Комиссии";
            this.btCommissions.UseVisualStyleBackColor = true;
            this.btCommissions.Click += new System.EventHandler(this.btCommissions_Click);
            // 
            // groupBox4
            // 
            this.groupBox4.BackColor = System.Drawing.Color.Wheat;
            this.groupBox4.Controls.Add(this.btInstrumentalExamination);
            this.groupBox4.Controls.Add(this.btConclusions);
            this.groupBox4.Controls.Add(this.btGenExam);
            this.groupBox4.Controls.Add(this.btCommissions);
            this.groupBox4.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.groupBox4.Location = new System.Drawing.Point(4, 733);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(414, 195);
            this.groupBox4.TabIndex = 10;
            this.groupBox4.TabStop = false;
            // 
            // btInstrumentalExamination
            // 
            this.btInstrumentalExamination.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btInstrumentalExamination.Location = new System.Drawing.Point(8, 103);
            this.btInstrumentalExamination.Name = "btInstrumentalExamination";
            this.btInstrumentalExamination.Size = new System.Drawing.Size(398, 38);
            this.btInstrumentalExamination.TabIndex = 3;
            this.btInstrumentalExamination.Text = "Иструментальные методы обследования";
            this.btInstrumentalExamination.UseVisualStyleBackColor = true;
            this.btInstrumentalExamination.Click += new System.EventHandler(this.btInstrumentalExamination_Click);
            // 
            // pnBottom
            // 
            this.pnBottom.Controls.Add(this.cbDepartment);
            this.pnBottom.Controls.Add(this.label7);
            this.pnBottom.Controls.Add(this.btPatients);
            this.pnBottom.Controls.Add(this.btRevert);
            this.pnBottom.Controls.Add(this.btVMP);
            this.pnBottom.Controls.Add(this.btOpen);
            this.pnBottom.Controls.Add(this.btTest);
            this.pnBottom.Controls.Add(this.btOpenDirectory);
            this.pnBottom.Controls.Add(this.btTestData);
            this.pnBottom.Controls.Add(this.btAction);
            this.pnBottom.Controls.Add(this.cbDoctors);
            this.pnBottom.Controls.Add(this.label27);
            this.pnBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnBottom.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.pnBottom.Location = new System.Drawing.Point(0, 932);
            this.pnBottom.Name = "pnBottom";
            this.pnBottom.Size = new System.Drawing.Size(1904, 52);
            this.pnBottom.TabIndex = 202;
            // 
            // cbDepartment
            // 
            this.cbDepartment.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cbDepartment.Enabled = false;
            this.cbDepartment.FormattingEnabled = true;
            this.cbDepartment.Location = new System.Drawing.Point(102, 12);
            this.cbDepartment.Name = "cbDepartment";
            this.cbDepartment.ReadOnly = false;
            this.cbDepartment.Size = new System.Drawing.Size(193, 27);
            this.cbDepartment.TabIndex = 80;
            this.cbDepartment.TabStop = false;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(14, 17);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(79, 18);
            this.label7.TabIndex = 81;
            this.label7.Text = "Отделение";
            // 
            // btPatients
            // 
            this.btPatients.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btPatients.BackColor = System.Drawing.SystemColors.ControlLight;
            this.btPatients.Image = global::FormsEditorEx.Properties.Resources.patient;
            this.btPatients.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btPatients.Location = new System.Drawing.Point(1783, 4);
            this.btPatients.Name = "btPatients";
            this.btPatients.Size = new System.Drawing.Size(114, 40);
            this.btPatients.TabIndex = 79;
            this.btPatients.TabStop = false;
            this.btPatients.Text = " Пациенты";
            this.btPatients.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btPatients.UseVisualStyleBackColor = false;
            this.btPatients.Click += new System.EventHandler(this.btPatients_Click);
            // 
            // btRevert
            // 
            this.btRevert.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btRevert.FlatAppearance.BorderColor = System.Drawing.SystemColors.ControlDarkDark;
            this.btRevert.Image = global::FormsEditorEx.Properties.Resources.previous;
            this.btRevert.Location = new System.Drawing.Point(1645, 4);
            this.btRevert.Name = "btRevert";
            this.btRevert.Size = new System.Drawing.Size(40, 40);
            this.btRevert.TabIndex = 47;
            this.btRevert.TabStop = false;
            this.btRevert.UseVisualStyleBackColor = true;
            this.btRevert.Click += new System.EventHandler(this.btRevert_Click);
            // 
            // btOpen
            // 
            this.btOpen.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btOpen.Enabled = false;
            this.btOpen.FlatAppearance.BorderColor = System.Drawing.SystemColors.ControlDarkDark;
            this.btOpen.Image = global::FormsEditorEx.Properties.Resources.document;
            this.btOpen.Location = new System.Drawing.Point(1691, 4);
            this.btOpen.Name = "btOpen";
            this.btOpen.Size = new System.Drawing.Size(40, 40);
            this.btOpen.TabIndex = 46;
            this.btOpen.TabStop = false;
            this.btOpen.UseVisualStyleBackColor = true;
            this.btOpen.Click += new System.EventHandler(this.btOpen_Click);
            // 
            // btOpenDirectory
            // 
            this.btOpenDirectory.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btOpenDirectory.FlatAppearance.BorderColor = System.Drawing.SystemColors.ControlDarkDark;
            this.btOpenDirectory.Image = ((System.Drawing.Image)(resources.GetObject("btOpenDirectory.Image")));
            this.btOpenDirectory.Location = new System.Drawing.Point(1737, 4);
            this.btOpenDirectory.Name = "btOpenDirectory";
            this.btOpenDirectory.Size = new System.Drawing.Size(40, 40);
            this.btOpenDirectory.TabIndex = 50;
            this.btOpenDirectory.TabStop = false;
            this.btOpenDirectory.UseVisualStyleBackColor = true;
            this.btOpenDirectory.Click += new System.EventHandler(this.btOpenDirectory_Click);
            // 
            // cbDoctors
            // 
            this.cbDoctors.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cbDoctors.Enabled = false;
            this.cbDoctors.FormattingEnabled = true;
            this.cbDoctors.Location = new System.Drawing.Point(436, 12);
            this.cbDoctors.Name = "cbDoctors";
            this.cbDoctors.ReadOnly = false;
            this.cbDoctors.Size = new System.Drawing.Size(193, 27);
            this.cbDoctors.TabIndex = 15;
            this.cbDoctors.TabStop = false;
            // 
            // btSearch
            // 
            this.btSearch.BackColor = System.Drawing.SystemColors.ControlLight;
            this.btSearch.Image = global::FormsEditorEx.Properties.Resources.loupe13;
            this.btSearch.Location = new System.Drawing.Point(249, 43);
            this.btSearch.Name = "btSearch";
            this.btSearch.Size = new System.Drawing.Size(24, 24);
            this.btSearch.TabIndex = 199;
            this.btSearch.TabStop = false;
            this.btSearch.UseVisualStyleBackColor = false;
            this.btSearch.Visible = false;
            this.btSearch.Click += new System.EventHandler(this.btSearch_Click);
            // 
            // label47
            // 
            this.label47.Location = new System.Drawing.Point(3, 9);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(77, 27);
            this.label47.TabIndex = 56;
            this.label47.Text = "Риск ВТЭО";
            this.label47.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label49
            // 
            this.label49.Location = new System.Drawing.Point(6, 42);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(74, 27);
            this.label49.TabIndex = 59;
            this.label49.Text = "Риск ГО";
            this.label49.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // clbVTEOPrevention
            // 
            this.clbVTEOPrevention.BackColor = System.Drawing.Color.Wheat;
            this.clbVTEOPrevention.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.clbVTEOPrevention.CheckOnClick = true;
            this.clbVTEOPrevention.FormattingEnabled = true;
            this.clbVTEOPrevention.Items.AddRange(new object[] {
            "Антикоагулянтная терапия",
            "Ранняя активизация",
            "Механ. способы проф-ки"});
            this.clbVTEOPrevention.Location = new System.Drawing.Point(40, 110);
            this.clbVTEOPrevention.Name = "clbVTEOPrevention";
            this.clbVTEOPrevention.Size = new System.Drawing.Size(206, 63);
            this.clbVTEOPrevention.TabIndex = 3;
            this.clbVTEOPrevention.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextBox_KeyDown);
            this.clbVTEOPrevention.Leave += new System.EventHandler(this.clbVTEOPrevention_Leave);
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Location = new System.Drawing.Point(40, 85);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(185, 18);
            this.label50.TabIndex = 62;
            this.label50.Text = "Способ профилактики ВТЭО";
            // 
            // gpVTEO
            // 
            this.gpVTEO.BackColor = System.Drawing.Color.Wheat;
            this.gpVTEO.Controls.Add(this.pnVTEO);
            this.gpVTEO.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.gpVTEO.Location = new System.Drawing.Point(4, 542);
            this.gpVTEO.Name = "gpVTEO";
            this.gpVTEO.Size = new System.Drawing.Size(414, 191);
            this.gpVTEO.TabIndex = 9;
            this.gpVTEO.TabStop = false;
            // 
            // pnVTEO
            // 
            this.pnVTEO.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.pnVTEO.Controls.Add(this.label47);
            this.pnVTEO.Controls.Add(this.label50);
            this.pnVTEO.Controls.Add(this.cbVTEORisk);
            this.pnVTEO.Controls.Add(this.clbVTEOPrevention);
            this.pnVTEO.Controls.Add(this.label49);
            this.pnVTEO.Controls.Add(this.cbGORisk);
            this.pnVTEO.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.pnVTEO.Location = new System.Drawing.Point(91, 10);
            this.pnVTEO.Name = "pnVTEO";
            this.pnVTEO.Size = new System.Drawing.Size(268, 181);
            this.pnVTEO.TabIndex = 63;
            // 
            // cbVTEORisk
            // 
            this.cbVTEORisk.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cbVTEORisk.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbVTEORisk.FormattingEnabled = true;
            this.cbVTEORisk.Items.AddRange(new object[] {
            "высокий",
            "умеренный",
            "низкий"});
            this.cbVTEORisk.Location = new System.Drawing.Point(86, 9);
            this.cbVTEORisk.Name = "cbVTEORisk";
            this.cbVTEORisk.ReadOnly = false;
            this.cbVTEORisk.Size = new System.Drawing.Size(171, 27);
            this.cbVTEORisk.TabIndex = 1;
            this.cbVTEORisk.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextBox_KeyDown);
            // 
            // cbGORisk
            // 
            this.cbGORisk.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cbGORisk.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbGORisk.FormattingEnabled = true;
            this.cbGORisk.Items.AddRange(new object[] {
            "высокий",
            "умеренный",
            "низкий"});
            this.cbGORisk.Location = new System.Drawing.Point(86, 42);
            this.cbGORisk.Name = "cbGORisk";
            this.cbGORisk.ReadOnly = false;
            this.cbGORisk.Size = new System.Drawing.Size(171, 27);
            this.cbGORisk.TabIndex = 2;
            this.cbGORisk.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cbGORisk_KeyDown);
            // 
            // pnStatusBar
            // 
            this.pnStatusBar.Controls.Add(this.ucStatusBar);
            this.pnStatusBar.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnStatusBar.Location = new System.Drawing.Point(0, 984);
            this.pnStatusBar.Name = "pnStatusBar";
            this.pnStatusBar.Size = new System.Drawing.Size(1904, 17);
            this.pnStatusBar.TabIndex = 203;
            // 
            // ucStatusBar
            // 
            this.ucStatusBar.Dock = System.Windows.Forms.DockStyle.Right;
            this.ucStatusBar.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ucStatusBar.Location = new System.Drawing.Point(737, 0);
            this.ucStatusBar.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.ucStatusBar.MaximumSize = new System.Drawing.Size(1167, 17);
            this.ucStatusBar.MinimumSize = new System.Drawing.Size(350, 17);
            this.ucStatusBar.Name = "ucStatusBar";
            this.ucStatusBar.Size = new System.Drawing.Size(1167, 17);
            this.ucStatusBar.TabIndex = 201;
            this.ucStatusBar.TabStop = false;
            // 
            // pnWorkingArea
            // 
            this.pnWorkingArea.BackColor = System.Drawing.SystemColors.ControlDark;
            this.pnWorkingArea.Controls.Add(this.pnSection4);
            this.pnWorkingArea.Controls.Add(this.pnSection3);
            this.pnWorkingArea.Controls.Add(this.pnSection2);
            this.pnWorkingArea.Controls.Add(this.pnSection1);
            this.pnWorkingArea.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnWorkingArea.Location = new System.Drawing.Point(0, 0);
            this.pnWorkingArea.Name = "pnWorkingArea";
            this.pnWorkingArea.Size = new System.Drawing.Size(1904, 932);
            this.pnWorkingArea.TabIndex = 205;
            // 
            // pnSection4
            // 
            this.pnSection4.BackColor = System.Drawing.SystemColors.Control;
            this.pnSection4.Controls.Add(this.groupBox5);
            this.pnSection4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnSection4.Location = new System.Drawing.Point(1215, 0);
            this.pnSection4.Name = "pnSection4";
            this.pnSection4.Padding = new System.Windows.Forms.Padding(4, 0, 8, 4);
            this.pnSection4.Size = new System.Drawing.Size(689, 932);
            this.pnSection4.TabIndex = 4;
            // 
            // pnSection3
            // 
            this.pnSection3.BackColor = System.Drawing.SystemColors.Control;
            this.pnSection3.Controls.Add(this.groupBox12);
            this.pnSection3.Controls.Add(this.gpVTEO);
            this.pnSection3.Controls.Add(this.groupBox4);
            this.pnSection3.Dock = System.Windows.Forms.DockStyle.Left;
            this.pnSection3.Location = new System.Drawing.Point(793, 0);
            this.pnSection3.Name = "pnSection3";
            this.pnSection3.Padding = new System.Windows.Forms.Padding(4, 0, 4, 4);
            this.pnSection3.Size = new System.Drawing.Size(422, 932);
            this.pnSection3.TabIndex = 3;
            // 
            // pnSection2
            // 
            this.pnSection2.BackColor = System.Drawing.SystemColors.Control;
            this.pnSection2.Controls.Add(this.gbDiagnoz);
            this.pnSection2.Dock = System.Windows.Forms.DockStyle.Left;
            this.pnSection2.Location = new System.Drawing.Point(295, 0);
            this.pnSection2.Name = "pnSection2";
            this.pnSection2.Padding = new System.Windows.Forms.Padding(4, 0, 4, 4);
            this.pnSection2.Size = new System.Drawing.Size(498, 932);
            this.pnSection2.TabIndex = 2;
            // 
            // pnSection1
            // 
            this.pnSection1.BackColor = System.Drawing.SystemColors.Control;
            this.pnSection1.Controls.Add(this.groupBox9);
            this.pnSection1.Controls.Add(this.groupBox10);
            this.pnSection1.Controls.Add(this.gpAddress);
            this.pnSection1.Controls.Add(this.gbSuperIntendent);
            this.pnSection1.Controls.Add(this.gbPersonal);
            this.pnSection1.Controls.Add(this.pnRecord);
            this.pnSection1.Dock = System.Windows.Forms.DockStyle.Left;
            this.pnSection1.Location = new System.Drawing.Point(0, 0);
            this.pnSection1.Name = "pnSection1";
            this.pnSection1.Padding = new System.Windows.Forms.Padding(8, 8, 4, 4);
            this.pnSection1.Size = new System.Drawing.Size(295, 932);
            this.pnSection1.TabIndex = 1;
            // 
            // gpAddress
            // 
            this.gpAddress.BackColor = System.Drawing.SystemColors.Control;
            this.gpAddress.Controls.Add(this.cbStreet);
            this.gpAddress.Controls.Add(this.cbTown);
            this.gpAddress.Controls.Add(this.cbStreetType);
            this.gpAddress.Controls.Add(this.rbVillage);
            this.gpAddress.Controls.Add(this.rbTown);
            this.gpAddress.Controls.Add(this.label12);
            this.gpAddress.Controls.Add(this.tbBuilding);
            this.gpAddress.Controls.Add(this.label15);
            this.gpAddress.Controls.Add(this.tbFlat);
            this.gpAddress.Controls.Add(this.label16);
            this.gpAddress.Controls.Add(this.tbKorpus);
            this.gpAddress.Controls.Add(this.label17);
            this.gpAddress.Controls.Add(this.label18);
            this.gpAddress.Controls.Add(this.tbOblast);
            this.gpAddress.Controls.Add(this.tbIndex);
            this.gpAddress.Controls.Add(this.label19);
            this.gpAddress.Dock = System.Windows.Forms.DockStyle.Top;
            this.gpAddress.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.gpAddress.Location = new System.Drawing.Point(8, 271);
            this.gpAddress.Margin = new System.Windows.Forms.Padding(3, 1, 3, 3);
            this.gpAddress.Name = "gpAddress";
            this.gpAddress.Size = new System.Drawing.Size(283, 217);
            this.gpAddress.TabIndex = 3;
            this.gpAddress.TabStop = false;
            // 
            // cbStreet
            // 
            this.cbStreet.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cbStreet.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cbStreet.FormattingEnabled = true;
            this.cbStreet.Items.AddRange(new object[] {
            "Октября",
            "Ленинградский"});
            this.cbStreet.Location = new System.Drawing.Point(78, 116);
            this.cbStreet.Name = "cbStreet";
            this.cbStreet.ReadOnly = false;
            this.cbStreet.Size = new System.Drawing.Size(196, 27);
            this.cbStreet.TabIndex = 5;
            this.cbStreet.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextBox_KeyDown);
            // 
            // cbTown
            // 
            this.cbTown.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cbTown.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cbTown.Enabled = false;
            this.cbTown.FormattingEnabled = true;
            this.cbTown.Location = new System.Drawing.Point(78, 83);
            this.cbTown.Name = "cbTown";
            this.cbTown.ReadOnly = false;
            this.cbTown.Size = new System.Drawing.Size(196, 27);
            this.cbTown.TabIndex = 3;
            this.cbTown.TextChanged += new System.EventHandler(this.cbTown_TextChanged);
            this.cbTown.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextBox_KeyDown);
            // 
            // cbStreetType
            // 
            this.cbStreetType.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cbStreetType.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cbStreetType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbStreetType.FormattingEnabled = true;
            this.cbStreetType.ItemHeight = 20;
            this.cbStreetType.Items.AddRange(new object[] {
            "ул.",
            "пр-т",
            "пр."});
            this.cbStreetType.Location = new System.Drawing.Point(16, 116);
            this.cbStreetType.Name = "cbStreetType";
            this.cbStreetType.ReadOnly = false;
            this.cbStreetType.Size = new System.Drawing.Size(56, 26);
            this.cbStreetType.TabIndex = 4;
            this.cbStreetType.SelectedIndexChanged += new System.EventHandler(this.cbStreetType_SelectedIndexChanged);
            this.cbStreetType.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextBox_KeyDown);
            // 
            // rbVillage
            // 
            this.rbVillage.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.rbVillage.AutoSize = true;
            this.rbVillage.Location = new System.Drawing.Point(202, 183);
            this.rbVillage.Name = "rbVillage";
            this.rbVillage.Size = new System.Drawing.Size(58, 22);
            this.rbVillage.TabIndex = 101;
            this.rbVillage.Text = "Село";
            this.rbVillage.UseVisualStyleBackColor = true;
            // 
            // rbTown
            // 
            this.rbTown.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.rbTown.AutoSize = true;
            this.rbTown.Checked = true;
            this.rbTown.Location = new System.Drawing.Point(134, 183);
            this.rbTown.Name = "rbTown";
            this.rbTown.Size = new System.Drawing.Size(62, 22);
            this.rbTown.TabIndex = 10;
            this.rbTown.TabStop = true;
            this.rbTown.Text = "Город";
            this.rbTown.UseVisualStyleBackColor = true;
            this.rbTown.CheckedChanged += new System.EventHandler(this.rbTown_CheckedChanged);
            // 
            // label12
            // 
            this.label12.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label12.Location = new System.Drawing.Point(5, 149);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(67, 26);
            this.label12.TabIndex = 23;
            this.label12.Text = "Дом";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tbBuilding
            // 
            this.tbBuilding.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.tbBuilding.Location = new System.Drawing.Point(78, 149);
            this.tbBuilding.Name = "tbBuilding";
            this.tbBuilding.Size = new System.Drawing.Size(34, 26);
            this.tbBuilding.TabIndex = 6;
            this.tbBuilding.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.tbBuilding.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TextBox_KeyPress);
            // 
            // label15
            // 
            this.label15.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label15.Location = new System.Drawing.Point(5, 181);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(67, 26);
            this.label15.TabIndex = 21;
            this.label15.Text = "Квартира";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tbFlat
            // 
            this.tbFlat.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.tbFlat.Location = new System.Drawing.Point(78, 181);
            this.tbFlat.Name = "tbFlat";
            this.tbFlat.Size = new System.Drawing.Size(34, 26);
            this.tbFlat.TabIndex = 8;
            this.tbFlat.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.tbFlat.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbFlat_KeyPress);
            // 
            // label16
            // 
            this.label16.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label16.Location = new System.Drawing.Point(131, 149);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(53, 26);
            this.label16.TabIndex = 19;
            this.label16.Text = "Корпус";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tbKorpus
            // 
            this.tbKorpus.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.tbKorpus.Location = new System.Drawing.Point(190, 149);
            this.tbKorpus.Name = "tbKorpus";
            this.tbKorpus.Size = new System.Drawing.Size(34, 26);
            this.tbKorpus.TabIndex = 7;
            this.tbKorpus.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.tbKorpus.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TextBox_KeyPress);
            // 
            // label17
            // 
            this.label17.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label17.Location = new System.Drawing.Point(6, 83);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(66, 26);
            this.label17.TabIndex = 15;
            this.label17.Text = "Город";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label18
            // 
            this.label18.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label18.Location = new System.Drawing.Point(6, 51);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(66, 26);
            this.label18.TabIndex = 13;
            this.label18.Text = "Область";
            this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tbOblast
            // 
            this.tbOblast.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.tbOblast.Location = new System.Drawing.Point(78, 51);
            this.tbOblast.Name = "tbOblast";
            this.tbOblast.Size = new System.Drawing.Size(196, 26);
            this.tbOblast.TabIndex = 2;
            this.tbOblast.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.tbOblast.TextChanged += new System.EventHandler(this.tbOblast_TextChanged);
            this.tbOblast.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TextBox_KeyPress);
            // 
            // tbIndex
            // 
            this.tbIndex.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.tbIndex.Location = new System.Drawing.Point(78, 19);
            this.tbIndex.Mask = "000000";
            this.tbIndex.Name = "tbIndex";
            this.tbIndex.Size = new System.Drawing.Size(90, 26);
            this.tbIndex.TabIndex = 1;
            this.tbIndex.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.tbIndex.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TextBox_KeyPress);
            // 
            // label19
            // 
            this.label19.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label19.Location = new System.Drawing.Point(9, 19);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(63, 26);
            this.label19.TabIndex = 10;
            this.label19.Text = "Индекс";
            this.label19.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // pnRecord
            // 
            this.pnRecord.BackColor = System.Drawing.SystemColors.Control;
            this.pnRecord.Controls.Add(this.btSearch);
            this.pnRecord.Controls.Add(this.label6);
            this.pnRecord.Controls.Add(this.label5);
            this.pnRecord.Controls.Add(this.tbAmbuCardNum);
            this.pnRecord.Controls.Add(this.tbClinicalRecordNum);
            this.pnRecord.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnRecord.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.pnRecord.Location = new System.Drawing.Point(8, 8);
            this.pnRecord.Name = "pnRecord";
            this.pnRecord.Size = new System.Drawing.Size(283, 78);
            this.pnRecord.TabIndex = 1;
            // 
            // MainFrame
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.AutoValidate = System.Windows.Forms.AutoValidate.EnableAllowFocusChange;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(1904, 1001);
            this.Controls.Add(this.pnWorkingArea);
            this.Controls.Add(this.pnBottom);
            this.Controls.Add(this.pnStatusBar);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "MainFrame";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.Text = "История болезни";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainFrame_FormClosing);
            this.Load += new System.EventHandler(this.MainFrame_Load);
            this.Click += new System.EventHandler(this.MainFrame_Click);
            this.Resize += new System.EventHandler(this.MainFrame_Resize);
            this.gbPersonal.ResumeLayout(false);
            this.gbPersonal.PerformLayout();
            this.gbDiagnoz.ResumeLayout(false);
            this.gbDiagnoz.PerformLayout();
            this.pnPlannedTreatment.ResumeLayout(false);
            this.pnChemTherapyToggle.ResumeLayout(false);
            this.pnChemTherapyToggle.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbChemHelp)).EndInit();
            this.pnVMPToggle.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pbVMPHelp)).EndInit();
            this.pnAnamnesis.ResumeLayout(false);
            this.groupBox9.ResumeLayout(false);
            this.groupBox10.ResumeLayout(false);
            this.groupBox10.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.pnComplications.ResumeLayout(false);
            this.pnComplications.PerformLayout();
            this.pnDiagnosisHeader.ResumeLayout(false);
            this.pnDiagnosisHeader.PerformLayout();
            this.pnMiscInfo.ResumeLayout(false);
            this.pnResult.ResumeLayout(false);
            this.pnCenteredResult.ResumeLayout(false);
            this.pnCenteredResult.PerformLayout();
            this.gbSuperIntendent.ResumeLayout(false);
            this.gbSuperIntendent.PerformLayout();
            this.groupBox12.ResumeLayout(false);
            this.groupBox12.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.pnBottom.ResumeLayout(false);
            this.pnBottom.PerformLayout();
            this.gpVTEO.ResumeLayout(false);
            this.pnVTEO.ResumeLayout(false);
            this.pnVTEO.PerformLayout();
            this.pnStatusBar.ResumeLayout(false);
            this.pnWorkingArea.ResumeLayout(false);
            this.pnSection4.ResumeLayout(false);
            this.pnSection3.ResumeLayout(false);
            this.pnSection2.ResumeLayout(false);
            this.pnSection1.ResumeLayout(false);
            this.gpAddress.ResumeLayout(false);
            this.gpAddress.PerformLayout();
            this.pnRecord.ResumeLayout(false);
            this.pnRecord.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btVMP;
        private System.Windows.Forms.TextBox tbSecondName;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tbFirstName;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox tbThirdName;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.GroupBox gbPersonal;
        private System.Windows.Forms.Label label23;
        private PlainRichTextBox rtbDiagnosisPre;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Button btOpen;
        private System.Windows.Forms.Button btRevert;
        private System.Windows.Forms.Button btOpenDirectory;
        private System.Windows.Forms.Label label28;
        private ComboBoxEx cbSex;
        private ComboBoxEx cbDoctors;
        private System.Windows.Forms.Button btAccompMedicationCopy;
        private System.Windows.Forms.GroupBox gbDiagnoz;
        private System.Windows.Forms.Label label36;
        private PlainRichTextBoxExt rtbAccompanying;
        private System.Windows.Forms.MaskedTextBox tbDateExam;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Button btTestData;
        private System.Windows.Forms.TextBox tbAmbuCardNum;
        private System.Windows.Forms.TextBox tbClinicalRecordNum;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private ComboBoxEx cbGenCondition;
        private System.Windows.Forms.Label label13;
        private ComboBoxEx cbNutrition;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.GroupBox groupBox9;
        private System.Windows.Forms.GroupBox groupBox10;
        private System.Windows.Forms.MaskedTextBox tbTimeExam;
        private ComboBoxEx cbRh;
        private System.Windows.Forms.Label label53;
        private ComboBoxEx cbBloodGroup;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.Label label8;
        private PlainRichTextBox rtbStatusLocalisPre;
        private PlainRichTextBoxExt rtbAccompMedicationPre;
        private System.Windows.Forms.Label label33;
        private PlainRichTextBoxExt rtbComplicationsPre;
        private System.Windows.Forms.Label label42;
        private PlainRichTextBoxExt rtbAccompMedicationPost;
        private System.Windows.Forms.Label label54;
        private PlainRichTextBox rtbPlannedOperationScope;
        private PlainRichTextBoxExt rtbComplaints;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.GroupBox groupBox5;
        private ComboBoxEx cbCondition;
        private System.Windows.Forms.Label label58;
        private System.Windows.Forms.Label label59;
        private ComboBoxEx cbTreatmentResult;
        private System.Windows.Forms.Label label57;
        private System.Windows.Forms.MaskedTextBox tbDateDischarge;
        private FocusableCheckBox cbSickLeave;
        private System.Windows.Forms.Label lbSickLeave1;
        private System.Windows.Forms.TextBox tbSickLeaveNum;
        private System.Windows.Forms.Label lbSickLeave2;
        private System.Windows.Forms.MaskedTextBox tbSickLeaveEnd;
        private System.Windows.Forms.MaskedTextBox tbSickLeaveStart;
        private FocusableCheckBox cbSickLeaveExt;
        private System.Windows.Forms.Label lbSickLeave4;
        private System.Windows.Forms.MaskedTextBox tbSickLeaveExtEnd;
        private System.Windows.Forms.MaskedTextBox tbSickLeaveExtStart;
        private System.Windows.Forms.Label lbSickLeave3;
        private System.Windows.Forms.Button btGenExam;
        private SharedLibrary.ComboBoxEx tbSuperintendent;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.Label label64;
        private System.Windows.Forms.GroupBox gbSuperIntendent;
        private System.Windows.Forms.Button btAction;
        private System.Windows.Forms.Button btTest;
        private System.Windows.Forms.Label label55;
        private PlainRichTextBox rtbDiagnosisPost;
        private PlainRichTextBoxExt rtbComplicationsPost;
        private System.Windows.Forms.Button btPatients;
        private System.Windows.Forms.Button btSearch;
        private System.Windows.Forms.GroupBox groupBox12;
        private System.Windows.Forms.Button btCommissions;
        private System.Windows.Forms.CheckBox cbIO;
        private System.Windows.Forms.MaskedTextBox tbDateBirth;
        private System.Windows.Forms.Button btConclusions;
        private ucStatusBarControl ucStatusBar;
        private System.Windows.Forms.GroupBox groupBox4;
        private ComboBoxEx cbSuperIntendentFormatted;
        private System.Windows.Forms.Button btInstrumentalExamination;
        private System.Windows.Forms.Button btOperTeamNeuro;
        private System.Windows.Forms.Button btLifeAnamnesis;
        private System.Windows.Forms.Button btAnamnesis;
        private System.Windows.Forms.Panel pnVMPToggle;
        private System.Windows.Forms.PictureBox pbVMPHelp;
        private FocusableCheckBox cbVMP;
        private ComboBoxEx cbSickLeaveEndType;
        private System.Windows.Forms.Panel pnBottom;
        private ComboBoxEx cbDepartment;
        private System.Windows.Forms.Label label7;
        private ComboBoxEx cbVTEORisk;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.Label label49;
        private ComboBoxEx cbGORisk;
        private System.Windows.Forms.CheckedListBox clbVTEOPrevention;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.GroupBox gpVTEO;
        private System.Windows.Forms.Label label9;
        private ComboBoxEx cbDisability;
        private System.Windows.Forms.Button btRecommendations;
        private ComboBoxEx cbWorkCapacity;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Panel pnChemTherapyToggle;
        private System.Windows.Forms.PictureBox pbChemHelp;
        private FocusableCheckBox cbChemTherapy;
        private System.Windows.Forms.Panel pnStatusBar;
        private System.Windows.Forms.Panel pnResult;
        private System.Windows.Forms.Panel pnWorkingArea;
        private System.Windows.Forms.Panel pnSection1;
        private System.Windows.Forms.Panel pnRecord;
        private System.Windows.Forms.Panel pnSection2;
        private System.Windows.Forms.Panel pnSection3;
        private System.Windows.Forms.Panel pnSection4;
        private System.Windows.Forms.GroupBox gpAddress;
        private SharedLibrary.ComboBoxEx cbStreet;
        private SharedLibrary.ComboBoxEx cbTown;
        private SharedLibrary.ComboBoxEx cbStreetType;
        private System.Windows.Forms.RadioButton rbVillage;
        private System.Windows.Forms.RadioButton rbTown;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox tbBuilding;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox tbFlat;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox tbKorpus;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox tbOblast;
        private System.Windows.Forms.MaskedTextBox tbIndex;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Button btDisabilityListInfo;
        private System.Windows.Forms.Panel pnVTEO;
        private System.Windows.Forms.Panel pnCenteredResult;
        private System.Windows.Forms.Panel pnMiscInfo;
        private System.Windows.Forms.Panel pnMiscSplitter1;
        private System.Windows.Forms.Button btLifeAnamnesisPost;
        private System.Windows.Forms.Button btAdditionalInfo;
        private System.Windows.Forms.Panel pnMiscSplitter2;
        private System.Windows.Forms.Label label22;
        private PlainRichTextBoxExt rtbMedication;
        private System.Windows.Forms.Label label21;
        private PlainRichTextBoxExt rtbTreatment;
        private System.Windows.Forms.Panel pnDiagnosisHeader;
        private System.Windows.Forms.Label label24;
        private TextBoxEx tbMKBDiagnosis;
        private System.Windows.Forms.Panel pnComplications;
        private System.Windows.Forms.Label label25;
        private TextBoxEx tbMKBComplications;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Panel pnSplitter2;
        private System.Windows.Forms.Panel pnSplitter1;
        private System.Windows.Forms.Label label31;
        private PlainRichTextBox rtbDiseaseMoreInfo;
        private System.Windows.Forms.Label label30;
        private PlainRichTextBox rtbPlannedExamination;
        private System.Windows.Forms.Panel pnSplitter4;
        private System.Windows.Forms.Panel pnPlannedTreatment;
        private System.Windows.Forms.Panel pnSplitter3;
        private System.Windows.Forms.Label label29;
        private PlainRichTextBox rtbAssignments;
        private System.Windows.Forms.Panel pnAnamnesis;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label32;
        private TextBoxEx tbMKBAccompanying;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label38;
        private PlainRichTextBoxExt rtbAccompanyingPost;
        private PlainRichTextBoxExt rtbMedicalAllergy;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Button btTransfusion;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Button btStatusLocalisPost;
    }
}

