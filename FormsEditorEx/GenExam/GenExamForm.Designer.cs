﻿namespace FormsEditorEx.GenExam
{
    partial class GenExamForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.pnOKCancel = new System.Windows.Forms.Panel();
            this.btOK = new System.Windows.Forms.Button();
            this.btTestData = new System.Windows.Forms.Button();
            this.pnResult = new System.Windows.Forms.Panel();
            this.rtbFinalResult = new System.Windows.Forms.RichTextBox();
            this.pnRecords = new System.Windows.Forms.Panel();
            this.btRemoveRecord = new System.Windows.Forms.Button();
            this.dgvRecords = new System.Windows.Forms.DataGridView();
            this.pnPadding = new System.Windows.Forms.Panel();
            this.tabView = new FormsEditorEx.GenExam.ucExamTabView();
            this.pnOKCancel.SuspendLayout();
            this.pnResult.SuspendLayout();
            this.pnRecords.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvRecords)).BeginInit();
            this.SuspendLayout();
            // 
            // pnOKCancel
            // 
            this.pnOKCancel.BackColor = System.Drawing.SystemColors.Control;
            this.pnOKCancel.Controls.Add(this.btOK);
            this.pnOKCancel.Controls.Add(this.btTestData);
            this.pnOKCancel.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnOKCancel.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.pnOKCancel.Location = new System.Drawing.Point(12, 503);
            this.pnOKCancel.Name = "pnOKCancel";
            this.pnOKCancel.Size = new System.Drawing.Size(1041, 45);
            this.pnOKCancel.TabIndex = 1;
            // 
            // btOK
            // 
            this.btOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.btOK.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btOK.Location = new System.Drawing.Point(939, 13);
            this.btOK.Name = "btOK";
            this.btOK.Size = new System.Drawing.Size(103, 32);
            this.btOK.TabIndex = 1;
            this.btOK.Text = "OK";
            this.btOK.UseVisualStyleBackColor = true;
            this.btOK.Click += new System.EventHandler(this.btOK_Click);
            // 
            // btTestData
            // 
            this.btTestData.Location = new System.Drawing.Point(0, 13);
            this.btTestData.Name = "btTestData";
            this.btTestData.Size = new System.Drawing.Size(63, 32);
            this.btTestData.TabIndex = 0;
            this.btTestData.Text = "TestData";
            this.btTestData.UseVisualStyleBackColor = true;
            this.btTestData.Click += new System.EventHandler(this.btTestData_Click);
            // 
            // pnResult
            // 
            this.pnResult.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.pnResult.BackColor = System.Drawing.SystemColors.Control;
            this.pnResult.Controls.Add(this.rtbFinalResult);
            this.pnResult.Controls.Add(this.pnRecords);
            this.pnResult.Controls.Add(this.pnPadding);
            this.pnResult.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnResult.Location = new System.Drawing.Point(616, 12);
            this.pnResult.Name = "pnResult";
            this.pnResult.Size = new System.Drawing.Size(437, 491);
            this.pnResult.TabIndex = 2;
            // 
            // rtbFinalResult
            // 
            this.rtbFinalResult.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rtbFinalResult.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.rtbFinalResult.Location = new System.Drawing.Point(24, 234);
            this.rtbFinalResult.Name = "rtbFinalResult";
            this.rtbFinalResult.ReadOnly = true;
            this.rtbFinalResult.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.Vertical;
            this.rtbFinalResult.ShowSelectionMargin = true;
            this.rtbFinalResult.Size = new System.Drawing.Size(413, 257);
            this.rtbFinalResult.TabIndex = 3;
            this.rtbFinalResult.TabStop = false;
            this.rtbFinalResult.Text = "";
            // 
            // pnRecords
            // 
            this.pnRecords.BackColor = System.Drawing.SystemColors.Control;
            this.pnRecords.Controls.Add(this.btRemoveRecord);
            this.pnRecords.Controls.Add(this.dgvRecords);
            this.pnRecords.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnRecords.Location = new System.Drawing.Point(24, 0);
            this.pnRecords.Name = "pnRecords";
            this.pnRecords.Size = new System.Drawing.Size(413, 234);
            this.pnRecords.TabIndex = 2;
            // 
            // btRemoveRecord
            // 
            this.btRemoveRecord.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btRemoveRecord.Location = new System.Drawing.Point(261, 189);
            this.btRemoveRecord.Margin = new System.Windows.Forms.Padding(0);
            this.btRemoveRecord.Name = "btRemoveRecord";
            this.btRemoveRecord.Size = new System.Drawing.Size(153, 36);
            this.btRemoveRecord.TabIndex = 2;
            this.btRemoveRecord.Text = "Удалить запись";
            this.btRemoveRecord.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btRemoveRecord.UseVisualStyleBackColor = true;
            this.btRemoveRecord.Click += new System.EventHandler(this.btRemoveRecord_Click);
            // 
            // dgvRecords
            // 
            this.dgvRecords.AllowUserToAddRows = false;
            this.dgvRecords.AllowUserToDeleteRows = false;
            this.dgvRecords.AllowUserToResizeColumns = false;
            this.dgvRecords.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.AliceBlue;
            this.dgvRecords.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvRecords.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvRecords.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dgvRecords.BackgroundColor = System.Drawing.SystemColors.ControlLight;
            this.dgvRecords.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgvRecords.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dgvRecords.ColumnHeadersVisible = false;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvRecords.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgvRecords.Dock = System.Windows.Forms.DockStyle.Top;
            this.dgvRecords.Location = new System.Drawing.Point(0, 0);
            this.dgvRecords.MultiSelect = false;
            this.dgvRecords.Name = "dgvRecords";
            this.dgvRecords.ReadOnly = true;
            this.dgvRecords.RowHeadersVisible = false;
            this.dgvRecords.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dgvRecords.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dgvRecords.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvRecords.Size = new System.Drawing.Size(413, 186);
            this.dgvRecords.TabIndex = 1;
            this.dgvRecords.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvRecords_CellClick);
            this.dgvRecords.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.dgvRecords_CellFormatting);
            this.dgvRecords.DataBindingComplete += new System.Windows.Forms.DataGridViewBindingCompleteEventHandler(this.dgvRecords_DataBindingComplete);
            // 
            // pnPadding
            // 
            this.pnPadding.BackColor = System.Drawing.SystemColors.Control;
            this.pnPadding.Dock = System.Windows.Forms.DockStyle.Left;
            this.pnPadding.Location = new System.Drawing.Point(0, 0);
            this.pnPadding.Name = "pnPadding";
            this.pnPadding.Size = new System.Drawing.Size(24, 491);
            this.pnPadding.TabIndex = 2;
            // 
            // tabView
            // 
            this.tabView.AutoScroll = true;
            this.tabView.BackColor = System.Drawing.SystemColors.Control;
            this.tabView.Dock = System.Windows.Forms.DockStyle.Left;
            this.tabView.Location = new System.Drawing.Point(12, 12);
            this.tabView.Name = "tabView";
            this.tabView.Size = new System.Drawing.Size(604, 491);
            this.tabView.TabIndex = 0;
            // 
            // PostGenExamForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(1065, 560);
            this.ControlBox = false;
            this.Controls.Add(this.pnResult);
            this.Controls.Add(this.tabView);
            this.Controls.Add(this.pnOKCancel);
            this.Name = "PostGenExamForm";
            this.Padding = new System.Windows.Forms.Padding(12);
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.Text = "Общее обследование";
            this.pnOKCancel.ResumeLayout(false);
            this.pnResult.ResumeLayout(false);
            this.pnRecords.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvRecords)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private ucExamTabView tabView;
        private System.Windows.Forms.Panel pnOKCancel;
        private System.Windows.Forms.Panel pnResult;
        private System.Windows.Forms.DataGridView dgvRecords;
        private System.Windows.Forms.Panel pnRecords;
        private System.Windows.Forms.Panel pnPadding;
        private System.Windows.Forms.Button btTestData;
        private System.Windows.Forms.RichTextBox rtbFinalResult;
        private System.Windows.Forms.Button btRemoveRecord;
        private System.Windows.Forms.Button btOK;
    }
}