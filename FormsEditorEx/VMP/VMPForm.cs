﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using Microsoft.Office.Interop.Word;
using System.Runtime.InteropServices;
using System.IO;
using System.Reflection;
using SharedLibrary;
using System.Linq;
using WordApplication = Microsoft.Office.Interop.Word.Application;
using Application = System.Windows.Forms.Application;
using SharedLibrary.Settings;
using FormsEditorEx.Database;

namespace FormsEditorEx.VMP
{
    public partial class VMPForm : Form
    {
        string lastpath;
        DatabaseAdapter dbAdapter;

        public bool IsDone { get; set; }

        public VMPForm(DatabaseAdapter dbAdapter)
        {
            this.dbAdapter = dbAdapter;

            InitializeComponent();
            InitComponents();

            Utility.ShrinkFormIfNeeded(this);

            this.Text = "ВМП";
            this.ActiveControl = tbSecondName;
            this.Visible = false;
        }

        private void InitComponents()
        {
            InfoClass info = AppConfig.Instance.Info;

            tbCMO.Text = AppConfig.Instance.Staff.Administration.CMO.ShortName;

            tbOblast.Text = "Ярославская";

            this.Controls.SetChildIndex(btSelectCommission, 0);

            foreach (var dep in info.Departments)
                tscbDepartment.Items.Add(dep);

            foreach (string type in info.VMP.TreatmentTypes)
                cbTreatmentType.Items.Add(type);

            foreach (string fin in info.VMP.FinanceTypes)
                cbVMPFinance.Items.Add(fin);

            cbDisability.DataSource = Static.DisabilityName.Values.ToList();
            cbDisability.SelectedIndex = -1;

            cbSex.DrawItem += Utility.cbxDesign_DrawItem;
            cbStreetType.DrawItem += Utility.cbxDesign_DrawItem;
            cbStatus.DrawItem += Utility.cbxDesign_DrawItem;
            cbVMPFinance.DrawItem += Utility.cbxDesign_DrawItem;
            cbTreatmentType.DrawItem += Utility.cbxDesign_DrawItem;
        }

        //private void CreateFormatting()
        //{
        //    docFormat = new DocFormat();

        //    Sizing sizing;
        //    PageFormat pageFormat;

        //    sizing = new Sizing();
        //    pageFormat = new PageFormat();
        //    pageFormat.Sizings = new Dictionary<string, Sizing>();
        //    docFormat.Pages = new Dictionary<int, PageFormat>();

        //    sizing.Header = 12;
        //    sizing.Content = 11;
        //    sizing.MaxLines = 60;

        //    pageFormat.Sizings.Add("size 0", sizing);

        //    sizing = new Sizing();

        //    sizing.Header = 11;
        //    sizing.Content = 11;
        //    sizing.MaxLines = 61;

        //    pageFormat.Sizings.Add("size 1", sizing);

        //    pageFormat.isScalable = true;
        //    pageFormat.MaxTopPadding = 3;

        //    docFormat.Pages.Add(3, pageFormat);
        //    string json = JsonConvert.SerializeObject(docFormat, Formatting.Indented);
        //    File.WriteAllText(Path.Settings + "format", json);
        //}

        private bool CheckFields()
        {
            if (tscbDepartment.SelectedIndex == -1)
            {
                MessageBox.Show("Не выбрано отделение");
                return false;
            }

            if (string.IsNullOrWhiteSpace(tbDoctor.Text.Trim()))
            {
                MessageBox.Show("Не указан лечащий врач");
                return false;
            }

            if (string.IsNullOrWhiteSpace(tbCardNumber.Text.Trim()))
            {
                MessageBox.Show("Не указан номер амбулаторной карты");
                return false;
            }

            if (string.IsNullOrWhiteSpace(tbSecondName.Text.Trim()))
            {
                MessageBox.Show("Не указана фамилия");
                return false;
            }

            if (string.IsNullOrWhiteSpace(tbFirstName.Text.Trim()))
            {
                MessageBox.Show("Не указано имя");
                return false;
            }

            if (string.IsNullOrWhiteSpace(tbThirdName.Text.Trim()))
            {
                MessageBox.Show("Не указано отчество");
                return false;
            }

            if (cbSex.SelectedIndex == -1)
            {
                MessageBox.Show("Не указан пол");
                return false;
            }

            return true;
        }

        private void btApply_Click(object sender, EventArgs e)
        {
            if (!CheckFields())
                return;

            this.Enabled = false;
            bool isDocFilled = false;

            ucStatusBar.ShowWarning("Выполняется...");

            WordApplication wordApp = null;
            Microsoft.Office.Interop.Word.Document doc = null;
            Range range;
            string temp = string.Empty;

            try
            {
                wordApp = new WordApplication();
                doc = wordApp.Documents.Open(Path.Combine(Paths.Instance.Settings, "template"), ReadOnly: true);

                doc.Activate();
                wordApp.ActiveWindow.View.ReadingLayout = false;

                WordInterop.FindAndReplace(wordApp, "%sn", tbSecondName.Text);
                WordInterop.FindAndReplace(wordApp, "%fn", tbFirstName.Text);
                WordInterop.FindAndReplace(wordApp, "%tn", tbThirdName.Text);

                temp = tbDateBorn.Text;

                int j = 0;
                for (int i = 0; i < temp.Length; i++)
                {
                    if (temp[i] != '.')
                    {
                        string find = String.Format("%d{0}", j);
                        string replace = temp[i].ToString();
                        WordInterop.FindAndReplace(wordApp, find, replace);
                        j++;
                    }
                }

                for (; j <= 7; j++)
                {
                    string find = String.Format("%d{0}", j);
                    WordInterop.FindAndReplace(wordApp, find, "");
                }

                WordInterop.FindAndReplace(wordApp, "%db", tbDateBorn.Text);

                temp = "";
                int index = cbSex.SelectedIndex + 1;
                if (index > 0)
                    temp = index.ToString();
                WordInterop.FindAndReplace(wordApp, "%sx", temp);

                temp = "";
                index = cbSex.SelectedIndex;
                if (index > -1)
                    temp = cbSex.GetItemText(cbSex.SelectedItem);
                WordInterop.FindAndReplace(wordApp, "%pl", temp);

                WordInterop.FindAndReplace(wordApp, "%addr0", tbIndex.Text);
                WordInterop.FindAndReplace(wordApp, "%addr1", $"{tbOblast.Text} область, {cbTown.Text}");

                switch (cbStreetType.SelectedIndex)
                {
                    case 1:
                        if (cbStreet.SelectedIndex > -1)
                        {
                            InfoClass.Prospect prospect = AppConfig.Instance.Info.Prospects[cbStreet.SelectedIndex];
                            if (prospect.Align == ProspectAlign.Left)
                                temp = string.Format("пр-т {0}", prospect.Name);
                            else
                                temp = string.Format("{0} пр-т", prospect.Name);
                        }
                        else
                            temp = string.Format("пр-т {0}", cbStreet.Text);
                        break;
                    case 2:
                        temp = string.Format("{0} проезд", cbStreet.Text);
                        break;
                    default:
                        temp = cbStreet.Text;
                        break;
                }

                WordInterop.FindAndReplace(wordApp, "%addr2", temp);                
                WordInterop.FindAndReplace(wordApp, "%addr3", tbBuilding.Text);
                WordInterop.FindAndReplace(wordApp, "%addr4", tbKorpus.Text);
                WordInterop.FindAndReplace(wordApp, "%addr5", tbFlat.Text);

                if (rbTown.Checked == true)
                    temp = "1";
                else
                    temp = "2";

                WordInterop.FindAndReplace(wordApp, "%ad6", temp);

                temp = "";
                if(tbIndex.Text.Length > 0)
                    temp += String.Format("{0} ", tbIndex.Text);

                temp += String.Format("{0}" + Symbols.NBSpace + "обл., ", tbOblast.Text);

                if (rbTown.Checked == true)
                    temp += "г." + Symbols.NBSpace;
                else
                    temp += "c." + Symbols.NBSpace;

                temp += cbTown.Text;

                temp += @",%split";

                switch(cbStreetType.SelectedIndex)
                {
                    case 1:
                        if (cbStreet.SelectedIndex > -1)
                        {
                            InfoClass.Prospect prospect = AppConfig.Instance.Info.Prospects[cbStreet.SelectedIndex];
                            if (prospect.Align == ProspectAlign.Left)
                                temp += String.Format("пр-т" + Symbols.NBSpace + "{0}", prospect.Name);
                            else
                                temp += String.Format("{0}" + Symbols.NBSpace + "пр-т", prospect.Name);
                        }
                        else
                            temp += String.Format("пр-т" + Symbols.NBSpace + "{0}", cbStreet.Text);
                        break;
                    case 2:
                        temp += String.Format("{0}" + Symbols.NBSpace + "проезд", cbStreet.Text);
                        break;
                    default:
                        temp += String.Format("ул." + Symbols.NBSpace + "{0}", cbStreet.Text);
                        break;
                }

                temp += String.Format(", д." + Symbols.NBSpace + "{0}", tbBuilding.Text);

                if (tbKorpus.Text.Length > 0)
                    temp += String.Format(", корп." + Symbols.NBSpace + "{0}", tbKorpus.Text);

                if (tbFlat.Text.Length > 0)
                    temp += String.Format(", кв." + Symbols.NBSpace + "{0}", tbFlat.Text);

                WordInterop.FindAndReplace(wordApp, "%addr", temp.Replace("%split", " "));

                string[] addr = temp.Split(new string[] { "%split" }, StringSplitOptions.None);
                if(addr.Length == 2)
                {
                    WordInterop.FindAndReplace(wordApp, "%addp1", addr[0]);
                    WordInterop.FindAndReplace(wordApp, "%addp2", addr[1]);

                    temp = addr[1];
                    if(tbPhone.TextLength > 0)
                        temp += String.Format(", тел." + Symbols.NBSpace + "{0}", tbPhone.Text);
                    WordInterop.FindAndReplace(wordApp, "%addp3", temp);
                }

                WordInterop.FindAndReplace(wordApp, "%ph", tbPhone.Text);
                WordInterop.FindAndReplace(wordApp, "%pn", tbPassId.Text);
                WordInterop.FindAndReplace(wordApp, "%pv", rtbPassFrom.Text);
                WordInterop.FindAndReplace(wordApp, "%pd", tbPassDate.Text);
                WordInterop.FindAndReplace(wordApp, "%pens_id", tbPensId.Text);

                temp = tbPensId.Text.Trim();

                j = 10;
                for (int i = temp.Length - 1; i >= 0; i--)
                {
                    if (temp[i] != '-')
                    {
                        string find = String.Format("%ps{0}", j);
                        string replace = temp[i].ToString();
                        WordInterop.FindAndReplace(wordApp, find, replace);
                        j--;
                    }
                }

                for (; j >= 0; j--)
                {
                    string find = String.Format("%ps{0}", j);
                    WordInterop.FindAndReplace(wordApp, find, "");
                }

                WordInterop.FindAndReplace(wordApp, "%ins_comp", tbInsComp.Text);
                WordInterop.FindAndReplace(wordApp, "%ins_id", tbInsId.Text);

                WordInterop.FindAndReplace(wordApp, "%inv", cbDisability.Text);
                WordInterop.FindAndReplace(wordApp, "%in", cbDisability.SelectedIndex > 0 ? cbDisability.SelectedIndex.ToString() : string.Empty);

                WordInterop.FindAndReplace(wordApp, "%ss", cbStatus.GetItemText(cbStatus.SelectedItem));

                temp = "";
                if (cbStatus.SelectedIndex >= 0)
                    temp = (cbStatus.SelectedIndex + 1).ToString();

                WordInterop.FindAndReplace(wordApp, "%si", temp);

                temp = tbMKBcode.Text.Replace(" ", string.Empty);
                WordInterop.FindAndReplace(wordApp, "%mkb", temp);

                j = 0;
                for (int i = 0; i < temp.Length; i++)
                {
                    string find = String.Format("%m{0}", j);
                    string replace = temp[i].ToString();
                    WordInterop.FindAndReplace(wordApp, find, replace);
                    j++;
                }

                for (; j <= 5; j++)
                {
                    string find = String.Format("%m{0}", j);
                    WordInterop.FindAndReplace(wordApp, find, "");
                }

                j = tbExemption.Text.Length - 1;
                for (int i = 2; i >= 0; i--, j--)
                {
                    if (j >= 0)
                        temp = tbExemption.Text[j].ToString();
                    else
                        temp = "0";

                    string find = String.Format("%e{0}", i);
                    WordInterop.FindAndReplace(wordApp, find, temp);
                }

                if (tbExemption.Text.Length >= 1)
                    temp = tbExemption.Text;
                else
                    temp = "";

                WordInterop.FindAndReplace(wordApp, "%en", temp);

                range = doc.Content;
                while (range.Find.Execute("%dgnz"))
                {
                    range.Text = rtbDiagnoz.Text;
                    range = doc.Content;
                }

                WordInterop.FindAndReplace(wordApp, "%grp", tbVMPGroup.Text);

                string finance = "";

                switch(cbVMPFinance.SelectedIndex)
                {
                    case (int)Finance.OMS:
                        finance = "ОМС";
                        break;
                    case (int)Finance.Region:
                        finance = "Рег";
                        break;
                }

                WordInterop.FindAndReplace(wordApp, "%fin1", finance);

                switch(cbVMPFinance.SelectedIndex)
                {
                    case (int)Finance.OMS:
                        temp = "ОМС";
                        break;
                    case (int)Finance.Region:
                        temp = "регионального бюджета";
                        break;
                    default:
                        temp = "";
                        break;
                }

                WordInterop.FindAndReplace(wordApp, "%fin2", temp);

                temp = "";
                if (cbTreatmentType.SelectedIndex > -1)
                    temp = cbTreatmentType.GetItemText(cbTreatmentType.SelectedItem);

                WordInterop.FindAndReplace(wordApp, "%trtype", temp);

                if (cbTreatmentType.SelectedIndex > -1)
                    temp = temp.Substring(0, temp.Length - 1) + "го";

                WordInterop.FindAndReplace(wordApp, "%trtypo", temp);

                temp = tbModelCode.Text;
                if(tbModelCode.TextLength > 0)
                    temp += " ";

                WordInterop.FindAndReplace(wordApp, "%model_code", temp);
                WordInterop.FindAndReplace(wordApp, "%model_desc", rtbModel.Text);

                temp = tbMethodCode.Text;
                if (tbMethodCode.TextLength > 0)
                    temp += " ";

                WordInterop.FindAndReplace(wordApp, "%mthd_code", temp);
                WordInterop.FindAndReplace(wordApp, "%mthd_desc", rtbMethod.Text);

                if (cbVMPFinance.SelectedIndex == (int)Finance.Region || cbVMPFinance.SelectedIndex == -1)
                {
                    WordInterop.FindAndReplace(wordApp, "%vmp_code1", string.Empty);
                    WordInterop.FindAndReplace(wordApp, "%vmp_code2", string.Empty);
                }
                else
                {
                    temp = Environment.NewLine + tbVMPcode.Text;
                    WordInterop.FindAndReplace(wordApp, "%vmp_code2", temp);

                    range = doc.Content;
                    while (range.Find.Execute("%vmp_code1"))
                    {
                        range.Text = Environment.NewLine;
                        range = doc.Range(range.End, range.End);

                        range.Text = $"Код вида ВМП: ";
                        range.Bold = 1;

                        range = doc.Range(range.End, range.End);
                        range.Text = tbVMPcode.Text;
                        range.Bold = 0;

                        range = doc.Content;
                    }
                }

                WordInterop.FindAndReplace(wordApp, "%vmp_code", tbVMPcode.Text);
                WordInterop.FindAndReplace(wordApp, "%vmp_desc", rtbVMPDesc.Text);

                WordInterop.FindAndReplace(wordApp, "%dctr", tbDoctor.Text);

                if(rtbOslozhnenie.Text.Length == 0)
                    temp = "нет";
                else
                    temp = rtbOslozhnenie.Text;

                WordInterop.FindAndReplace(wordApp, "%oslozhn", temp);

                if (rtbSoputstv.Text.Length == 0)
                    temp = "нет";
                else
                    temp = rtbSoputstv.Text;

                WordInterop.FindAndReplace(wordApp, "%soput", temp);
                WordInterop.FindAndReplace(wordApp, "%cmpl", rtbComplaints.Text);
                WordInterop.FindAndReplace(wordApp, "%anmnz", rtbAnamnez.Text);
                WordInterop.FindAndReplace(wordApp, "%nstr", rtbInstrumentalExamination.Text);
                WordInterop.FindAndReplace(wordApp, "%gnrl", rtbGeneralExamination.Text);
                WordInterop.FindAndReplace(wordApp, "%chrm", tbChairman.Text);

                Regex reg = new Regex(@"[а-яА-Я]+\s[а-яА-Я]\.[а-яА-Я]\.");
                MatchCollection matches = reg.Matches(tbMembers.Text);

                temp = "";

                for (int i = 0; i < matches.Count; i++ )
                {
                    temp += matches[i].Value;
                    if (i + 1 < matches.Count)
                        temp += ", ";
                }

                WordInterop.FindAndReplace(wordApp, "%mmbrs", temp);

                WordInterop.FindAndReplace(wordApp, "%cardid", tbCardNumber.Text);
                WordInterop.FindAndReplace(wordApp, "%zav", tbSuperintendent.Text);
                WordInterop.FindAndReplace(wordApp, "%cmo", tbCMO.Text);

                WordInterop.FindAndReplace(wordApp, "%dtc", tbCommissionDate.Text);
                WordInterop.FindAndReplace(wordApp, "%dtg", tbGospDate.Text);
                WordInterop.FindAndReplace(wordApp, "%dto", tbOperationDate.Text);

                temp = Static.DepartmentNames[tscbDepartment.SelectedIndex];

                if (tscbDepartment.SelectedIndex != (int)Departments.Surgical)
                    temp = temp.ToLowerInvariant();

                WordInterop.FindAndReplace(wordApp, "%dprt", temp);

                if (cbSex.SelectedIndex == 1)
                    temp = "ке";
                else
                    temp = "у";

                WordInterop.FindAndReplace(wordApp, "%sp1", temp);

                if (cbSex.SelectedIndex == 1)
                    temp = "й";
                else
                    temp = "му";

                WordInterop.FindAndReplace(wordApp, "%sp2", temp);

                range = doc.Content;
                temp = "предупрежден";

                if(range.Find.Execute("%wrndmale"))
                {
                    range.Text = temp;

                    if(cbSex.SelectedIndex == 0)
                        range.Underline = WdUnderline.wdUnderlineSingle;
                }

                range = doc.Content;
                temp += "а";

                if (range.Find.Execute("%wrndfemale"))
                {
                    range.Text = temp;

                    if (cbSex.SelectedIndex == 1)
                        range.Underline = WdUnderline.wdUnderlineSingle;
                }

                range = doc.Content;
                temp = "согласен";

                if (range.Find.Execute("%agrdmale"))
                {
                    range.Text = temp;

                    if (cbSex.SelectedIndex == 0)
                        range.Underline = WdUnderline.wdUnderlineSingle;
                }

                range = doc.Content;
                temp = "согласна";

                if (range.Find.Execute("%agrdfemale"))
                {
                    range.Text = temp;

                    if (cbSex.SelectedIndex == 1)
                        range.Underline = WdUnderline.wdUnderlineSingle;
                }

                temp = string.Empty;

                if (!cbIO.Checked)
                    temp = "Заведующий";
                else
                    temp = "И.о. заведующего";

                WordInterop.FindAndReplace(wordApp, "%st", temp);

                Patient.Instance.PatientData.Conclusions.BuildDocString(doc, "%cncl");

                int year = DateTime.Now.Year;

                WordInterop.FindAndReplace(wordApp, "%yr1", year.ToString());
                WordInterop.FindAndReplace(wordApp, "%yr2", (year + 1).ToString());
                WordInterop.FindAndReplace(wordApp, "%yr3", (year + 2).ToString());

                WordInterop.AdjustPages(doc, AppConfig.Instance.DocFormatVMP);

                temp = Path.Combine(GetSolutionsDirectory(), GetFileName(finance));

                doc.SaveAs(temp);
                lastpath = temp;
                isDocFilled = true;
                btOpen.Enabled = true;

                SetOutputData();

                if (Patient.Save(dbAdapter, ucStatusBar))
                { 
                    ucStatusBar.ShowOK("Заполнение формы выполнено успешно. Данные пациента сохранены.");
                    IsDone = true;
                    btClose.Text = "OK";
                }

            }
            catch(COMException comex)
            {
                if (comex.ErrorCode == -2146822932)
                {
                    MessageBox.Show("Файл " + temp + " недоступен для записи. Скорее всего, открыт в текстовом редакторе");
                    ucStatusBar.ShowError("Заполнить форму не удалось. Данные не сохранены");
                }
                else
                {
                    ucStatusBar.ShowError("Заполнить форму не удалось. Данные не сохранены");
                    Logger.AddToLogs(MethodBase.GetCurrentMethod().Name, comex.ToString());
                }
            }
            catch(Exception ex)
            {
                if (isDocFilled)
                    ucStatusBar.ShowError("Форма заполнена. Но данные пациента не сохранены.");
                else
                {
                    ucStatusBar.ShowError("Заполнить форму не удалось. Данные пациента не сохранены.");
                    try
                    {
                        if (doc != null)
                            doc.Close(WdSaveOptions.wdDoNotSaveChanges);
                    }
                    catch { }
                }
                Logger.AddToLogs(MethodBase.GetCurrentMethod().Name, ex.ToString());
            }
            finally
            {
                this.Enabled = true;
                this.Refresh();

                try
                {
                    if (doc != null)
                        doc.Close(WdSaveOptions.wdDoNotSaveChanges);
                    ReleaseCOM(doc);
                    if (wordApp != null)
                        wordApp.Quit(WdSaveOptions.wdPromptToSaveChanges);
                    ReleaseCOM(wordApp);
                }
                catch (Exception ex)
                {
                    Logger.AddToLogs(MethodBase.GetCurrentMethod().Name, ex.ToString());
                }
            }
        }

        private string GetSolutionsDirectory()
        {
            string path = Path.Combine(Paths.Instance.Solutions, tscbDepartment.Text);

            if (!Directory.Exists(path))
                Directory.CreateDirectory(path);

            path = Path.Combine(path, tbDoctor.Text);

            if (!Directory.Exists(path))
                Directory.CreateDirectory(path);

            return path;
        }
        private string GetFileName(string finance)
        {
            return Utility.RemoveSpecialCharacters($"{tbCardNumber.Text.Trim()} {tbSecondName.Text.Trim()} {tbFirstName.Text.Substring(0, 1)}.{tbThirdName.Text.Substring(0, 1)}. {Utility.DateToShortString(DateTime.Now)} ВМП {finance}.doc");
        }
       
        private void TextBox_KeyDown(object sender, KeyEventArgs e)
        {
            Utility.ReturnToTab(this, e);

            if (e.Shift && e.KeyCode == Keys.F7)
            {
                using(AdminForm adminForm = new AdminForm())
                {
                    adminForm.ShowDialog();
                }
            }
        }

        private void tbFlat_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyData == Keys.Enter)
            {
                e.SuppressKeyPress = true;
                this.ActiveControl = tbPhone;
            }
        }

        private void ClearForm()
        {
            Utility.ClearControls(this, new List<string>() { "tbCMO" });

            rbTown.Checked = true;
            cbIO.Checked = false;
            tbOblast.Text = "Ярославская";
        }

        private void btOpen_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start(lastpath);
        }
        private void btOpenDirectory_Click(object sender, EventArgs e)
        {
#if !DEBUG
            System.Diagnostics.Process.Start(GetSolutionsDirectory());
#else
            System.Diagnostics.Process.Start(GetSolutionsDirectory());
#endif
        }

        private void FillTestData()
        {
            tbDateBorn.Text = "19041960";
            cbStatus.SelectedIndex = 5;
            tbInsId.Text = "7655555550000555";
            tbInsComp.Text = "ООО РГС Медицина";
            tbPensId.Text = "05456111555";
            tbPassDate.Text = "11042006";
            rtbPassFrom.Text = "Красноперекопским РОВД гор. Ярославля";
            tbPassId.Text = "7800555000";
            tbPhone.Text = "8-905-111-33-00";
            cbTown.Text = "Ярославль";
            cbStreet.Text = "Архангельский";
            cbStreetType.SelectedIndex = 2;
            tbBuilding.Text = "3";
            tbFlat.Text = "56";
            tbKorpus.Text = "2";
            tbIndex.Text = "150020";
            tbMethodCode.Text = "324";
            tbModelCode.Text = "13533";
            cbTreatmentType.SelectedIndex = 0;
            rtbModel.Text = "злокачественные новообразования тела матки (местнораспространенные формы). Злокачественные новообразования эндометрия (I - III стадия) с осложненным соматическим статусом (тяжелая степень ожирения, тяжелая степень сахарного диабета и т.д.)";
            cbVMPFinance.SelectedIndex = 0;
            tbVMPGroup.Text = "20";
            tbVMPcode.Text = "09.00.20.002";
            tbMKBcode.Text = "C54";
            rtbVMPDesc.Text = "Реконструктивно-пластические, микрохирургические, обширные циторедуктивные, расширенно-комбинированные хирургические вмешательства, в том числе с применением физических факторов (гипертермия, радиочастотная термоаблация, фотодинамическая терапия, лазерная и криодеструкция и др.) при злокачественных новообразованиях, в том числе у детей";
            rtbMethod.Text = "экстирпация матки с придатками";
            tbOperationDate.Text = "18092018";
            tbGospDate.Text = "14092018";
            tbCommissionDate.Text = "11092018";
        }

        private void MainFrame_FormClosed(object sender, FormClosedEventArgs e)
        {
            try
            {
                Visible = false;

            }
            catch (Exception ex)
            {
                Logger.AddToLogs(MethodBase.GetCurrentMethod().Name, ex.ToString());
            }
        }

        private void ReleaseCOM(object o)
        {
            try
            {
                while (System.Runtime.InteropServices.Marshal.ReleaseComObject(o) > 0) ;
            }
            catch (Exception ex)
            {
                Logger.AddToLogs(MethodBase.GetCurrentMethod().Name, ex.ToString());
            }
            finally
            {
                o = null;
            }
        }
        private void MainFrame_Load(object sender, EventArgs e)
        {
#if !DEBUG
            miSettings.Visible = false;
            btTestData.Visible = false;
#endif

            this.Activate();
        }
        private void miVMP_Click(object sender, EventArgs e)
        {
            //try
            //{
            //    using (VMPSettings vmpForm = new VMPSettings(AppConfig.Instance.Info, tscbDepartment.SelectedIndex))
            //    {
            //        //vmpForm.Owner = this;
            //        vmpForm.ShowDialog(this);
            //        this.BringToFront();
            //    }
            //}
            //catch(Exception ex)
            //{
            //    Logger.AddToLogs(MethodBase.GetCurrentMethod().Name, ex.ToString());
            //}
        }

        private void btVMP_Click(object sender, EventArgs e)
        {
            if(tscbDepartment.SelectedIndex == -1)
            {
                MessageBox.Show("Нужно выбрать отделение");
                return;
            }

            SharedLibrary.Settings.VMP vmp = new SharedLibrary.Settings.VMP();
            SimpleResult vmpLoadResult = vmp.Load(Paths.Instance.VMP, tscbDepartment.SelectedIndex);

            if(!vmpLoadResult.Done)
            {
                Logger.AddToLogs(MethodBase.GetCurrentMethod().Name, vmpLoadResult.Message);
                ucStatusBar.ShowError($"Не удалось загрузить данные ВМП для выбранного отделения ({tscbDepartment.Text})");
                return;
            }
            
            using(VMPSelect vmpSelectForm = new VMPSelect(vmp.Data, AppConfig.Instance.Info.VMP.TreatmentTypes, tscbDepartment.SelectedIndex))
            {          
                if (vmpSelectForm.ShowDialog(this) == DialogResult.OK)
                {
                    cbVMPFinance.SelectedIndex = vmpSelectForm.FinanceType;
                    tbVMPGroup.Text = vmpSelectForm.Group;
                    tbVMPcode.Text = vmpSelectForm.VMPCode;
                    rtbVMPDesc.Text = vmpSelectForm.VMPDesc;
                    tbMKBcode.Text = vmpSelectForm.MKB;
                    tbModelCode.Text = vmpSelectForm.ModelCode;
                    rtbModel.Text = vmpSelectForm.Model;
                    cbTreatmentType.SelectedIndex = vmpSelectForm.TreatmentType;
                    tbMethodCode.Text = vmpSelectForm.MethodCode;
                    rtbMethod.Text = vmpSelectForm.Method;
                };
            }

            tbCommissionDate.Focus();
        }
        private void miStaff_Click(object sender, EventArgs e)
        {

        }

        private void tbOblast_TextChanged(object sender, EventArgs e)
        {
            PopulateTownsIfNeeded();
            PopulateProspectsIfNeeded();
        }

        private void PopulateTownsIfNeeded()
        {
            if (rbTown.Checked && tbOblast.Text == "Ярославская")
                foreach (string town in AppConfig.Instance.Info.Cities)
                    cbTown.Items.Add(town);
            else
            {
                cbTown.SelectedIndex = -1;
                cbTown.Items.Clear();
            }
        }

        private void PopulateProspectsIfNeeded()
        {
            if (rbTown.Checked && cbTown.Text == "Ярославль" && cbStreetType.SelectedIndex == 1 && tbOblast.Text == "Ярославская")
            {
                cbStreet.SelectedIndex = -1;
                cbStreet.Items.Clear();

                foreach (InfoClass.Prospect pr in AppConfig.Instance.Info.Prospects)
                    cbStreet.Items.Add(pr.Name);
            }
            else
            {
                cbStreet.SelectedIndex = -1;
                cbStreet.Items.Clear();
            }
        }

        private void cbTown_TextChanged(object sender, EventArgs e)
        {
            PopulateProspectsIfNeeded();
        }

        private void cbStreetType_SelectedIndexChanged(object sender, EventArgs e)
        {
            PopulateProspectsIfNeeded();
        }

        private void rbTown_CheckedChanged(object sender, EventArgs e)
        {
            PopulateTownsIfNeeded();
            PopulateProspectsIfNeeded();

            if (!rbTown.Checked)
                cbStreetType.SelectedIndex = 0;
        }

        private void btSelectCommission_Click(object sender, EventArgs e)
        {
            using (CommissionSelect commissionSelectForm = new CommissionSelect(tscbDepartment.SelectedIndex))
            {
                if(commissionSelectForm.ShowDialog(this) == DialogResult.OK)
                    SetCommission(commissionSelectForm.Members);
            }
        }

        public void SetCommission(List<string> members)
        {
            tbChairman.Text = members[0];

            string text = string.Empty;

            for(int i = 1; i < members.Count; i++)
            {
                text += members[i];

                if (i + 1 < members.Count)
                    text += Environment.NewLine;
            }

            tbMembers.Text = text;
        }

        private void tbMembers_TextChanged(object sender, EventArgs e)
        {
            // amount of padding to add
            const int padding = 3;
            // get number of lines (first line is 0, so add 1)
            int numLines = this.tbMembers.GetLineFromCharIndex(this.tbMembers.TextLength) + 1;
            if (numLines > 4)
            {
                numLines = 4;
                tbMembers.ScrollBars = ScrollBars.Vertical;
            }
            // get border thickness
            int border = this.tbMembers.Height - this.tbMembers.ClientSize.Height;
            // set height (height of one line * number of lines + spacing)
            this.tbMembers.Height = this.tbMembers.Font.Height * numLines + padding + border;
        }
        private void btTestData_Click(object sender, EventArgs e)
        {
            FillTestData();
        }
        private void btPrint_Click(object sender, EventArgs e)
        {
            Clipboard.SetData(DataFormats.Text, "1,1,2,2,3,4,4,4,5,6,7,8,9,9");
        }

        private void btClose_Click(object sender, EventArgs e)
        {
            Hide();
            btClose.Text = "Отмена";
        }
        //public void SetInputData(VMPInputData data)
        private void SetInputData()
        {
            Patient p = Patient.Instance;
            TemporaryData td = p.PatientData.TemporaryFields;

            tscbDepartment.SelectedIndex = p.Department.HasValue ? p.Department.Value : -1;

            if (td.SuperIntendent.HasValue)
            {
                tbSuperintendent.Text = AppConfig.Instance.Staff[td.SuperIntendent.Value].ShortName;
            }
            else if(p.Department.HasValue)
            {
                tbSuperintendent.Text = AppConfig.Instance.Staff.Departments.HeadByDepartment[p.Department.Value].ShortName;
            }

            cbIO.Checked = td.IO.HasValue;
            tbDoctor.Text = AppConfig.Instance.Staff[p.Doctor.Value].ShortName;
            tbSecondName.Text = p.SecondName;
            tbFirstName.Text = p.FirstName;
            tbThirdName.Text = p.ThirdName;
            cbSex.SelectedIndex = p.Sex.Value;
            tbDateBorn.Text = Utility.DateToShortString(p.DateBirth);
            tbCardNumber.Text = p.AmbuCardNum;
            rtbSoputstv.Text = !string.IsNullOrEmpty(p.Accompanying) ? p.Accompanying : Static.Cues[FieldWithCue.Accompanying];
            rtbComplaints.Text = !string.IsNullOrEmpty(p.Complaints) ? p.Complaints : Static.ComplaintsDefault;
            rtbAnamnez.Text = p.Anamnez.EmptyIfNull();
            rtbInstrumentalExamination.Text = p.InstrExam.EmptyIfNull();
            rtbGeneralExamination.Text = p.PatientData.GenExam.GetString();

            rtbDiagnoz.Text = p.Diagnosis.EmptyIfNull();
            rtbOslozhnenie.Text = !string.IsNullOrEmpty(p.Complications) ? p.Complications : Static.Cues[FieldWithCue.Complications];

            if (!string.IsNullOrEmpty(p.PersonalInfo))
            {
                PersonalInfo pi = Patient.Instance.PatientData.PersonalInfo;

                if (!string.IsNullOrEmpty(pi.Index))
                    tbIndex.Text = pi.Index;

                if (!string.IsNullOrEmpty(pi.Oblast))
                    tbOblast.Text = pi.Oblast;
                if (!string.IsNullOrEmpty(pi.Town))
                    cbTown.Text = pi.Town;

                if (pi.IsVillage.HasValue)
                    rbVillage.Checked = true;
                if (pi.StreetType.HasValue)
                    cbStreetType.SelectedIndex = pi.StreetType.Value;

                if (!string.IsNullOrEmpty(pi.Street))
                    cbStreet.Text = pi.Street;
                if (!string.IsNullOrEmpty(pi.Building))
                    tbBuilding.Text = pi.Building;
                if (!string.IsNullOrEmpty(pi.Korpus))
                    tbKorpus.Text = pi.Korpus;
                if (!string.IsNullOrEmpty(pi.Flat))
                    tbFlat.Text = pi.Flat;

                if (!string.IsNullOrEmpty(pi.Phone))
                    tbPhone.Text = pi.Phone;

                if (!string.IsNullOrEmpty(pi.PassId))
                    tbPassId.Text = pi.PassId;
                if (!string.IsNullOrEmpty(pi.PassFrom))
                    rtbPassFrom.Text = pi.PassFrom;
                if (pi.PassDate.HasValue)
                    tbPassDate.Text = Utility.DateToShortString(pi.PassDate.Value);

                if (!string.IsNullOrEmpty(pi.PensId))
                    tbPensId.Text = pi.PensId;
                if (!string.IsNullOrEmpty(pi.InsComp))
                    tbInsComp.Text = pi.InsComp;
                if (!string.IsNullOrEmpty(pi.InsId))
                    tbInsId.Text = pi.InsId;
                if (pi.Disability.HasValue)
                    cbDisability.SelectedIndex = pi.Disability.Value;
                if (!string.IsNullOrEmpty(pi.Exemption))
                    tbExemption.Text = pi.Exemption;

                if (pi.SocialStatus.HasValue)
                    cbStatus.SelectedIndex = pi.SocialStatus.Value;
            }
        }

        private void VMPForm_Shown(object sender, EventArgs e)
        {
            SetInputData();
            IsDone = false;
            this.CenterToScreen();
            tbIndex.Focus();
        }
        public void Reset()
        {
            ClearForm();
            btOpen.Enabled = false;
            IsDone = false;
        }

        private void btOpenDirectoryOld_Click(object sender, EventArgs e)
        {
            if(File.Exists(Paths.SolutionsOld))
                System.Diagnostics.Process.Start(Paths.SolutionsOld);
        }
        private void SetOutputData()
        {
            PersonalInfo pi = Patient.Instance.PatientData.PersonalInfo;

            pi.Index = !string.IsNullOrWhiteSpace(tbIndex.Text) ? tbIndex.Text.Trim() : null;
            pi.Oblast = !string.IsNullOrWhiteSpace(tbOblast.Text) ? tbOblast.Text.Trim() : null;
            pi.Town = !string.IsNullOrWhiteSpace(cbTown.Text) ? cbTown.Text.Trim() : null;

            pi.IsVillage = rbVillage.Checked ? true : (bool?)null;
            pi.StreetType = cbStreetType.SelectedIndex > -1 ? cbStreetType.SelectedIndex : (int?)null;

            pi.Street = !string.IsNullOrWhiteSpace(cbStreet.Text) ? cbStreet.Text.Trim() : null;
            pi.Building = !string.IsNullOrWhiteSpace(tbBuilding.Text) ? tbBuilding.Text.Trim() : null;
            pi.Korpus = !string.IsNullOrWhiteSpace(tbKorpus.Text) ? tbKorpus.Text.Trim() : null;
            pi.Flat = !string.IsNullOrWhiteSpace(tbFlat.Text) ? tbFlat.Text.Trim() : null;

            pi.Phone = !string.IsNullOrWhiteSpace(tbPhone.Text) ? tbPhone.Text.Trim() : null;

            pi.PassId = !string.IsNullOrWhiteSpace(tbPassId.Text) ? tbPassId.Text.Trim() : null;
            pi.PassFrom = !string.IsNullOrWhiteSpace(rtbPassFrom.Text) ? rtbPassFrom.Text.Trim() : null;

            pi.PassDate = tbPassDate.MaskCompleted ? DateTime.Parse(tbPassDate.Text) : (DateTime?)null;

            pi.PensId = !string.IsNullOrWhiteSpace(tbPensId.Text) ? tbPensId.Text.Trim() : null;
            pi.InsComp = !string.IsNullOrWhiteSpace(tbInsComp.Text) ? tbInsComp.Text.Trim() : null;
            pi.InsId = !string.IsNullOrWhiteSpace(tbInsId.Text) ? tbInsId.Text.Trim() : null;
            //pi.Disability = !string.IsNullOrWhiteSpace(tbInv.Text) ? tbInv.Text.Trim() : null;
            pi.Exemption = !string.IsNullOrWhiteSpace(tbExemption.Text) ? tbExemption.Text.Trim() : null;
            pi.SocialStatus = cbStatus.SelectedIndex > -1 ? cbStatus.SelectedIndex : (int?)null;

            Patient p = Patient.Instance;
            TemporaryData td = p.PatientData.TemporaryFields;

            string method = rtbMethod.Text.Trim();

            if (!Static.DoesOperativeTreatment[Patient.Instance.Department.Value] || td.IsChemTherapy.HasValue)
                p.Medication = method;
            else
                p.Treatment = method;

            p.PatientData.TemporaryFields.PlannedOperationScope = rtbMethod.Text.Trim();
            p.MKBDiagnosis = !string.IsNullOrEmpty(tbMKBcode.Text) ? tbMKBcode.Text.Trim() : null;
        }

        private void TextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            Utility.ReturnToTab(this, e);
        }
    }
}
