﻿using SharedLibrary;

namespace FormsEditorEx.Database
{
    public class PatientLoaded : DatabaseResult
    {
        public PatientLoaded() : base(true, StatusBarIndicator.OK, "Данные пациента загружены") { }
    }
    public class PatientNotFound : DatabaseResult
    {
        public PatientNotFound(string id) : base(false, StatusBarIndicator.ERROR, $"Пациент не найден в базе данных (id = {id})") { }
    }
    //public class PatientLoadError : QueryResultBase
    //{
    //    public PatientLoadError(string id) : base(false, StatusBarIndicator.ERROR, $"Пациент не найден в базе данных (id = {id})") { }
    //}
}
