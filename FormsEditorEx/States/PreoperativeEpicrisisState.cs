﻿using SharedLibrary;

namespace FormsEditorEx
{
    public class PreoperativeEpicrisisState : MainFrameStateBase
    {
        public PreoperativeEpicrisisState(MainFrame handler) : base(handler)
        {
            Type = MainFrameState.PreoperativeEpicrisis;
            Handler.PatientToPreoperativeEpicrisisFields();
            Handler.SetPreoperativeEpicrisisWorkspace(Static.ActionButtonText[Type]);
        }
        public override IMainFrameState Action(ucStatusBarControl ucStatusBar)
        {
            if (Handler.CheckPreoperativeEpicrisisData())
            {
                Handler.PreoperativeEpicrisisFieldsToPatient();

                Handler.Enabled = false;

                if (Middleware.FillAndSavePatientAction(
                    Static.FillStrategies[DocType.PreoperativeEpicrisis], Handler.dbAdapter, ucStatusBar, out string outputPath))
                {
                    if (!string.IsNullOrEmpty(outputPath))
                        Handler.SetLastPath(outputPath);

                    Handler.Enabled = true;

                    return new DischargeSummaryState(Handler);
                }

                Handler.Enabled = true;
            }

            return this;
        }
        public override void FillTestData()
        {
            Handler.FillPreoperativeEpicrisisTestData();
        }
        public override IMainFrameState Revert(ucStatusBarControl ucStatusBar)
        {
            if(ShowRevertPrompt())
            {
                Patient.Instance.Stage = (int)MainFrameState.Anamnesis;
                Patient.Instance.PatientData.GenExam.Revert();

                ucStatusBar.ShowOK("Произведен откат на предыдущий этап");

                return new AnamnesisState(Handler);
            }

            return this;
        }
    }
}
