﻿using System;
using System.Data;
using System.Windows.Forms;
using System.Drawing;
using SharedLibrary;
using FormsEditorEx.Database;
using FormsEditorEx.PatientForm;
using System.IO;
using SharedLibrary.Settings;

namespace FormsEditorEx
{
    public partial class PatientsForm : Form
    {
        DatabaseAdapter dbAdapter;
        DataTable dtPatients = new DataTable();
        ToolTip tt = new ToolTip();
        MainFrame mainFrame;

        private const string REFRESH_BUTTON_TEXT = " Проходят лечение";

        public PatientsForm(DatabaseAdapter databaseAdapter)
        {
            InitializeComponent();

            this.dbAdapter = databaseAdapter;

            mainFrame = new MainFrame(dbAdapter) { Owner = this };
            mainFrame.PatientSearch += Frm_SearchClick;

            InitDataGrid();
            InitControls(AppConfig.Instance.Info);

            Patient.PatientAssigned += Patient_PatientAssignedOrDischarged;
            Patient.PatientDischarged += Patient_PatientAssignedOrDischarged;

            RefreshData();
        }

        private void Patient_PatientAssignedOrDischarged(object sender, EventArgs e)
        {
            RefreshData();
        }

        private void InitControls(InfoClass info)
        {
            btRefresh.BackColor = Color.FromArgb(50, 190, 166);
            btRefresh.ForeColor = Color.WhiteSmoke;

            cbDepartment.DataSource = info.Departments;
            cbDepartment.SelectedIndex = -1;

            this.cbDepartment.SelectedIndexChanged += new System.EventHandler(this.cbDepartment_SelectedIndexChanged);
            cbDepartment.SelectedIndex = AppConfig.Instance.LastDepartment;

            cbDoctors.DisplayMember = "ShortName";
            cbDoctors.ValueMember = "PersonnelId";
            
            if(AppConfig.Instance.LastDepartment >= 0 && AppConfig.Instance.LastDoctor.HasValue) 
            {
                cbDoctors.SelectedValue = AppConfig.Instance.LastDoctor.Value;
                OnSelectedDoctorChanged();
            }

            pnTop.MouseClick += PatientsForm_MouseClick;
            pnFilters.MouseClick += PatientsForm_MouseClick;
            pnButtons.MouseClick += PatientsForm_MouseClick;

            btAbout.Text = $"HMCpro {Static.AppVersion}";

#if !DEBUG
            btTest1.Visible = false;
#endif
        }
        private void SetDepartmentFilters()
        {
            if(cbDepartment.SelectedIndex < 0)
            {
                return;
            }

            cbDoctors.SelectedIndexChanged -= cbDoctors_SelectedIndexChanged;
            cbDoctors.DataSource = AppConfig.Instance.Staff.Departments.DoctorsByDepartment[cbDepartment.SelectedIndex];
            cbDoctors.SelectedIndex = -1;
            cbDoctors.SelectedIndexChanged += cbDoctors_SelectedIndexChanged;
        }
        private void SetRefreshButtonText(string docName = null)
        {
            string value = string.Empty;

            if (cbDepartment.SelectedIndex > -1)
            {
                if (!string.IsNullOrEmpty(docName))
                    value = docName;
                else
                    value = cbDepartment.Text;
            }

            btRefresh.Text = REFRESH_BUTTON_TEXT + $" ({value})";
        }

        private bool CheckDepartment()
        {
            if(cbDepartment.SelectedIndex < 0)
            {
                MessageBox.Show("Не выбрано отделение", "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return false;
            }

            return true;
        }
        private bool CheckDoctor()
        {
            if (cbDoctors.SelectedIndex < 0)
            {
                MessageBox.Show("Не выбрано лечащий врач", "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return false;
            }

            return true;
        }

        private void LoadPatient(string id)
        {
            if (Patient.Load(id, dbAdapter, ucStatusBar))
            {
                btBack.Visible = true;

                if (ValidatePatient())
                {
                    mainFrame.LoadPatient(cbDepartment.SelectedIndex, (long)cbDoctors.SelectedValue);
                    ShowMainFrame();
                }
                else
                {
                    MessageBox.Show("Требуется дополнить базовую информацию пациента");
                    EditPatient();
                }

            }
        }
        private void ShowMainFrame()
        {
            mainFrame.Show();
            mainFrame.Activate();
            Hide();
        }
        private void InitDataGrid()
        {
            dgvPatients.AlternatingRowsDefaultCellStyle.BackColor = ColorTranslator.FromHtml("#EFEFEF");
            dgvPatients.DataSource = new BindingSource(dtPatients, string.Empty);
        }
        private void SetColumnsAppearance(DataGridView dgv)
        {
            DataGridViewColumn col;

            col = dgv.Columns["ClinicalRecordNum"];

            if (col != null)
            {
                col.HeaderText = "История болезни";
                col.Width = 100;
            }

            col = dgv.Columns["AmbuCardNum"];

            if (col != null)
            {
                col.HeaderText = "Амбулат. карта";
                col.Width = 100;
            }

            col = dgv.Columns["SecondName"];

            if (col != null)
            {
                col.HeaderText = "Фамилия";
                col.Width = 150;
            }

            col = dgv.Columns["FirstName"];

            if (col != null)
            {
                col.HeaderText = "Имя";
                col.Width = 150;
            }

            col = dgv.Columns["ThirdName"];

            if (col != null)
            {
                col.HeaderText = "Отчество";
                col.Width = 200;
            }

            col = dgv.Columns["DateExam"];

            if (col != null)
            {
                col.HeaderText = "Дата осмотра";
                col.Width = 100;
            }

            col = dgv.Columns["DateDischarge"];

            if (col != null)
            {
                col.HeaderText = "Дата выписки";
                col.Width = 100;
            }

            col = dgv.Columns["Department"];

            if (col != null)
            {
                col.HeaderText = "Отделение";
                col.Width = 150;
            }

            col = dgv.Columns["Doctor"];

            if (col != null)
            {
                col.HeaderText = "Лечащий врач";
                col.Width = 190;
            }

            col = dgv.Columns["Duration"];

            if (col != null)
            {
                col.HeaderText = "Суток";
                col.AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
                col.Width = 90;
            }

            int grid_width = 0;

            for (int i = 0; i < dgv.Columns.Count; i++)
            {
                col = dgv.Columns[i];

                if (col.Visible)
                {
                    grid_width += col.Width;
                }
            }

            this.ClientSize = new Size(grid_width + 4, this.ClientRectangle.Height);

            col = dgv.Columns["Duration"];

            if (col != null)
            {
                col.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            }
        }

        private void dgvPatients_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
            if (dtPatients.Columns.Count > 0)
                SetColumnsAppearance(dgvPatients);

            dgvPatients.ClearSelection();
        }
        private void SortDateExam(bool isSearch = false)
        {
            var col = dgvPatients.Columns["DateExam"];

            if (col is null)
                return;

            if (!isSearch)
            {
                dgvPatients.Sort(col, System.ComponentModel.ListSortDirection.Ascending);
            }
            else
            {
                dgvPatients.Sort(col, System.ComponentModel.ListSortDirection.Descending);
            }
        }
        private void btRefresh_Click(object sender, EventArgs e)
        {
            RefreshData();
        }

        public void RefreshData()
        {
            int? dep = cbDepartment.SelectedIndex > -1 ? cbDepartment.SelectedIndex : (int?)null;
            long? doc = cbDoctors.SelectedValue as long?;

            if(!dep.HasValue || !doc.HasValue)
            {
                return;
            }

            if (dep.HasValue)
                Middleware.HandleDBAction(() => dbAdapter.LoadRefreshPatientList(dtPatients, dep, doc), ucStatusBar);
            else
                Middleware.HandleDBAction(() => dbAdapter.LoadLast100PatientList(dtPatients), ucStatusBar);

            SortDateExam(!dep.HasValue);
        }

        private void dgvPatients_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            string colName = dgvPatients.Columns[e.ColumnIndex].Name;

            switch (colName)
            {
                case "DateExam":
                case "DateDischarge":
                    if (double.TryParse(e.Value.ToString(), out double unix))
                        e.Value = Utility.UnixTimestampToDateString(unix);
                    break;
                case "Department":
                    if (int.TryParse(e.Value.ToString(), out int dep_id))
                        e.Value = AppConfig.Instance.Info.Departments[dep_id] ?? "???";
                    break;
                case "Doctor":
                    {
                        if (long.TryParse(e.Value.ToString(), out long doc_id))
                        {
                            e.Value = AppConfig.Instance.Staff[doc_id].ShortName;
                        }
                        break;
                    }
            }
        }

        private void dgvPatients_SortCompare(object sender, DataGridViewSortCompareEventArgs e)
        {
            if (e.Column.Name == "DateExam" || e.Column.Name == "DateDischarge")
            {
                e.SortResult = double.Parse(e.CellValue1.ToString()).CompareTo(double.Parse(e.CellValue2.ToString()));
                e.Handled = true;//pass by the default sorting
            }
        }

        private void PatientsForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (MessageBox.Show(this, "Завершить работу приложения?", "", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                e.Cancel = false;
                AppConfig.SaveLocalSettings(cbDepartment.SelectedIndex, cbDoctors.SelectedValue as long?);
            }
            else
            {
                e.Cancel = true;
            }
        }

        private void btSearch_Click(object sender, EventArgs e)
        {
            Middleware.HandleDBAction(() => dbAdapter.LoadSearchPatientList(dtPatients,
                !tbAmbuCardNum.IsCueOn ? tbAmbuCardNum.Text.Trim() : null,
                !tbSecondName.IsCueOn ? Utility.ToTitleCase(tbSecondName.Text.Trim()) : null,
                !tbFirstName.IsCueOn ? Utility.ToTitleCase(tbFirstName.Text.Trim()) : null,
                !tbThirdName.IsCueOn ? Utility.ToTitleCase(tbThirdName.Text.Trim()) : null,
                dpStart.Checked ? dpStart.Value : (DateTime?)null,
                dpEnd.Checked ? dpEnd.Value : (DateTime?)null),
                ucStatusBar);

            SortDateExam(true);
        }

        private void btResetFilters_Click(object sender, EventArgs e)
        {
            tbAmbuCardNum.SetText(string.Empty);
            tbSecondName.SetText(string.Empty);
            tbFirstName.SetText(string.Empty);
            tbThirdName.SetText(string.Empty);

            dpStart.Checked = false;
            dpEnd.Checked = false;
        }

        private void btDelete_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show("Подтверджение удаления", "", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2);
            if (result == DialogResult.OK)
            {
                if (dgvPatients.SelectedRows.Count > 0)
                    DeleteEntry(dgvPatients.SelectedRows[0].Cells["AmbuCardNum"].Value.ToString());
            }
        }

        private void DeleteEntry(string id)
        {
            if (Middleware.HandleDBAction(() => dbAdapter.DeletePatient(id), ucStatusBar))
            {
                int row_id = dgvPatients.CurrentRow.Index;

                dgvPatients.Rows.Remove(dgvPatients.CurrentRow);

                if (dgvPatients.Rows.Count > 0)
                    if (row_id == 0)
                    {
                        dgvPatients.Rows[0].Selected = true;
                    }
                    else
                    {
                        dgvPatients.Rows[row_id - 1].Selected = true;
                    }
                
                if(Patient.Instance.AmbuCardNum == id)
                {
                    mainFrame.ClearAndReset();
                    btBack.Visible = false;
                }
            }
        }

        private void dgvPatients_SelectionChanged(object sender, EventArgs e)
        {
            if (dgvPatients.SelectedRows.Count > 0)
            {
                btLoadEntry.Enabled = btDelete.Enabled = true;
            }
            else
            {
                btLoadEntry.Enabled = btDelete.Enabled = false;
            }
        }

        private void PatientsForm_Resize(object sender, EventArgs e)
        {
            PositionControls();
        }

        private void PositionControls()
        {
            gbSearch.Left = (ClientRectangle.Width - gbSearch.Width) / 2;
            btLast100.Left = gbSearch.Left;
            btRefresh.Left = btLast100.Left + btLast100.Width + 6;
        }

        private void btLoadEntry_Click(object sender, EventArgs e)
        {
            if (CheckDepartment())
            {
                if (dgvPatients.SelectedRows.Count > 0)
                {
                    string id = dgvPatients.SelectedRows[0].Cells["AmbuCardNum"].Value.ToString();
                    if (!string.IsNullOrEmpty(id))
                        LoadPatient(id);
                    else
                    {
                        ucStatusBar.ShowError("У пациента отсутствует номер амбулаторной карты");
                    }
                }
            }
        }

        private void dgvPatients_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            btLoadEntry_Click(null, EventArgs.Empty);
        }
        public void OnBtSearchClick(string cardNum)
        {
            btResetFilters_Click(this, EventArgs.Empty);

            tbAmbuCardNum.SetText(cardNum);

            btSearch_Click(this, EventArgs.Empty);

            Show();
            mainFrame.Hide();
        }
        private void btLast100_Click(object sender, EventArgs e)
        {
            Middleware.HandleDBAction(() => dbAdapter.LoadLast100PatientList(dtPatients), ucStatusBar);

            SortDateExam(true);
        }
        private void TextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyData == Keys.Enter)
            {
                e.SuppressKeyPress = true;
                btSearch_Click(this, EventArgs.Empty);
            }
        }
        private void cbDepartment_SelectedIndexChanged(object sender, EventArgs e)
        {
            SetDepartmentFilters();
            btBack.Visible = false;
        }

        private void PatientsForm_Load(object sender, EventArgs e)
        {
            this.Activate();
        }

        private void cbDoctors_SelectedIndexChanged(object sender, EventArgs e)
        {
            OnSelectedDoctorChanged();
        }
        private void OnSelectedDoctorChanged()
        {
            long? docId = cbDoctors.SelectedValue as long?;

            if(!docId.HasValue)
            {
                return;
            }

            string docName = AppConfig.Instance.Staff[docId.Value].ShortName;
            AppConfig.Instance.OperTeam.Surgeon = docName;

            SetRefreshButtonText(docName);
            RefreshData();
            btBack.Visible = false;
        }

        private void cbDoctors_KeyDown(object sender, KeyEventArgs e)
        {
            Utility.ReturnToTab(this, e);         
        }

        private void PatientsForm_MouseClick(object sender, MouseEventArgs e)
        {
            this.ActiveControl = null;  
        }

        private void btNewPatient_Click(object sender, EventArgs e)
        {
            if (CheckDepartment())
            {
                using (var frm = new EditPatientForm(dbAdapter, true, cbDepartment.SelectedIndex))
                {
                    frm.SearchClick += Frm_SearchClick;

                    if (frm.ShowDialog(this) == DialogResult.OK)
                    {
                        ucStatusBar.ShowOK("Пациент создан");
                        LoadPatient(frm.PatientID);
                    }
                }
            }
        }
        private void Frm_SearchClick(object sender, LoadPatientEventArgs e)
        {
            OnBtSearchClick(e.PatientId);
        }

        private void btTest1_Click(object sender, EventArgs e)
        {
            //DBTweaks.TweakStaffData();
            //Patient.Load("469954", ucStatusBar);
            //Patient.Instance.PatientData.PersonalInfo.Disability = null;
            //Patient.Save(ucStatusBar);
            //var res = DbHandler.Instance.CheckVersion();
            //DBTweaks.MigrateStaffToDB();

            //DBTweaks.ReformatVMP(AppConfig.Instance.Info);
        }

        private void cbDoctors_Validated(object sender, EventArgs e)
        {
            btBack.Visible = false;
            AppConfig.Instance.OperTeam.Surgeon = cbDoctors.Text;
        }

        private void btBack_Click(object sender, EventArgs e)
        {
            mainFrame.Enabled = true;
            ShowMainFrame();
        }

        private void cmsMenu_Opening(object sender, System.ComponentModel.CancelEventArgs e)
        {
            var dgv_rel_mousePos = dgvPatients.PointToClient(MousePosition);
            var hti = dgvPatients.HitTest(dgv_rel_mousePos.X, dgv_rel_mousePos.Y);
            if (hti.RowIndex == -1)
            {
                e.Cancel = true;
            }
        }

        private void miLoad_Click(object sender, EventArgs e)
        {
            btLoadEntry_Click(sender, e);
        }

        private void dgvPatients_CellMouseUp(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                dgvPatients.ClearSelection();

                if (e.RowIndex > -1)
                {
                    dgvPatients.Rows[e.RowIndex].Selected = true;
                    dgvPatients.CurrentCell = dgvPatients.Rows[e.RowIndex].Cells[1];
                }
            }
        }

        private void miDelete_Click(object sender, EventArgs e)
        {
            btDelete_Click(this, EventArgs.Empty);
        }

        private void btAbout_Click(object sender, EventArgs e)
        {
            using (var frm = new AboutForm())
                frm.ShowDialog(this);
        }

        private void btEdit_Click(object sender, EventArgs e)
        {
            if (dgvPatients.SelectedRows.Count > 0)
            {
                btBack.Visible = false;

                if (Patient.Load(dgvPatients.SelectedRows[0].Cells["AmbuCardNum"].Value.ToString(), dbAdapter, ucStatusBar))
                {
                    EditPatient();
                }
            }
        }
        private void EditPatient()
        {
            using (var frm = new EditPatientForm(dbAdapter, false))
            {
                if (frm.ShowDialog(this) == DialogResult.OK)
                {
                    OnBtSearchClick(frm.PatientID);
                    ucStatusBar.ShowOK("Базовые данные пациента изменены");
                }
            }
        }
        private void miEdit_Click(object sender, EventArgs e)
        {
            btEdit_Click(sender, e);
        }
        private bool ValidatePatient()
        {
            Patient p = Patient.Instance;

            return !string.IsNullOrEmpty(p.AmbuCardNum) && !string.IsNullOrEmpty(p.SecondName) && !string.IsNullOrEmpty(p.FirstName)
                && !string.IsNullOrEmpty(p.ThirdName) && p.Sex.HasValue && p.DateBirth.HasValue;
        }

        private void btGuide_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start(Static.GuideURL);
        }

        private void btChangelog_Click(object sender, EventArgs e)
        {
            string filename = Path.Combine(Paths.Instance.Solutions, "changelog.txt");

            if (File.Exists(filename))
                System.Diagnostics.Process.Start(filename);
        }

        private void btDiagnostics_Click(object sender, EventArgs e)
        {
            mainFrame.Dump();
            Logger.CopyLogsToFileHosting(cbDoctors.Text.Trim(), ucStatusBar);
        }
    }
}
