﻿using System;
using System.Threading;
using System.Threading.Tasks;
using System.IO;
using SharedLibrary;
using System.Reflection;

namespace FormsEditorEx
{
    public static class Logger
    {
        private static readonly object lcLogs = new Object();
        public static void AddToLogs(string location, string message)
        {
            Task.Factory.StartNew(delegate
            {
                lock (lcLogs)
                {
                    File.AppendAllText(Paths.LOCAL_LOGS_FILENAME, DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToLongTimeString() + " " + location + " : " + message + Environment.NewLine);
                }
            }, CancellationToken.None, TaskCreationOptions.PreferFairness, TaskScheduler.Default);
        }
        public static void AddToLogs(string message)
        {
            AddToLogs(string.Empty, message);
        }
        public static void CopyLogsToFileHosting(string doc, ucStatusBarControl ucStatusBar)
        {
            try
            {
                string logs_original_path = Paths.LOCAL_LOGS_FILENAME;
                if (File.Exists(logs_original_path))
                {
                    string dir = Paths.Instance.RemoteLogs;

                    if (!Directory.Exists(dir))
                    {
                        Directory.CreateDirectory(dir);
                    }

                    string logs_destination_path = Path.Combine(dir, Utility.RemoveSpecialCharacters($"{Utility.DateToExtendedShortString(DateTime.Now)} {doc}.txt"));

                    File.Copy(logs_original_path, logs_destination_path, true);

                    ucStatusBar.ShowOK("Данные о сбоях приложения отправлены на файлообменник");
                }
                else
                    ucStatusBar.ShowError("Файл с данными о сбоях приложения не найден (скорее всего, ни один сбой еще не был зафиксирован");
            }
            catch(Exception ex)
            {
                ucStatusBar.ShowError("Не удалось отправить данные на файлообменник");
                Logger.AddToLogs(MethodBase.GetCurrentMethod().Name, ex.ToString());
            }
        }
    }
}
